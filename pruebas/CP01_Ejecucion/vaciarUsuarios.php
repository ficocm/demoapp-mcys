<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once("../../modelo/usuario/util.php");

$conexion = Conectar::conexionSinRestriccion();

$conexion->query("DELETE FROM admisiondetalle where AdmDetFKDepTec is not NULL");

$respuesta = $conexion->errno;
if($respuesta==0){
	echo "SE ELIMINO TODOS LOS AYUDANTES TÉCNICOS LA BD de PRUEBA<br>";
}else{
	echo "OCURRIO ERRORES ($respuesta)";
	die();
}
$conexion->query("DELETE FROM admisiondetalle");
$respuesta = $conexion->errno;
if($respuesta==0){
	echo "SE ELIMINO TODOS LOS DETALLES<br>";
}else{
	echo "OCURRIO ERRORES ($respuesta)";
	die();
}

$conexion->query("DELETE FROM usuario WHERE UsuFKTip is NULL or (UsuFKTip != 22 and UsuFKTip != 23)");
$respuesta = $conexion->errno;
if($respuesta==0){
	echo "SE ELIMINO TODOS LOS USUARIOS<br>";
}else{
	echo "OCURRIO ERRORES ($respuesta)";
	die();
}
?>