-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-10-2018 a las 13:49:59
-- Versión del servidor: 5.7.24-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `DuaPrueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admisionarea`
--

CREATE TABLE `admisionarea` (
  `AdmAreId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AdmAreFKAreId` int(8) DEFAULT NULL,
  `AdmAreFKAdmCabId` int(8) DEFAULT NULL,
  `AdmAreFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admisionaula`
--

CREATE TABLE `admisionaula` (
  `AdmAulId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AdmAulFKAulId` int(8) DEFAULT NULL,
  `AdmAulFKAdmPabId` int(8) DEFAULT NULL,
  `AdmAulCap` int(8) DEFAULT NULL,
  `AdmAulAfo` int(8) DEFAULT NULL,
  `AdmAulFil` int(8) DEFAULT NULL,
  `AdmAulCol` int(8) DEFAULT NULL,
  `AdmAulFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admisioncabecera`
--

CREATE TABLE `admisioncabecera` (
  `AdmCabId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AdmCabNumPos` varchar(8) DEFAULT NULL,
  `AdmCabFec` date DEFAULT NULL,
  `AdmCabAno` int(4) DEFAULT NULL,
  `AdmCabFKTipPro` int(8) DEFAULT NULL,
  `AdmCabNumFas` int(2) DEFAULT NULL,
  `AdmCabNumEva` int(2) DEFAULT NULL,
  `AdmCabPosBio` int(5) DEFAULT NULL,
  `AdmCabPosIng` int(5) DEFAULT NULL,
  `AdmCabPosSoc` int(5) DEFAULT NULL,
  `AdmCabCat` varchar(100) DEFAULT NULL,
  `AdmCabTur` int(8) DEFAULT NULL COMMENT 'Turno del proceso de admision (Unico, Manana, Tarde)',
  `AdmCabFKEstReg` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admisioncabecera`
--

INSERT INTO `admisioncabecera` (`AdmCabId`, `AdmCabNumPos`, `AdmCabFec`, `AdmCabAno`, `AdmCabFKTipPro`, `AdmCabNumFas`, `AdmCabNumEva`, `AdmCabPosBio`, `AdmCabPosIng`, `AdmCabPosSoc`, `AdmCabCat`, `AdmCabTur`, `AdmCabFKEstReg`) VALUES
(1, '2', '2018-10-16', NULL, 13, 2, NULL, NULL, NULL, NULL, NULL, NULL, 32),
(2, NULL, '2018-10-18', 2019, 13, 1, 1, NULL, NULL, NULL, NULL, NULL, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admisiondetalle`
--

CREATE TABLE `admisiondetalle` (
  `AdmDetId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AdmDetFKAdmCabId` int(8) DEFAULT NULL,
  `AdmDetFKUsu` int(8) DEFAULT NULL,
  `AdmDetFKCar` int(8) DEFAULT NULL,
  `AdmDetFKCar2` int(8) DEFAULT NULL,
  `AdmDetLug1` varchar(200) DEFAULT NULL COMMENT 'Lugar del primer cargo',
  `AdmDetLug2` varchar(200) DEFAULT NULL COMMENT 'Lugar del segundo cargo',
  `AdmDetFKAul` int(8) DEFAULT NULL,
  `AdmDetFKPab` int(8) DEFAULT NULL,
  `AdmDetFKPab2` int(8) DEFAULT NULL,
  `AdmDetFKAre` int(8) DEFAULT NULL,
  `AdmDetFKAre2` int(8) DEFAULT NULL,
  `AdmDetFKArePue` int(8) DEFAULT NULL,
  `AdmDetFKArePue2` int(8) DEFAULT NULL,
  `AdmDetDetAsi` varchar(15) DEFAULT NULL,
  `AdmDetCorEnv` int(1) DEFAULT '0',
  `AdmDetFKDepTec` int(8) DEFAULT NULL,
  `AdmDetObs` varchar(200) DEFAULT NULL,
  `AdmDetFKEstReg` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `admisiondetalle`
--
DELIMITER $$
CREATE TRIGGER `bitacoraadmisiondetalleactualizar` AFTER UPDATE ON `admisiondetalle` FOR EACH ROW INSERT bitacoraadmisiondetalle (AdmDetId, AdmDetFKAdmCabIdAnt, AdmDetFKAdmCabIdNue, AdmDetFKUsuAnt, AdmDetFKUsuNue, AdmDetFKCarAnt, AdmDetFKCarNue, AdmDetFKCar2Ant, AdmDetFKCar2Nue, AdmDetLug1Ant, AdmDetLug1Nue, AdmDetLug2Ant, AdmDetLug2Nue, AdmDetFKAulAnt, AdmDetFKAulNue, AdmDetFKPabAnt, AdmDetFKPabNue, AdmDetFKPab2Ant, AdmDetFKPab2Nue, AdmDetFKAreAnt, AdmDetFKAreNue, AdmDetFKAre2Ant, AdmDetFKAre2Nue, AdmDetFKArePueAnt, AdmDetFKArePueNue, AdmDetFKArePue2Ant, AdmDetFKArePue2Nue, AdmDetDetAsiAnt, AdmDetDetAsiNue, AdmDetCorEnvAnt, AdmDetCorEnvNue, AdmDetFKDepTecAnt, AdmDetFKDepTecNue, AdmDetFKEstRegAnt, AdmDetFKEstRegNue, usuario, fecha, operacion, host)
VALUES (NEW.AdmDetId, OLD.AdmDetFKAdmCabId, NEW.AdmDetFKAdmCabId, OLD.AdmDetFKUsu, NEW.AdmDetFKUsu, OLD.AdmDetFKCar, NEW.AdmDetFKCar, OLD.AdmDetFKCar2, NEW.AdmDetFKCar2, OLD.AdmDetLug1, NEW.AdmDetLug1, OLD.AdmDetLug2, NEW.AdmDetLug2, OLD.AdmDetFKAul, NEW.AdmDetFKAul, OLD.AdmDetFKPab, NEW.AdmDetFKPab, OLD.AdmDetFKPab2, NEW.AdmDetFKPab2, OLD.AdmDetFKAre, NEW.AdmDetFKAre, OLD.AdmDetFKAre2, NEW.AdmDetFKAre2, OLD.AdmDetFKArePue, NEW.AdmDetFKArePue, OLD.AdmDetFKArePue2, NEW.AdmDetFKArePue2, OLD.AdmDetDetAsi, NEW.AdmDetDetAsi, OLD.AdmDetCorEnv, NEW.AdmDetCorEnv, OLD.AdmDetFKDepTec, NEW.AdmDetFKDepTec, OLD.AdmDetFKEstReg, NEW.AdmDetFKEstReg, substring(user(),1,instr(user(),'@')-1), now(), 'ACTUALIZAR',substring(user(),(instr(user(),'@')+1)) )
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bitacoraadmisiondetalleeliminar` BEFORE DELETE ON `admisiondetalle` FOR EACH ROW INSERT bitacoraadmisiondetalle (AdmDetId, AdmDetFKAdmCabIdAnt, AdmDetFKUsuAnt, AdmDetFKCarAnt, AdmDetFKCar2Ant, AdmDetLug1Ant, AdmDetLug2Ant, AdmDetFKAulAnt, AdmDetFKPabAnt, AdmDetFKPab2Ant, AdmDetFKAreAnt, AdmDetFKAre2Ant, AdmDetFKArePueAnt, AdmDetFKArePue2Ant, AdmDetDetAsiAnt, AdmDetCorEnvAnt, AdmDetFKDepTecAnt, AdmDetFKEstRegAnt, usuario, fecha, operacion, host)
VALUES (OLD.AdmDetId, OLD.AdmDetFKAdmCabId, OLD.AdmDetFKUsu, OLD.AdmDetFKCar, OLD.AdmDetFKCar2, OLD.AdmDetLug1, OLD.AdmDetLug2, OLD.AdmDetFKAul, OLD.AdmDetFKPab, OLD.AdmDetFKPab2, OLD.AdmDetFKAre, OLD.AdmDetFKAre2, OLD.AdmDetFKArePue, OLD.AdmDetFKArePue2, OLD.AdmDetDetAsi, OLD.AdmDetCorEnv, OLD.AdmDetFKDepTec, OLD.AdmDetFKEstReg,  substring(user(),1,instr(user(),'@')-1) , now(), 'ELIMINAR',substring(user(),(instr(user(),'@')+1)) )
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bitacoraadmisiondetalleinsertar` AFTER INSERT ON `admisiondetalle` FOR EACH ROW INSERT bitacoraadmisiondetalle (AdmDetId, AdmDetFKAdmCabIdNue, AdmDetFKUsuNue, AdmDetFKCarNue, AdmDetFKCar2Nue, AdmDetLug1Nue, AdmDetLug2Nue, AdmDetFKAulNue, AdmDetFKPabNue, AdmDetFKPab2Nue, AdmDetFKAreNue, AdmDetFKAre2Nue, AdmDetFKArePueNue, AdmDetFKArePue2Nue, AdmDetDetAsiNue, AdmDetCorEnvNue, AdmDetFKDepTecNue, AdmDetFKEstRegNue, usuario, fecha, operacion, host)
VALUES (NEW.AdmDetId, NEW.AdmDetFKAdmCabId, NEW.AdmDetFKUsu, NEW.AdmDetFKCar, NEW.AdmDetFKCar2, NEW.AdmDetLug1, NEW.AdmDetLug2, NEW.AdmDetFKAul, NEW.AdmDetFKPab, NEW.AdmDetFKPab2, NEW.AdmDetFKAre, NEW.AdmDetFKAre2, NEW.AdmDetFKArePue, NEW.AdmDetFKArePue2, NEW.AdmDetDetAsi, NEW.AdmDetCorEnv, NEW.AdmDetFKDepTec, NEW.AdmDetFKEstReg, substring(user(),1,instr(user(),'@')-1), now(), 'INSERTAR',substring(user(),(instr(user(),'@')+1)) )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admisionpabellon`
--

CREATE TABLE `admisionpabellon` (
  `AdmPabId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AdmPabFKPabId` int(8) DEFAULT NULL,
  `AdmPabFKAdmAreId` int(8) DEFAULT NULL,
  `AdmPabFKAreIdLog` int(8) DEFAULT NULL,
  `AdmPabFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `AreId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AreIdLog` varchar(2) DEFAULT NULL,
  `AreDes` varchar(40) DEFAULT NULL,
  `AreFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`AreId`, `AreIdLog`, `AreDes`, `AreFKEstReg`) VALUES
(1, '1', 'BIOMEDICAS', 18),
(2, '2', 'INGENIERIAS', 18),
(3, '3', 'SOCIALES', 18),
(4, NULL, 'CEPRUNSA', 18),
(5, NULL, 'AGRONOMIA', 18),
(6, NULL, 'OTRO', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areapuerta`
--

CREATE TABLE `areapuerta` (
  `ArePueId` int(8) NOT NULL COMMENT 'Clave primaria',
  `ArePueDes` varchar(200) DEFAULT NULL,
  `ArePueFKAre` int(8) DEFAULT NULL,
  `ArePueFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `areapuerta`
--

INSERT INTO `areapuerta` (`ArePueId`, `ArePueDes`, `ArePueFKAre`, `ArePueFKEstReg`) VALUES
(1, 'PUERTA AV. INDEPENDENCIA', 2, 18),
(2, 'PUERTA AV. VENEZUELA', 2, 18),
(3, 'PUERTA PAUCARPATA', 2, 18),
(4, 'PUERTA VIRGEN DEL PILAR', 3, 18),
(5, 'PUERTA VIRGEN DEL PILAR', 1, 18),
(6, 'PUERTA ALCIDES CARRION', 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aula`
--

CREATE TABLE `aula` (
  `AulId` int(8) NOT NULL COMMENT 'Clave primaria',
  `AulNum` varchar(50) DEFAULT NULL COMMENT 'nombre aula',
  `AulAfo` int(4) DEFAULT NULL COMMENT 'Capacidad de aula',
  `AulPis` int(4) DEFAULT NULL COMMENT 'Aforo de aula',
  `AulCol` int(3) DEFAULT NULL COMMENT 'piso del pabellon',
  `AulFil` int(5) DEFAULT NULL COMMENT 'Filas de aula',
  `AulCap` int(4) DEFAULT NULL COMMENT 'Columnas de aula',
  `AulFKPab` int(8) DEFAULT NULL COMMENT 'Pabellon de aula',
  `AulFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aula`
--

INSERT INTO `aula` (`AulId`, `AulNum`, `AulAfo`, `AulPis`, `AulCol`, `AulFil`, `AulCap`, `AulFKPab`, `AulFKEstReg`) VALUES
(1, '201', 100, 2, 10, 10, 100, 1, 18),
(2, '202', 100, 2, 10, 10, 100, 1, 18),
(3, '302', 55, 3, 10, 5, 50, 1, 18),
(4, '303', 56, 3, 10, 5, 50, 1, 18),
(5, '301-A', 56, 3, 10, 5, 50, 1, 18),
(6, '301-b', 55, 3, 10, 5, 50, 1, 18),
(7, '310', 48, 3, 8, 6, 48, 2, 18),
(8, '311-A', 38, 3, 8, 6, 48, 2, 18),
(9, '311-B', 41, 3, 10, 5, 50, 2, 18),
(10, '203-01', 50, 2, 10, 6, 60, 3, 18),
(11, '204-02', 50, 2, 10, 6, 60, 3, 18),
(12, '206-01', 50, 2, 10, 7, 70, 3, 18),
(13, '207-04', 50, 2, 10, 7, 70, 3, 18),
(14, '306-05', 50, 3, 8, 7, 56, 3, 18),
(15, '301', 50, 3, 10, 5, 50, 4, 18),
(16, '302', 42, 3, 10, 4, 40, 4, 18),
(17, '303', 50, 3, 10, 5, 50, 4, 18),
(18, '101', 50, 1, 10, 7, 70, 5, 18),
(19, '102-A', 40, 1, 10, 6, 60, 5, 18),
(20, '102-B', 88, 1, 10, 9, 90, 5, 18),
(21, '202', 40, 2, 10, 7, 70, 5, 18),
(22, '301', 50, 3, 10, 7, 70, 5, 18),
(23, '101', 90, 1, 8, 11, 88, 6, 18),
(24, '102', 90, 1, 8, 11, 88, 6, 18),
(25, '204', 50, 2, 8, 6, 48, 6, 18),
(26, '203', 50, 2, 8, 6, 48, 6, 18),
(27, '404', 52, 4, 10, 5, 50, 6, 18),
(28, '403', 52, 4, 8, 6, 48, 6, 18),
(29, '402', 52, 4, 8, 6, 48, 6, 18),
(30, '401', 0, 4, 6, 6, 36, 6, 18),
(31, '208', 40, 2, 8, 5, 40, 9, 18),
(32, '209', 40, 2, 8, 5, 40, 9, 18),
(33, '303', 40, 3, 8, 5, 40, 9, 18),
(34, '304', 40, 3, 8, 5, 40, 9, 18),
(35, '305', 40, 3, 8, 5, 40, 9, 18),
(36, '308', 40, 3, 8, 5, 40, 9, 18),
(37, '309', 40, 3, 8, 5, 40, 9, 18),
(38, '301', 40, 3, 8, 5, 40, 9, 18),
(39, '402', 40, 4, 8, 5, 40, 9, 18),
(40, '403', 40, 4, 8, 5, 40, 9, 18),
(41, '404', 40, 4, 8, 5, 40, 9, 18),
(42, '407', 40, 4, 8, 5, 40, 9, 18),
(43, '408', 40, 4, 8, 5, 40, 9, 18),
(44, '409', 40, 4, 8, 5, 40, 9, 18),
(45, '201', 48, 2, 10, 5, 50, 10, 18),
(46, '202', 48, 2, 10, 5, 50, 10, 18),
(47, '203', 48, 2, 10, 5, 50, 10, 18),
(48, '301', 48, 3, 10, 5, 50, 10, 18),
(49, '302', 48, 3, 10, 5, 50, 10, 18),
(50, '201', 40, 2, 10, 4, 40, 11, 18),
(51, '201-A', 40, 2, 10, 4, 40, 11, 18),
(52, '301', 40, 3, 10, 4, 40, 11, 18),
(53, '302', 40, 3, 10, 4, 40, 11, 18),
(54, '303', 40, 3, 10, 4, 40, 11, 18),
(55, '209', 60, 2, 6, 10, 60, 12, 18),
(56, '210', 60, 2, 6, 10, 60, 12, 18),
(57, '211', 70, 2, 10, 7, 70, 12, 18),
(58, '212', 70, 2, 10, 7, 70, 12, 18),
(59, '213', 70, 2, 10, 7, 70, 12, 18),
(60, '301', 120, 3, 16, 8, 128, 12, 18),
(61, '203', 70, 2, 8, 10, 80, 13, 18),
(62, '204', 32, 2, 8, 4, 32, 13, 18),
(63, '205', 32, 2, 8, 4, 32, 13, 18),
(64, '206', 32, 2, 8, 5, 40, 13, 18),
(65, '207', 32, 2, 8, 8, 64, 13, 18),
(66, '208', 26, 2, 6, 5, 30, 13, 18),
(67, '303', 70, 3, 8, 10, 80, 13, 18),
(68, '304', 32, 3, 8, 4, 32, 13, 18),
(69, '305', 70, 3, 8, 10, 80, 13, 18),
(70, '306', 48, 3, 8, 8, 64, 13, 18),
(71, '01', 0, 2, 8, 4, 32, 14, 18),
(72, '02', 0, 2, 8, 4, 32, 14, 18),
(73, '201', 48, 2, 8, 6, 48, 15, 18),
(74, '202', 48, 2, 8, 6, 48, 15, 18),
(75, '203', 48, 2, 8, 6, 48, 15, 18),
(76, '301', 48, 3, 8, 6, 48, 15, 18),
(77, '302', 48, 3, 8, 6, 48, 15, 18),
(78, '303', 48, 3, 8, 6, 48, 15, 18),
(79, '304', 48, 3, 8, 6, 48, 15, 18),
(80, '101', 80, 1, 6, 14, 84, 16, 18),
(81, '102', 60, 1, 6, 11, 66, 16, 18),
(82, '302', 48, 3, 8, 6, 48, 16, 18),
(83, '303', 48, 3, 8, 6, 48, 16, 18),
(84, '304', 48, 3, 8, 6, 48, 16, 18),
(85, '401', 80, 4, 8, 6, 48, 16, 18),
(86, '402', 48, 4, 8, 6, 48, 16, 18),
(87, '403', 48, 4, 8, 6, 48, 16, 18),
(88, '404', 48, 4, 8, 6, 48, 16, 18),
(89, '101', 36, 1, 6, 5, 30, 17, 18),
(90, '201', 32, 2, 6, 5, 30, 17, 18),
(91, '202', 48, 2, 6, 8, 48, 17, 18),
(92, '203', 48, 2, 6, 8, 48, 17, 18),
(93, '204', 32, 2, 6, 5, 30, 17, 18),
(94, '303', 32, 3, 6, 5, 30, 17, 18),
(95, '401', 32, 4, 6, 5, 30, 17, 18),
(96, '402', 72, 4, 12, 6, 72, 17, 18),
(97, '403', 48, 4, 6, 8, 48, 17, 18),
(98, '202', 48, 2, 8, 6, 48, 18, 18),
(99, '203', 48, 2, 8, 6, 48, 18, 18),
(100, '303', 48, 3, 8, 6, 48, 18, 18),
(101, '304', 48, 3, 8, 6, 48, 18, 18),
(102, '305', 48, 3, 8, 6, 48, 18, 18),
(103, '101', 80, 1, 8, 14, 112, 19, 18),
(104, '201', 48, 2, 8, 6, 48, 19, 18),
(105, '202', 48, 2, 8, 6, 48, 19, 18),
(106, '203', 48, 2, 8, 6, 48, 19, 18),
(107, '301', 48, 3, 8, 6, 48, 19, 18),
(108, '302', 48, 3, 8, 6, 48, 19, 18),
(109, '303', 48, 3, 8, 6, 48, 19, 18),
(110, '304', 48, 3, 8, 6, 48, 19, 18),
(111, '305', 48, 2, 8, 6, 48, 19, 18),
(112, '306', 48, 2, 8, 6, 48, 19, 18),
(113, '401', 48, 4, 8, 6, 48, 19, 18),
(114, '402', 48, 4, 8, 6, 48, 19, 18),
(115, '403', 48, 4, 8, 6, 48, 19, 18),
(116, '404', 48, 4, 8, 6, 48, 19, 18),
(117, '201', 55, 2, 8, 6, 48, 20, 18),
(118, '202', 55, 2, 8, 6, 48, 20, 18),
(119, '301', 55, 3, 8, 9, 72, 20, 18),
(120, '201', 48, 2, 8, 6, 48, 21, 18),
(121, '202', 48, 2, 8, 6, 48, 21, 18),
(122, '203', 48, 2, 8, 6, 48, 21, 18),
(123, '204', 24, 2, 4, 6, 24, 21, 18),
(124, '205', 24, 2, 4, 6, 24, 21, 18),
(125, '301', 48, 3, 8, 6, 48, 21, 18),
(126, '101', 72, 1, 8, 9, 72, 22, 18),
(127, '201', 48, 2, 8, 6, 48, 22, 18),
(128, '202', 48, 2, 8, 6, 48, 22, 18),
(129, '205', 40, 2, 8, 5, 40, 22, 18),
(130, '301', 48, 3, 8, 6, 48, 22, 18),
(131, '302', 48, 3, 8, 6, 48, 22, 18),
(132, '101-B', 40, 1, 8, 6, 48, 23, 18),
(133, '102-B', 48, 1, 8, 5, 40, 23, 18),
(134, '103-A', 48, 1, 8, 6, 48, 23, 18),
(135, '105-B', 48, 1, 10, 5, 50, 23, 18),
(136, '104-B-1', 48, 1, 8, 6, 48, 23, 18),
(137, '201-B', 48, 2, 10, 5, 50, 23, 18),
(138, '205-A', 48, 2, 10, 5, 50, 23, 18),
(139, '202-B', 48, 2, 8, 6, 48, 23, 18),
(140, '203-B', 96, 2, 8, 10, 80, 23, 18),
(141, '302', 48, 3, 10, 6, 60, 23, 18),
(142, '303-B', 48, 3, 10, 5, 50, 23, 18),
(143, '304', 48, 3, 10, 5, 50, 23, 18),
(144, '105', 96, 1, 12, 8, 96, 24, 18),
(145, '106', 96, 1, 12, 8, 96, 24, 18),
(146, '107', 96, 1, 12, 8, 96, 24, 18),
(147, '108', 96, 1, 12, 8, 96, 24, 18),
(148, '109-A', 96, 1, 12, 9, 108, 24, 18),
(149, '109-B', 84, 1, 12, 8, 96, 24, 18),
(150, '202', 90, 2, 12, 15, 180, 24, 18),
(151, '203', 60, 2, 10, 6, 60, 24, 18),
(152, '204', 60, 2, 10, 6, 60, 24, 18),
(153, '205', 60, 2, 10, 6, 60, 24, 18),
(154, '206', 60, 2, 10, 6, 60, 24, 18),
(155, 'M1', 40, 2, 8, 5, 40, 24, 18),
(156, '201', 72, 2, 8, 9, 72, 25, 18),
(157, '202', 48, 2, 8, 6, 48, 25, 18),
(158, '303', 48, 3, 8, 6, 48, 25, 18),
(159, '302', 72, 3, 8, 9, 72, 25, 18),
(160, '301', 48, 3, 8, 6, 48, 25, 18),
(161, '101', 60, 1, 10, 6, 60, 26, 18),
(162, '102', 60, 1, 10, 6, 60, 26, 18),
(163, '201', 60, 2, 10, 7, 70, 26, 18),
(164, '202', 60, 2, 10, 6, 60, 26, 18),
(165, '203', 60, 2, 10, 6, 60, 26, 18),
(166, '201', 48, 1, 8, 6, 48, 27, 18),
(167, '202', 48, 1, 8, 6, 48, 27, 18),
(168, '301', 48, 3, 8, 6, 48, 27, 18),
(169, '301', 48, 3, 8, 6, 48, 27, 18),
(170, '301', 60, 3, 10, 6, 60, 28, 18),
(171, '302', 60, 3, 10, 6, 60, 28, 18),
(172, '303', 60, 3, 10, 6, 60, 28, 18),
(173, '304', 60, 3, 10, 6, 60, 28, 18),
(174, '102', 42, 1, 8, 5, 40, 32, 18),
(175, '201', 54, 2, 8, 7, 56, 32, 18),
(176, '202', 48, 2, 8, 6, 48, 32, 18),
(177, '301', 66, 3, 8, 7, 56, 32, 18),
(178, '302', 54, 3, 8, 7, 56, 32, 18),
(179, '303', 48, 3, 8, 6, 48, 32, 18),
(180, '304', 35, 3, 6, 6, 36, 32, 18),
(181, '203', 50, 2, 10, 5, 50, 33, 18),
(182, '204', 50, 2, 10, 5, 50, 33, 18),
(183, '205', 50, 2, 10, 5, 50, 33, 18),
(184, '206', 50, 2, 10, 5, 50, 33, 18),
(185, '305', 50, 3, 10, 5, 50, 33, 18),
(186, '306', 50, 3, 10, 5, 50, 33, 18),
(187, '307', 50, 3, 10, 5, 50, 33, 18),
(188, '308', 50, 3, 10, 5, 50, 33, 18),
(189, '6', 60, 2, 6, 10, 60, 34, 18),
(190, '8', 60, 2, 6, 10, 60, 34, 18),
(191, '11', 60, 3, 6, 10, 60, 34, 18),
(192, '12', 36, 3, 6, 6, 36, 34, 18),
(193, '13', 68, 3, 6, 11, 66, 34, 18),
(194, '14', 36, 3, 6, 6, 36, 34, 18),
(195, '15', 56, 2, 8, 7, 56, 35, 18),
(196, '18', 72, 3, 8, 10, 80, 35, 18),
(197, '19', 56, 3, 8, 8, 64, 35, 18),
(198, '23', 56, 1, 8, 7, 56, 36, 18),
(199, '24', 56, 1, 8, 7, 56, 36, 18),
(200, '25', 40, 2, 8, 4, 32, 36, 18),
(201, '26', 56, 2, 8, 7, 56, 36, 18),
(202, '27', 56, 2, 8, 7, 56, 36, 18),
(203, '28', 40, 3, 8, 4, 32, 36, 18),
(204, '29', 56, 3, 8, 7, 56, 36, 18),
(205, '30', 56, 3, 8, 7, 56, 36, 18),
(206, '1', 56, 1, 8, 7, 56, 37, 18),
(207, '2', 56, 1, 8, 7, 56, 37, 18),
(208, '3', 56, 1, 8, 7, 56, 37, 18),
(209, '4', 56, 1, 8, 7, 56, 37, 18),
(210, '5', 56, 2, 8, 7, 56, 37, 18),
(211, '6', 56, 2, 8, 7, 56, 37, 18),
(212, '7', 56, 2, 8, 7, 56, 37, 18),
(213, '8', 56, 2, 10, 5, 50, 37, 18),
(214, '9', 56, 2, 10, 5, 50, 37, 18),
(215, '10', 56, 3, 8, 7, 56, 37, 18),
(216, '101', 50, 1, 10, 5, 50, 38, 18),
(217, '102', 50, 1, 10, 5, 50, 38, 18),
(218, '201', 50, 2, 10, 5, 50, 38, 18),
(219, '202', 50, 2, 10, 5, 50, 38, 18),
(220, '301', 50, 3, 10, 5, 50, 38, 18),
(221, '302', 50, 3, 10, 5, 50, 38, 18),
(222, '102', 69, 1, 8, 8, 64, 39, 18),
(223, '204', 32, 2, 8, 4, 32, 39, 18),
(224, '205', 32, 2, 8, 4, 32, 39, 18),
(225, '207', 32, 2, 8, 4, 32, 39, 18),
(226, '208', 32, 2, 8, 4, 32, 39, 18),
(227, '209', 56, 2, 8, 7, 56, 39, 18),
(228, '303', 56, 3, 8, 7, 56, 39, 18),
(229, '304', 32, 3, 8, 4, 32, 39, 18),
(230, '305', 32, 3, 8, 4, 32, 39, 18),
(231, '306', 56, 3, 8, 7, 56, 39, 18),
(232, '307', 56, 3, 8, 7, 56, 39, 18),
(233, '201', 48, 2, 8, 6, 48, 40, 18),
(234, '202', 48, 2, 8, 6, 48, 40, 18),
(235, '203', 48, 2, 8, 6, 48, 40, 18),
(236, '301', 48, 3, 8, 6, 48, 40, 18),
(237, '303', 48, 3, 8, 6, 48, 40, 18),
(238, '304', 64, 3, 8, 8, 64, 40, 18),
(239, '203', 50, 2, 10, 5, 50, 41, 18),
(240, '204', 50, 2, 10, 5, 50, 41, 18),
(241, '301', 50, 3, 10, 5, 50, 41, 18),
(242, '302', 50, 3, 10, 5, 50, 41, 18),
(243, '303', 50, 3, 10, 5, 50, 41, 18),
(244, '304', 50, 3, 10, 5, 50, 41, 18),
(245, '401', 40, 4, 8, 5, 40, 42, 18),
(246, '402', 30, 4, 4, 7, 28, 42, 18),
(247, '302', 40, 3, 8, 5, 40, 42, 18),
(248, '301', 40, 3, 8, 5, 40, 42, 18),
(249, '201', 40, 2, 8, 5, 40, 42, 18),
(250, '202', 40, 2, 8, 5, 40, 42, 18),
(251, '203', 64, 2, 8, 8, 64, 42, 18),
(252, '101', 64, 1, 8, 8, 64, 42, 18),
(253, '201', 72, 2, 8, 8, 64, 43, 18),
(254, '202', 72, 2, 8, 8, 64, 43, 18),
(255, '203', 0, 2, 10, 5, 50, 43, 18),
(256, '204', 0, 3, 10, 5, 50, 43, 18),
(257, '304', 50, 3, 8, 6, 48, 43, 18),
(258, '305', 50, 3, 8, 6, 48, 43, 18),
(259, '306', 50, 3, 8, 6, 48, 43, 18),
(260, '307', 50, 3, 8, 6, 48, 43, 18),
(261, '308', 50, 3, 8, 6, 48, 43, 18),
(262, '105', 32, 1, 8, 4, 32, 44, 18),
(263, '101', 32, 1, 8, 4, 32, 44, 18),
(264, '102', 70, 1, 8, 9, 72, 44, 18),
(265, '106', 70, 2, 8, 8, 64, 44, 18),
(266, '204', 48, 2, 8, 6, 48, 44, 18),
(267, '311', 32, 3, 8, 4, 32, 44, 18),
(268, '310', 72, 3, 8, 9, 72, 44, 18),
(269, '309', 32, 3, 8, 4, 32, 44, 18),
(270, '308', 32, 3, 8, 4, 32, 44, 18),
(271, '307', 32, 3, 8, 4, 32, 44, 18),
(272, '306', 32, 3, 8, 4, 32, 44, 18),
(273, '305', 32, 3, 8, 4, 32, 44, 18),
(274, '104', 70, 1, 8, 9, 72, 47, 18),
(275, '102', 33, 1, 8, 4, 32, 47, 18),
(276, '101', 46, 1, 7, 7, 49, 47, 18),
(277, '204', 70, 2, 8, 8, 64, 47, 18),
(278, '201', 46, 2, 7, 6, 42, 47, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacoraadmisiondetalle`
--

CREATE TABLE `bitacoraadmisiondetalle` (
  `BitAdmDetId` int(5) NOT NULL,
  `AdmDetId` int(5) DEFAULT NULL,
  `AdmDetFKAdmCabIdAnt` int(5) DEFAULT NULL,
  `AdmDetFKAdmCabIdNue` int(5) DEFAULT NULL,
  `AdmDetFKUsuAnt` int(5) DEFAULT NULL,
  `AdmDetFKUsuNue` int(5) DEFAULT NULL,
  `AdmDetFKCarAnt` int(5) DEFAULT NULL,
  `AdmDetFKCarNue` int(5) DEFAULT NULL,
  `AdmDetFKCar2Ant` int(5) DEFAULT NULL,
  `AdmDetFKCar2Nue` int(5) DEFAULT NULL,
  `AdmDetLug1Ant` varchar(200) DEFAULT NULL,
  `AdmDetLug1Nue` varchar(200) DEFAULT NULL,
  `AdmDetLug2Ant` varchar(200) DEFAULT NULL,
  `AdmDetLug2Nue` varchar(200) DEFAULT NULL,
  `AdmDetFKAulAnt` int(5) DEFAULT NULL,
  `AdmDetFKAulNue` int(5) DEFAULT NULL,
  `AdmDetFKPabAnt` int(5) DEFAULT NULL,
  `AdmDetFKPabNue` int(5) DEFAULT NULL,
  `AdmDetFKPab2Ant` int(5) DEFAULT NULL,
  `AdmDetFKPab2Nue` int(5) DEFAULT NULL,
  `AdmDetFKAreAnt` int(5) DEFAULT NULL,
  `AdmDetFKAreNue` int(5) DEFAULT NULL,
  `AdmDetFKAre2Ant` int(5) DEFAULT NULL,
  `AdmDetFKAre2Nue` int(5) DEFAULT NULL,
  `AdmDetFKArePueAnt` int(5) DEFAULT NULL,
  `AdmDetFKArePueNue` int(5) DEFAULT NULL,
  `AdmDetFKArePue2Ant` int(5) DEFAULT NULL,
  `AdmDetFKArePue2Nue` int(5) DEFAULT NULL,
  `AdmDetDetAsiAnt` varchar(15) DEFAULT NULL,
  `AdmDetDetAsiNue` varchar(15) DEFAULT NULL,
  `AdmDetCorEnvAnt` int(1) DEFAULT '0',
  `AdmDetCorEnvNue` int(1) DEFAULT '0',
  `AdmDetFKDepTecAnt` int(5) DEFAULT NULL,
  `AdmDetFKDepTecNue` int(5) DEFAULT NULL,
  `AdmDetFKEstRegAnt` int(5) DEFAULT NULL,
  `AdmDetFKEstRegNue` int(5) DEFAULT NULL,
  `usuario` char(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `operacion` varchar(100) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacorausuario`
--

CREATE TABLE `bitacorausuario` (
  `BitUsuId` int(5) NOT NULL,
  `UsuId` int(5) DEFAULT NULL,
  `UsuDniAnt` char(20) DEFAULT NULL,
  `UsuDniNue` char(20) DEFAULT NULL,
  `UsuCuiAnt` char(20) DEFAULT NULL,
  `UsuCuiNue` char(20) DEFAULT NULL,
  `UsuNomAnt` char(100) DEFAULT NULL,
  `UsuNomNue` char(100) DEFAULT NULL,
  `UsuApeAnt` char(100) DEFAULT NULL,
  `UsuApeNue` char(100) DEFAULT NULL,
  `UsuCorEleAnt` char(100) DEFAULT NULL,
  `UsuCorEleNue` char(100) DEFAULT NULL,
  `UsuTelAnt` char(100) DEFAULT NULL,
  `UsuTelNue` char(100) DEFAULT NULL,
  `UsuDirAnt` char(100) DEFAULT NULL,
  `UsuDirNue` char(100) DEFAULT NULL,
  `UsuNumProAnt` int(5) DEFAULT NULL,
  `UsuNumProNue` int(5) DEFAULT NULL,
  `UsuGenAnt` int(5) DEFAULT NULL,
  `UsuGenNue` int(5) DEFAULT NULL,
  `UsuFKTip1Ant` int(5) DEFAULT NULL,
  `UsuFKTip1Nue` int(5) DEFAULT NULL,
  `UsuFKTip2Ant` int(5) DEFAULT NULL,
  `UsuFKTip2Nue` int(5) DEFAULT NULL,
  `UsuFKCatAnt` int(5) DEFAULT NULL,
  `UsuFKCatNue` int(5) DEFAULT NULL,
  `UsuNomUsuAnt` char(100) DEFAULT NULL,
  `UsuNomUsuNue` char(100) DEFAULT NULL,
  `UsuConUsuAnt` char(100) DEFAULT NULL,
  `UsuConUsuNue` char(100) DEFAULT NULL,
  `UsuFKEstRegAnt` int(5) DEFAULT NULL,
  `UsuFKEstRegNue` int(5) DEFAULT NULL,
  `usuario` char(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `operacion` varchar(100) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bitacorausuario`
--

INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(11228, 1, NULL, NULL, NULL, NULL, NULL, 'ADMINISTRADOR', NULL, 'ADMINISTRADOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, 'ADMINISTRADOR', NULL, 'ADMINISTRADOR@123', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11229, 3165, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, 19, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11230, 3166, NULL, NULL, NULL, NULL, NULL, 'OPERADOR2', NULL, 'OPERADOR2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'OPERADOR2', NULL, 'OPERADOR2', NULL, 19, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11231, 3167, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, 19, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11232, 3168, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, 19, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11233, 4723, NULL, NULL, NULL, '20134239', NULL, 'ESTEFANI UBERTA', NULL, 'ABRIL GARATE', NULL, 'eabrilg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11234, 4724, NULL, NULL, NULL, '20084402', NULL, 'JHONNY JESUS', NULL, 'ACHAHUI PILARES', NULL, 'jachaui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11235, 4725, NULL, NULL, NULL, '20154577', NULL, 'EDY', NULL, 'ACHINQUIPA TAPIA', NULL, 'eachinquipa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11236, 4726, NULL, NULL, NULL, '20082142', NULL, 'ALEJANDRA HELENN', NULL, 'ACOSTA CANO', NULL, 'aacostac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11237, 4727, NULL, NULL, NULL, '20134271', NULL, 'ALBHERT FREDDY', NULL, 'ACOSTA TACO', NULL, 'aacostat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11238, 4728, NULL, NULL, NULL, '20134336', NULL, 'SCHENA DEL ROCIO', NULL, 'ACUNA FLORES', NULL, 'sacuna@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11239, 4729, NULL, NULL, NULL, '20080302', NULL, 'JUAN CARLOS', NULL, 'ADCO APAZA', NULL, 'jadcoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11240, 4730, NULL, NULL, NULL, '20142525', NULL, 'PATRICK DANIEL', NULL, 'ADRIAN TEJADA', NULL, 'padrian@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11241, 4731, NULL, NULL, NULL, '20144370', NULL, 'PHYERO ALEXANDER', NULL, 'AGUAYO FLORES', NULL, 'paguayo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11242, 4732, NULL, NULL, NULL, '20131348', NULL, 'JHOJAR', NULL, 'AGUILAR CCOLQUE', NULL, 'jaguilarcco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11243, 4733, NULL, NULL, NULL, '20154586', NULL, 'RICK RANDALL', NULL, 'AGUILAR GAVILAN', NULL, 'raguilarg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11244, 4734, NULL, NULL, NULL, '20174293', NULL, 'DANIEL ANGEL', NULL, 'AGUILAR HUCHANI', NULL, 'daguilarh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11245, 4735, NULL, NULL, NULL, '20121084', NULL, 'MARJORIE DAYANNA', NULL, 'AGUILAR MARTINEZ', NULL, 'maguilarma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11246, 4736, NULL, NULL, NULL, '20154631', NULL, 'ROBERT ANGELO', NULL, 'AGUILAR PARISACA', NULL, 'raguilarp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11247, 4737, NULL, NULL, NULL, '20083507', NULL, 'DAJHAN EDILIA', NULL, 'AGUILAR PUMA', NULL, 'daguilarpu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11248, 4738, NULL, NULL, NULL, '20171016', NULL, 'EDWAR FRANCISC', NULL, 'AGUILAR SALAS', NULL, 'eaguilarsa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11249, 4739, NULL, NULL, NULL, '20143740', NULL, 'SILVIA MAGALY', NULL, 'AGUILAR ZUNIGA', NULL, 'saguilar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11250, 4740, NULL, NULL, NULL, '20134299', NULL, 'ALEX MOISES', NULL, 'AGUIRRE BENAVIDES', NULL, 'aaguirre@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11251, 4741, NULL, NULL, NULL, '20134274', NULL, 'CARMEN ROSA', NULL, 'AGUIRRE LLANLLAYA', NULL, 'caguirrel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11252, 4742, NULL, NULL, NULL, '20173677', NULL, 'XIOMARA GERALDINE', NULL, 'AGUIRRE URVIOLA', NULL, 'xaguirre@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11253, 4743, NULL, NULL, NULL, '20163837', NULL, 'MARICIELO JUDITH', NULL, 'ALAN AYALA', NULL, 'malan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11254, 4744, NULL, NULL, NULL, '20113784', NULL, 'ADALIT GITZELA', NULL, 'ALANOCA CHOQUE', NULL, 'aalanocac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11255, 4745, NULL, NULL, NULL, '20174254', NULL, 'JHON NOLBERTO', NULL, 'ALATA CHOQUECOTA', NULL, 'jalatac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11256, 4746, NULL, NULL, NULL, '20163794', NULL, 'PERCY ENRIQUE', NULL, 'ALATA ORMENO', NULL, 'palatao@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11257, 4747, NULL, NULL, NULL, '20134290', NULL, 'JORGE RONNY', NULL, 'ALATRISTA ESPINOZA', NULL, 'jalatrista@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11258, 4748, NULL, NULL, NULL, '20121341', NULL, 'LEONARDO JOSE LUIS', NULL, 'ALBARRACIN PENA', NULL, 'lalbarracinpe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11259, 4749, NULL, NULL, NULL, '20164735', NULL, 'ANDERSON BORIS', NULL, 'ALEGRIA SUCAZACA', NULL, 'aalegria@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11260, 4750, NULL, NULL, NULL, '20173679', NULL, 'SOPHIA NATIVIDAD', NULL, 'ALEMAN MEDINA', NULL, 'salemanm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11261, 4751, NULL, NULL, NULL, '19892460', NULL, 'WILLY BILL', NULL, 'ALFEREZ PURGUAYA', NULL, 'walferez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11262, 4752, NULL, NULL, NULL, '20091608', NULL, 'LUCY AURORA', NULL, 'ALI GALARZA', NULL, 'laliga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11263, 4753, NULL, NULL, NULL, '20154658', NULL, 'LEYDY DEL PILAR', NULL, 'ALIAGA APAZA', NULL, 'laliaga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11264, 4754, NULL, NULL, NULL, '20144415', NULL, 'CANDY MILAGROS', NULL, 'ALIAGA CARI', NULL, 'caliaga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11265, 4755, NULL, NULL, NULL, '20151480', NULL, 'CARLOS ENRIQUE', NULL, 'ALIENDE PACA', NULL, 'caliende@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11266, 4756, NULL, NULL, NULL, '20134244', NULL, 'MELLISSA', NULL, 'ALLCA CARDENAS', NULL, 'mallca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11267, 4757, NULL, NULL, NULL, '20174250', NULL, 'LUIS BRYAN FERMIN', NULL, 'ALMEYDA RAMIREZ', NULL, 'lalmeyda@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11268, 4758, NULL, NULL, NULL, '20134238', NULL, 'MICHELA YOIS', NULL, 'ALPACA CALCIN', NULL, 'malpaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11269, 4759, NULL, NULL, NULL, '20161042', NULL, 'ERICK ROGER', NULL, 'ALPACA SAYRA', NULL, 'ealpaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11270, 4760, NULL, NULL, NULL, '20124120', NULL, 'NALDY KRISSIA', NULL, 'ALVA PIMENTEL', NULL, 'nalva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11271, 4761, NULL, NULL, NULL, '20174312', NULL, 'ANGELA DANITZA', NULL, 'ALVARADO CARRASCO', NULL, 'aalvaradoca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11272, 4762, NULL, NULL, NULL, '20174282', NULL, 'JEFFRY SAUL', NULL, 'ALVAREZ BEGAZO', NULL, 'jalvarezbeg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11273, 4763, NULL, NULL, NULL, '20154593', NULL, 'MARIO ALONSO', NULL, 'ALVAREZ BRICENO', NULL, 'malvarezbr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11274, 4764, NULL, NULL, NULL, '20164698', NULL, 'ANGIE NICOLE', NULL, 'ALVAREZ CARAZAS', NULL, 'aalvarezcar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11275, 4765, NULL, NULL, NULL, '20164647', NULL, 'CRISTIAN', NULL, 'ALVAREZ FUENTES', NULL, 'calvarezf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11276, 4766, NULL, NULL, NULL, '20134222', NULL, 'MARIZETH DE FATIMA', NULL, 'ALVAREZ GAMIO', NULL, 'malvarezga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11277, 4767, NULL, NULL, NULL, '20164628', NULL, 'NADIA LIGIA', NULL, 'ALVAREZ HERRERA', NULL, 'nalvarezhe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11278, 4768, NULL, NULL, NULL, '20134325', NULL, 'PABLO CESAR', NULL, 'ALVAREZ MACHACA', NULL, 'palvarezma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11279, 4769, NULL, NULL, NULL, '20151520', NULL, 'INGRID MARIA DEL ROSARIO', NULL, 'ALVAREZ MARTINEZ', NULL, 'ialvarez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11280, 4770, NULL, NULL, NULL, '20164652', NULL, 'MARILYN YOHANNA', NULL, 'ALVAREZ MOLINA', NULL, 'malvarezmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11281, 4771, NULL, NULL, NULL, '20174204', NULL, 'LEIDY STEFANY', NULL, 'ALVAREZ USCAMAYTA', NULL, 'lalvarezu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11282, 4772, NULL, NULL, NULL, '20144309', NULL, 'SASHET ALEJANDRA', NULL, 'ALVAREZ VALDIVIA', NULL, 'salvarezva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11283, 4773, NULL, NULL, NULL, '20174326', NULL, 'ALEJANDRO ADAN', NULL, 'ALVAREZ VILCHEZ', NULL, 'aalvarezvil@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11284, 4774, NULL, NULL, NULL, '20161031', NULL, 'VIRNA MAYTE', NULL, 'ALVARO CAHUANTICO', NULL, 'valvaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11285, 4775, NULL, NULL, NULL, '20124135', NULL, 'DALIA INGRID', NULL, 'ALVIS COYLA', NULL, 'dalvisc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11286, 4776, NULL, NULL, NULL, '20131387', NULL, 'ROSARIO', NULL, 'ALVIS MAMANI', NULL, 'ralvis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11287, 4777, NULL, NULL, NULL, '20103629', NULL, 'MIRIAM JOEL', NULL, 'AMBROSIO QUISPE', NULL, 'mambrosio@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11288, 4778, NULL, NULL, NULL, '20154614', NULL, 'JOAQUIN', NULL, 'AMEZQUITA ESPINOZA', NULL, 'jamezquitae@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11289, 4779, NULL, NULL, NULL, '20134337', NULL, 'ELARD MAURICIO', NULL, 'ANARPUMA CAIRA', NULL, 'eanarpuma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11290, 4780, NULL, NULL, NULL, '20143754', NULL, 'PAMELA GLICERIA', NULL, 'ANASCO QUICANO', NULL, 'panascoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11291, 4781, NULL, NULL, NULL, '20154591', NULL, 'MARIELA ZENAIDA', NULL, 'ANCCO CHOQUE', NULL, 'manccocho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11292, 4782, NULL, NULL, NULL, '20131361', NULL, 'DANIEL TITO', NULL, 'ANCCO MACHACA', NULL, 'danccom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11293, 4783, NULL, NULL, NULL, '20172425', NULL, 'HENRY JAVIER', NULL, 'ANCCO MAMANI', NULL, 'hancco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11294, 4784, NULL, NULL, NULL, '20134218', NULL, 'JIANCARLO', NULL, 'ANCCO PANIHUARA', NULL, 'janccop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11295, 4785, NULL, NULL, NULL, '20134197', NULL, 'YAMPOL MICHAEL', NULL, 'ANCO SUCARI', NULL, 'yancos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11296, 4786, NULL, NULL, NULL, '20151517', NULL, 'ELVIS DARIO', NULL, 'ANCONEIRA CHECA', NULL, 'eanconeirac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11297, 4787, NULL, NULL, NULL, '20133679', NULL, 'EVELYN PAOLA', NULL, 'ANDIA LUQUE', NULL, 'eandial@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11298, 4788, NULL, NULL, NULL, '20174227', NULL, 'JERSON RUBEL', NULL, 'ANDIA RAMOS', NULL, 'jandiar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11299, 4789, NULL, NULL, NULL, '20141339', NULL, 'KRISTOFER MICHEL', NULL, 'ANDRADE CANAZA', NULL, 'kandrade@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11300, 4790, NULL, NULL, NULL, '20143497', NULL, 'ANTONIO OMAR', NULL, 'ANGELES CERVANTES', NULL, 'aangeles@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11301, 4791, NULL, NULL, NULL, '20134323', NULL, 'HELEN GUADALUPE', NULL, 'ANTACABANA MONTES', NULL, 'hantacabana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11302, 4792, NULL, NULL, NULL, '20154627', NULL, 'MARIA FERNANDA', NULL, 'APAZA APAZA', NULL, 'mapazaap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11303, 4793, NULL, NULL, NULL, '20075038', NULL, 'HELARD DERECK', NULL, 'APAZA ARIZACA', NULL, 'hapazaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11304, 4794, NULL, NULL, NULL, '20131386', NULL, 'ALEXANDRA', NULL, 'APAZA CASTILLO', NULL, 'aapazacas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11305, 4795, NULL, NULL, NULL, '20124114', NULL, 'JARDY YAIR', NULL, 'APAZA CRUZ', NULL, 'japazacru@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11306, 4796, NULL, NULL, NULL, '20072503', NULL, 'FLORA', NULL, 'APAZA HUACHANI', NULL, 'fapazah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11307, 4797, NULL, NULL, NULL, '20161035', NULL, 'ARNOLD ERICK', NULL, 'APAZA HUISA', NULL, 'aapazahu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11308, 4798, NULL, NULL, NULL, '20144298', NULL, 'ANNIE MARGIE', NULL, 'APAZA LLOSA', NULL, 'aapazall@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11309, 4799, NULL, NULL, NULL, '20154719', NULL, 'ANA CECILIA', NULL, 'APAZA MAMANI', NULL, 'aapazam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11310, 4800, NULL, NULL, NULL, '20164672', NULL, 'ANGIE LAURA', NULL, 'APAZA MAYTA', NULL, 'aapazamayt@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11311, 4801, NULL, NULL, NULL, '20174187', NULL, 'MIGUEL HERNAN', NULL, 'APAZA PACCO', NULL, 'mapazapac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11312, 4802, NULL, NULL, NULL, '20112165', NULL, 'KEDYM', NULL, 'APAZA PACORI', NULL, 'kapazapa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11313, 4803, NULL, NULL, NULL, '20164632', NULL, 'DEISY JASMIN', NULL, 'APAZA PUMA', NULL, 'dapazapu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11314, 4804, NULL, NULL, NULL, '20114198', NULL, 'MARY LUZ', NULL, 'APAZA PUMA', NULL, 'mapazapum@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11315, 4805, NULL, NULL, NULL, '20151510', NULL, 'PILAR GLADYS', NULL, 'APAZA RAMIREZ', NULL, 'papazara@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11316, 4806, NULL, NULL, NULL, '20120524', NULL, 'JUAN GANDER', NULL, 'APAZA SUCASAIRE', NULL, 'japazas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11317, 4807, NULL, NULL, NULL, '20174314', NULL, 'MIRIAN JUDITH', NULL, 'APAZA UMIYAURI', NULL, 'mapazau@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11318, 4808, NULL, NULL, NULL, '20124104', NULL, 'TANIA MERCEDES', NULL, 'APAZA YANQUI', NULL, 'tapazay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11319, 4809, NULL, NULL, NULL, '20174215', NULL, 'ADRIAN JAMERSON', NULL, 'APAZA YAURI', NULL, 'aapazay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11320, 4810, NULL, NULL, NULL, '20174251', NULL, 'MARA NAYIB', NULL, 'APAZA ZEBALLOS', NULL, 'mapazaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11321, 4811, NULL, NULL, NULL, '20154604', NULL, 'CLEYSZON YORDY', NULL, 'APFATA CHALLA', NULL, 'capfata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11322, 4812, NULL, NULL, NULL, '20174245', NULL, 'LISBETH VALERIA', NULL, 'APOMAYTA MAMANI', NULL, 'lapomayta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11323, 4813, NULL, NULL, NULL, '20141374', NULL, 'MARIA ELENA', NULL, 'AQQUEPUCHO JUNO', NULL, 'maqquepucho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11324, 4814, NULL, NULL, NULL, '20091891', NULL, 'EDUARDO JESUS', NULL, 'AQUEPUCHO MEZA', NULL, 'eaquepuchom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11325, 4815, NULL, NULL, NULL, '20164609', NULL, 'LIDIA PATRICIA', NULL, 'AQUIMA CCALLO', NULL, 'laquimacc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11326, 4816, NULL, NULL, NULL, '20162144', NULL, 'MARTHA', NULL, 'ARAGON AGUILAR', NULL, 'maragona@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11327, 4817, NULL, NULL, NULL, '20098173', NULL, 'WALDO', NULL, 'ARANA POMA', NULL, 'waranap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11328, 4818, NULL, NULL, NULL, '20041539', NULL, 'LINO SANDRO', NULL, 'ARAOZ HUALLPA', NULL, 'laraoz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11329, 4819, NULL, NULL, NULL, '20170991', NULL, 'MAYELI CINTHIA', NULL, 'ARAPA MAMANI', NULL, 'marapama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11330, 4820, NULL, NULL, NULL, '20000194', NULL, 'JUAN CARLOS', NULL, 'ARAPA SUMERINDE', NULL, 'jarapasu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11331, 4821, NULL, NULL, NULL, '20144392', NULL, 'ROSARIO JUDITH', NULL, 'ARCE PAREDES', NULL, 'rarcepa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11332, 4822, NULL, NULL, NULL, '20080609', NULL, 'RALPH JORGINHO', NULL, 'ARCE ZEBALLOS', NULL, 'rarcez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11333, 4823, NULL, NULL, NULL, '19993080', NULL, 'OSCAR ALBERTO', NULL, 'ARENAS ARENAS', NULL, 'ovilcaar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11334, 4824, NULL, NULL, NULL, '20164591', NULL, 'RONALDO BERLY', NULL, 'ARENAS DIAZ', NULL, 'rarenasd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11335, 4825, NULL, NULL, NULL, '20154598', NULL, 'YENSI RAMIRO', NULL, 'ARENAS QUISPE', NULL, 'yarenas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11336, 4826, NULL, NULL, NULL, '20174228', NULL, 'MARCO ANTONIO', NULL, 'ARHUIRE CONDORI', NULL, 'marhuireco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11337, 4827, NULL, NULL, NULL, '20174203', NULL, 'CARMEN CECILIA', NULL, 'ARI RUELAS', NULL, 'carir@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11338, 4828, NULL, NULL, NULL, '20141337', NULL, 'YNES JACKELINE', NULL, 'ARIAS JIMENEZ', NULL, 'yariasj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11339, 4829, NULL, NULL, NULL, '20101464', NULL, 'ALEJANDRA CAROLINA', NULL, 'ARIAS PORTUGAL', NULL, 'aariasp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11340, 4830, NULL, NULL, NULL, '20124154', NULL, 'LIZ MERLY', NULL, 'ARIAS RAMIREZ', NULL, 'lariasra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11341, 4831, NULL, NULL, NULL, '20154672', NULL, 'YULI', NULL, 'ARIZACA APAZA', NULL, 'yarizacaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11342, 4832, NULL, NULL, NULL, '20134261', NULL, 'CARMEN VANESA', NULL, 'AROCUTIPA AMANQUI', NULL, 'carocutipaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11343, 4833, NULL, NULL, NULL, '20144333', NULL, 'FREDY', NULL, 'AROCUTIPA MAMANI', NULL, 'farocutipama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11344, 4834, NULL, NULL, NULL, '20091241', NULL, 'SOL LUCERO', NULL, 'ARONI LOAYZA', NULL, 'saroni@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11345, 4835, NULL, NULL, NULL, '20173672', NULL, 'JOSE DARIO', NULL, 'AROSQUIPA RODRIGO', NULL, 'jarosquipar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11346, 4836, NULL, NULL, NULL, '20154610', NULL, 'GABRIELA LIZZETT', NULL, 'AROTAIPE TORRES', NULL, 'garotaipet@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11347, 4837, NULL, NULL, NULL, '20112162', NULL, 'OLGA GRACIELA', NULL, 'AROTAYPE OLLACHICA', NULL, 'oarotaype@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11348, 4838, NULL, NULL, NULL, '20153989', NULL, 'CARMEN LIZ', NULL, 'ARREDONDO PALACIOS', NULL, 'carredondo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11349, 4839, NULL, NULL, NULL, '20134225', NULL, 'JOSE CARLOS', NULL, 'ARRUE OCSA', NULL, 'jarrue@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11350, 4840, NULL, NULL, NULL, '20143541', NULL, 'ELSA VANESSA', NULL, 'ASCUNA SIVINCHA', NULL, 'eascuna@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11351, 4841, NULL, NULL, NULL, '20064890', NULL, 'JONNATHAN JOSE MIGUEL', NULL, 'ASIN LEON', NULL, 'jasin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11352, 4842, NULL, NULL, NULL, '20131352', NULL, 'GERSON MODESTO', NULL, 'ASQUI MAMANI', NULL, 'gasqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11353, 4843, NULL, NULL, NULL, '20104431', NULL, 'JESUS ARMANDO', NULL, 'ASTO HUAMANI', NULL, 'jasto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11354, 4844, NULL, NULL, NULL, '20153954', NULL, 'NIEVES MAGDALENA', NULL, 'ASTULLE AZA', NULL, 'nastulle@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11355, 4845, NULL, NULL, NULL, '20154657', NULL, 'EVA MARILUZ', NULL, 'ATAMARI HANARI', NULL, 'eatamari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11356, 4846, NULL, NULL, NULL, '20124173', NULL, 'DARIANA SUSIBEL', NULL, 'ATASI CARRION', NULL, 'datasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11357, 4847, NULL, NULL, NULL, '20154717', NULL, 'FLOR DE MARIA', NULL, 'ATAUCURI HUAMANI', NULL, 'fataucuri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11358, 4848, NULL, NULL, NULL, '20154647', NULL, 'JAQUELYN MILAGROS', NULL, 'AUCAHUAQUI CCOROPUNA', NULL, 'jaucahuaqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11359, 4849, NULL, NULL, NULL, '20143507', NULL, 'MISHELL MILAGROS', NULL, 'AUCAHUAQUI CCOROPUNA', NULL, 'maucahuaqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11360, 4850, NULL, NULL, NULL, '20142477', NULL, 'IVAN DIEGO', NULL, 'AVALOS COLQUEHUANCA', NULL, 'iavalosc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11361, 4851, NULL, NULL, NULL, '20130927', NULL, 'ALEXANDER GONZALO', NULL, 'AVALOS PALOMINO', NULL, 'aavalosp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11362, 4852, NULL, NULL, NULL, '20084413', NULL, 'MARCELO RODRIGO', NULL, 'AVALOS TORRES', NULL, 'mavalost@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11363, 4853, NULL, NULL, NULL, '20164733', NULL, 'XIOMARA ESTEFANIA', NULL, 'AVILES GARCIA', NULL, 'xaviles@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11364, 4854, NULL, NULL, NULL, '20133657', NULL, 'TANIA MAYLA', NULL, 'AYAQUE ANCCO', NULL, 'tayaque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11365, 4855, NULL, NULL, NULL, '20164729', NULL, 'WALTER JESUS', NULL, 'AYME ESPINOZA', NULL, 'waymee@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11366, 4856, NULL, NULL, NULL, '20154634', NULL, 'MILAGROS OKSSANA BRUNELA', NULL, 'AYNAYANQUI VARGAS', NULL, 'maynayanqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11367, 4857, NULL, NULL, NULL, '20174229', NULL, 'FIORELLA MARIA LUZ', NULL, 'AYQUI DIAZ', NULL, 'fayqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11368, 4858, NULL, NULL, NULL, '20143734', NULL, 'ANA CECILIA YENNIFER', NULL, 'AYQUI VALDIVIA', NULL, 'aayqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11369, 4859, NULL, NULL, NULL, '20174285', NULL, 'BRANDON ERICKSON', NULL, 'BACA ZARATE', NULL, 'bbaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11370, 4860, NULL, NULL, NULL, '20134285', NULL, 'KEVIN RICHARD', NULL, 'BAEZ EGUIA', NULL, 'kbaez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11371, 4861, NULL, NULL, NULL, '20171018', NULL, 'VIVIAN ERIKA', NULL, 'BALLESTEROS PENA', NULL, 'vballesteros@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11372, 4862, NULL, NULL, NULL, '20163788', NULL, 'SABRINA RAQUEL', NULL, 'BALLON QUENAYA', NULL, 'sballonq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11373, 4863, NULL, NULL, NULL, '20164638', NULL, 'DENNIS JOSE', NULL, 'BALLON RIOS', NULL, 'dballon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11374, 4864, NULL, NULL, NULL, '20141373', NULL, 'GABRIELA ROXANA', NULL, 'BANDA MOSCOSO', NULL, 'gbandam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11375, 4865, NULL, NULL, NULL, '20124178', NULL, 'MERLY DEL PILAR', NULL, 'BANDA RICALDE', NULL, 'mbandar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11376, 4866, NULL, NULL, NULL, '20131351', NULL, 'ALEJANDRA', NULL, 'BARCO MITA', NULL, 'abarco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11377, 4867, NULL, NULL, NULL, '20170994', NULL, 'ERIKA MADELINE', NULL, 'BARRAZA ALVARO', NULL, 'ebarraza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11378, 4868, NULL, NULL, NULL, '20174211', NULL, 'ABRAHAN GERSON', NULL, 'BARREDA VELASQUEZ', NULL, 'abarredave@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11379, 4869, NULL, NULL, NULL, '20154584', NULL, 'LUIS ADRIAN', NULL, 'BARRERA MAYTA', NULL, 'lbarrera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11380, 4870, NULL, NULL, NULL, '20104152', NULL, 'KAREN MELANIE', NULL, 'BARRIGA ZEVALLOS', NULL, 'kbarrigaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11381, 4871, NULL, NULL, NULL, '20142779', NULL, 'ROMAN CARLOS', NULL, 'BARRIONUEVO LLAMOCCA', NULL, 'rbarrionuevo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11382, 4872, NULL, NULL, NULL, '20122719', NULL, 'ELKA RAISSA', NULL, 'BARRIOS ANKASS', NULL, 'ebarriosan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11383, 4873, NULL, NULL, NULL, '20114146', NULL, 'NIGEL JESUS', NULL, 'BARRIOS PATINO', NULL, 'nbarriosp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11384, 4874, NULL, NULL, NULL, '20174278', NULL, 'LUIS ENRIQUE', NULL, 'BASURCO HERMOZA', NULL, 'lbasurcoh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11385, 4875, NULL, NULL, NULL, '20164704', NULL, 'MAYKOL CAIN', NULL, 'BAUTISTA CHECCLLO', NULL, 'mbautistach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11386, 4876, NULL, NULL, NULL, '20134252', NULL, 'WILFREDO', NULL, 'BAUTISTA FLORES', NULL, 'wbautistaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11387, 4877, NULL, NULL, NULL, '20131347', NULL, 'LUCY PAMELA STEFANNY', NULL, 'BAZAN RAMOS', NULL, 'lbazanr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11388, 4878, NULL, NULL, NULL, '20163807', NULL, 'DIANA GUADALUPE', NULL, 'BECERRA MAMANI', NULL, 'dbecerram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11389, 4879, NULL, NULL, NULL, '20134241', NULL, 'JEANPIERE', NULL, 'BECERRA ROJAS', NULL, 'jbecerra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11390, 4880, NULL, NULL, NULL, '20082238', NULL, 'EDGARD GIOVANNY', NULL, 'BEDREGAL CARDENAS', NULL, 'ebedregalc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11391, 4881, NULL, NULL, NULL, '20123231', NULL, 'LUIS FERNANDO', NULL, 'BEDREGAL GARCIA', NULL, 'lbedregalg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11392, 4882, NULL, NULL, NULL, '20111427', NULL, 'ESTEPHANY', NULL, 'BEDREGAL SONCCO', NULL, 'ebedregals@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11393, 4883, NULL, NULL, NULL, '20133675', NULL, 'YOSELIN', NULL, 'BEDREGAL VARGAS', NULL, 'ybedregalva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11394, 4884, NULL, NULL, NULL, '20134318', NULL, 'JESUS LEONARDO', NULL, 'BEGAZO CACERES', NULL, 'jbegazo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11395, 4885, NULL, NULL, NULL, '20075040', NULL, 'LIZETH CINTHYA', NULL, 'BEGAZO PACSI', NULL, 'lbegazop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11396, 4886, NULL, NULL, NULL, '20134364', NULL, 'STEPHANE ELIZABETH', NULL, 'BEGAZO QUISPE', NULL, 'sbegazoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11397, 4887, NULL, NULL, NULL, '20061793', NULL, 'JACKELINE DAGNELI', NULL, 'BEJAR ACROTA', NULL, 'jbejara@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(11398, 4888, NULL, NULL, NULL, '20164575', NULL, 'EDUARDO RAUL', NULL, 'BEJAR CAZA', NULL, 'ebejarca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11399, 4889, NULL, NULL, NULL, '20112514', NULL, 'LORENA ANGELICA', NULL, 'BEJARANO GOMEZ', NULL, 'lbejarano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11400, 4890, NULL, NULL, NULL, '20174223', NULL, 'GONZALO JAVIER', NULL, 'BELLIDO DAVILA', NULL, 'gbellidod@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11401, 4891, NULL, NULL, NULL, '20172437', NULL, 'VALERY VALETSA', NULL, 'BENAVENTE POSTIGO', NULL, 'vbenaventepo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11402, 4892, NULL, NULL, NULL, '20162142', NULL, 'MILAGROS XIMENA', NULL, 'BENAVENTE VILLALTA', NULL, 'mbenaventev@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11403, 4893, NULL, NULL, NULL, '20102959', NULL, 'JUAN CARLOS', NULL, 'BENITES MOJO', NULL, 'jbenitesm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11404, 4894, NULL, NULL, NULL, '20152325', NULL, 'KARINA CARMEN', NULL, 'BENITO CONDORI', NULL, 'kbenito@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11405, 4895, NULL, NULL, NULL, '20164725', NULL, 'DIANA KAROLINA', NULL, 'BENITO PALLANI', NULL, 'dbenitop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11406, 4896, NULL, NULL, NULL, '20131365', NULL, 'ROLANDO', NULL, 'BERNEDO CRUZ', NULL, 'rbernedoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11407, 4897, NULL, NULL, NULL, '20041661', NULL, 'CYNTIA', NULL, 'BERNEDO LIPA', NULL, 'cbernedol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11408, 4898, NULL, NULL, NULL, '20164686', NULL, 'GINA KELLY', NULL, 'BOLANOS GUERRERO', NULL, 'gbolanosg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11409, 4899, NULL, NULL, NULL, '20173670', NULL, 'SAMUEL JAMIN', NULL, 'BONIFACIO LOPEZ', NULL, 'sbonifacio@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11410, 4900, NULL, NULL, NULL, '20134258', NULL, 'FREDDY ALONSO', NULL, 'BORJA CONCHA', NULL, 'fborja@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11411, 4901, NULL, NULL, NULL, '20144372', NULL, 'MAURICIO ALEJANDRO', NULL, 'BORJA CONCHA', NULL, 'mborjac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11412, 4902, NULL, NULL, NULL, '20144382', NULL, 'GABRIELA MILAGROS', NULL, 'BORJA HUANCA', NULL, 'gborja@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11413, 4903, NULL, NULL, NULL, '20132476', NULL, 'CLAUDIA DENISSE', NULL, 'BRAVO GONZALES', NULL, 'cbravog@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11414, 4904, NULL, NULL, NULL, '20133659', NULL, 'CINDY JULISA', NULL, 'BRONCANO MONTES', NULL, 'cbroncano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11415, 4905, NULL, NULL, NULL, '20154685', NULL, 'JONATHAN ENRIQUE', NULL, 'BUSTAMANTE CHUCTAYA', NULL, 'jbustamante@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11416, 4906, NULL, NULL, NULL, '20083729', NULL, 'CARLA PAOLA', NULL, 'BUSTAMANTE MOTTA', NULL, 'cbustamantemo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11417, 4907, NULL, NULL, NULL, '20134326', NULL, 'SUSAN WENDY', NULL, 'BUSTAMANTE SOTO', NULL, 'sbustamante@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11418, 4908, NULL, NULL, NULL, '20172450', NULL, 'KATHERINE JAZMIN', NULL, 'BUSTINZA CONDORI', NULL, 'kbustinzac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11419, 4909, NULL, NULL, NULL, '20154709', NULL, 'KIARA KRISTEL', NULL, 'BUTRON GALVEZ', NULL, 'kbutron@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11420, 4910, NULL, NULL, NULL, '20096619', NULL, 'MARLENE IVONNE', NULL, 'BUTRON OLIVERA', NULL, 'mbutrono@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11421, 4911, NULL, NULL, NULL, '20161044', NULL, 'BRENDA EVELYN', NULL, 'BUTRON QUISPE', NULL, 'bbutron@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11422, 4912, NULL, NULL, NULL, '20164576', NULL, 'NOHELIA ALEXANDRA', NULL, 'CABRERA CALISAYA', NULL, 'ncabrerac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11423, 4913, NULL, NULL, NULL, '20121320', NULL, 'MICHAEL CHARLES', NULL, 'CABRERA CHOQUE', NULL, 'mcabreracho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11424, 4914, NULL, NULL, NULL, '20154664', NULL, 'MARIA CELESTE', NULL, 'CABRERA GUTIERREZ', NULL, 'mcabrerag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11425, 4915, NULL, NULL, NULL, '20121319', NULL, 'CARLO GUSTAVO', NULL, 'CABRERA RODRIGUEZ', NULL, 'ccabrerar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11426, 4916, NULL, NULL, NULL, '20081168', NULL, 'LEONARDO ALEXANDER', NULL, 'CACERES AYLLON', NULL, 'lcaceresa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11427, 4917, NULL, NULL, NULL, '20133687', NULL, 'YOBER ALEXANDER', NULL, 'CACERES CARHUAS', NULL, 'ycacerescar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11428, 4918, NULL, NULL, NULL, '20161052', NULL, 'DIANA CLARA', NULL, 'CACERES CHAINA', NULL, 'dcaceresc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11429, 4919, NULL, NULL, NULL, '20141378', NULL, 'THANIA LAURA', NULL, 'CACERES HUACARPUMA', NULL, 'tcaceresh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11430, 4920, NULL, NULL, NULL, '20174329', NULL, 'STEPHANY CLARIBETH', NULL, 'CACERES HUANQUI', NULL, 'scacereshu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11431, 4921, NULL, NULL, NULL, '20154565', NULL, 'JOSE ALONSO', NULL, 'CACERES LANCHIPA', NULL, 'jcaceresl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11432, 4922, NULL, NULL, NULL, '20081631', NULL, 'CLAUDIA ALEJANDRA', NULL, 'CACERES LEON', NULL, 'ccaceresl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11433, 4923, NULL, NULL, NULL, '20164619', NULL, 'LUIS FERNANDO', NULL, 'CACERES LOPEZ', NULL, 'lcaceresl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11434, 4924, NULL, NULL, NULL, '20154563', NULL, 'JOSUE ISRRAEL', NULL, 'CACERES ZAVALA', NULL, 'jcaceresz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11435, 4925, NULL, NULL, NULL, '20134333', NULL, 'MARY INES', NULL, 'CAHUA NOA', NULL, 'mcahua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11436, 4926, NULL, NULL, NULL, '20151502', NULL, 'YASHIRA LIA', NULL, 'CAHUA OCHOA', NULL, 'ycahuao@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11437, 4927, NULL, NULL, NULL, '20154660', NULL, 'PATRICIA DEL CARMEN', NULL, 'CAHUANA APAZA', NULL, 'pcahuanaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11438, 4928, NULL, NULL, NULL, '20174220', NULL, 'SAUL SANTOS', NULL, 'CAHUANA CONDORI', NULL, 'scahuanac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11439, 4929, NULL, NULL, NULL, '20174224', NULL, 'LIZ VALERI', NULL, 'CAHUANA GAMERO', NULL, 'lcahuanag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11440, 4930, NULL, NULL, NULL, '20143493', NULL, 'JOE ANDRE', NULL, 'CAHUAS CADENAS', NULL, 'jcahuas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11441, 4931, NULL, NULL, NULL, '20142066', NULL, 'DIANA FIORELLA', NULL, 'CAHUI TORRES', NULL, 'dcahui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11442, 4932, NULL, NULL, NULL, '20132485', NULL, 'WALTER EDUARDO', NULL, 'CAHUINA LUQUE', NULL, 'wcahuina@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11443, 4933, NULL, NULL, NULL, '20144305', NULL, 'FIORELLA JOVITA', NULL, 'CAJA FUENTES', NULL, 'fcaja@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11444, 4934, NULL, NULL, NULL, '20142476', NULL, 'JANISS', NULL, 'CALAPUJA GUTIERREZ', NULL, 'jcalapuja@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11445, 4935, NULL, NULL, NULL, '20123013', NULL, 'FIDEL CRISTIAN', NULL, 'CALAPUJA MAMANI', NULL, 'fcalapujam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11446, 4936, NULL, NULL, NULL, '20112145', NULL, 'DAMARIS JHOANNA', NULL, 'CALCINA FLORES', NULL, 'dcalcinafl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11447, 4937, NULL, NULL, NULL, '20144329', NULL, 'HECTOR CRISTHIAN', NULL, 'CALDERON GIRON', NULL, 'hcalderong@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11448, 4938, NULL, NULL, NULL, '20171006', NULL, 'DIEGO ADOLFO', NULL, 'CALDERON GUTIERREZ', NULL, 'dcalderong@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11449, 4939, NULL, NULL, NULL, '20082339', NULL, 'ANDREA LUZ', NULL, 'CALDERON PERALTA', NULL, 'acalderonpe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11450, 4940, NULL, NULL, NULL, '19960446', NULL, 'LUZ KARINA', NULL, 'CALDERON RODRIGUEZ', NULL, 'lcalderonr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11451, 4941, NULL, NULL, NULL, '20072464', NULL, 'MARIO ANDRES', NULL, 'CALDERON RODRIGUEZ', NULL, 'mcalderonr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11452, 4942, NULL, NULL, NULL, '20080552', NULL, 'CARLOS ENRIQUE', NULL, 'CALDERON VILLASANTE', NULL, 'ccalderonv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11453, 4943, NULL, NULL, NULL, '20154585', NULL, 'MARLENY', NULL, 'CALIZAYA CHATA', NULL, 'mcalizayach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11454, 4944, NULL, NULL, NULL, '20130436', NULL, 'ARTURO ANDRES', NULL, 'CALIZAYA MARTINEZ', NULL, 'acalizayam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11455, 4945, NULL, NULL, NULL, '20141379', NULL, 'ELIZABETH ESTEFANY', NULL, 'CALIZAYA SUNCHULLI', NULL, 'ecalizayas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11456, 4946, NULL, NULL, NULL, '20153981', NULL, 'GERALDINE GISSELY', NULL, 'CALIZAYA URQUIZO', NULL, 'gcalizayau@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11457, 4947, NULL, NULL, NULL, '20171029', NULL, 'GABRIEL SEBASTIAN', NULL, 'CALLA PARI', NULL, 'gcallap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11458, 4948, NULL, NULL, NULL, '20113626', NULL, 'SERGIO ALBERTO', NULL, 'CALLACONDO SUCASACA', NULL, 'scallacondo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11459, 4949, NULL, NULL, NULL, '20144358', NULL, 'YHON JESUS', NULL, 'CALLATA ARIZANCA', NULL, 'ycallataar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11460, 4950, NULL, NULL, NULL, '20134291', NULL, 'VICTORIA JANETT', NULL, 'CALLATA LLERENA', NULL, 'vcallatal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11461, 4951, NULL, NULL, NULL, '20122731', NULL, 'VARINIA MAYRA', NULL, 'CALLE CARRENO', NULL, 'vcallec@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11462, 4952, NULL, NULL, NULL, '20102454', NULL, 'CRISTIAN EDUARDO', NULL, 'CALLENOVA FLORES', NULL, 'ccallenova@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11463, 4953, NULL, NULL, NULL, '20104500', NULL, 'ALFREDO', NULL, 'CALLISAYA TARQUI', NULL, 'acallisaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11464, 4954, NULL, NULL, NULL, '20164673', NULL, 'JUAN PATRICIO', NULL, 'CALLISAYA TARQUI', NULL, 'jcallisaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11465, 4955, NULL, NULL, NULL, '20153990', NULL, 'LIZET MIRIAM', NULL, 'CALLOAPAZA MACHACA', NULL, 'lcalloapaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11466, 4956, NULL, NULL, NULL, '20154716', NULL, 'JOSE DANILO', NULL, 'CALSINA SANCHEZ', NULL, 'jcalsinas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11467, 4957, NULL, NULL, NULL, '20080270', NULL, 'EMILIO FERNANDO', NULL, 'CAMACHO MONROY', NULL, 'ecamacho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11468, 4958, NULL, NULL, NULL, '20133667', NULL, 'ALEXANDRA', NULL, 'CAMACHO OCHOA', NULL, 'acamacho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11469, 4959, NULL, NULL, NULL, '20154679', NULL, 'MICHAEL BRAYHAN', NULL, 'CAMERCCOA QUISPE', NULL, 'mcamerccoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11470, 4960, NULL, NULL, NULL, '20133685', NULL, 'MARIBEL DEL CARMEN', NULL, 'CAMERO VELASQUEZ', NULL, 'mcamero@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11471, 4961, NULL, NULL, NULL, '20161039', NULL, 'LINA SHARON', NULL, 'CANAHUIRE QUISPE', NULL, 'lcanahuireq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11472, 4962, NULL, NULL, NULL, '20144406', NULL, 'CLARA MONICA', NULL, 'CANAZA ARAPA', NULL, 'ccanaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11473, 4963, NULL, NULL, NULL, '20154667', NULL, 'GABRIELA', NULL, 'CANAZA CHIPANA', NULL, 'gcanazach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11474, 4964, NULL, NULL, NULL, '20085243', NULL, 'YOEL CESAR', NULL, 'CANAZA RAMOS', NULL, 'ycanazar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11475, 4965, NULL, NULL, NULL, '20172431', NULL, 'MARIA VICTORIA', NULL, 'CANAZA SARAVIA', NULL, 'mcanazasa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11476, 4966, NULL, NULL, NULL, '20133676', NULL, 'VICTORIA CELY', NULL, 'CANCINO CARCAUSTO', NULL, 'vcancino@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11477, 4967, NULL, NULL, NULL, '20131349', NULL, 'ARTURO UBALDINO', NULL, 'CANDIA HOLGADO', NULL, 'acandiah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11478, 4968, NULL, NULL, NULL, '20096785', NULL, 'NANCY', NULL, 'CANO MAMANI', NULL, 'ncano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11479, 4969, NULL, NULL, NULL, '20083217', NULL, 'AMPARO MARGOT', NULL, 'CAPQUEQUI CRUZ', NULL, 'acapquequi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11480, 4970, NULL, NULL, NULL, '20134354', NULL, 'MICAELA', NULL, 'CARAPI RETAMOZO', NULL, 'mcarapi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11481, 4971, NULL, NULL, NULL, '20134358', NULL, 'CAMILA FIORELLA', NULL, 'CARAZAS MANRIQUE', NULL, 'ccarazasm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11482, 4972, NULL, NULL, NULL, '20144331', NULL, 'RICARDO LUIS ALFREDO', NULL, 'CARAZAS MONTERO', NULL, 'rcarazasm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11483, 4973, NULL, NULL, NULL, '20172423', NULL, 'MILAGROS URSULA', NULL, 'CARBAJAL FLORES', NULL, 'mcarbajalf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11484, 4974, NULL, NULL, NULL, '20144403', NULL, 'KATERINA XIMENA', NULL, 'CARBAJAL QUIROZ', NULL, 'kcarbajalq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11485, 4975, NULL, NULL, NULL, '20143749', NULL, 'SAYRA ANTHUANE', NULL, 'CARBAJAL URRUNAGA', NULL, 'scarbajal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11486, 4976, NULL, NULL, NULL, '20153983', NULL, 'ALONDRA VANESSA', NULL, 'CARCAMO CARI', NULL, 'acarcamo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11487, 4977, NULL, NULL, NULL, '20098271', NULL, 'KAREN YOMARA', NULL, 'CARDENAS ARANA', NULL, 'kcardenasa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11488, 4978, NULL, NULL, NULL, '20164707', NULL, 'LUIS GONZALO DAVID', NULL, 'CARDENAS CARDENAS', NULL, 'lcardenascar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11489, 4979, NULL, NULL, NULL, '20132833', NULL, 'RAUL EDGAR', NULL, 'CARDENAS HERNANDEZ', NULL, 'rcardenash@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11490, 4980, NULL, NULL, NULL, '20060994', NULL, 'JENNIFER SOLEDAD', NULL, 'CARDENAS MOGOLLON', NULL, 'jcardenasm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11491, 4981, NULL, NULL, NULL, '20114168', NULL, 'ALISON MASIEL', NULL, 'CARDENAS PUMACALLAHUA', NULL, 'acardenasp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11492, 4982, NULL, NULL, NULL, '20154724', NULL, 'FERNANDO JAVIER', NULL, 'CARDENAS QUISPE', NULL, 'fcardenasq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11493, 4983, NULL, NULL, NULL, '20171022', NULL, 'ETHEL ESTEFANY', NULL, 'CARDENAS SOSA', NULL, 'ecardenasso@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11494, 4984, NULL, NULL, NULL, '20174243', NULL, 'FLOR DE MARIA', NULL, 'CARI CHOQUE', NULL, 'fcari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11495, 4985, NULL, NULL, NULL, '20134267', NULL, 'JEANPIER RAUL', NULL, 'CARI LANDEO', NULL, 'jcaril@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11496, 4986, NULL, NULL, NULL, '20154642', NULL, 'GONZALO ANDRES', NULL, 'CARI SUMARI', NULL, 'gcari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11497, 4987, NULL, NULL, NULL, '20113797', NULL, 'CRISTIAN ABEL', NULL, 'CARI TORIBIO', NULL, 'ccarit@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11498, 4988, NULL, NULL, NULL, '20144324', NULL, 'MARITZA MARIBEL', NULL, 'CARLOS CHUCTAYA', NULL, 'mcarlosch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11499, 4989, NULL, NULL, NULL, '20134201', NULL, 'EDEVALDO ANDRE', NULL, 'CARNERO CALDERON', NULL, 'ecarneroc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11500, 4990, NULL, NULL, NULL, '20154635', NULL, 'JULIO CESAR', NULL, 'CARNERO LAZO', NULL, 'jcarnero@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11501, 4991, NULL, NULL, NULL, '20154722', NULL, 'FABRICIO JESUS', NULL, 'CARNERO VELIZ', NULL, 'fcarnero@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11502, 4992, NULL, NULL, NULL, '20154680', NULL, 'SHIRLEY GABRIELA', NULL, 'CARPIO HUANCARA', NULL, 'scarpioh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11503, 4993, NULL, NULL, NULL, '20081206', NULL, 'NORMAN JOSEPH', NULL, 'CARPIO SANCHEZ', NULL, 'ncarpios@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11504, 4994, NULL, NULL, NULL, '20162139', NULL, 'DIANA REYNA', NULL, 'CARRANZA MOSTAJO', NULL, 'dcarranza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11505, 4995, NULL, NULL, NULL, '20174263', NULL, 'MIGUEL ALVARO', NULL, 'CARRASCO ARIAS', NULL, 'mcarrascoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11506, 4996, NULL, NULL, NULL, '20098237', NULL, 'JAMES GIANCARLO', NULL, 'CARRASCO TURPO', NULL, 'jcarrascotu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11507, 4997, NULL, NULL, NULL, '20144401', NULL, 'MIKI JOHNSON', NULL, 'CARTAGENA CHURATA', NULL, 'mcartagena@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11508, 4998, NULL, NULL, NULL, '20154702', NULL, 'JUDITH NOELIA', NULL, 'CARTAGENA FLORES', NULL, 'jcartagena@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11509, 4999, NULL, NULL, NULL, '20164579', NULL, 'SAMANTHA NICOLLE', NULL, 'CASA CHORA', NULL, 'scasac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11510, 5000, NULL, NULL, NULL, '20154723', NULL, 'ARNOLD RODRIGO', NULL, 'CASA ZAVALA', NULL, 'acasaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11511, 5001, NULL, NULL, NULL, '20164646', NULL, 'FRANK ANTONY', NULL, 'CASALI QUISPE', NULL, 'fcasali@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11512, 5002, NULL, NULL, NULL, '20151486', NULL, 'ALLISON MELINA', NULL, 'CASANI QUISPE', NULL, 'acasani@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11513, 5003, NULL, NULL, NULL, '20154682', NULL, 'MARIJOSE ARIANA', NULL, 'CASAPIA PARI', NULL, 'mcasapiap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11514, 5004, NULL, NULL, NULL, '20154590', NULL, 'DIANA CECILIA', NULL, 'CASAPIA PARODI', NULL, 'dcasapia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11515, 5005, NULL, NULL, NULL, '20123226', NULL, 'LUZ FATIMA', NULL, 'CASTILLO BANOS', NULL, 'lcastillob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11516, 5006, NULL, NULL, NULL, '20124161', NULL, 'BRYAM FRANCISCO', NULL, 'CASTILLO CARNERO', NULL, 'bcastilloca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11517, 5007, NULL, NULL, NULL, '20082010', NULL, 'CARLOS ALBERTO', NULL, 'CASTILLO MAQUERA', NULL, 'ccastilloma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11518, 5008, NULL, NULL, NULL, '20164583', NULL, 'LUCERO VALERY', NULL, 'CASTILLO VASQUEZ', NULL, 'lcastillov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11519, 5009, NULL, NULL, NULL, '20143743', NULL, 'FLOR DE MARIA', NULL, 'CASTRO CUBA CHECYA', NULL, 'fcastrocuba@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11520, 5010, NULL, NULL, NULL, '20174191', NULL, 'SERGIO OMAR', NULL, 'CASTRO BARRIOS', NULL, 'scastrob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11521, 5011, NULL, NULL, NULL, '20150385', NULL, 'LUIS ALBERTO', NULL, 'CASTRO QUISPE', NULL, 'lcastroq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11522, 5012, NULL, NULL, NULL, '20164734', NULL, 'NATALIE DIANA', NULL, 'CASTRO QUISPE', NULL, 'ncastroq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11523, 5013, NULL, NULL, NULL, '20134221', NULL, 'URSULA JUDITH', NULL, 'CASTRO SACSI', NULL, 'ucastros@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11524, 5014, NULL, NULL, NULL, '20154648', NULL, 'ELIZABETH LISBETH', NULL, 'CASTRO SUCARI', NULL, 'ecastrosu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11525, 5015, NULL, NULL, NULL, '20082176', NULL, 'KATHERINE LOURDES', NULL, 'CATACORA PONCE DE LEON', NULL, 'kcatacorap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11526, 5016, NULL, NULL, NULL, '20163793', NULL, 'LEYLI ROCIO', NULL, 'CATARI CCAMI', NULL, 'lcatari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11527, 5017, NULL, NULL, NULL, '20172418', NULL, 'BRYAN CESAR', NULL, 'CAYLLAHUA CHAMBI', NULL, 'bcayllahua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11528, 5018, NULL, NULL, NULL, '20144325', NULL, 'LUCIA DEL CARMEN', NULL, 'CAYRA INCAHUANACO', NULL, 'lcayrai@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11529, 5019, NULL, NULL, NULL, '20141388', NULL, 'ROSARIO YSABEL', NULL, 'CCAHUACHIA TINTAYA', NULL, 'rccahuachia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11530, 5020, NULL, NULL, NULL, '20134308', NULL, 'CARLOS', NULL, 'CCAHUANA AGUILAR', NULL, 'cccahuanaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11531, 5021, NULL, NULL, NULL, '20098214', NULL, 'EDGAR', NULL, 'CCAHUANA AGUILAR', NULL, 'eccahuanaag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11532, 5022, NULL, NULL, NULL, '20143729', NULL, 'DANIRIA', NULL, 'CCAHUANA RODRIGUEZ', NULL, 'dccahuanar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11533, 5023, NULL, NULL, NULL, '20143736', NULL, 'ELENA BERENICE', NULL, 'CCAHUAYA CHURATA', NULL, 'eccahuaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11534, 5024, NULL, NULL, NULL, '20172445', NULL, 'LIZ BETT', NULL, 'CCALLA DIAZ', NULL, 'lccallad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11535, 5025, NULL, NULL, NULL, '20141377', NULL, 'CLELIA', NULL, 'CCALLO PACCA', NULL, 'cccallo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11536, 5026, NULL, NULL, NULL, '20151485', NULL, 'CANDY JULISZA', NULL, 'CCAMA BARRIONUEVO', NULL, 'cccama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11537, 5027, NULL, NULL, NULL, '20144352', NULL, 'CRISTIAN JORDANI', NULL, 'CCAMA PINTO', NULL, 'cccamap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11538, 5028, NULL, NULL, NULL, '20174199', NULL, 'JANETH VANESA', NULL, 'CCANAZA COTACALLAPA', NULL, 'jccanaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11539, 5029, NULL, NULL, NULL, '20131367', NULL, 'EDGAR', NULL, 'CCAPA CUTI', NULL, 'eccapacu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11540, 5030, NULL, NULL, NULL, '20121300', NULL, 'EDITH', NULL, 'CCAPA PILA', NULL, 'eccapap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11541, 5031, NULL, NULL, NULL, '20144349', NULL, 'JUNIOR EDUARDO', NULL, 'CCARI FLORES', NULL, 'jccarif@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11542, 5032, NULL, NULL, NULL, '20061852', NULL, 'ANGELA JACKELINE', NULL, 'CCARI YAURE', NULL, 'accariy@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11543, 5033, NULL, NULL, NULL, '20112109', NULL, 'DAVID', NULL, 'CCAYUSI CONDO', NULL, 'dccayusi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11544, 5034, NULL, NULL, NULL, '20134260', NULL, 'DAHYAN', NULL, 'CCONISLLA HUANAHUI', NULL, 'dcconislla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11545, 5035, NULL, NULL, NULL, '20131368', NULL, 'SHIRLEY KIMBERLY', NULL, 'CCORA CONDORI', NULL, 'sccora@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11546, 5036, NULL, NULL, NULL, '20072534', NULL, 'NELIDA ROXANA', NULL, 'CCORA MAMANI', NULL, 'nccora@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11547, 5037, NULL, NULL, NULL, '20164604', NULL, 'HEBER ROMULO', NULL, 'CCORIMANYA INGA', NULL, 'hccorimanya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11548, 5038, NULL, NULL, NULL, '20153976', NULL, 'MARIA FERNANDA', NULL, 'CCOSI ESPINOZA', NULL, 'mccosi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11549, 5039, NULL, NULL, NULL, '20083739', NULL, 'ESMERALDA VERONICA', NULL, 'CCOYA APAZA', NULL, 'eccoya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11550, 5040, NULL, NULL, NULL, '20174300', NULL, 'ROSMERY', NULL, 'CCOYORI DELGADO', NULL, 'rccoyori@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11551, 5041, NULL, NULL, NULL, '20163772', NULL, 'DIEGO ALBERTO', NULL, 'CERDENA HUANQUI', NULL, 'dcerdena@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11552, 5042, NULL, NULL, NULL, '20124134', NULL, 'GUADALUPE', NULL, 'CEREZO HALLASI', NULL, 'gcerezo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11553, 5043, NULL, NULL, NULL, '20114153', NULL, 'YDENCE MASAMI', NULL, 'CERPA HERRERA', NULL, 'ycerpah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11554, 5044, NULL, NULL, NULL, '20164703', NULL, 'NADIA AZUCENA', NULL, 'CERVANTES ALMONTE', NULL, 'ncervantesa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11555, 5045, NULL, NULL, NULL, '20174195', NULL, 'LESLY SAYDA', NULL, 'CESPEDES CAIRA', NULL, 'lcespedesc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11556, 5046, NULL, NULL, NULL, '20120109', NULL, 'MARYLIN', NULL, 'CHACNAMA BOBADILLA', NULL, 'mchacnama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11557, 5047, NULL, NULL, NULL, '20131370', NULL, 'JACKELINE MABEL', NULL, 'CHACO SALHUA', NULL, 'jchacos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11558, 5048, NULL, NULL, NULL, '20124159', NULL, 'ELIZABETH', NULL, 'CHACON ANGULO', NULL, 'echacona@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11559, 5049, NULL, NULL, NULL, '20134245', NULL, 'NILDA RUTH', NULL, 'CHACON CARRILLO', NULL, 'nchacon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11560, 5050, NULL, NULL, NULL, '20131371', NULL, 'GIANN FRANCO', NULL, 'CHACON CORNEJO', NULL, 'gchaconco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11561, 5051, NULL, NULL, NULL, '20144381', NULL, 'BRENDA PATRICIA', NULL, 'CHACON MOLINA', NULL, 'bchacon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11562, 5052, NULL, NULL, NULL, '20131389', NULL, 'JONATHAN CHRISTIAN', NULL, 'CHACON PAZ', NULL, 'jchaconp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11563, 5053, NULL, NULL, NULL, '20164719', NULL, 'VILMA ZAIDA', NULL, 'CHAHUA QUISPE', NULL, 'vchahua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11564, 5054, NULL, NULL, NULL, '20144357', NULL, 'JASU MADELAYN', NULL, 'CHAHUAILLO SOTO', NULL, 'jchahuaillo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11565, 5055, NULL, NULL, NULL, '20164648', NULL, 'LUIS ALEXIS', NULL, 'CHALCO ABARCA', NULL, 'lchalcoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11566, 5056, NULL, NULL, NULL, '20174271', NULL, 'LUZ MARINA', NULL, 'CHALCO LAZARTE', NULL, 'lchalcol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11567, 5057, NULL, NULL, NULL, '20121344', NULL, 'PILAR IRENE', NULL, 'CHALLCO OCHOCHOQUE', NULL, 'pchallco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(11568, 5058, NULL, NULL, NULL, '20163806', NULL, 'BEATRIZ', NULL, 'CHAMA TACO', NULL, 'bchama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11569, 5059, NULL, NULL, NULL, '20144299', NULL, 'JESUS MIGUEL', NULL, 'CHAMBE CACERES', NULL, 'jchambe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11570, 5060, NULL, NULL, NULL, '20162401', NULL, 'BRIYITH MAGALI', NULL, 'CHAMBI HUAMANI', NULL, 'bchambih@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11571, 5061, NULL, NULL, NULL, '20163812', NULL, 'ALEJANDRA PAOLA', NULL, 'CHAMBI JULI', NULL, 'achambij@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11572, 5062, NULL, NULL, NULL, '20102925', NULL, 'JAVIER FRANK', NULL, 'CHAMBI MAMANI', NULL, 'jchambimam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11573, 5063, NULL, NULL, NULL, '20081429', NULL, 'RICARDO CARLOS', NULL, 'CHAMBI MAMANI', NULL, 'rchambima@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11574, 5064, NULL, NULL, NULL, '20064134', NULL, 'DIEGO ARMANDO', NULL, 'CHAMBI MOLLEAPAZA', NULL, 'dchambimo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11575, 5065, NULL, NULL, NULL, '20154617', NULL, 'YENNY KATHERINE', NULL, 'CHAMBI SAYCO', NULL, 'ychambis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11576, 5066, NULL, NULL, NULL, '20154606', NULL, 'KEVIN JOSUE', NULL, 'CHAMBILLA CRUZ', NULL, 'kchambilla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11577, 5067, NULL, NULL, NULL, '20164614', NULL, 'ANGEL JESUS', NULL, 'CHAMBILLA LINARES', NULL, 'achambillali@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11578, 5068, NULL, NULL, NULL, '20172448', NULL, 'YULISA', NULL, 'CHAMPI HUARCA', NULL, 'ychampih@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11579, 5069, NULL, NULL, NULL, '20174296', NULL, 'YILL YUNET', NULL, 'CHANG VALDIVIA', NULL, 'ychang@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11580, 5070, NULL, NULL, NULL, '20124090', NULL, 'KARIME BRIGITTE', NULL, 'CHAPA RODRIGUEZ', NULL, 'kchapa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11581, 5071, NULL, NULL, NULL, '20104462', NULL, 'JAIME RODRIGO', NULL, 'CHARA BAUTISTA', NULL, 'jcharab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11582, 5072, NULL, NULL, NULL, '20162151', NULL, 'ALEX GONZALO', NULL, 'CHARA CHUNGA', NULL, 'achara@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11583, 5073, NULL, NULL, NULL, '20163795', NULL, 'ELIZABETH MARLUVE', NULL, 'CHARA TUNQUIPA', NULL, 'echara@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11584, 5074, NULL, NULL, NULL, '20153961', NULL, 'ALVARO DANIEL', NULL, 'CHARCA CALLE', NULL, 'acharca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11585, 5075, NULL, NULL, NULL, '20173665', NULL, 'GONZALO ALFREDO', NULL, 'CHATA QUINTANO', NULL, 'gchataq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11586, 5076, NULL, NULL, NULL, '20173669', NULL, 'OLENKHA DAYANNA', NULL, 'CHAUCA BARRIOS', NULL, 'ochauca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11587, 5077, NULL, NULL, NULL, '20141345', NULL, 'ALEXSANDER CRISTIAN', NULL, 'CHAUPE PINO', NULL, 'achaupe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11588, 5078, NULL, NULL, NULL, '20164699', NULL, 'MARICIELO', NULL, 'CHAVEZ CABRERA', NULL, 'mchavezcab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11589, 5079, NULL, NULL, NULL, '20153966', NULL, 'ROSMERY EVANGELINA', NULL, 'CHAVEZ CACERES', NULL, 'rchavez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11590, 5080, NULL, NULL, NULL, '20151443', NULL, 'RUTH MERY THAIS', NULL, 'CHAVEZ CCAMA', NULL, 'rchavezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11591, 5081, NULL, NULL, NULL, '20134230', NULL, 'DEISY PAMELA', NULL, 'CHAVEZ HUANACO', NULL, 'dchavezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11592, 5082, NULL, NULL, NULL, '20096753', NULL, 'FIORELLA SABEL', NULL, 'CHAVEZ HUISACAYNA', NULL, 'fchavezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11593, 5083, NULL, NULL, NULL, '20164701', NULL, 'EMILIO DE JESUS', NULL, 'CHAVEZ MUNOZ', NULL, 'echavezmu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11594, 5084, NULL, NULL, NULL, '20114132', NULL, 'SAMYLETH STEEFANY', NULL, 'CHAVEZ UGARTE', NULL, 'schavezu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11595, 5085, NULL, NULL, NULL, '20164671', NULL, 'STEFANO LEANDRO', NULL, 'CHAVEZ VARGAS', NULL, 'schavezva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11596, 5086, NULL, NULL, NULL, '20131342', NULL, 'YOSELIN', NULL, 'CHECCA FLORES', NULL, 'ychecca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11597, 5087, NULL, NULL, NULL, '20144315', NULL, 'ANA CECILIA ARACELI', NULL, 'CHECMAPOCCO CHAHUARA', NULL, 'achecmapocco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11598, 5088, NULL, NULL, NULL, '20141366', NULL, 'NELSON JAMER', NULL, 'CHECYA ANCCASI', NULL, 'nchecyaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11599, 5089, NULL, NULL, NULL, '20104214', NULL, 'YESSENIA DEL PILAR', NULL, 'CHIARELLA GARCIA', NULL, 'ychiarella@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11600, 5090, NULL, NULL, NULL, '20174294', NULL, 'EDWIN FREDY', NULL, 'CHINO BERNA', NULL, 'echinob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11601, 5091, NULL, NULL, NULL, '20174311', NULL, 'LADY BRIGHITTE', NULL, 'CHIPANA ARCIBIA', NULL, 'lchipanaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11602, 5092, NULL, NULL, NULL, '20144301', NULL, 'ZULMA', NULL, 'CHIPANA LOPEZ', NULL, 'zchipana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11603, 5093, NULL, NULL, NULL, '20121318', NULL, 'KAREN PIEDAD', NULL, 'CHIRE TAPIA', NULL, 'kchiret@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11604, 5094, NULL, NULL, NULL, '20173662', NULL, 'JOSE MIGUEL', NULL, 'CHIRINOS CACYA', NULL, 'jchirinoscac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11605, 5095, NULL, NULL, NULL, '20173675', NULL, 'MAYERLY MAGDA', NULL, 'CHIRINOS CACYA', NULL, 'mchirinosca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11606, 5096, NULL, NULL, NULL, '20144386', NULL, 'YEISON HUMBERTO', NULL, 'CHIRINOS CHAUPI', NULL, 'ychirinos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11607, 5097, NULL, NULL, NULL, '20164665', NULL, 'FABIOLA CRISTINA', NULL, 'CHIRINOS LLAMOCA', NULL, 'fchirinos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11608, 5098, NULL, NULL, NULL, '20164670', NULL, 'DINA', NULL, 'CHIRINOS MAMANI', NULL, 'dchirinosm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11609, 5099, NULL, NULL, NULL, '20161034', NULL, 'GHADELLY DANNA', NULL, 'CHIRINOS VIZCARRA', NULL, 'gchirinos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11610, 5100, NULL, NULL, NULL, '20151503', NULL, 'ROGELIO', NULL, 'CHITE LAZARTE', NULL, 'rchitel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11611, 5101, NULL, NULL, NULL, '20144362', NULL, 'MARGOT EMMA', NULL, 'CHITE YTALAQUE', NULL, 'mchitey@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11612, 5102, NULL, NULL, NULL, '20112107', NULL, 'LAURA GABRIELA', NULL, 'CHIUCHE ANCONEYRA', NULL, 'lchiuche@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11613, 5103, NULL, NULL, NULL, '20130832', NULL, 'JUAN JUNIOR', NULL, 'CHOCO JORGE', NULL, 'jchocoj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11614, 5104, NULL, NULL, NULL, '20151513', NULL, 'GLADYS MABEL', NULL, 'CHOQUE ARIAS', NULL, 'gchoquea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11615, 5105, NULL, NULL, NULL, '20114107', NULL, 'BENJI', NULL, 'CHOQUE COPARI', NULL, 'bchoqueco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11616, 5106, NULL, NULL, NULL, '20152660', NULL, 'ANNIE VERONICA', NULL, 'CHOQUE PANCA', NULL, 'achoquepa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11617, 5107, NULL, NULL, NULL, '20102370', NULL, 'ALEXANDRA STEPHANIE', NULL, 'CHOQUE QUISPE', NULL, 'achoquequ@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11618, 5108, NULL, NULL, NULL, '20120347', NULL, 'DIEGO', NULL, 'CHOQUE QUISPETUPAC', NULL, 'dchoqueq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11619, 5109, NULL, NULL, NULL, '20151488', NULL, 'GENOVEVA', NULL, 'CHOQUE SAICO', NULL, 'gchoques@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11620, 5110, NULL, NULL, NULL, '20174232', NULL, 'URSULA GUADALUPE', NULL, 'CHOQUE VASQUEZ', NULL, 'uchoquev@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11621, 5111, NULL, NULL, NULL, '20031554', NULL, 'VANIA JAMILET', NULL, 'CHOQUE VILLASANTE', NULL, 'vchoquevi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11622, 5112, NULL, NULL, NULL, '20133698', NULL, 'DAYAN SHIRLEY', NULL, 'CHOQUEHUANCA APAZA', NULL, 'dchoquehuancaap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11623, 5113, NULL, NULL, NULL, '20144377', NULL, 'JOAN JOSE', NULL, 'CHOQUEHUANCA CANDIA', NULL, 'jchoquehuancaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11624, 5114, NULL, NULL, NULL, '20164728', NULL, 'MARIA MAGDALENA', NULL, 'CHOQUEHUANCA CHAMPI', NULL, 'mchoquehuancach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11625, 5115, NULL, NULL, NULL, '20153169', NULL, 'LUIS ANGEL', NULL, 'CHOQUEHUANCA SALCEDO', NULL, 'lchoquehuancas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11626, 5116, NULL, NULL, NULL, '20173659', NULL, 'CRISTHIAN ALEX', NULL, 'CHOQUEHUANCA VELASQUEZ', NULL, 'cchoquehuancav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11627, 5117, NULL, NULL, NULL, '20153979', NULL, 'VANESA NAYSHA', NULL, 'CHOQUEPATA RAMOS', NULL, 'vchoquepatar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11628, 5118, NULL, NULL, NULL, '20144365', NULL, 'LISSET MADELEYNE', NULL, 'CHORA ALFARO', NULL, 'lchoraa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11629, 5119, NULL, NULL, NULL, '20143765', NULL, 'GLORIA OBDULIA', NULL, 'CHORA CORNEJO', NULL, 'gchora@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11630, 5120, NULL, NULL, NULL, '20144375', NULL, 'KARLA MILAGROS', NULL, 'CHUCTAYA HUANCA', NULL, 'kchuctaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11631, 5121, NULL, NULL, NULL, '20174230', NULL, 'ORTWIN RHUAYAN', NULL, 'CHUCTAYA ZUNIGA', NULL, 'ochuctayaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11632, 5122, NULL, NULL, NULL, '20151509', NULL, 'XIOMARA', NULL, 'CHUCUYA RAMOS', NULL, 'xchucuya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11633, 5123, NULL, NULL, NULL, '20174194', NULL, 'BRAYAN ALEXIS', NULL, 'CHUMA MAQQUERA', NULL, 'bchuma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11634, 5124, NULL, NULL, NULL, '20154684', NULL, 'LENY EDITH', NULL, 'CHUQUICANA QUISPE', NULL, 'lchuquicana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11635, 5125, NULL, NULL, NULL, '20154588', NULL, 'CLAUDIA ESTEFANY', NULL, 'CHURA JAVIER', NULL, 'cchuraj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11636, 5126, NULL, NULL, NULL, '20134235', NULL, 'MARYORI SOLEDAD', NULL, 'CHURA PAZ', NULL, 'mchura@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11637, 5127, NULL, NULL, NULL, '20143728', NULL, 'ROSMERY', NULL, 'CJURO PENA', NULL, 'rcjuro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11638, 5128, NULL, NULL, NULL, '20081800', NULL, 'LIZETH KATHERINE', NULL, 'COA HUAMANI', NULL, 'lcoah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11639, 5129, NULL, NULL, NULL, '20134208', NULL, 'MELIZA LUCERO', NULL, 'COA SUPO', NULL, 'mcoas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11640, 5130, NULL, NULL, NULL, '20096629', NULL, 'DIANA VANESSA', NULL, 'COA YQUISE', NULL, 'dcoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11641, 5131, NULL, NULL, NULL, '20141338', NULL, 'NATALY KEYLA', NULL, 'COAGUILA CRISTOBAL', NULL, 'ncoaguila@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11642, 5132, NULL, NULL, NULL, '20121342', NULL, 'ANDREA ALEJANDRA', NULL, 'COAGUILA ROJAS', NULL, 'acoaguilaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11643, 5133, NULL, NULL, NULL, '20124162', NULL, 'TAJANEE MERCEDES', NULL, 'COAQUIRA CHALCO', NULL, 'tcoaquira@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11644, 5134, NULL, NULL, NULL, '20164577', NULL, 'GINA MARIA', NULL, 'COAQUIRA MAMANI', NULL, 'gcoaquiram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11645, 5135, NULL, NULL, NULL, '20134342', NULL, 'LIZ DEYSI', NULL, 'COAQUIRA PEREZ', NULL, 'lcoaquirap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11646, 5136, NULL, NULL, NULL, '20154671', NULL, 'MARISOL NATALY', NULL, 'COAQUIRA TACO', NULL, 'mcoaquirat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11647, 5137, NULL, NULL, NULL, '20101667', NULL, 'DIEGO ALBERTO', NULL, 'COAQUIRA VILCARANI', NULL, 'dcoaquiravi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11648, 5138, NULL, NULL, NULL, '20112624', NULL, 'GABRIELA AMPARO', NULL, 'COAYLA VIZCARRA', NULL, 'gcoayla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11649, 5139, NULL, NULL, NULL, '20061441', NULL, 'ROSELLY MIREYA', NULL, 'COCHON PINTO', NULL, 'rcochon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11650, 5140, NULL, NULL, NULL, '20121331', NULL, 'MILTON', NULL, 'COILA BELIZARIO', NULL, 'mcoilab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11651, 5141, NULL, NULL, NULL, '20161030', NULL, 'LUZ AMANDA', NULL, 'COLQUE CASTILLA', NULL, 'lcolqueca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11652, 5142, NULL, NULL, NULL, '20102376', NULL, 'KEVING', NULL, 'COLQUE HUANACO', NULL, 'kcolque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11653, 5143, NULL, NULL, NULL, '20114114', NULL, 'KAREN ELIZABETH', NULL, 'COLQUE IQUIAPAZA', NULL, 'kcolquei@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11654, 5144, NULL, NULL, NULL, '20174286', NULL, 'JHOSELIN', NULL, 'COLQUE MACHACCA', NULL, 'jcolquema@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11655, 5145, NULL, NULL, NULL, '20134215', NULL, 'KATHERINE LIZBETH', NULL, 'CONCHACALLA DIAZ', NULL, 'kconchacalla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11656, 5146, NULL, NULL, NULL, '20141368', NULL, 'SASHENKA ANTUANET', NULL, 'CONDESO DIAZ', NULL, 'scondeso@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11657, 5147, NULL, NULL, NULL, '20171023', NULL, 'LUIS ANGEL', NULL, 'CONDORI ALCAZAR', NULL, 'lcondorial@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11658, 5148, NULL, NULL, NULL, '20132520', NULL, 'LIZETH', NULL, 'CONDORI CABANA', NULL, 'lcondorica@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11659, 5149, NULL, NULL, NULL, '20112170', NULL, 'SANDRA VERONICA', NULL, 'CONDORI CARI', NULL, 'scondoricar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11660, 5150, NULL, NULL, NULL, '20132436', NULL, 'RAUL FELIX', NULL, 'CONDORI CARPIO', NULL, 'rcondoricarp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11661, 5151, NULL, NULL, NULL, '20171003', NULL, 'EVELIN ANGELA', NULL, 'CONDORI CLAROS', NULL, 'econdoricl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11662, 5152, NULL, NULL, NULL, '20113789', NULL, 'AYNA MARUXA', NULL, 'CONDORI CONDORI', NULL, 'acondoricon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11663, 5153, NULL, NULL, NULL, '20122718', NULL, 'ROSARIO LISBETH', NULL, 'CONDORI CUEVA', NULL, 'rcondoricue@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11664, 5154, NULL, NULL, NULL, '20074469', NULL, 'GRACE LUERLIND', NULL, 'CONDORI GUZMAN', NULL, 'gcondorig@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11665, 5155, NULL, NULL, NULL, '20083683', NULL, 'ANA BEATRIZ', NULL, 'CONDORI HERNANDEZ', NULL, 'acondorihe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11666, 5156, NULL, NULL, NULL, '20131384', NULL, 'JANAI REYNA', NULL, 'CONDORI LOPEZ', NULL, 'jcondorilo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11667, 5157, NULL, NULL, NULL, '20163814', NULL, 'VALERIA THAIS', NULL, 'CONDORI MENDOZA', NULL, 'vcondorimen@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11668, 5158, NULL, NULL, NULL, '20141353', NULL, 'LISBETH', NULL, 'CONDORI POZO', NULL, 'lcondorip@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11669, 5159, NULL, NULL, NULL, '20133673', NULL, 'YOBANA RUTH', NULL, 'CONDORI QQUELLUYA', NULL, 'ycondoriq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11670, 5160, NULL, NULL, NULL, '20153975', NULL, 'KIMBERLY YULIANA', NULL, 'CONDORI QUINTO', NULL, 'kcondoriquin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11671, 5161, NULL, NULL, NULL, '20153986', NULL, 'DEISY ESMERALDA', NULL, 'CONDORI SUNI', NULL, 'dcondorisun@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11672, 5162, NULL, NULL, NULL, '20174325', NULL, 'ADOLFO JESUS', NULL, 'CONDORI TITO', NULL, 'acondoriti@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11673, 5163, NULL, NULL, NULL, '20112129', NULL, 'YORMAR MARTIN', NULL, 'CONDORI URBINA', NULL, 'ycondoriur@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11674, 5164, NULL, NULL, NULL, '20171008', NULL, 'JUAN FABRICIO', NULL, 'CONDORI VALENCIA', NULL, 'jcondorival@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11675, 5165, NULL, NULL, NULL, '20154592', NULL, 'VANESSA YSABEL', NULL, 'CONDORI VALENCIA', NULL, 'vcondoriva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11676, 5166, NULL, NULL, NULL, '20174219', NULL, 'LUIS RONALDO', NULL, 'CONTRERAS ATAMARI', NULL, 'lcontrerasa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11677, 5167, NULL, NULL, NULL, '20023653', NULL, 'AURELIO VICTOR', NULL, 'CONTRERAS VELIZ', NULL, 'acontrerasv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11678, 5168, NULL, NULL, NULL, '20100848', NULL, 'MARICIELO ANGELA', NULL, 'CORAHUA YANQUI', NULL, 'mcorahuay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11679, 5169, NULL, NULL, NULL, '20091408', NULL, 'MEDALY DIANI', NULL, 'CORDOVA BELTRAN', NULL, 'mcordovab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11680, 5170, NULL, NULL, NULL, '20154710', NULL, 'EMERSON DANIEL', NULL, 'CORDOVA BOZA', NULL, 'ecordova@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11681, 5171, NULL, NULL, NULL, '20133682', NULL, 'RENZO WASHINGTON', NULL, 'CORDOVA BUSTOS', NULL, 'rcordovab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11682, 5172, NULL, NULL, NULL, '20161057', NULL, 'MIRTHA RUBI', NULL, 'CORDOVA CARTAGENA', NULL, 'mcordovac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11683, 5173, NULL, NULL, NULL, '20134294', NULL, 'MARILUZ ROSMERY', NULL, 'CORDOVA RAMOS', NULL, 'mcordovar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11684, 5174, NULL, NULL, NULL, '20154629', NULL, 'MELITON JOSE', NULL, 'CORICAZA TACO', NULL, 'mcoricazat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11685, 5175, NULL, NULL, NULL, '20120862', NULL, 'RUDITH', NULL, 'CORIMANYA CONDO', NULL, 'rcorimanyaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11686, 5176, NULL, NULL, NULL, '20171000', NULL, 'SANTIAGO', NULL, 'CORNEJO ARRISUENO', NULL, 'scornejoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11687, 5177, NULL, NULL, NULL, '20114111', NULL, 'NIDIA DEYSI', NULL, 'CORNEJO BUSTINZA', NULL, 'ncornejobu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11688, 5178, NULL, NULL, NULL, '20124157', NULL, 'DILMAN EBER', NULL, 'CORNEJO CATASI', NULL, 'dcornejoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11689, 5179, NULL, NULL, NULL, '20172422', NULL, 'JOEL BRANDON', NULL, 'CORNEJO MANRIQUE', NULL, 'jcornejoma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11690, 5180, NULL, NULL, NULL, '20010570', NULL, 'GIANNY LISSET', NULL, 'CORNEJO QUISPE', NULL, 'gcornejoqu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11691, 5181, NULL, NULL, NULL, '20154661', NULL, 'ROBERTO CARLOS', NULL, 'CORRALES GUZMAN', NULL, 'rcorrales@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11692, 5182, NULL, NULL, NULL, '20104355', NULL, 'ANTHONY ENMANUEL', NULL, 'COSI SANDOVAL', NULL, 'acosi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11693, 5183, NULL, NULL, NULL, '20124158', NULL, 'EVONY SHEYLA', NULL, 'COSI ZEGARRA', NULL, 'ecosiz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11694, 5184, NULL, NULL, NULL, '20154615', NULL, 'DENILSON', NULL, 'CRUZ BALLON', NULL, 'dcruzb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11695, 5185, NULL, NULL, NULL, '20154691', NULL, 'JHONY ANDRE', NULL, 'CRUZ CARBAJAL', NULL, 'jcruzca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11696, 5186, NULL, NULL, NULL, '20152466', NULL, 'PERCY ANTONIO', NULL, 'CRUZ HANAMPA', NULL, 'pcruzh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11697, 5187, NULL, NULL, NULL, '20100419', NULL, 'HEIDY SHIOMARA', NULL, 'CRUZ HUACHACA', NULL, 'hcruzh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11698, 5188, NULL, NULL, NULL, '20161038', NULL, 'MARIA ALICIA', NULL, 'CRUZ HUSCCA', NULL, 'mcruzhus@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11699, 5189, NULL, NULL, NULL, '20173660', NULL, 'ELIZABETH ROCIO', NULL, 'CRUZ NEGRON', NULL, 'ecruzne@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11700, 5190, NULL, NULL, NULL, '20120698', NULL, 'YENSY LUCERO', NULL, 'CRUZ RAMIREZ', NULL, 'ycruzr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11701, 5191, NULL, NULL, NULL, '20164664', NULL, 'BRUNA AMANDA', NULL, 'CRUZADO MEDINA', NULL, 'bcruzado@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11702, 5192, NULL, NULL, NULL, '20120820', NULL, 'MARIA DEL PILAR', NULL, 'CUBA CASO', NULL, 'mcubaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11703, 5193, NULL, NULL, NULL, '20153166', NULL, 'KARLO MAX', NULL, 'CUCHO VILCA', NULL, 'kcucho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11704, 5194, NULL, NULL, NULL, '20151500', NULL, 'MELANIE DIANA', NULL, 'CUEVA TICONA', NULL, 'mcueva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11705, 5195, NULL, NULL, NULL, '20174261', NULL, 'ALDO RANIERO', NULL, 'CUEVA TORRES', NULL, 'acuevat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11706, 5196, NULL, NULL, NULL, '20113616', NULL, 'HEYDI MARY', NULL, 'CUEVA YANCAPALLO', NULL, 'hcuevay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11707, 5197, NULL, NULL, NULL, '20174258', NULL, 'HILDA ELIZABETH', NULL, 'CUEVAS MAMANI', NULL, 'hcuevasm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11708, 5198, NULL, NULL, NULL, '20143738', NULL, 'LIZ ERIKA', NULL, 'CUITO CAHUANA', NULL, 'lcuitoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11709, 5199, NULL, NULL, NULL, '20112166', NULL, 'ZORAIDA DORIS', NULL, 'CUNO CHAMBI', NULL, 'zcunoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11710, 5200, NULL, NULL, NULL, '20141340', NULL, 'EDITH ALESSANDRA', NULL, 'CUPI SANCA', NULL, 'ecupi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11711, 5201, NULL, NULL, NULL, '20172446', NULL, 'SOPHIA ANGELA', NULL, 'CUSIRRAMOS PONCE DE LEON', NULL, 'scusirramosp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11712, 5202, NULL, NULL, NULL, '20174284', NULL, 'RONALDO', NULL, 'CUTI ATAJO', NULL, 'rcutia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11713, 5203, NULL, NULL, NULL, '20144344', NULL, 'CRISTHIAN ALVARO', NULL, 'CUTI TACO', NULL, 'ccutit@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11714, 5204, NULL, NULL, NULL, '20164601', NULL, 'JESSICA KATTY', NULL, 'CUTIPA CHECCA', NULL, 'jcutipach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11715, 5205, NULL, NULL, NULL, '20134310', NULL, 'RICHARD', NULL, 'CUTIPA CHECCA', NULL, 'rcutipach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11716, 5206, NULL, NULL, NULL, '20164634', NULL, 'MERY LUZ', NULL, 'DAVILA BELLIDO', NULL, 'mdavilabe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11717, 5207, NULL, NULL, NULL, '20134347', NULL, 'JOHN RODRIGO', NULL, 'DAVILA ESTEFANERO', NULL, 'jdavilae@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11718, 5208, NULL, NULL, NULL, '20144384', NULL, 'VANESSA MARLENE', NULL, 'DAVILA MENDOZA', NULL, 'vdavila@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11719, 5209, NULL, NULL, NULL, '20164645', NULL, 'DARIAN SANTIAGO', NULL, 'DAZA ALARCON', NULL, 'ddazaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11720, 5210, NULL, NULL, NULL, '20141344', NULL, 'JUAN DAVID', NULL, 'DE LA CRUZ ARHUIRE', NULL, 'jdelacruza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11721, 5211, NULL, NULL, NULL, '20133665', NULL, 'MERILIN THALIA', NULL, 'DE LA CRUZ CRUZ', NULL, 'mdelacruzcr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11722, 5212, NULL, NULL, NULL, '20134242', NULL, 'BRENO ALBERTO', NULL, 'DE LA CRUZ ESPINAL', NULL, 'bdelacruze@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11723, 5213, NULL, NULL, NULL, '20122717', NULL, 'JUNIOR GERMAN', NULL, 'DE LA CRUZ GALLEGOS', NULL, 'jdelacruzga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11724, 5214, NULL, NULL, NULL, '20134319', NULL, 'MORELIA JACKELINE', NULL, 'DE LA CRUZ MENDOZA', NULL, 'mdelacruzm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11725, 5215, NULL, NULL, NULL, '20102770', NULL, 'JESUS', NULL, 'DE LA CRUZ POSTIGO', NULL, 'jdelacruzp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11726, 5216, NULL, NULL, NULL, '20131388', NULL, 'CINTHIA LIZETH', NULL, 'DEL CARPIO APAZA', NULL, 'cdelcarpioap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11727, 5217, NULL, NULL, NULL, '20153951', NULL, 'JOSE', NULL, 'DEL CARPIO PUMAHUANCA', NULL, 'jdelcarpio@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11728, 5218, NULL, NULL, NULL, '20164617', NULL, 'DANIELA PATRICIA', NULL, 'DEL CARPIO ZUNIGA', NULL, 'ddelcarpio@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11729, 5219, NULL, NULL, NULL, '20104225', NULL, 'YILLMAR CESAR', NULL, 'DELGADO ARIAS', NULL, 'ydelgadoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11730, 5220, NULL, NULL, NULL, '20084418', NULL, 'GABRIEL', NULL, 'DELGADO CHOCO', NULL, 'gdelgadoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11731, 5221, NULL, NULL, NULL, '20164731', NULL, 'ERICK JOSEPH', NULL, 'DELGADO HUERTA', NULL, 'edelgado@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11732, 5222, NULL, NULL, NULL, '20164685', NULL, 'DARIEN DORIAN', NULL, 'DELGADO NINA', NULL, 'ddelgadon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11733, 5223, NULL, NULL, NULL, '20141356', NULL, 'GARLET LALESKA', NULL, 'DELGADO RAMOS', NULL, 'gdelgador@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11734, 5224, NULL, NULL, NULL, '20132484', NULL, 'ANTUANETH YENY', NULL, 'DENOS MENACHO', NULL, 'adenos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11735, 5225, NULL, NULL, NULL, '20090944', NULL, 'CHRISTIAN JESUS', NULL, 'DEZA MAMANI', NULL, 'cdezama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11736, 5226, NULL, NULL, NULL, '20174188', NULL, 'LESLIE MAYLEE', NULL, 'DEZA TORRES', NULL, 'ldeza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11737, 5227, NULL, NULL, NULL, '20171025', NULL, 'DIEGO ADRIAN', NULL, 'DIANDERAS TORRES', NULL, 'ddianderast@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(11738, 5228, NULL, NULL, NULL, '20171017', NULL, 'CARMEN ROSA', NULL, 'DIAZ AYAMAMANI', NULL, 'cdiazay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11739, 5229, NULL, NULL, NULL, '20151522', NULL, 'CARLO FABRICIO', NULL, 'DIAZ DIAZ', NULL, 'cdiazdia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11740, 5230, NULL, NULL, NULL, '20133663', NULL, 'JUNETT ROMINA', NULL, 'DIAZ PATINO', NULL, 'jdiazpat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11741, 5231, NULL, NULL, NULL, '20174249', NULL, 'LEONARDO MANUEL', NULL, 'DIAZ REVILLA', NULL, 'ldiazr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11742, 5232, NULL, NULL, NULL, '20134212', NULL, 'MIGUEL RODRIGO', NULL, 'DIAZ RODRIGUEZ', NULL, 'mdiazr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11743, 5233, NULL, NULL, NULL, '20174189', NULL, 'ANDREA FIORELLA', NULL, 'DIAZ TORRES', NULL, 'adiazto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11744, 5234, NULL, NULL, NULL, '20174301', NULL, 'LUCIA CAROLINA', NULL, 'DONGO SANTOS', NULL, 'ldongos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11745, 5235, NULL, NULL, NULL, '20154708', NULL, 'MARIE JANE', NULL, 'DOUMENZ VILLALOBOS', NULL, 'mdoumenz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11746, 5236, NULL, NULL, NULL, '20154670', NULL, 'LUIS FERNANDO', NULL, 'ENCINAS ALVARO', NULL, 'lencinas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11747, 5237, NULL, NULL, NULL, '20141357', NULL, 'RODRIGO', NULL, 'ESCOBEDO NUNEZ', NULL, 'rescobedo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11748, 5238, NULL, NULL, NULL, '20173664', NULL, 'DIANA', NULL, 'ESPINAL CAHUANA', NULL, 'despinal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11749, 5239, NULL, NULL, NULL, '20154595', NULL, 'ADRIANA LUISA', NULL, 'ESPINAL HUACALLO', NULL, 'aespinalh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11750, 5240, NULL, NULL, NULL, '20163802', NULL, 'JUAN DIEGO', NULL, 'ESPINO HERRERA', NULL, 'jespinoh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11751, 5241, NULL, NULL, NULL, '20144393', NULL, 'IVAN CIRO', NULL, 'ESPINOZA FLORES', NULL, 'iespinoza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11752, 5242, NULL, NULL, NULL, '20143766', NULL, 'YENNIFFER FATIMA', NULL, 'ESPINOZA FLORES', NULL, 'yespinozaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11753, 5243, NULL, NULL, NULL, '20171020', NULL, 'JOSE RODOLFO', NULL, 'ESPINOZA GUEVARA', NULL, 'jespinozag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11754, 5244, NULL, NULL, NULL, '20134204', NULL, 'PAULO CESAR', NULL, 'ESPINOZA MOLINA', NULL, 'pespinoza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11755, 5245, NULL, NULL, NULL, '20134237', NULL, 'ALISON LUCERO', NULL, 'ESPINOZA VALENCIA', NULL, 'aespinoza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11756, 5246, NULL, NULL, NULL, '20131354', NULL, 'NOEMI', NULL, 'ESPIRILLA CAMARGO', NULL, 'nespirilla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11757, 5247, NULL, NULL, NULL, '20144304', NULL, 'LAURA RAQUEL', NULL, 'ESQUIVEL MOSCOSO', NULL, 'lesquivel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11758, 5248, NULL, NULL, NULL, '20051202', NULL, 'MARIA GABRIELA', NULL, 'ESQUIVEL PANUERA', NULL, 'mesquivelp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11759, 5249, NULL, NULL, NULL, '20141358', NULL, 'ALEX RODRIGO', NULL, 'ESTANO QUISPE', NULL, 'aestano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11760, 5250, NULL, NULL, NULL, '20164621', NULL, 'BRAYHAN GONZALO', NULL, 'ESTEBAN QUISPE', NULL, 'besteban@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11761, 5251, NULL, NULL, NULL, '20134343', NULL, 'ALICIA GRACIELA', NULL, 'ESTRADA CCAHUANA', NULL, 'aestradac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11762, 5252, NULL, NULL, NULL, '20143731', NULL, 'GEANNY LIZBETH', NULL, 'FALCON LUQUE', NULL, 'gfalcon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11763, 5253, NULL, NULL, NULL, '20154619', NULL, 'WENDY ESTHEFANY', NULL, 'FAYLOC CHOQUE', NULL, 'wfayloc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11764, 5254, NULL, NULL, NULL, '20030192', NULL, 'ARLETTE KARINA', NULL, 'FERNANDEZ CASTRO', NULL, 'afernandezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11765, 5255, NULL, NULL, NULL, '20143758', NULL, 'ROXANA', NULL, 'FERNANDEZ CONDO', NULL, 'rfernandezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11766, 5256, NULL, NULL, NULL, '20144419', NULL, 'JOSE ARTURO', NULL, 'FERNANDEZ DEMANUEL', NULL, 'jfernandezd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11767, 5257, NULL, NULL, NULL, '20074207', NULL, 'CARLOS FERNANDO', NULL, 'FERNANDEZ HERRERA', NULL, 'cfernandezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11768, 5258, NULL, NULL, NULL, '20154564', NULL, 'RUTH CINDY', NULL, 'FERNANDEZ PENA', NULL, 'rfernandezp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11769, 5259, NULL, NULL, NULL, '20174322', NULL, 'DAMARIS DANIELA', NULL, 'FERNANDEZ VALLEJOS', NULL, 'dfernandezv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11770, 5260, NULL, NULL, NULL, '20081761', NULL, 'FELY RUTH', NULL, 'FIGUEROA HUANQUI', NULL, 'ffigueroah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11771, 5261, NULL, NULL, NULL, '20172432', NULL, 'JEAN CARLOS GABRIEL', NULL, 'FLORES APARICIO', NULL, 'jfloresap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11772, 5262, NULL, NULL, NULL, '20084432', NULL, 'ALBERTO SEBASTIAN', NULL, 'FLORES AYQUIPA', NULL, 'afloresay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11773, 5263, NULL, NULL, NULL, '20141351', NULL, 'CARLOS EDUARDO', NULL, 'FLORES BENAVENTE', NULL, 'cflores@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11774, 5264, NULL, NULL, NULL, '20072539', NULL, 'RAMIRO EULOGIO', NULL, 'FLORES BENITO', NULL, 'rfloresb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11775, 5265, NULL, NULL, NULL, '20122720', NULL, 'KIARA NAVENKA', NULL, 'FLORES CAJAHUACCHA', NULL, 'kfloresca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11776, 5266, NULL, NULL, NULL, '20164599', NULL, 'CARLOS FABRICIO', NULL, 'FLORES FERNANDEZ', NULL, 'cfloresf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11777, 5267, NULL, NULL, NULL, '20134283', NULL, 'MAGNOLIA MERCEDES', NULL, 'FLORES FLORES', NULL, 'mfloresfl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11778, 5268, NULL, NULL, NULL, '20174267', NULL, 'YADIRA YAMILI', NULL, 'FLORES HEREDIA', NULL, 'yfloreshe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11779, 5269, NULL, NULL, NULL, '20154652', NULL, 'YECENIA IMACULADA', NULL, 'FLORES HUACHO', NULL, 'yfloreshu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11780, 5270, NULL, NULL, NULL, '20164663', NULL, 'SHIRLEY MAYRA', NULL, 'FLORES HUAYTA', NULL, 'sfloresh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11781, 5271, NULL, NULL, NULL, '20133664', NULL, 'BRIGITTE', NULL, 'FLORES JACHA', NULL, 'bfloresj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11782, 5272, NULL, NULL, NULL, '20102225', NULL, 'LUIS ALBERTO', NULL, 'FLORES JIHUANA', NULL, 'lfloresji@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11783, 5273, NULL, NULL, NULL, '20164737', NULL, 'PAUL SERGIO', NULL, 'FLORES LARICO', NULL, 'pfloresla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11784, 5274, NULL, NULL, NULL, '20133674', NULL, 'ALEXIS HUMBERTO', NULL, 'FLORES LOPE', NULL, 'afloreslope@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11785, 5275, NULL, NULL, NULL, '20163965', NULL, 'NATALY YANIZ', NULL, 'FLORES MAMANI', NULL, 'nfloresmam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11786, 5276, NULL, NULL, NULL, '20063407', NULL, 'RENALDO', NULL, 'FLORES MARTINEZ', NULL, 'rfloresma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11787, 5277, NULL, NULL, NULL, '20144303', NULL, 'HUBERTH ALFREDO', NULL, 'FLORES MAYTA', NULL, 'hfloresm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11788, 5278, NULL, NULL, NULL, '20174216', NULL, 'ANA BENITA', NULL, 'FLORES MOLINA', NULL, 'afloresmol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11789, 5279, NULL, NULL, NULL, '20153977', NULL, 'DAYANA MARIA FERNANDA', NULL, 'FLORES MOROCO', NULL, 'dfloresmor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11790, 5280, NULL, NULL, NULL, '20020813', NULL, 'JORGE ARTURO', NULL, 'FLORES PAZ', NULL, 'jfloresp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11791, 5281, NULL, NULL, NULL, '20131366', NULL, 'LAYDY KARINA', NULL, 'FLORES PEREZ', NULL, 'lflorespe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11792, 5282, NULL, NULL, NULL, '20164644', NULL, 'DIANA JULIANA', NULL, 'FLORES QUISPE', NULL, 'dfloresquis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11793, 5283, NULL, NULL, NULL, '20124069', NULL, 'HERNAN SAMUEL', NULL, 'FLORES RAMOS', NULL, 'hfloresram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11794, 5284, NULL, NULL, NULL, '20112612', NULL, 'ZULEYCA MONICA', NULL, 'FLORES SARMIENTO', NULL, 'zfloress@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11795, 5285, NULL, NULL, NULL, '20144404', NULL, 'JOSE LUIS', NULL, 'FLORES SUBELETA', NULL, 'jfloressu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11796, 5286, NULL, NULL, NULL, '20164607', NULL, 'LIZBETH KATHERINE', NULL, 'FLORES TICONA', NULL, 'lfloresti@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11797, 5287, NULL, NULL, NULL, '20124143', NULL, 'DAYANA STEPHANIE', NULL, 'FLORES TORRES', NULL, 'dfloresto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11798, 5288, NULL, NULL, NULL, '20154594', NULL, 'KAREN MARGOT', NULL, 'FLORES VALDEZ', NULL, 'kfloresval@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11799, 5289, NULL, NULL, NULL, '20154703', NULL, 'MADYURIT MARIBEL', NULL, 'FLORES VALDEZ', NULL, 'mfloresv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11800, 5290, NULL, NULL, NULL, '20171007', NULL, 'CARLOS DAVID', NULL, 'FLOREZ RAMIREZ', NULL, 'cflorez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11801, 5291, NULL, NULL, NULL, '20131341', NULL, 'DANNY JESUS', NULL, 'FRANCO GUTIERREZ', NULL, 'dfrancog@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11802, 5292, NULL, NULL, NULL, '20164659', NULL, 'JOSE ARMANDO', NULL, 'FUENTES VILCA', NULL, 'jfuentes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11803, 5293, NULL, NULL, NULL, '20072567', NULL, 'EDEN ARSENIO', NULL, 'FUNES HUIZACAYNA', NULL, 'efunes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11804, 5294, NULL, NULL, NULL, '20123824', NULL, 'DONOWAN ROGER', NULL, 'GALARZA VILCA', NULL, 'dgalarzav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11805, 5295, NULL, NULL, NULL, '20114135', NULL, 'GEREMY JUVENAL', NULL, 'GALDOS ARENAS', NULL, 'ggaldosa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11806, 5296, NULL, NULL, NULL, '20154659', NULL, 'OSCAR ALBERTO', NULL, 'GALLARDO ANDIA', NULL, 'ogallardo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11807, 5297, NULL, NULL, NULL, '20154696', NULL, 'ANYELA NOHELY', NULL, 'GALLEGOS CARRILLO', NULL, 'agallegosc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11808, 5298, NULL, NULL, NULL, '20134263', NULL, 'WENDY PAOLA', NULL, 'GALLEGOS SIU', NULL, 'wgallegos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11809, 5299, NULL, NULL, NULL, '20174240', NULL, 'ISIS ARELIS', NULL, 'GAMA GOMEZ', NULL, 'igamag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11810, 5300, NULL, NULL, NULL, '20153956', NULL, 'JHOMAR RICARDO', NULL, 'GAMARRA AROCENA', NULL, 'jgamarraa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11811, 5301, NULL, NULL, NULL, '20154662', NULL, 'EVELYN MEYLIN', NULL, 'GAMERO QUISPE', NULL, 'egamero@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11812, 5302, NULL, NULL, NULL, '20144316', NULL, 'MIGUEL ANGEL', NULL, 'GAONA ANCO', NULL, 'mgaona@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11813, 5303, NULL, NULL, NULL, '20114179', NULL, 'JHORDANW SMITH', NULL, 'GARATE CACERES', NULL, 'jgaratec@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11814, 5304, NULL, NULL, NULL, '20133671', NULL, 'PAOLA LORENA', NULL, 'GARAY GUTIERREZ', NULL, 'pgarayg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11815, 5305, NULL, NULL, NULL, '20174214', NULL, 'JENYFER KATHERINE', NULL, 'GARCIA ACHIRE', NULL, 'jgarciaac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11816, 5306, NULL, NULL, NULL, '20124128', NULL, 'ANDRE RICARDO', NULL, 'GARCIA CATERIANO', NULL, 'agarciacat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11817, 5307, NULL, NULL, NULL, '20062738', NULL, 'BENJI MIGUEL', NULL, 'GARCIA CATERIANO', NULL, 'bgarciac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11818, 5308, NULL, NULL, NULL, '20098274', NULL, 'OSCAR ORLANDO', NULL, 'GARCIA DELGADO', NULL, 'ogarciad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11819, 5309, NULL, NULL, NULL, '20134253', NULL, 'WALTER HUGO', NULL, 'GARCIA DELGADO', NULL, 'wgarcia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11820, 5310, NULL, NULL, NULL, '20154581', NULL, 'CATHERINE KIMBERLEY', NULL, 'GARCIA GALLARDO', NULL, 'cgarciaga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11821, 5311, NULL, NULL, NULL, '20174238', NULL, 'ALESSANDRO MARTIN', NULL, 'GARCIA GUTIERREZ', NULL, 'agarciagu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11822, 5312, NULL, NULL, NULL, '20144426', NULL, 'KATHERINE', NULL, 'GARCIA GUTIERREZ', NULL, 'kgarciagu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11823, 5313, NULL, NULL, NULL, '20151501', NULL, 'LUIS FERNANDO', NULL, 'GARCIA IQUENO', NULL, 'lgarciai@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11824, 5314, NULL, NULL, NULL, '20154601', NULL, 'GERARDO HUMBERTO', NULL, 'GARCIA LLERENA', NULL, 'ggarcial@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11825, 5315, NULL, NULL, NULL, '20114215', NULL, 'GERSON ALEJANDRO', NULL, 'GARCIA MANRIQUE', NULL, 'ggarciama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11826, 5316, NULL, NULL, NULL, '20102717', NULL, 'ROXANA', NULL, 'GARCIA QUELLHUA', NULL, 'rgarciaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11827, 5317, NULL, NULL, NULL, '20172430', NULL, 'ALEJANDRA YADHIRA', NULL, 'GARCIA UCANAY', NULL, 'agarciauc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11828, 5318, NULL, NULL, NULL, '20085171', NULL, 'ISABEL JOHANNY', NULL, 'GAYOZO HUAMANI', NULL, 'igayozo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11829, 5319, NULL, NULL, NULL, '20164587', NULL, 'FREDDY JOSEPH', NULL, 'GIL ORUE', NULL, 'fgilo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11830, 5320, NULL, NULL, NULL, '20174272', NULL, 'LUIS ALBERTO', NULL, 'GOMEZ CCASANI', NULL, 'lgomezcc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11831, 5321, NULL, NULL, NULL, '20174239', NULL, 'NOE HERMOGENES', NULL, 'GOMEZ DE LA CRUZ', NULL, 'ngomezd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11832, 5322, NULL, NULL, NULL, '20144294', NULL, 'LUZ FIORELLA', NULL, 'GOMEZ FLORES', NULL, 'lgomezf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11833, 5323, NULL, NULL, NULL, '20151515', NULL, 'JESUS REYNERIO', NULL, 'GOMEZ MENDOZA', NULL, 'jgomezm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11834, 5324, NULL, NULL, NULL, '20164708', NULL, 'JAIME GONZALO', NULL, 'GOMEZ PACCORI', NULL, 'jgomezpa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11835, 5325, NULL, NULL, NULL, '20012716', NULL, 'CANDY FIORELLA', NULL, 'GOMEZ POSTIGO', NULL, 'cgomezp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11836, 5326, NULL, NULL, NULL, '20134255', NULL, 'ANDREY', NULL, 'GONZALES HINOJOSA', NULL, 'agonzalesh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11837, 5327, NULL, NULL, NULL, '20174200', NULL, 'VILDER DIOGENES', NULL, 'GONZALES MEDINA', NULL, 'vgonzalesme@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11838, 5328, NULL, NULL, NULL, '20134228', NULL, 'YORMAN', NULL, 'GONZALES MONTOYA', NULL, 'ygonzalesmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11839, 5329, NULL, NULL, NULL, '20154580', NULL, 'IRMA JIMENA', NULL, 'GONZALES VARGAS', NULL, 'igonzalesv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11840, 5330, NULL, NULL, NULL, '20143767', NULL, 'LADY ROSA', NULL, 'GOYZUETA ALPACA', NULL, 'lgoyzuetaal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11841, 5331, NULL, NULL, NULL, '20143733', NULL, 'EVA CLEOFE', NULL, 'GRANADA HILARES', NULL, 'egranada@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11842, 5332, NULL, NULL, NULL, '20154605', NULL, 'GABRIELA EDITH', NULL, 'GRANDA GRADOS', NULL, 'ggrandag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11843, 5333, NULL, NULL, NULL, '20134311', NULL, 'ELARD JESUS', NULL, 'GRANDA PACHECO', NULL, 'egrandap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11844, 5334, NULL, NULL, NULL, '20134362', NULL, 'MARIBEL', NULL, 'GRANDE MOROCO', NULL, 'mgrandem@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11845, 5335, NULL, NULL, NULL, '20154637', NULL, 'YOLANDA ISABEL', NULL, 'GUERRA CHUQUISPUMA', NULL, 'yguerrac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11846, 5336, NULL, NULL, NULL, '20001271', NULL, 'RICHARD ANTONIO', NULL, 'GUERRA SILVA', NULL, 'rguerras@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11847, 5337, NULL, NULL, NULL, '20163797', NULL, 'MARILIA DEL CARMEN', NULL, 'GUERRERO FLORES', NULL, 'mguerrerof@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11848, 5338, NULL, NULL, NULL, '20154656', NULL, 'BETTY ROCIO', NULL, 'GUILLEN MAMANI', NULL, 'bguillenm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11849, 5339, NULL, NULL, NULL, '20164580', NULL, 'BRIAM MANUEL', NULL, 'GUITTON VILCA', NULL, 'bguitton@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11850, 5340, NULL, NULL, NULL, '20010868', NULL, 'YANINA LIZ', NULL, 'GUTIERREZ ADRIAZOLA', NULL, 'ygutierrezad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11851, 5341, NULL, NULL, NULL, '20112146', NULL, 'MANUEL', NULL, 'GUTIERREZ APAZA', NULL, 'mgutierrezap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11852, 5342, NULL, NULL, NULL, '20163789', NULL, 'FRANCIS AMERICA', NULL, 'GUTIERREZ HANCCO', NULL, 'fgutierrezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11853, 5343, NULL, NULL, NULL, '20144312', NULL, 'PABLO LEOPOLDO', NULL, 'GUTIERREZ IDME', NULL, 'pgutierrezi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11854, 5344, NULL, NULL, NULL, '20153982', NULL, 'YENI MARGOT', NULL, 'GUTIERREZ MAMANI', NULL, 'ygutierrezm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11855, 5345, NULL, NULL, NULL, '20144422', NULL, 'ROMMEL GABRIEL', NULL, 'GUTIERREZ MAQUE', NULL, 'rgutierrezma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11856, 5346, NULL, NULL, NULL, '20091776', NULL, 'VICTOR RAUL', NULL, 'GUTIERREZ MAYTA', NULL, 'vgutierrezma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11857, 5347, NULL, NULL, NULL, '20043281', NULL, 'JOHN LARRY', NULL, 'GUTIERREZ PAUCA', NULL, 'jgutierrezp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11858, 5348, NULL, NULL, NULL, '20085165', NULL, 'MARIO ANTONIO', NULL, 'GUTIERREZ PERALTA', NULL, 'mgutierrezpe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11859, 5349, NULL, NULL, NULL, '20154693', NULL, 'RAUL ALEJANDRO', NULL, 'GUTIERREZ QUISPE', NULL, 'rgutierrezqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11860, 5350, NULL, NULL, NULL, '20142417', NULL, 'ALDO MARCOS', NULL, 'GUTIERREZ TACCA', NULL, 'agutierrezt@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11861, 5351, NULL, NULL, NULL, '20123004', NULL, 'GERALDINE LISBET', NULL, 'GUTIERREZ TICONA', NULL, 'ggutierrezti@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11862, 5352, NULL, NULL, NULL, '20161045', NULL, 'ROCIO', NULL, 'GUTIERREZ TOLA', NULL, 'rgutierreztol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11863, 5353, NULL, NULL, NULL, '20154706', NULL, 'WILI ARMANDO', NULL, 'GUTIERREZ TURPO', NULL, 'wgutierrezt@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11864, 5354, NULL, NULL, NULL, '20085222', NULL, 'SHIRLEY JEANETH', NULL, 'GUTIERREZ VELARDE', NULL, 'sgutierrezve@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11865, 5355, NULL, NULL, NULL, '20134331', NULL, 'WILLIAMS JHOSEP', NULL, 'GUTIERREZ VELARDE', NULL, 'wgutierrezve@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11866, 5356, NULL, NULL, NULL, '20163813', NULL, 'DANFEER MANUEL', NULL, 'GUTIERREZ ZAVALA', NULL, 'dgutierrezz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11867, 5357, NULL, NULL, NULL, '20163773', NULL, 'NICANOR', NULL, 'GUZMAN ALCCA', NULL, 'nguzmana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11868, 5358, NULL, NULL, NULL, '20083595', NULL, 'NOHELIA MIGEDITH', NULL, 'GUZMAN YDME', NULL, 'nguzmany@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11869, 5359, NULL, NULL, NULL, '20174259', NULL, 'DANIELA', NULL, 'HACHA MOLLO', NULL, 'dhacha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11870, 5360, NULL, NULL, NULL, '20154700', NULL, 'LIZETT MAYESLY', NULL, 'HANCCO ASQUI', NULL, 'lhanccoas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11871, 5361, NULL, NULL, NULL, '20172421', NULL, 'PATRICIA PILAR', NULL, 'HANCCO ASTETE', NULL, 'phanccoas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11872, 5362, NULL, NULL, NULL, '20173663', NULL, 'YANETT FABIOLA', NULL, 'HANCCO CHANI', NULL, 'yhanccocha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11873, 5363, NULL, NULL, NULL, '20163791', NULL, 'AYDEE NANCY', NULL, 'HANCCO FLORES', NULL, 'ahanccof@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11874, 5364, NULL, NULL, NULL, '20154686', NULL, 'STEPHANIE PAOLA', NULL, 'HANCCO SUMERINDE', NULL, 'shanccos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11875, 5365, NULL, NULL, NULL, '20131364', NULL, 'MARCIA CAMILA', NULL, 'HEREDIA PUMA', NULL, 'mherediap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11876, 5366, NULL, NULL, NULL, '20104466', NULL, 'EDWIN MIJAEL', NULL, 'HERENCIA MAMANI', NULL, 'eherencia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11877, 5367, NULL, NULL, NULL, '20174313', NULL, 'NICOLE VANESSA', NULL, 'HERRERA HERRERA', NULL, 'nherrerah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11878, 5368, NULL, NULL, NULL, '20134297', NULL, 'JORGE ALONSO', NULL, 'HERRERA MELGAR', NULL, 'jherreram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11879, 5369, NULL, NULL, NULL, '20141355', NULL, 'CARMEN ROXANA', NULL, 'HERRERA MORAN', NULL, 'cherreram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11880, 5370, NULL, NULL, NULL, '20144321', NULL, 'RUBY RUTH', NULL, 'HERRERA NAVENTA', NULL, 'rherrera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11881, 5371, NULL, NULL, NULL, '20173668', NULL, 'MARIA ANGELA', NULL, 'HERRERA PORTUGAL', NULL, 'mherrerapo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11882, 5372, NULL, NULL, NULL, '20123369', NULL, 'MELANNY AYLIN', NULL, 'HERRERA RIVERA', NULL, 'mherrerar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11883, 5373, NULL, NULL, NULL, '20174209', NULL, 'CRISTHIAN FROILAN', NULL, 'HILARES JIMENEZ', NULL, 'chilares@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11884, 5374, NULL, NULL, NULL, '20162146', NULL, 'ALEJANDRINA MARGOT', NULL, 'HILARIO HUACCHA', NULL, 'ahilario@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11885, 5375, NULL, NULL, NULL, '20141375', NULL, 'JORGE ISAAC', NULL, 'HILASACA MACEDO', NULL, 'jhilasacam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11886, 5376, NULL, NULL, NULL, '20154678', NULL, 'KEYKO ALEJANDRA', NULL, 'HINOJOSA FLORES', NULL, 'khinojosa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11887, 5377, NULL, NULL, NULL, '20174235', NULL, 'CAMILA MILAGROS', NULL, 'HINOJOSA TORRES', NULL, 'chinojosa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11888, 5378, NULL, NULL, NULL, '20164573', NULL, 'CARLOS KENYI', NULL, 'HITO PRADO', NULL, 'chito@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11889, 5379, NULL, NULL, NULL, '20153950', NULL, 'CLAUDIA CRISTINA', NULL, 'HOLGUINO CHULLO', NULL, 'cholguino@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11890, 5380, NULL, NULL, NULL, '20163778', NULL, 'YOSELIN LISETH', NULL, 'HOLGUINO CUTIPA', NULL, 'yholguinoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11891, 5381, NULL, NULL, NULL, '20074526', NULL, 'VERONICA ROCIO', NULL, 'HUACAN GARCIA', NULL, 'vhuacan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11892, 5382, NULL, NULL, NULL, '19981375', NULL, 'FERNANDO FRANCISCO', NULL, 'HUACARPUMA AVENDANO', NULL, 'fhuacarpuma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11893, 5383, NULL, NULL, NULL, '20161037', NULL, 'WILLIAM ANDERSON', NULL, 'HUACASI ALVAREZ', NULL, 'whuacasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11894, 5384, NULL, NULL, NULL, '20074527', NULL, 'JESUS HENRY', NULL, 'HUACASI SANTA CRUZ', NULL, 'jhuacasis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11895, 5385, NULL, NULL, NULL, '20164726', NULL, 'MILAGROS DEYSI', NULL, 'HUACHO HUAMPO', NULL, 'mhuacho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11896, 5386, NULL, NULL, NULL, '20172451', NULL, 'MIRIAM', NULL, 'HUACHO QUISPE', NULL, 'mhuachoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11897, 5387, NULL, NULL, NULL, '20154689', NULL, 'KATHERYNE PILAR', NULL, 'HUAHUACHAMBI CONDORI', NULL, 'khuahuachambi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11898, 5388, NULL, NULL, NULL, '20134247', NULL, 'MARILUZ MARLENY', NULL, 'HUALLANCA OCHOA', NULL, 'mhuallanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11899, 5389, NULL, NULL, NULL, '20171013', NULL, 'MIGUEL ANGEL', NULL, 'HUALLPA ARONI', NULL, 'mhuallpaar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11900, 5390, NULL, NULL, NULL, '20060209', NULL, 'MARIA DEL CARMEN', NULL, 'HUALLPA FLORES', NULL, 'mhuallpaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11901, 5391, NULL, NULL, NULL, '20172427', NULL, 'JACKELIN ALEXANDRA', NULL, 'HUALLPA SALAS', NULL, 'jhuallpasa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11902, 5392, NULL, NULL, NULL, '20164639', NULL, 'FREDDY SANTIAGO JUNIOR', NULL, 'HUALLULLO MAYANGA', NULL, 'fhuallullo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11903, 5393, NULL, NULL, NULL, '20162148', NULL, 'BERLY FABRICIO', NULL, 'HUAMAN ARI', NULL, 'bhuamana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11904, 5394, NULL, NULL, NULL, '20174221', NULL, 'ANALY CHARO', NULL, 'HUAMAN CHAMBI', NULL, 'ahuamancha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11905, 5395, NULL, NULL, NULL, '20134199', NULL, 'KATHERIN ISELA', NULL, 'HUAMAN CHAUCA', NULL, 'khuamanch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11906, 5396, NULL, NULL, NULL, '20131377', NULL, 'LENIN FRANCISCO', NULL, 'HUAMAN ROSAS', NULL, 'lhuamanr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(11907, 5397, NULL, NULL, NULL, '20153949', NULL, 'RICHARD ALONSO', NULL, 'HUAMAN VALDEZ', NULL, 'rhuamanv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11908, 5398, NULL, NULL, NULL, '20174275', NULL, 'WILINTON', NULL, 'HUAMANI ANDRADE', NULL, 'whuamanian@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11909, 5399, NULL, NULL, NULL, '20083700', NULL, 'MILTON FRANCISCO', NULL, 'HUAMANI ARAGON', NULL, 'mhuamaniar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11910, 5400, NULL, NULL, NULL, '20085283', NULL, 'ANTONY NELSON', NULL, 'HUAMANI BARRIOS', NULL, 'ahuamaniba@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11911, 5401, NULL, NULL, NULL, '20164629', NULL, 'MILENA ROCIO', NULL, 'HUAMANI CANCHARI', NULL, 'mhuamanican@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11912, 5402, NULL, NULL, NULL, '20134356', NULL, 'JHON WILIAM', NULL, 'HUAMANI CHAMBI', NULL, 'jhuamanicham@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11913, 5403, NULL, NULL, NULL, '20102254', NULL, 'ADRIANA ISABEL', NULL, 'HUAMANI CHUQUIRIMAY', NULL, 'ahuamanich@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11914, 5404, NULL, NULL, NULL, '20144361', NULL, 'MARIELA GABI', NULL, 'HUAMANI COLQUE', NULL, 'mhuamanicol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11915, 5405, NULL, NULL, NULL, '20172429', NULL, 'KARLA LISSETH', NULL, 'HUAMANI DELGADO', NULL, 'khuamanid@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11916, 5406, NULL, NULL, NULL, '20134217', NULL, 'HECTOR DAVID', NULL, 'HUAMANI EQUINO', NULL, 'hhuamanie@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11917, 5407, NULL, NULL, NULL, '20141380', NULL, 'CLAUDIA ISABEL', NULL, 'HUAMANI GARCIA', NULL, 'chuamaniga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11918, 5408, NULL, NULL, NULL, '20164636', NULL, 'BEATRIZ MARISOL', NULL, 'HUAMANI HUARANCCA', NULL, 'bhuamanihua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11919, 5409, NULL, NULL, NULL, '20164706', NULL, 'JUAN CARLOS', NULL, 'HUAMANI HUILLCA', NULL, 'jhuamanihui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11920, 5410, NULL, NULL, NULL, '20134214', NULL, 'YELTSIN PATRICIO', NULL, 'HUAMANI LAURI', NULL, 'yhuamanila@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11921, 5411, NULL, NULL, NULL, '20172444', NULL, 'ALEJANDRA ALIXON', NULL, 'HUAMANI MONTANEZ', NULL, 'ahuamanimon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11922, 5412, NULL, NULL, NULL, '20098335', NULL, 'ANA PAULA', NULL, 'HUAMANI PEREYRA', NULL, 'ahuamanipe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11923, 5413, NULL, NULL, NULL, '20082143', NULL, 'JOSE ANTONIO', NULL, 'HUAMANI QUINTANA', NULL, 'jhuamaniqu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11924, 5414, NULL, NULL, NULL, '20134270', NULL, 'FRANCHESCA LUCERO', NULL, 'HUAMANI QUISPE', NULL, 'fhuamaniqu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11925, 5415, NULL, NULL, NULL, '20154568', NULL, 'PLINIO RICARDO', NULL, 'HUAMANI VALVERDE', NULL, 'phuamaniv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11926, 5416, NULL, NULL, NULL, '20102823', NULL, 'MARCIA ADA', NULL, 'HUAMANI VILLENA', NULL, 'mhuamanivil@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11927, 5417, NULL, NULL, NULL, '20141350', NULL, 'LIZBETH JUDITH', NULL, 'HUAMANTALLA APAZA', NULL, 'lhuamantalla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11928, 5418, NULL, NULL, NULL, '20174324', NULL, 'JORGE LUIS', NULL, 'HUAMANTUMA MONROY', NULL, 'jhuamantumamo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11929, 5419, NULL, NULL, NULL, '20134352', NULL, 'RICHARD GONZALO', NULL, 'HUANACO APAZA', NULL, 'rhuanaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11930, 5420, NULL, NULL, NULL, '20164691', NULL, 'FLOR MARLEY', NULL, 'HUANCA ALARCON', NULL, 'fhuancaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11931, 5421, NULL, NULL, NULL, '20121137', NULL, 'MARIZA', NULL, 'HUANCA AYCAYA', NULL, 'mhuancaay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11932, 5422, NULL, NULL, NULL, '20134264', NULL, 'LIZETH FLORANGEL', NULL, 'HUANCA CUTI', NULL, 'lhuancac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11933, 5423, NULL, NULL, NULL, '20134360', NULL, 'FANY', NULL, 'HUANCA HERMOZA', NULL, 'fhuancahe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11934, 5424, NULL, NULL, NULL, '20143730', NULL, 'DELIA ELIANA', NULL, 'HUANCA HUAHUISA', NULL, 'dhuanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11935, 5425, NULL, NULL, NULL, '20164581', NULL, 'DRAYCI MAYENKA', NULL, 'HUANCA IQUIAPAZA', NULL, 'dhuancai@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11936, 5426, NULL, NULL, NULL, '20164709', NULL, 'LUIS DANIEL', NULL, 'HUANCA MAMANI', NULL, 'lhuancamam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11937, 5427, NULL, NULL, NULL, '20096639', NULL, 'EDWIN', NULL, 'HUANCA SANCA', NULL, 'ehuancas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11938, 5428, NULL, NULL, NULL, '20164692', NULL, 'SERGIO ANDRE', NULL, 'HUANCA TICONA', NULL, 'shuancat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11939, 5429, NULL, NULL, NULL, '20102997', NULL, 'JESSENIA VICTORIA', NULL, 'HUANCA TURPO', NULL, 'jhuancat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11940, 5430, NULL, NULL, NULL, '20110519', NULL, 'YAMILETH YAJAIDA', NULL, 'HUANCA ZAMATA', NULL, 'yhuancaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11941, 5431, NULL, NULL, NULL, '20170995', NULL, 'DANIEL VICTOR', NULL, 'HUAQUISTO VALENCIA', NULL, 'dhuaquistov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11942, 5432, NULL, NULL, NULL, '20123225', NULL, 'JOSE CARLOS', NULL, 'HUARACALLO HUAMANI', NULL, 'jhuaracalloh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11943, 5433, NULL, NULL, NULL, '20112142', NULL, 'JESUS ARTURO', NULL, 'HUARACHA TICONA', NULL, 'jhuarachat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11944, 5434, NULL, NULL, NULL, '20154582', NULL, 'SAUL DAVID', NULL, 'HUARANCA QUISPE', NULL, 'shuarancaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11945, 5435, NULL, NULL, NULL, '20174255', NULL, 'MERCEDES RUTH', NULL, 'HUARCA APAZA', NULL, 'mhuarcaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11946, 5436, NULL, NULL, NULL, '20132475', NULL, 'JOSELYN PAMELA', NULL, 'HUARCA NUNEZ', NULL, 'jhuarcan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11947, 5437, NULL, NULL, NULL, '20174248', NULL, 'SHEILA EMILY', NULL, 'HUARCA VILCA', NULL, 'shuarcav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11948, 5438, NULL, NULL, NULL, '20113863', NULL, 'VERONICA', NULL, 'HUASCOPE APAZA', NULL, 'vhuascope@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11949, 5439, NULL, NULL, NULL, '20085276', NULL, 'VICTOR LUIS', NULL, 'HUAYAPA MAMANI', NULL, 'vhuayapam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11950, 5440, NULL, NULL, NULL, '20112172', NULL, 'EDWIN', NULL, 'HUAYCHO CHARA', NULL, 'ehuaycho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11951, 5441, NULL, NULL, NULL, '20164603', NULL, 'ALVA IDALIA', NULL, 'HUAYLLANI MAMANI', NULL, 'ahuayllanim@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11952, 5442, NULL, NULL, NULL, '19991432', NULL, 'CESAR VICTOR', NULL, 'HUAYLLARO CUELLAR', NULL, 'chuayllaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11953, 5443, NULL, NULL, NULL, '20174277', NULL, 'JHOSEPH ANTONY', NULL, 'HUAYLLAZO HUACCHA', NULL, 'jhuayllazoh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11954, 5444, NULL, NULL, NULL, '20143085', NULL, 'JOSE ANDRE', NULL, 'HUAYNA PERALTA', NULL, 'jhuaynap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11955, 5445, NULL, NULL, NULL, '20143732', NULL, 'LOURDES GRECIA', NULL, 'HUAYNACHO VITA', NULL, 'lhuaynacho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11956, 5446, NULL, NULL, NULL, '20131369', NULL, 'EDER', NULL, 'HUAYNASI KANA', NULL, 'ehuaynasik@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11957, 5447, NULL, NULL, NULL, '20144302', NULL, 'JORGE LUIS', NULL, 'HUAYTA -', NULL, 'jhuaytah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11958, 5448, NULL, NULL, NULL, '20154692', NULL, 'KARINA MILAGROS', NULL, 'HUAYTA CHAMBI', NULL, 'khuaytach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11959, 5449, NULL, NULL, NULL, '20163774', NULL, 'ANGELICA', NULL, 'HUAYTA ORURO', NULL, 'ahuayta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11960, 5450, NULL, NULL, NULL, '20154576', NULL, 'VALERIA', NULL, 'HUERTA ACOSTA', NULL, 'vhuerta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11961, 5451, NULL, NULL, NULL, '19971498', NULL, 'JOSE ANTONIO', NULL, 'HUICHE AGUILAR', NULL, 'jhuiche@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11962, 5452, NULL, NULL, NULL, '20154641', NULL, 'DANIELA', NULL, 'HUILLCA CABRERA', NULL, 'dhuillcac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11963, 5453, NULL, NULL, NULL, '20164586', NULL, 'ELVIS', NULL, 'HUILLCA CJUNO', NULL, 'ehuillcac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11964, 5454, NULL, NULL, NULL, '20141348', NULL, 'OSCAR ABEL', NULL, 'HUILLCA PINTO', NULL, 'ohuillca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11965, 5455, NULL, NULL, NULL, '20161043', NULL, 'SAMUEL JOSUE', NULL, 'HUMASI APAZA', NULL, 'shumasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11966, 5456, NULL, NULL, NULL, '20172433', NULL, 'MARCIAL', NULL, 'HURCAGAS ALLCCA', NULL, 'mhurcagas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11967, 5457, NULL, NULL, NULL, '20144295', NULL, 'GRECIA DE LOS ANGELES', NULL, 'HURTADO TORRES', NULL, 'ghurtado@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11968, 5458, NULL, NULL, NULL, '20121301', NULL, 'JOSE GREGORI', NULL, 'IBARCENA VASQUEZ', NULL, 'jibarcenav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11969, 5459, NULL, NULL, NULL, '20174297', NULL, 'PATRICK GUILLERMO', NULL, 'IBARRA CORNEJO', NULL, 'pibarra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11970, 5460, NULL, NULL, NULL, '20161036', NULL, 'ROXANA MARLENY', NULL, 'IDME PEQUENA', NULL, 'ridmep@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11971, 5461, NULL, NULL, NULL, '20141343', NULL, 'NEEL ALLISON', NULL, 'IMATA CHAMOCHUMBI', NULL, 'nimata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11972, 5462, NULL, NULL, NULL, '20072488', NULL, 'GABRIELA PAOLA', NULL, 'INCALLA VALDIVIA', NULL, 'gincalla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11973, 5463, NULL, NULL, NULL, '20163800', NULL, 'LUCIANA LIZBETH', NULL, 'INUMA VILAVILA', NULL, 'linuma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11974, 5464, NULL, NULL, NULL, '20134276', NULL, 'ROSMERI', NULL, 'IPANAQUE DEL CARPIO', NULL, 'ripanaque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11975, 5465, NULL, NULL, NULL, '20132885', NULL, 'YORLY MARI', NULL, 'IQUIAPAZA ALMONTE', NULL, 'yiquiapazaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11976, 5466, NULL, NULL, NULL, '20134236', NULL, 'ANYI ARACELY', NULL, 'IQUIAPAZA CHOQUE', NULL, 'aiquiapaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11977, 5467, NULL, NULL, NULL, '20144332', NULL, 'DIANA', NULL, 'IQUIAPAZA IQUIAPAZA', NULL, 'diquiapaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11978, 5468, NULL, NULL, NULL, '20154624', NULL, 'ALEXANDRA MILAGROS', NULL, 'ISMODES ALHUIRCA', NULL, 'aismodes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11979, 5469, NULL, NULL, NULL, '20140873', NULL, 'GIANCARLO CESAR', NULL, 'ITO AYQUE', NULL, 'gitoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11980, 5470, NULL, NULL, NULL, '20161033', NULL, 'FLOR MARIA', NULL, 'JACOBO CONDO', NULL, 'fjacoboc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11981, 5471, NULL, NULL, NULL, '20154583', NULL, 'WILMAR MARCIAL', NULL, 'JACOBO HERRERA', NULL, 'wjacoboh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11982, 5472, NULL, NULL, NULL, '20114171', NULL, 'DIANA MAVEL', NULL, 'JACOBO HUAMANI', NULL, 'djacoboh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11983, 5473, NULL, NULL, NULL, '20164624', NULL, 'ROSALI ESTEFANI', NULL, 'JALISTO VALDIVIA', NULL, 'rjalisto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11984, 5474, NULL, NULL, NULL, '20164669', NULL, 'NATALY LIANA', NULL, 'JALLURANA YANQUI', NULL, 'njallurana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11985, 5475, NULL, NULL, NULL, '20072451', NULL, 'MARIA DEL CARMEN', NULL, 'JARA RODRIGUEZ', NULL, 'mjararo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11986, 5476, NULL, NULL, NULL, '20172443', NULL, 'SHAKIRA LADY', NULL, 'JIMENEZ DELGADO', NULL, 'sjimenezd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11987, 5477, NULL, NULL, NULL, '20141349', NULL, 'JESUS EDUARDO', NULL, 'JOACHIN CALCINA', NULL, 'jjoachin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11988, 5478, NULL, NULL, NULL, '20114053', NULL, 'JONATHAN ANTHONY', NULL, 'JUAREZ CORNEJO', NULL, 'jjuarezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11989, 5479, NULL, NULL, NULL, '20130508', NULL, 'JIMMY ROMARIO', NULL, 'JUAREZ RAMIREZ', NULL, 'jjuarezr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11990, 5480, NULL, NULL, NULL, '20124125', NULL, 'SALOMON', NULL, 'JUSTO APAZA', NULL, 'sjusto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11991, 5481, NULL, NULL, NULL, '20164611', NULL, 'KELLY ESTEFANY', NULL, 'JUSTO MENDOZA', NULL, 'kjustom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11992, 5482, NULL, NULL, NULL, '20174330', NULL, 'YOMIRA CECILIA', NULL, 'JUSTO MENDOZA', NULL, 'yjusto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11993, 5483, NULL, NULL, NULL, '20134349', NULL, 'DEBORA ESTHER', NULL, 'KALA MAMANI', NULL, 'dkala@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11994, 5484, NULL, NULL, NULL, '20174303', NULL, 'ESTHER REBECA', NULL, 'KANA NUNONCA', NULL, 'ekanan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11995, 5485, NULL, NULL, NULL, '20122002', NULL, 'YAHAIRA YNGRIT', NULL, 'KQRIMAYA GUTIERREZ', NULL, 'ykqrimaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11996, 5486, NULL, NULL, NULL, '20174328', NULL, 'DINA ROSALIA', NULL, 'KUNO TACO', NULL, 'dkuno@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11997, 5487, NULL, NULL, NULL, '20164674', NULL, 'LUCERO RUBI', NULL, 'LA ROSA AMPUERO', NULL, 'llarosa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11998, 5488, NULL, NULL, NULL, '20151475', NULL, 'RUTH MERY', NULL, 'LABRA HUILLCA', NULL, 'rlabra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(11999, 5489, NULL, NULL, NULL, '20154663', NULL, 'YENI VANESSA', NULL, 'LAGUNA CACERES', NULL, 'ylaguna@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12000, 5490, NULL, NULL, NULL, '20131379', NULL, 'PAMELA CLAUDIA', NULL, 'LAGUNA LAURA', NULL, 'plaguna@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12001, 5491, NULL, NULL, NULL, '20134348', NULL, 'ELSEN PILAR', NULL, 'LAIME GASCA', NULL, 'elaimeg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12002, 5492, NULL, NULL, NULL, '20154622', NULL, 'BROWN LEIBLINGER', NULL, 'LANZA CHAPARRO', NULL, 'blanza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12003, 5493, NULL, NULL, NULL, '20174318', NULL, 'ANNIE DE LOS ANGELES', NULL, 'LAROTA CASSA', NULL, 'alarotac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12004, 5494, NULL, NULL, NULL, '20154600', NULL, 'LUIS FERNANDO', NULL, 'LAROTA ZEVALLOS', NULL, 'llarotaz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12005, 5495, NULL, NULL, NULL, '20131353', NULL, 'NERY OFELIA', NULL, 'LAURA APAZA', NULL, 'nlauraa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12006, 5496, NULL, NULL, NULL, '20152656', NULL, 'CRISTIAN ALVARO', NULL, 'LAURA CONDORI', NULL, 'claura@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12007, 5497, NULL, NULL, NULL, '20112120', NULL, 'NORAH FRANSHESKA', NULL, 'LAURA GAONA', NULL, 'nlaurag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12008, 5498, NULL, NULL, NULL, '20171010', NULL, 'RAMIRO ALEJANDRO', NULL, 'LAURA HINOSTROZA', NULL, 'rlaurah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12009, 5499, NULL, NULL, NULL, '20163780', NULL, 'ALVARO NAZARIO', NULL, 'LAURA MELENDRES', NULL, 'alauram@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12010, 5500, NULL, NULL, NULL, '20172428', NULL, 'MARIA GABRIELA', NULL, 'LAVILLA SARMIENTO', NULL, 'mlavilla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12011, 5501, NULL, NULL, NULL, '20124124', NULL, 'JHONKER', NULL, 'LAYME MAQUERA', NULL, 'jlaymem@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12012, 5502, NULL, NULL, NULL, '20104428', NULL, 'LIZANDRO ALONZO', NULL, 'LAYME QUENAYA', NULL, 'llaymeq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12013, 5503, NULL, NULL, NULL, '20144421', NULL, 'GLEDY JENNIFER', NULL, 'LAZARO LLAZA', NULL, 'glazarol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12014, 5504, NULL, NULL, NULL, '20143768', NULL, 'GLENY GUADALUPE', NULL, 'LAZARO TACO', NULL, 'glazarot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12015, 5505, NULL, NULL, NULL, '20142474', NULL, 'ELIZABETH', NULL, 'LAZO AYAMAMANI', NULL, 'elazoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12016, 5506, NULL, NULL, NULL, '20172434', NULL, 'ALEXANDER GUSTAVO', NULL, 'LAZO DIAZ', NULL, 'alazodi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12017, 5507, NULL, NULL, NULL, '20154673', NULL, 'RENAU WALTER', NULL, 'LAZO TERNERO', NULL, 'rlazot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12018, 5508, NULL, NULL, NULL, '20134281', NULL, 'YOSELIN LAURA', NULL, 'LEANDREZ ACHATA', NULL, 'yleandrez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12019, 5509, NULL, NULL, NULL, '20141362', NULL, 'MARYLUZ', NULL, 'LEON CHARCA', NULL, 'mleonc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12020, 5510, NULL, NULL, NULL, '20141342', NULL, 'NEIL ALEXIS', NULL, 'LEON DELGADO', NULL, 'nleond@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12021, 5511, NULL, NULL, NULL, '20134329', NULL, 'SUSAN', NULL, 'LEON FARFAN', NULL, 'sleon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12022, 5512, NULL, NULL, NULL, '20163799', NULL, 'YURGUEN YOEL', NULL, 'LEON PAMPA', NULL, 'yleonpa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12023, 5513, NULL, NULL, NULL, '20151493', NULL, 'KELLY MARIEL', NULL, 'LEON ROJAS', NULL, 'kleonr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12024, 5514, NULL, NULL, NULL, '20072528', NULL, 'OSCAR', NULL, 'LIMA ASTO', NULL, 'olima@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12025, 5515, NULL, NULL, NULL, '20164714', NULL, 'KEYLE LISBETH', NULL, 'LIMASCCA TAIPE', NULL, 'klimascca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12026, 5516, NULL, NULL, NULL, '20083652', NULL, 'KELVIN LESLIE', NULL, 'LINAREZ ALEJANDRO', NULL, 'klinareza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12027, 5517, NULL, NULL, NULL, '20172447', NULL, 'FRANK BRUNO', NULL, 'LINO RIVA', NULL, 'flino@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12028, 5518, NULL, NULL, NULL, '20112163', NULL, 'LUCIA MILAGROS', NULL, 'LIRA RODRIGUEZ', NULL, 'llirar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12029, 5519, NULL, NULL, NULL, '20152491', NULL, 'ALONSO NILTON', NULL, 'LIZARRAGA FLORES', NULL, 'alizarraga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12030, 5520, NULL, NULL, NULL, '20141346', NULL, 'ELIZABETH', NULL, 'LLAIQUE HACHA', NULL, 'ellaique@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12031, 5521, NULL, NULL, NULL, '20134265', NULL, 'LUIGI JOSE', NULL, 'LLAMOCA CHACALLA', NULL, 'lllamoca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12032, 5522, NULL, NULL, NULL, '20134292', NULL, 'JOSE LUIS', NULL, 'LLAMOCA CHACON', NULL, 'jllamocach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12033, 5523, NULL, NULL, NULL, '20098215', NULL, 'ESTEPHANY MILAGROS', NULL, 'LLAMOCA RODRIGUEZ', NULL, 'ellamocaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12034, 5524, NULL, NULL, NULL, '20090590', NULL, 'JOSBE', NULL, 'LLAMOCCA TRIVENO', NULL, 'jllamocca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12035, 5525, NULL, NULL, NULL, '20134335', NULL, 'JOSE ALFREDO', NULL, 'LLANO MAMANI', NULL, 'jllano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12036, 5526, NULL, NULL, NULL, '20174270', NULL, 'ROSARIO MILAGROS', NULL, 'LLANO MAMANI', NULL, 'rllanom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12037, 5527, NULL, NULL, NULL, '20172436', NULL, 'ARNOLD JULIAN', NULL, 'LLANOS CHAINA', NULL, 'allanosch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12038, 5528, NULL, NULL, NULL, '20090819', NULL, 'ELMER YONATAN', NULL, 'LLAYQUE LIBANDRO', NULL, 'ellayque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12039, 5529, NULL, NULL, NULL, '19981602', NULL, 'OSCAR ENRIQUE', NULL, 'LLERENA CRUZ', NULL, 'ollerenacr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12040, 5530, NULL, NULL, NULL, '20134216', NULL, 'PAMELA VICTORIA', NULL, 'LLERENA HUARACHA', NULL, 'pllerenahu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12041, 5531, NULL, NULL, NULL, '20124101', NULL, 'SHEMUEL ISRAEL', NULL, 'LLERENA MORFFINO', NULL, 'sllerenam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12042, 5532, NULL, NULL, NULL, '20144336', NULL, 'XIMENA LUCIA', NULL, 'LLERENA PAZ', NULL, 'xllerenap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12043, 5533, NULL, NULL, NULL, '20134332', NULL, 'JOEL OSMAR', NULL, 'LLERENA PIZARRO', NULL, 'jllerenapi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12044, 5534, NULL, NULL, NULL, '20174279', NULL, 'EVELYN ANAHI', NULL, 'LLERENA VILCA', NULL, 'ellerenav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12045, 5535, NULL, NULL, NULL, '20174273', NULL, 'YAKELIN MERCEDES', NULL, 'LLOCLLA LIMACHE', NULL, 'ylloclla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12046, 5536, NULL, NULL, NULL, '20174260', NULL, 'ABRAHAM', NULL, 'LLOCLLA LIMACHI', NULL, 'alloclla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12047, 5537, NULL, NULL, NULL, '20111472', NULL, 'OSWALDO CARLOS', NULL, 'LLOCLLE APAZA', NULL, 'olloclle@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12048, 5538, NULL, NULL, NULL, '20072371', NULL, 'ARCADIO URIEL', NULL, 'LLOQUE ZAMATA', NULL, 'alloque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12049, 5539, NULL, NULL, NULL, '20144307', NULL, 'JOSE GABRIEL', NULL, 'LOAYZA RAMIREZ', NULL, 'jloayzar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12050, 5540, NULL, NULL, NULL, '20151492', NULL, 'CARLO RODRIGO', NULL, 'LOPEZ CARDENAS', NULL, 'clopezca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12051, 5541, NULL, NULL, NULL, '20151519', NULL, 'PEYSCH KATHERINE', NULL, 'LOPEZ CONDORI', NULL, 'plopez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12052, 5542, NULL, NULL, NULL, '20097679', NULL, 'LUXOR ALFONSO', NULL, 'LOPEZ CRUZ', NULL, 'llopezcr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12053, 5543, NULL, NULL, NULL, '20172449', NULL, 'CLAUDIA', NULL, 'LOPEZ GUEVARA', NULL, 'clopezgu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12054, 5544, NULL, NULL, NULL, '20174289', NULL, 'JOEL', NULL, 'LOPEZ HUAYHUA', NULL, 'jlopezhu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12055, 5545, NULL, NULL, NULL, '20144425', NULL, 'SUSANA ELIZABETH', NULL, 'LOPEZ JACOBO', NULL, 'slopezj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12056, 5546, NULL, NULL, NULL, '20142540', NULL, 'KATIUSCA XIMENA', NULL, 'LOPEZ LLAMOSAS', NULL, 'klopezl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12057, 5547, NULL, NULL, NULL, '20154607', NULL, 'GINA PIERINA', NULL, 'LOPEZ MAMANI', NULL, 'glopezm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12058, 5548, NULL, NULL, NULL, '20171014', NULL, 'THALIA LETTY', NULL, 'LOPEZ QUISPE', NULL, 'tlopezq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12059, 5549, NULL, NULL, NULL, '20153988', NULL, 'GRECIA YASMINE', NULL, 'LOPEZ TORREBLANCA', NULL, 'glopez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12060, 5550, NULL, NULL, NULL, '20143744', NULL, 'CARLA LUCIA', NULL, 'LOPEZ VILLANUEVA', NULL, 'clopezv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12061, 5551, NULL, NULL, NULL, '20174198', NULL, 'RICARDO MANUEL', NULL, 'LOZADA AQUIPUCHO', NULL, 'rlozadaaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12062, 5552, NULL, NULL, NULL, '20091298', NULL, 'JULIO AUGUSTO', NULL, 'LOZANO BARRIGA', NULL, 'jlozanob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12063, 5553, NULL, NULL, NULL, '20171012', NULL, 'GARY GEOVANI', NULL, 'LUCANA CALLALLI', NULL, 'glucanac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12064, 5554, NULL, NULL, NULL, '20134210', NULL, 'JANDIRA BEATRIZ', NULL, 'LUJAN MORAN', NULL, 'jlujan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12065, 5555, NULL, NULL, NULL, '20021164', NULL, 'ELMER GERARDO', NULL, 'LUJANO VARGAS', NULL, 'elujano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12066, 5556, NULL, NULL, NULL, '20121020', NULL, 'JUAN CARLOS', NULL, 'LUNA APAZA', NULL, 'jlunaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12067, 5557, NULL, NULL, NULL, '20154695', NULL, 'HERNAN ALBERTO', NULL, 'LUNA PAREDES', NULL, 'hlunap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12068, 5558, NULL, NULL, NULL, '20164679', NULL, 'SERGIO RODRIGO', NULL, 'LUNA UGARTE', NULL, 'slunau@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12069, 5559, NULL, NULL, NULL, '20174231', NULL, 'NATALY VANESSA', NULL, 'LUQUE GUITTON', NULL, 'nluquegu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12070, 5560, NULL, NULL, NULL, '20151512', NULL, 'RUBELA', NULL, 'MACHACA CONDORI', NULL, 'rmachacaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12071, 5561, NULL, NULL, NULL, '20141341', NULL, 'JORGE RODRIGO', NULL, 'MACHACA JUAREZ', NULL, 'jmachacaj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12072, 5562, NULL, NULL, NULL, '20141371', NULL, 'CECILIA SONIA', NULL, 'MACHACA SANCHEZ', NULL, 'cmachacas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12073, 5563, NULL, NULL, NULL, '20164722', NULL, 'LUCY ELENA', NULL, 'MAGAN CORONADO', NULL, 'lmagan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12074, 5564, NULL, NULL, NULL, '20134328', NULL, 'STEFANY JORDANA', NULL, 'MAGAN PALOMINO', NULL, 'smagan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12075, 5565, NULL, NULL, NULL, '20164730', NULL, 'ALESSANDRO MARCIO', NULL, 'MALAGA DELGADO', NULL, 'amalagade@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12076, 5566, NULL, NULL, NULL, '20154621', NULL, 'ALEXA ROXANA', NULL, 'MALAGA DELGADO', NULL, 'amalagad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12077, 5567, NULL, NULL, NULL, '20163784', NULL, 'CYNTHIA VANESSA', NULL, 'MALDONADO CHAMBI', NULL, 'cmaldonadoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(12078, 5568, NULL, NULL, NULL, '20164694', NULL, 'ROSMERY JULIA', NULL, 'MALDONADO CONDORI', NULL, 'rmaldonadoco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12079, 5569, NULL, NULL, NULL, '20103795', NULL, 'JOHN WALTER', NULL, 'MALDONADO PARI', NULL, 'jmaldonadopa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12080, 5570, NULL, NULL, NULL, '20144363', NULL, 'RICHARD ELMER', NULL, 'MALDONADO QUISPE', NULL, 'rmaldonadoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12081, 5571, NULL, NULL, NULL, '20153955', NULL, 'MARIA MARCELA', NULL, 'MAMANI ABADO', NULL, 'mmamaniab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12082, 5572, NULL, NULL, NULL, '20164608', NULL, 'JAHUI', NULL, 'MAMANI AMANQUI', NULL, 'jmamaniam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12083, 5573, NULL, NULL, NULL, '20142473', NULL, 'SHARON HELEN', NULL, 'MAMANI ANCCO', NULL, 'smamanian@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12084, 5574, NULL, NULL, NULL, '20154650', NULL, 'ANALID MERCEDES', NULL, 'MAMANI APAZA', NULL, 'amamaniap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12085, 5575, NULL, NULL, NULL, '20171027', NULL, 'ANGEL CRISTHIAN', NULL, 'MAMANI ARCOS', NULL, 'amamaniarc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12086, 5576, NULL, NULL, NULL, '20096662', NULL, 'GIOVANNI ROSLYN', NULL, 'MAMANI CACERES', NULL, 'gmamanicac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12087, 5577, NULL, NULL, NULL, '20174205', NULL, 'MARIA REYNA ISABEL', NULL, 'MAMANI CARLOS', NULL, 'mmamanica@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12088, 5578, NULL, NULL, NULL, '20162140', NULL, 'YADHIRA SUGEY', NULL, 'MAMANI CASTILLO', NULL, 'ymamanicast@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12089, 5579, NULL, NULL, NULL, '20153964', NULL, 'THALIA ROSAISELA', NULL, 'MAMANI CCAPIRA', NULL, 'tmamanicc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12090, 5580, NULL, NULL, NULL, '20174222', NULL, 'VALERY CECILIA', NULL, 'MAMANI CENTENO', NULL, 'vmamanice@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12091, 5581, NULL, NULL, NULL, '20161056', NULL, 'MARTHA CHANEL', NULL, 'MAMANI CHAMBI', NULL, 'mmamanich@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12092, 5582, NULL, NULL, NULL, '20085195', NULL, 'BILLY ALMANDY', NULL, 'MAMANI CHAUPI', NULL, 'bmamanic@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12093, 5583, NULL, NULL, NULL, '20134305', NULL, 'KATY MARIA', NULL, 'MAMANI CHILI', NULL, 'kmamanich@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12094, 5584, NULL, NULL, NULL, '20075078', NULL, 'MAGDALENA MARGARI', NULL, 'MAMANI CHIPANA', NULL, 'mmamanichi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12095, 5585, NULL, NULL, NULL, '20144297', NULL, 'EROS YUNIOR RUMARIO', NULL, 'MAMANI CHOQUE', NULL, 'emamanichoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12096, 5586, NULL, NULL, NULL, '20170992', NULL, 'MELIZA', NULL, 'MAMANI CHOQUECABANA', NULL, 'mmamanichoquec@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12097, 5587, NULL, NULL, NULL, '20151476', NULL, 'YENI', NULL, 'MAMANI CHOQUECABANA', NULL, 'ymamanicho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12098, 5588, NULL, NULL, NULL, '20171015', NULL, 'HIDALUZ YUSHARA', NULL, 'MAMANI CHOQUEHUANCA', NULL, 'hmamanich@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12099, 5589, NULL, NULL, NULL, '20161058', NULL, 'MARITZA JACKELINE', NULL, 'MAMANI CHOQUEHUANCA', NULL, 'mmamanichoque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12100, 5590, NULL, NULL, NULL, '20150541', NULL, 'NIDWARD DENYS', NULL, 'MAMANI CHOQUEHUANCA', NULL, 'nmamanicho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12101, 5591, NULL, NULL, NULL, '20143742', NULL, 'BETTY MARTINA', NULL, 'MAMANI CORDOVA', NULL, 'bmamanicor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12102, 5592, NULL, NULL, NULL, '20131358', NULL, 'PAMELA PAOLA', NULL, 'MAMANI CUADROS', NULL, 'pmamanicua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12103, 5593, NULL, NULL, NULL, '20144402', NULL, 'CARLA ROSA', NULL, 'MAMANI ESQUIA', NULL, 'cmamanie@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12104, 5594, NULL, NULL, NULL, '20153959', NULL, 'YULIANA YULIZA', NULL, 'MAMANI FLORES', NULL, 'ymamanifl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12105, 5595, NULL, NULL, NULL, '20164612', NULL, 'DEYSI YUDITH', NULL, 'MAMANI HACCA', NULL, 'dmamanihac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12106, 5596, NULL, NULL, NULL, '20144423', NULL, 'SAYDA PILAR', NULL, 'MAMANI HACCA', NULL, 'smamaniha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12107, 5597, NULL, NULL, NULL, '20174266', NULL, 'BRIAM LEE', NULL, 'MAMANI HANCCO', NULL, 'bmamaniha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12108, 5598, NULL, NULL, NULL, '20141382', NULL, 'DIONI WASHINGTON', NULL, 'MAMANI HINCHO', NULL, 'dmamanih@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12109, 5599, NULL, NULL, NULL, '20164595', NULL, 'DIEGO FERNANDO', NULL, 'MAMANI HUAYTA', NULL, 'dmamanihuay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12110, 5600, NULL, NULL, NULL, '20133693', NULL, 'LUCERO KATHERINE', NULL, 'MAMANI LIMA', NULL, 'lumamanil@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12111, 5601, NULL, NULL, NULL, '20134309', NULL, 'YOMIRA MARIBEL', NULL, 'MAMANI LLERENA', NULL, 'ymamanill@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12112, 5602, NULL, NULL, NULL, '20124139', NULL, 'JULITZA GRISALIDA', NULL, 'MAMANI MAMANI', NULL, 'jumamanima@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12113, 5603, NULL, NULL, NULL, '20144373', NULL, 'DAVID SALVADOR', NULL, 'MAMANI MEDINA', NULL, 'dmamanime@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12114, 5604, NULL, NULL, NULL, '20064476', NULL, 'HELENE GLENY', NULL, 'MAMANI MENDOZA', NULL, 'hmamanime@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12115, 5605, NULL, NULL, NULL, '20164738', NULL, 'ERIKA CIELO', NULL, 'MAMANI MOLINA', NULL, 'emamanimo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12116, 5606, NULL, NULL, NULL, '20151484', NULL, 'ROCIO ESTEFANY', NULL, 'MAMANI MONTESINOS', NULL, 'rmamanimo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12117, 5607, NULL, NULL, NULL, '20174298', NULL, 'ELENA CLAUDIA', NULL, 'MAMANI MOSCOSO', NULL, 'emamanimos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12118, 5608, NULL, NULL, NULL, '20164689', NULL, 'NELLY LIZBETH', NULL, 'MAMANI MUNOZ', NULL, 'nmamanimu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12119, 5609, NULL, NULL, NULL, '20154675', NULL, 'ANGEL NOE', NULL, 'MAMANI NINACONDOR', NULL, 'amamanin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12120, 5610, NULL, NULL, NULL, '20141360', NULL, 'SUSAN ESTHER', NULL, 'MAMANI NOA', NULL, 'smamanin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12121, 5611, NULL, NULL, NULL, '20152665', NULL, 'JAIRO WILBER', NULL, 'MAMANI OCSA', NULL, 'jmamanioc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12122, 5612, NULL, NULL, NULL, '20144395', NULL, 'KATHERINE YOMIRA', NULL, 'MAMANI PARI', NULL, 'kmamanip@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12123, 5613, NULL, NULL, NULL, '20174197', NULL, 'MARLENY', NULL, 'MAMANI PEREZ', NULL, 'mmamaniper@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12124, 5614, NULL, NULL, NULL, '20174276', NULL, 'JOSE RUBEN', NULL, 'MAMANI POZO', NULL, 'jmamanipoz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12125, 5615, NULL, NULL, NULL, '20172452', NULL, 'ROCIO MARIANELA', NULL, 'MAMANI QUISPE', NULL, 'romamaniq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12126, 5616, NULL, NULL, NULL, '20171024', NULL, 'IRENE ANGELA', NULL, 'MAMANI RAMOS', NULL, 'imamanira@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12127, 5617, NULL, NULL, NULL, '20151477', NULL, 'ROSA ANGELICA', NULL, 'MAMANI RODRIGUEZ', NULL, 'rmamaniro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12128, 5618, NULL, NULL, NULL, '20151491', NULL, 'ROSA MIRIAM', NULL, 'MAMANI SAAVEDRA', NULL, 'rmamanis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12129, 5619, NULL, NULL, NULL, '20084901', NULL, 'YURI AMERICO', NULL, 'MAMANI VELASQUEZ', NULL, 'ymamanive@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12130, 5620, NULL, NULL, NULL, '20154569', NULL, 'KATHERIN MARTINA', NULL, 'MAMANI VILCA', NULL, 'kmamaniv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12131, 5621, NULL, NULL, NULL, '20144388', NULL, 'RUTH', NULL, 'MAMANI VILCA', NULL, 'rumamaniv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12132, 5622, NULL, NULL, NULL, '20174319', NULL, 'FLOR MILAGROS', NULL, 'MAMANI YUNGA', NULL, 'fmamaniy@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12133, 5623, NULL, NULL, NULL, '20154715', NULL, 'NOEMI MARIBEL', NULL, 'MAMANI ZUNIGA', NULL, 'nmamaniz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12134, 5624, NULL, NULL, NULL, '20143753', NULL, 'BRIZEIDA PAHOLA', NULL, 'MANCHEGO BUSTAMANTE', NULL, 'bmanchegob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12135, 5625, NULL, NULL, NULL, '20154711', NULL, 'XIOMARA MILAGROS', NULL, 'MANCHEGO PUMATANCA', NULL, 'xmanchego@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12136, 5626, NULL, NULL, NULL, '20172442', NULL, 'PERCY ANGEL', NULL, 'MANGO MANCHEGO', NULL, 'pmango@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12137, 5627, NULL, NULL, NULL, '20172435', NULL, 'ANNY SILVINA', NULL, 'MANRIQUE AYQUI', NULL, 'amanriqueay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12138, 5628, NULL, NULL, NULL, '20102432', NULL, 'RODRIGO ARMANDO', NULL, 'MANRIQUE GARCIA', NULL, 'rmanriqueg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12139, 5629, NULL, NULL, NULL, '20144317', NULL, 'JERSON EDUARDO', NULL, 'MANRIQUE GUZMAN', NULL, 'jmanriquegu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12140, 5630, NULL, NULL, NULL, '20064911', NULL, 'CRISTHIAN', NULL, 'MANRIQUE LLERENA', NULL, 'cmanriquel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12141, 5631, NULL, NULL, NULL, '20134256', NULL, 'XIOMARA IVONNE', NULL, 'MANRIQUE PUMA', NULL, 'xmanrique@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12142, 5632, NULL, NULL, NULL, '20081061', NULL, 'ANDRE RAMIRO', NULL, 'MANRIQUE SILVA', NULL, 'amanriques@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12143, 5633, NULL, NULL, NULL, '20141363', NULL, 'DENNIS JESUS', NULL, 'MANSILLA JARA', NULL, 'dmansillaj@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12144, 5634, NULL, NULL, NULL, '20164574', NULL, 'CLAUDIA ALEJANDRA', NULL, 'MANTILLA CASTILLO', NULL, 'cmantilla@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12145, 5635, NULL, NULL, NULL, '20154625', NULL, 'FIAMA FRANCHESSCA', NULL, 'MANZANARES PASTOR', NULL, 'fmanzanares@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12146, 5636, NULL, NULL, NULL, '20131363', NULL, 'ROCIO ESPERANZA', NULL, 'MAQUE APAZA', NULL, 'rmaquea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12147, 5637, NULL, NULL, NULL, '20144364', NULL, 'EDGAR SANTOS', NULL, 'MAQUE SANCHEZ', NULL, 'emaques@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12148, 5638, NULL, NULL, NULL, '20164618', NULL, 'WALTER JONNY', NULL, 'MAQUERA LAZARO', NULL, 'wmaqueral@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12149, 5639, NULL, NULL, NULL, '20154636', NULL, 'KATIA YASMIN', NULL, 'MAQUI GALDOS', NULL, 'kmaqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12150, 5640, NULL, NULL, NULL, '20144330', NULL, 'RICARDO AXEL', NULL, 'MAQUITO SOTO', NULL, 'rmaquitos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12151, 5641, NULL, NULL, NULL, '20151506', NULL, 'GUSTAVO ARMANDO', NULL, 'MARIN CACERES', NULL, 'gmarinc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12152, 5642, NULL, NULL, NULL, '20161048', NULL, 'JOSUE', NULL, 'MARIN HUAMANI', NULL, 'jmarin@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12153, 5643, NULL, NULL, NULL, '20161054', NULL, 'MARIA ALEJANDRA', NULL, 'MARQUEZ GARCIA', NULL, 'mmarquezga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12154, 5644, NULL, NULL, NULL, '20154597', NULL, 'JUDITH ISABEL', NULL, 'MARQUEZ PAPEL', NULL, 'jmarquezp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12155, 5645, NULL, NULL, NULL, '20174280', NULL, 'LUIS DAVID', NULL, 'MARTINEZ CACERES', NULL, 'lmartinezcac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12156, 5646, NULL, NULL, NULL, '20134306', NULL, 'ANGELA ROSARIO', NULL, 'MARTINEZ CAHUANA', NULL, 'amartinezca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12157, 5647, NULL, NULL, NULL, '20144348', NULL, 'CARLOS SEGUNDO', NULL, 'MARTINEZ GUTIERREZ', NULL, 'cmartinezg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12158, 5648, NULL, NULL, NULL, '20131372', NULL, 'LUIS ANTONIO', NULL, 'MARTINEZ MAMANI', NULL, 'lmartinezma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12159, 5649, NULL, NULL, NULL, '20084371', NULL, 'MARIELA EVELYN', NULL, 'MARTINEZ MARTINEZ', NULL, 'mmartinezmar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12160, 5650, NULL, NULL, NULL, '20154698', NULL, 'NELIDA BEATRIZ', NULL, 'MATAQQE PINEDA', NULL, 'nmataqqe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12161, 5651, NULL, NULL, NULL, '20173667', NULL, 'MARCO ANTONIO', NULL, 'MAYHUIRE HUAMANI', NULL, 'mmayhuireh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12162, 5652, NULL, NULL, NULL, '20132478', NULL, 'YESSICA ROSMERY', NULL, 'MAYTA HUAYLLA', NULL, 'ymaytah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12163, 5653, NULL, NULL, NULL, '20112168', NULL, 'YANIRA MADELEINE', NULL, 'MAYURI VILCA', NULL, 'ymayuri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12164, 5654, NULL, NULL, NULL, '20085305', NULL, 'VERONICA FRANCIS', NULL, 'MEDINA ACURIO', NULL, 'vmedinaac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12165, 5655, NULL, NULL, NULL, '20174257', NULL, 'CINTHIA MELISSA', NULL, 'MEDINA APAZA', NULL, 'cmedinaap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12166, 5656, NULL, NULL, NULL, '20141367', NULL, 'JEAN ANGEL', NULL, 'MEDINA LEVANO', NULL, 'jmedinale@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12167, 5657, NULL, NULL, NULL, '20131378', NULL, 'STEFFANY DORKAS', NULL, 'MEDINA MOROCCOYRI', NULL, 'smedinamo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12168, 5658, NULL, NULL, NULL, '20154578', NULL, 'ELDA DEL ROSARIO', NULL, 'MEDINA OLAZABAL', NULL, 'emedinao@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12169, 5659, NULL, NULL, NULL, '20064868', NULL, 'DANIEL AMADOR', NULL, 'MEDINA RODRIGUEZ', NULL, 'dmedinar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12170, 5660, NULL, NULL, NULL, '20134340', NULL, 'ALEX FERNANDO', NULL, 'MEDINA TTICA', NULL, 'amedinat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12171, 5661, NULL, NULL, NULL, '20164683', NULL, 'JONATHAN SMITH', NULL, 'MEDINA VARAS', NULL, 'jmedinavar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12172, 5662, NULL, NULL, NULL, '20164677', NULL, 'CLAUDIA CATALINA', NULL, 'MEJIA CARRASCO', NULL, 'cmejiac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12173, 5663, NULL, NULL, NULL, '20123229', NULL, 'MICHAEL', NULL, 'MEJIA PEREZ', NULL, 'mmejiap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12174, 5664, NULL, NULL, NULL, '19991848', NULL, 'ALFONSO YTALO', NULL, 'MEJIA RAMOS', NULL, 'amejiar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12175, 5665, NULL, NULL, NULL, '20164582', NULL, 'VICTOR STEVEN', NULL, 'MELGAR BENAVENTE', NULL, 'vmelgar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12176, 5666, NULL, NULL, NULL, '20174202', NULL, 'RAUL ALFONSO', NULL, 'MELGAR VELARDE', NULL, 'rmelgar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12177, 5667, NULL, NULL, NULL, '20080365', NULL, 'MIJAIL NIXON', NULL, 'MELO VALDIVIA', NULL, 'mmelov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12178, 5668, NULL, NULL, NULL, '20154567', NULL, 'WILLY RANDU', NULL, 'MENACHO CALLI', NULL, 'wmenacho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12179, 5669, NULL, NULL, NULL, '20174264', NULL, 'HECTOR ALONSO', NULL, 'MENDIGURE MONTALVO', NULL, 'hmendigure@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12180, 5670, NULL, NULL, NULL, '20133666', NULL, 'NATALY KARINA', NULL, 'MENDOZA BUSTINZA', NULL, 'nmendozabu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12181, 5671, NULL, NULL, NULL, '20043623', NULL, 'DIANA LUZ', NULL, 'MENDOZA CALIZAYA', NULL, 'dmendozac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12182, 5672, NULL, NULL, NULL, '20098260', NULL, 'PERCY IVAN', NULL, 'MENDOZA CARDENAS', NULL, 'pmendozac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12183, 5673, NULL, NULL, NULL, '20144334', NULL, 'MARILUZ VERONICA', NULL, 'MENDOZA CHAMBILLA', NULL, 'mmendozac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12184, 5674, NULL, NULL, NULL, '20161029', NULL, 'FRANCESCA GEANINA', NULL, 'MENDOZA COAGUILA', NULL, 'fmendozaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12185, 5675, NULL, NULL, NULL, '20174196', NULL, 'LUISA NOEMI', NULL, 'MENDOZA COAQUIRA', NULL, 'lmendozacoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12186, 5676, NULL, NULL, NULL, '20142480', NULL, 'ASUNCION LUPE', NULL, 'MENDOZA LUQUE', NULL, 'amendozaluq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12187, 5677, NULL, NULL, NULL, '20152657', NULL, 'LIZ PAMELA', NULL, 'MENDOZA MENDOZA', NULL, 'lmendozame@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12188, 5678, NULL, NULL, NULL, '20141352', NULL, 'CHESLY MIRIAM', NULL, 'MENDOZA OCSA', NULL, 'cmendozao@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12189, 5679, NULL, NULL, NULL, '20100820', NULL, 'EDGAR JESUS', NULL, 'MENDOZA RAMOS', NULL, 'emendozar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12190, 5680, NULL, NULL, NULL, '20112131', NULL, 'CARMEN ROSA', NULL, 'MENDOZA VARGAS', NULL, 'cmendozavar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12191, 5681, NULL, NULL, NULL, '20144394', NULL, 'HENRY ANDREE', NULL, 'MENDOZA ZUNIGA', NULL, 'hmendoza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12192, 5682, NULL, NULL, NULL, '20164678', NULL, 'WILBERT FERNANDO', NULL, 'MENESES VILCA', NULL, 'wmeneses@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12193, 5683, NULL, NULL, NULL, '20083784', NULL, 'DANTE', NULL, 'MERCADO CASTRO', NULL, 'dmercadoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12194, 5684, NULL, NULL, NULL, '20142735', NULL, 'MIRIAM MARYIT', NULL, 'MERCADO TACO', NULL, 'mmercado@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12195, 5685, NULL, NULL, NULL, '20164598', NULL, 'KARYMETH ALEJANDRA', NULL, 'MEZA CACERES', NULL, 'kmezac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12196, 5686, NULL, NULL, NULL, '20164572', NULL, 'MILEIDI BEATRIZ', NULL, 'MEZA PIMENTA', NULL, 'mmezapi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12197, 5687, NULL, NULL, NULL, '20151478', NULL, 'ALICIA', NULL, 'MEZA SOSA', NULL, 'amezas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12198, 5688, NULL, NULL, NULL, '20154609', NULL, 'ELARD JULIO', NULL, 'MEZA VASQUEZ', NULL, 'emezav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12199, 5689, NULL, NULL, NULL, '20134350', NULL, 'YDALIA YESSICA', NULL, 'MINAYA PAREDES', NULL, 'yminaya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12200, 5690, NULL, NULL, NULL, '20074551', NULL, 'LUPE', NULL, 'MIRABAL MAMANI', NULL, 'lmirabal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12201, 5691, NULL, NULL, NULL, '20174226', NULL, 'ZUMICO YHEREMY', NULL, 'MIRANDA CANO', NULL, 'zmiranda@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12202, 5692, NULL, NULL, NULL, '20141365', NULL, 'CLAUDIA GUADALUPE', NULL, 'MIRANDA DIAZ', NULL, 'cmirandad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12203, 5693, NULL, NULL, NULL, '19981877', NULL, 'MARIO LUIS', NULL, 'MIRANDA ESQUIVEL', NULL, 'mmirandaes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12204, 5694, NULL, NULL, NULL, '20144413', NULL, 'NADINNE JOHELY', NULL, 'MIRANDA VERA', NULL, 'nmirandav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12205, 5695, NULL, NULL, NULL, '20143992', NULL, 'IRENE THALIA', NULL, 'MITA MAMANI', NULL, 'imita@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12206, 5696, NULL, NULL, NULL, '20164649', NULL, 'MARCO ANTONIO', NULL, 'MITMA BAEZ', NULL, 'mmitma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12207, 5697, NULL, NULL, NULL, '20102297', NULL, 'NESTOR FRANCISCO', NULL, 'MOGOLLON CCAHUANA', NULL, 'nmogollon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12208, 5698, NULL, NULL, NULL, '20122795', NULL, 'MAYLI LISSY YAMILA', NULL, 'MOGROVEJO RAMOS', NULL, 'mmogrovejora@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12209, 5699, NULL, NULL, NULL, '20174274', NULL, 'SHARONN YOMIRA', NULL, 'MOGROVEJO REVILLA', NULL, 'smogrovejor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12210, 5700, NULL, NULL, NULL, '20174193', NULL, 'ALEJANDRA ANGELICA', NULL, 'MOINA URQUIZO', NULL, 'amoina@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12211, 5701, NULL, NULL, NULL, '20174206', NULL, 'YESENIA PATRICIA', NULL, 'MOJO MURILLO', NULL, 'ymojo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12212, 5702, NULL, NULL, NULL, '20174247', NULL, 'JOSE DANIEL', NULL, 'MOLINA QUISPE', NULL, 'jmolinaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12213, 5703, NULL, NULL, NULL, '20163771', NULL, 'ZAYDA-MEDALI', NULL, 'MOLLAPAZA RODRIGUEZ', NULL, 'zmollapaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12214, 5704, NULL, NULL, NULL, '20161050', NULL, 'EVELIN MAGALI', NULL, 'MOLLEHUARA CHACO', NULL, 'emollehuara@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12215, 5705, NULL, NULL, NULL, '20134295', NULL, 'LUIS HERNAN', NULL, 'MOLLINEDO AROHUANCA', NULL, 'lmollinedoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12216, 5706, NULL, NULL, NULL, '20143748', NULL, 'KLENDI', NULL, 'MONCCA LAYME', NULL, 'kmoncca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12217, 5707, NULL, NULL, NULL, '20134279', NULL, 'ALEX KEVIN', NULL, 'MONTALVO GUTIERREZ', NULL, 'amontalvog@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12218, 5708, NULL, NULL, NULL, '20174242', NULL, 'RUTH ZURAMA', NULL, 'MONTANEZ CHARCCAHUANA', NULL, 'rmontanez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12219, 5709, NULL, NULL, NULL, '20154687', NULL, 'KAREN CHESIRA', NULL, 'MONTANEZ NAVENTA', NULL, 'kmontanezn@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12220, 5710, NULL, NULL, NULL, '20154602', NULL, 'JUNIOR ALEJANDRO', NULL, 'MONTES MONTES', NULL, 'jmontesmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12221, 5711, NULL, NULL, NULL, '20141376', NULL, 'ANGELA SHEYLA', NULL, 'MONTESINOS CALDERON', NULL, 'amontesinos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12222, 5712, NULL, NULL, NULL, '20144351', NULL, 'NICOLE ALEXANDRA', NULL, 'MONTOYA ALMONTE', NULL, 'nmontoyaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12223, 5713, NULL, NULL, NULL, '20134203', NULL, 'VANESSA INES', NULL, 'MONTOYA ALMONTE', NULL, 'vmontoya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12224, 5714, NULL, NULL, NULL, '20144430', NULL, 'DIANA PATRICIA', NULL, 'MORA NUNEZ', NULL, 'dmoran@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12225, 5715, NULL, NULL, NULL, '20110362', NULL, 'SERGIO ENRIQUE', NULL, 'MORALES ESPIRILLA', NULL, 'smoralese@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12226, 5716, NULL, NULL, NULL, '20134324', NULL, 'JESUS FRANCISCO', NULL, 'MORALES MARROQUIN', NULL, 'jmoralesm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12227, 5717, NULL, NULL, NULL, '20121330', NULL, 'CARLOS ANDRE', NULL, 'MORAN CORDOVA', NULL, 'cmoran@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12228, 5718, NULL, NULL, NULL, '20170998', NULL, 'PEDRO FRANK', NULL, 'MORAYA DURAND', NULL, 'pmoraya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12229, 5719, NULL, NULL, NULL, '20144300', NULL, 'CATERINE VIKY', NULL, 'MOROCO CUTIRE', NULL, 'cmoroco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12230, 5720, NULL, NULL, NULL, '20164643', NULL, 'ELARD ABELARDO RICHARD', NULL, 'MOSCOSO VILCA', NULL, 'emoscosov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12231, 5721, NULL, NULL, NULL, '20104509', NULL, 'YULEYDI JETZABE', NULL, 'MOSCOSO VILCA', NULL, 'ymoscoso@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12232, 5722, NULL, NULL, NULL, '20074436', NULL, 'OLAF', NULL, 'MUNIZ VILCAPAZA', NULL, 'omuniz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12233, 5723, NULL, NULL, NULL, '20151368', NULL, 'DANIELA JOSELINE', NULL, 'MUNOZ CONDORI', NULL, 'dmunozco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12234, 5724, NULL, NULL, NULL, '20131385', NULL, 'ROMARIO MAX', NULL, 'MUNOZ NAVARRO', NULL, 'rmunozna@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12235, 5725, NULL, NULL, NULL, '20150944', NULL, 'FRANK ALEX', NULL, 'MURGA DIAZ', NULL, 'fmurga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12236, 5726, NULL, NULL, NULL, '20133662', NULL, 'SABINA', NULL, 'MURIEL TAPARA', NULL, 'smuriel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12237, 5727, NULL, NULL, NULL, '20081193', NULL, 'HENRY ESTEBAN', NULL, 'NALVANDIAN SEGOVIA', NULL, 'aquispecc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12238, 5728, NULL, NULL, NULL, '20134240', NULL, 'PAOLO RONALD', NULL, 'NARVAEZ PRADO', NULL, 'pnarvaez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12239, 5729, NULL, NULL, NULL, '20174269', NULL, 'LUIS DIEGO', NULL, 'NAVARRO SACRAMENTO', NULL, 'lnavarros@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12240, 5730, NULL, NULL, NULL, '20162150', NULL, 'HILARY KIMBERLY', NULL, 'NEIRA MAMANI', NULL, 'hneira@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12241, 5731, NULL, NULL, NULL, '20123382', NULL, 'FIORELLA CATHERINE', NULL, 'NEIRA NAUPA', NULL, 'fneiran@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12242, 5732, NULL, NULL, NULL, '20163801', NULL, 'JHADDER ANDRES', NULL, 'NEIRA SANCHEZ', NULL, 'jneiras@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12243, 5733, NULL, NULL, NULL, '20133694', NULL, 'NEYDEL MILAGROS', NULL, 'NEYRA DEL CARPIO', NULL, 'nneyrad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12244, 5734, NULL, NULL, NULL, '20174320', NULL, 'PIERO ENZO', NULL, 'NEYRA FLORES', NULL, 'pneyra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12245, 5735, NULL, NULL, NULL, '20172417', NULL, 'LUZ GRACIELA', NULL, 'NEYRA MONTOYA', NULL, 'lneyramo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12246, 5736, NULL, NULL, NULL, '20164697', NULL, 'LESLIE GIANELLA', NULL, 'NIETO BUTRON', NULL, 'lnietob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12247, 5737, NULL, NULL, NULL, '20153984', NULL, 'PIERINA JAZMIN', NULL, 'NIETO PANTIGOSO', NULL, 'pnietop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(12248, 5738, NULL, NULL, NULL, '20154603', NULL, 'ANITA ROSA', NULL, 'NIFLA VIZA', NULL, 'aniflav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12249, 5739, NULL, NULL, NULL, '20120830', NULL, 'CHRISTOPHER ANTHONY', NULL, 'NINA ADCO', NULL, 'cninaad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12250, 5740, NULL, NULL, NULL, '20161051', NULL, 'KJELL ANTONY', NULL, 'NINA AGUILAR', NULL, 'kninaag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12251, 5741, NULL, NULL, NULL, '20174310', NULL, 'KAREN FRANCISCA', NULL, 'NINA AMANQUI', NULL, 'kninaam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12252, 5742, NULL, NULL, NULL, '20131267', NULL, 'CHRISTIAN MAURICIO', NULL, 'NINA LOPEZ', NULL, 'cninal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12253, 5743, NULL, NULL, NULL, '20098227', NULL, 'LADY KARINA', NULL, 'NINA VENTURA', NULL, 'lninave@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12254, 5744, NULL, NULL, NULL, '20153953', NULL, 'ANA', NULL, 'NOA ILACHOQUE', NULL, 'anoai@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12255, 5745, NULL, NULL, NULL, '20141359', NULL, 'JOSE LUIS', NULL, 'NOA MAMANI', NULL, 'jnoam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12256, 5746, NULL, NULL, NULL, '20164594', NULL, 'ANGIE RAQUEL', NULL, 'NONONCA MAMANI', NULL, 'anononca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12257, 5747, NULL, NULL, NULL, '20141354', NULL, 'NATALY FIORELLA', NULL, 'NUNEZ CHAVEZ', NULL, 'nnunezch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12258, 5748, NULL, NULL, NULL, '20140936', NULL, 'VICTOR ABEL', NULL, 'NUNEZ CHOQUE', NULL, 'vnunez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12259, 5749, NULL, NULL, NULL, '20154644', NULL, 'SANDRA MARCELA', NULL, 'NUNEZ FIGUEROA', NULL, 'snunezf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12260, 5750, NULL, NULL, NULL, '20151521', NULL, 'JECKER JEAN PAUL', NULL, 'NUNEZ REQUENA', NULL, 'jnunezr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12261, 5751, NULL, NULL, NULL, '20064553', NULL, 'JOSE MANUEL', NULL, 'OBANDO CORNEJO', NULL, 'jobandoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12262, 5752, NULL, NULL, NULL, '20151497', NULL, 'JAIME ALONSO', NULL, 'OBANDO GARCIA', NULL, 'jobando@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12263, 5753, NULL, NULL, NULL, '20104440', NULL, 'MARIA PIA', NULL, 'OBLITAS FLORES', NULL, 'moblitasf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12264, 5754, NULL, NULL, NULL, '20164606', NULL, 'CARLOS ALBERTO', NULL, 'OCHARAN PAREDES', NULL, 'cocharan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12265, 5755, NULL, NULL, NULL, '20072553', NULL, 'JORGE DANIEL', NULL, 'OCHOA MENDOZA', NULL, 'jochoam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12266, 5756, NULL, NULL, NULL, '20098200', NULL, 'CLAUDIA YESIKA', NULL, 'OCSA GONZALES', NULL, 'cocsag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12267, 5757, NULL, NULL, NULL, '20142478', NULL, 'DIANA', NULL, 'OLAECHEA HUILLCA', NULL, 'dolaecheah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12268, 5758, NULL, NULL, NULL, '20011408', NULL, 'MARIBEL LUZ', NULL, 'OLAECHEA USCA', NULL, 'molaechea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12269, 5759, NULL, NULL, NULL, '20154677', NULL, 'NOELIA VALERY', NULL, 'OLAZABAL BEGAZO', NULL, 'nolazabalb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12270, 5760, NULL, NULL, NULL, '20164590', NULL, 'ALBERT CHRISTIAN', NULL, 'OLIVERA PEREIRA', NULL, 'aolivera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12271, 5761, NULL, NULL, NULL, '20172419', NULL, 'LUZ KAREN', NULL, 'OLIVERA RODRIGUEZ', NULL, 'loliveraro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12272, 5762, NULL, NULL, NULL, '20134353', NULL, 'YERALDINE HERMELINDA', NULL, 'OLLACHICA CANCINO', NULL, 'yollachicac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12273, 5763, NULL, NULL, NULL, '20085169', NULL, 'RICARDO SAMUEL', NULL, 'OLLACHICA SURI', NULL, 'rollachica@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12274, 5764, NULL, NULL, NULL, '20114138', NULL, 'MILAGROS TANIA', NULL, 'OPORTO YUNGA', NULL, 'moportoy@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12275, 5765, NULL, NULL, NULL, '20141334', NULL, 'DAMARY CELESTE', NULL, 'ORELLANA FERNANDEZ', NULL, 'dorellana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12276, 5766, NULL, NULL, NULL, '20163803', NULL, 'CRISTHIAN', NULL, 'ORTEGA RODRIGUEZ', NULL, 'cortegar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12277, 5767, NULL, NULL, NULL, '20131380', NULL, 'DALIA KRISTTEL', NULL, 'ORTIZ AGUILAR', NULL, 'dortiza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12278, 5768, NULL, NULL, NULL, '20151495', NULL, 'SHIRLEY PATRICIA', NULL, 'ORTIZ DELGADO', NULL, 'sortizd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12279, 5769, NULL, NULL, NULL, '20134205', NULL, 'GISVEL KARIM', NULL, 'ORTIZ PACOMPIA', NULL, 'gortiz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12280, 5770, NULL, NULL, NULL, '20144327', NULL, 'VIANNEY VANESSA', NULL, 'ORTIZ PUESCAS', NULL, 'vortiz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12281, 5771, NULL, NULL, NULL, '19992095', NULL, 'JUAN CARLOS', NULL, 'ORTIZ QUILLA', NULL, 'jortizq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12282, 5772, NULL, NULL, NULL, '20153967', NULL, 'DANIELA', NULL, 'ORUE DIPAZ', NULL, 'dorue@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12283, 5773, NULL, NULL, NULL, '20100522', NULL, 'JOSE JOAQUIN', NULL, 'OVIEDO CARPIO', NULL, 'joviedoca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12284, 5774, NULL, NULL, NULL, '20098252', NULL, 'JOSUE DAVID', NULL, 'OVIEDO LACUTA', NULL, 'joviedola@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12285, 5775, NULL, NULL, NULL, '20142471', NULL, 'ALEXANDER ALEX', NULL, 'OVIEDO SAYRA', NULL, 'aoviedos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12286, 5776, NULL, NULL, NULL, '20098291', NULL, 'DAVID DIEGO', NULL, 'OVIEDO TURPO', NULL, 'doviedot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12287, 5777, NULL, NULL, NULL, '20164676', NULL, 'ALEXANDER MARCELINO', NULL, 'OYOLA VALDIVIA', NULL, 'aoyola@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12288, 5778, NULL, NULL, NULL, '20153970', NULL, 'EDITH', NULL, 'PACCAYA PAUCCARA', NULL, 'epaccayap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12289, 5779, NULL, NULL, NULL, '20124116', NULL, 'ELVA SOLEDAD', NULL, 'PACCO CHOQUICAJIA', NULL, 'epaccoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12290, 5780, NULL, NULL, NULL, '20143763', NULL, 'ALISON MARGOT', NULL, 'PACCO HUAMANI', NULL, 'apacco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12291, 5781, NULL, NULL, NULL, '20104062', NULL, 'STEPHANIE', NULL, 'PACHECO BUSTINZA', NULL, 'spachecob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12292, 5782, NULL, NULL, NULL, '20134303', NULL, 'LEONARDO DAVID', NULL, 'PACHECO CCAMAQUE', NULL, 'lpachecocc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12293, 5783, NULL, NULL, NULL, '20122919', NULL, 'GERMAIN RODRIGO', NULL, 'PACHECO FLORES', NULL, 'gpachecof@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12294, 5784, NULL, NULL, NULL, '20154639', NULL, 'JOISE LUCERO', NULL, 'PACHECO FLORES', NULL, 'jpachecof@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12295, 5785, NULL, NULL, NULL, '20164721', NULL, 'ANDRE ELLIT', NULL, 'PACHECO PERRY', NULL, 'apachecope@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12296, 5786, NULL, NULL, NULL, '20170993', NULL, 'BRISSA FRANCESCA', NULL, 'PACHECO QUISPE', NULL, 'bpachecoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12297, 5787, NULL, NULL, NULL, '20164651', NULL, 'YULIANA IVETH', NULL, 'PACHECO QUISPE', NULL, 'ypachecoq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12298, 5788, NULL, NULL, NULL, '20041607', NULL, 'ANA ZENAYDA', NULL, 'PACHECO SOTOMAYOR', NULL, 'apachecoso@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12299, 5789, NULL, NULL, NULL, '20144424', NULL, 'FAVIO', NULL, 'PACHECO VASQUEZ', NULL, 'fpachecov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12300, 5790, NULL, NULL, NULL, '20124092', NULL, 'YANIRA AYLIN', NULL, 'PACO DIAZ', NULL, 'ypaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12301, 5791, NULL, NULL, NULL, '20062254', NULL, 'HELBER PERCY', NULL, 'PACO MIRANDA', NULL, 'hpacom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12302, 5792, NULL, NULL, NULL, '20144367', NULL, 'IVY JHEAN', NULL, 'PACO SARMIENTO', NULL, 'ipaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12303, 5793, NULL, NULL, NULL, '20144390', NULL, 'JELSON', NULL, 'PACORI MITA', NULL, 'jpacorim@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12304, 5794, NULL, NULL, NULL, '20095440', NULL, 'RODMI LEYVER', NULL, 'PACURI VITA', NULL, 'rpacuri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12305, 5795, NULL, NULL, NULL, '20133678', NULL, 'ANGYE FERNANDA', NULL, 'PAJA PUMA', NULL, 'apajap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12306, 5796, NULL, NULL, NULL, '20141361', NULL, 'RICHARD', NULL, 'PALLANI KANA', NULL, 'rpallani@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12307, 5797, NULL, NULL, NULL, '20143745', NULL, 'MIRLHEY DE LOS ANGELES', NULL, 'PALMA LIMAHUAYA', NULL, 'mpalma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12308, 5798, NULL, NULL, NULL, '20124183', NULL, 'HAROL JEYSON', NULL, 'PALMA NINA', NULL, 'hpalma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12309, 5799, NULL, NULL, NULL, '20174262', NULL, 'JOSEPH RYU', NULL, 'PALOMINO ALARICO', NULL, 'jpalominoala@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12310, 5800, NULL, NULL, NULL, '20164690', NULL, 'BRAYAN JOSE', NULL, 'PALOMINO FLORES', NULL, 'bpalominof@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12311, 5801, NULL, NULL, NULL, '20164739', NULL, 'CRISTHIAN ADOLFO', NULL, 'PALOMINO MAMANI', NULL, 'cpalominom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12312, 5802, NULL, NULL, NULL, '20174217', NULL, 'JOSE LUIS', NULL, 'PALOMINO ROJAS', NULL, 'jpalominor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12313, 5803, NULL, NULL, NULL, '20161625', NULL, 'ANA LUCIA', NULL, 'PALOMINO TITO', NULL, 'apalominot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12314, 5804, NULL, NULL, NULL, '20134307', NULL, 'ADRIAN', NULL, 'PANCA BRAVO', NULL, 'apanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12315, 5805, NULL, NULL, NULL, '20162141', NULL, 'LUIS DAVID', NULL, 'PANCA FERNANDEZ', NULL, 'lpancaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12316, 5806, NULL, NULL, NULL, '20054851', NULL, 'ORLANDO DAVID', NULL, 'PANCCA TURPO', NULL, 'opanccat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12317, 5807, NULL, NULL, NULL, '20153957', NULL, 'BERUSHKA NIEVES', NULL, 'PANIBRA MENDIVEL', NULL, 'bpanibra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12318, 5808, NULL, NULL, NULL, '20133882', NULL, 'IVONNE MILAGROS', NULL, 'PANUERA CHOQUE', NULL, 'ipanuera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12319, 5809, NULL, NULL, NULL, '20074564', NULL, 'JESUS GONZALO', NULL, 'PANUERA CHOQUE', NULL, 'jpanuerac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12320, 5810, NULL, NULL, NULL, '20124102', NULL, 'KEWIN JHON JAIRO', NULL, 'PARCO CORRALES', NULL, 'kparco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12321, 5811, NULL, NULL, NULL, '20154649', NULL, 'NATHALY ANAIS', NULL, 'PAREDES CARI', NULL, 'nparedescar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12322, 5812, NULL, NULL, NULL, '20174256', NULL, 'CECILIA CLARA', NULL, 'PAREDES MAMANI', NULL, 'cparedesmam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12323, 5813, NULL, NULL, NULL, '20095910', NULL, 'VICTOR ALEJANDRO', NULL, 'PAREDES QUICO', NULL, 'vparedesq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12324, 5814, NULL, NULL, NULL, '20174292', NULL, 'PIERO ENZO', NULL, 'PAREDES VALDIVIA', NULL, 'pparedesv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12325, 5815, NULL, NULL, NULL, '20134280', NULL, 'SARA GLADYS', NULL, 'PARIAPAZA VARGAS', NULL, 'spariapaza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12326, 5816, NULL, NULL, NULL, '20144387', NULL, 'MAGALY LILY', NULL, 'PARILLO SUPO', NULL, 'mparillo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12327, 5817, NULL, NULL, NULL, '20173661', NULL, 'LALY XELENE', NULL, 'PARIONA SONCCO', NULL, 'lpariona@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12328, 5818, NULL, NULL, NULL, '20154562', NULL, 'DAYANA ROCIO', NULL, 'PARISACA MATURANA', NULL, 'dparisaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12329, 5819, NULL, NULL, NULL, '20163810', NULL, 'CARMEN DEBORA', NULL, 'PARIZACA SUCARI', NULL, 'cparizacas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12330, 5820, NULL, NULL, NULL, '20143762', NULL, 'MARIO AUGUSTO', NULL, 'PARIZACA SUCARI', NULL, 'mparizaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12331, 5821, NULL, NULL, NULL, '20163785', NULL, 'JOSUE DANIEL', NULL, 'PARIZAKA CRUZ', NULL, 'jparizakac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12332, 5822, NULL, NULL, NULL, '20021557', NULL, 'ALBERTO LEOPOLDO', NULL, 'PARRA QUISPE', NULL, 'aparra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12333, 5823, NULL, NULL, NULL, '20114200', NULL, 'JOSE MIGUEL', NULL, 'PASTOR BORNAZ', NULL, 'jpastorb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12334, 5824, NULL, NULL, NULL, '20154712', NULL, 'WENDY MARGOTH', NULL, 'PASTOR BORNAZ', NULL, 'wpastor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12335, 5825, NULL, NULL, NULL, '20174237', NULL, 'ANDREA MILAGROS', NULL, 'PASTOR LANCHIPA', NULL, 'apastor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12336, 5826, NULL, NULL, NULL, '20144418', NULL, 'PAULA LUCIA', NULL, 'PASTOR ZUNIGA', NULL, 'ppastorz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12337, 5827, NULL, NULL, NULL, '20152663', NULL, 'JHONN ALBERT', NULL, 'PATA HUARACALLO', NULL, 'jpata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12338, 5828, NULL, NULL, NULL, '20096736', NULL, 'CHRISTIAN ALEX', NULL, 'PATA ROQUE', NULL, 'cpata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12339, 5829, NULL, NULL, NULL, '20161028', NULL, 'ANGELICA PATRICIA', NULL, 'PAUCA MANTILLA', NULL, 'apaucama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12340, 5830, NULL, NULL, NULL, '20153972', NULL, 'FRAN CRISTIAN', NULL, 'PAUCAR ALMANZA', NULL, 'fpaucar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12341, 5831, NULL, NULL, NULL, '20134211', NULL, 'MARISOL', NULL, 'PAUCAR PACHECO', NULL, 'mpaucarp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12342, 5832, NULL, NULL, NULL, '20085263', NULL, 'CARLO ANDRE', NULL, 'PAULET RODRIGUEZ', NULL, 'cpauletr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12343, 5833, NULL, NULL, NULL, '20154674', NULL, 'NATALY DORA', NULL, 'PAZ LAYME', NULL, 'npazl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12344, 5834, NULL, NULL, NULL, '20134259', NULL, 'GRETA', NULL, 'PAZ RIVERA', NULL, 'gpazri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12345, 5835, NULL, NULL, NULL, '20161053', NULL, 'YEANY', NULL, 'PELAYO LIMA', NULL, 'ypelayo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12346, 5836, NULL, NULL, NULL, '20154720', NULL, 'MILENA STACEY', NULL, 'PENA LLANQUECHA', NULL, 'mpenal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12347, 5837, NULL, NULL, NULL, '20134229', NULL, 'VALERIA LUCERO', NULL, 'PENA SARAVIA', NULL, 'vpenas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12348, 5838, NULL, NULL, NULL, '20151494', NULL, 'FIORELLA YANELY', NULL, 'PENALBA HANAMPA', NULL, 'fpenalba@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12349, 5839, NULL, NULL, NULL, '20144318', NULL, 'PIEYTHI EVELYN', NULL, 'PERALES MARROQUIN', NULL, 'pperales@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12350, 5840, NULL, NULL, NULL, '20153980', NULL, 'SANTOS', NULL, 'PERALTA HUAYNA', NULL, 'speralta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12351, 5841, NULL, NULL, NULL, '20164615', NULL, 'JOEL ALEJANDRO', NULL, 'PEREIRA VALDIVIA', NULL, 'jpereira@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12352, 5842, NULL, NULL, NULL, '20164716', NULL, 'JORGE ALONZO', NULL, 'PEREZ CHAUCA', NULL, 'jperezch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12353, 5843, NULL, NULL, NULL, '20080354', NULL, 'CARLOS LUIS MIGUEL', NULL, 'PEREZ CHUCTAYA', NULL, 'cperezch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12354, 5844, NULL, NULL, NULL, '20152664', NULL, 'NOHELIA SOLEILY', NULL, 'PEREZ HUAMANI', NULL, 'nperez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12355, 5845, NULL, NULL, NULL, '20084421', NULL, 'JEFFRY LUIS', NULL, 'PEREZ VELARDE', NULL, 'jperezv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12356, 5846, NULL, NULL, NULL, '20163798', NULL, 'WASHINTON DEIVIS', NULL, 'PHOCCO CHARCA', NULL, 'wphocco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12357, 5847, NULL, NULL, NULL, '20124170', NULL, 'DANTHE JHOKLER', NULL, 'PILCO RENDON', NULL, 'dpilcor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12358, 5848, NULL, NULL, NULL, '20075089', NULL, 'JHOSSEFF MARCOS', NULL, 'PILCO RENDON', NULL, 'jpilcor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12359, 5849, NULL, NULL, NULL, '20164675', NULL, 'KARLA MELISSA', NULL, 'PIMENTEL MOGROVEJO', NULL, 'kpimentel@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12360, 5850, NULL, NULL, NULL, '20111679', NULL, 'GLICERIO', NULL, 'PINTO APAZA', NULL, 'gpintoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12361, 5851, NULL, NULL, NULL, '19962216', NULL, 'GIANCARLO', NULL, 'PINTO MEDINA', NULL, 'gpintome@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12362, 5852, NULL, NULL, NULL, '20062362', NULL, 'JAVIER ARTURO', NULL, 'PINTO MEDINA', NULL, 'jpintom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12363, 5853, NULL, NULL, NULL, '20164588', NULL, 'YARID MILAGROS', NULL, 'PINTO TACO', NULL, 'ypintota@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12364, 5854, NULL, NULL, NULL, '20164592', NULL, 'ALISSON ANTUANE', NULL, 'PISFIL COTRADO', NULL, 'apisfil@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12365, 5855, NULL, NULL, NULL, '20133695', NULL, 'FIORELLA MILAGROS', NULL, 'PIZARRO CHAMBILLA', NULL, 'fpizarroc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12366, 5856, NULL, NULL, NULL, '20144337', NULL, 'JUDITH', NULL, 'POLO TUIRO', NULL, 'jpolo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12367, 5857, NULL, NULL, NULL, '20124112', NULL, 'HARRIET JANIA', NULL, 'PONCE ASTETE', NULL, 'hponcea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12368, 5858, NULL, NULL, NULL, '20174246', NULL, 'PAULO ANDRE', NULL, 'PONCE ASTETE', NULL, 'pponcea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12369, 5859, NULL, NULL, NULL, '20112126', NULL, 'CARLA', NULL, 'PONCE AYERBE', NULL, 'cponcea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12370, 5860, NULL, NULL, NULL, '20144359', NULL, 'LUIS ANGEL', NULL, 'PONCE CUSI', NULL, 'lponcecu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12371, 5861, NULL, NULL, NULL, '20143879', NULL, 'SERGIO RAUL', NULL, 'PONCE GONZALES', NULL, 'sponce@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12372, 5862, NULL, NULL, NULL, '20161055', NULL, 'MAURICIO RENATO', NULL, 'PONCE MERCADO', NULL, 'mponcemer@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12373, 5863, NULL, NULL, NULL, '20174236', NULL, 'ALVARO VIDAL', NULL, 'PONCE PUMA', NULL, 'aponcep@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12374, 5864, NULL, NULL, NULL, '19940848', NULL, 'AMPARO', NULL, 'PORTILLO AGUILAR', NULL, 'aportilloa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12375, 5865, NULL, NULL, NULL, '20164724', NULL, 'AARON ANDREE', NULL, 'PORTOCARRERO LOPEZ', NULL, 'aportocarrerol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12376, 5866, NULL, NULL, NULL, '20104448', NULL, 'CARLOS JHOEL', NULL, 'PORTOCARRERO QUICANA', NULL, 'cportocarreroq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12377, 5867, NULL, NULL, NULL, '20174331', NULL, 'LIDIA ESTHER', NULL, 'PRADO CABANA', NULL, 'lpradoca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12378, 5868, NULL, NULL, NULL, '20174333', NULL, 'SERGIO ELMER', NULL, 'PUMA APAZA', NULL, 'spumaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12379, 5869, NULL, NULL, NULL, '20134314', NULL, 'CRISTIAN ROMARIO', NULL, 'PUMA CAHUI', NULL, 'cpumac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12380, 5870, NULL, NULL, NULL, '20174335', NULL, 'FANY NOEMI', NULL, 'PUMA CCASA', NULL, 'fpumacc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12381, 5871, NULL, NULL, NULL, '20151483', NULL, 'STEPHANI', NULL, 'PUMA FIGUEROA', NULL, 'spumaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12382, 5872, NULL, NULL, NULL, '20140369', NULL, 'ZOYKA LIGIA', NULL, 'PUMA ROQUE', NULL, 'zpumar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12383, 5873, NULL, NULL, NULL, '20133686', NULL, 'TANIA LUCERO', NULL, 'PUMACAHUA MAMANI', NULL, 'tpumacahua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12384, 5874, NULL, NULL, NULL, '20154681', NULL, 'JHOEL STEVEN', NULL, 'PUMACHARA ENRIQUEZ', NULL, 'jpumacharae@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12385, 5875, NULL, NULL, NULL, '20140349', NULL, 'KARLA VANESSA', NULL, 'PUMATANCA LLAIQUI', NULL, 'kpumatanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12386, 5876, NULL, NULL, NULL, '20174290', NULL, 'CLAUDIA CAROLINA', NULL, 'PURIZACA PAJUELO', NULL, 'cpurizaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12387, 5877, NULL, NULL, NULL, '20123823', NULL, 'LUIS HERNAN', NULL, 'QQUENTA TINTAYA', NULL, 'lqquentat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12388, 5878, NULL, NULL, NULL, '20144313', NULL, 'MARCELO', NULL, 'QUEHUE PAUCARA', NULL, 'mquehue@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12389, 5879, NULL, NULL, NULL, '20164715', NULL, 'ESTHER', NULL, 'QUENTA LLANO', NULL, 'equental@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12390, 5880, NULL, NULL, NULL, '19872828', NULL, 'CESAR AUGUSTO', NULL, 'QUEVEDO ANCCO', NULL, 'cquevedoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12391, 5881, NULL, NULL, NULL, '20164702', NULL, 'HENRY DENIS', NULL, 'QUICANA CUAQUIRA', NULL, 'hquicana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12392, 5882, NULL, NULL, NULL, '20072430', NULL, 'ERNESTO ESADIAS', NULL, 'QUICANO CHURO', NULL, 'equicanoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12393, 5883, NULL, NULL, NULL, '20134538', NULL, 'BRIGITH KRINTY', NULL, 'QUILCA BARRERA', NULL, 'bquilcab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12394, 5884, NULL, NULL, NULL, '20134202', NULL, 'IRIS ANTONELA', NULL, 'QUINTA VARGAS', NULL, 'iquinta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12395, 5885, NULL, NULL, NULL, '20174334', NULL, 'DANIEL RICARDO', NULL, 'QUINTANA AYAQUE', NULL, 'dquintanaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12396, 5886, NULL, NULL, NULL, '20163790', NULL, 'CHRISTIAN MATIAS', NULL, 'QUINTANA CHOQUE', NULL, 'cquintanac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12397, 5887, NULL, NULL, NULL, '20143727', NULL, 'PATRICIA DEBORA', NULL, 'QUISPE AGUILAR', NULL, 'pquispe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12398, 5888, NULL, NULL, NULL, '20091759', NULL, 'WILLIAM', NULL, 'QUISPE ARIAS', NULL, 'wquispeari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12399, 5889, NULL, NULL, NULL, '20141370', NULL, 'SHIRLEY MIRELLA', NULL, 'QUISPE CARCAUSTO', NULL, 'squispecar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12400, 5890, NULL, NULL, NULL, '20164616', NULL, 'HELBER RODRIGO', NULL, 'QUISPE CAYO', NULL, 'hquispecay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12401, 5891, NULL, NULL, NULL, '20130868', NULL, 'ROBERT', NULL, 'QUISPE CCAPA', NULL, 'rquispeccap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12402, 5892, NULL, NULL, NULL, '20144429', NULL, 'JANY XIMENA', NULL, 'QUISPE CCASO', NULL, 'jquispeccas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12403, 5893, NULL, NULL, NULL, '20112670', NULL, 'CYNTHIA IVANNEA', NULL, 'QUISPE CENTENO', NULL, 'cquispece@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12404, 5894, NULL, NULL, NULL, '20104249', NULL, 'EDGAR', NULL, 'QUISPE CHALCO', NULL, 'equispechalc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12405, 5895, NULL, NULL, NULL, '20161027', NULL, 'JUDITH LUZMILA', NULL, 'QUISPE CHAMBILLA', NULL, 'jquispechambi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12406, 5896, NULL, NULL, NULL, '20174332', NULL, 'ALVARO RICHARD', NULL, 'QUISPE CHAMPI', NULL, 'aquispechamp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12407, 5897, NULL, NULL, NULL, '20082164', NULL, 'MARITA YULMA', NULL, 'QUISPE CHAPI', NULL, 'mquispechap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12408, 5898, NULL, NULL, NULL, '20133689', NULL, 'SAUL FRANCO', NULL, 'QUISPE CHIPANA', NULL, 'squispechi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12409, 5899, NULL, NULL, NULL, '20161024', NULL, 'ALEXANDRA NAYELI', NULL, 'QUISPE CHOQUE', NULL, 'aquispecho@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12410, 5900, NULL, NULL, NULL, '20154449', NULL, 'MIRELLA MABEL', NULL, 'QUISPE CHOQUEHUANCA', NULL, 'mquispechoque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12411, 5901, NULL, NULL, NULL, '20041618', NULL, 'SILVIA ROSARIO', NULL, 'QUISPE CONDORI', NULL, 'squispec@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12412, 5902, NULL, NULL, NULL, '20091234', NULL, 'FERNANDO SAUL', NULL, 'QUISPE CUNO', NULL, 'fquispecu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12413, 5903, NULL, NULL, NULL, '20153952', NULL, 'CARLOS ANDREE', NULL, 'QUISPE DIAZ', NULL, 'cquispedi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12414, 5904, NULL, NULL, NULL, '20134251', NULL, 'NORMA MILAGROS', NULL, 'QUISPE ESQUIVEL', NULL, 'nquispee@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12415, 5905, NULL, NULL, NULL, '20082984', NULL, 'CRISTIAN', NULL, 'QUISPE GUZMAN', NULL, 'cquispegu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12416, 5906, NULL, NULL, NULL, '20154705', NULL, 'SHEYLLA MARIANA', NULL, 'QUISPE HERNANDEZ', NULL, 'squispehe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12417, 5907, NULL, NULL, NULL, '20121328', NULL, 'RAMIRO RONNY', NULL, 'QUISPE HUALLPA', NULL, 'rquispehuall@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12418, 5908, NULL, NULL, NULL, '20143756', NULL, 'YEMIRA ISABEL', NULL, 'QUISPE HUAMAN', NULL, 'yquispehu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(12419, 5909, NULL, NULL, NULL, '20173680', NULL, 'RUTH NILDA', NULL, 'QUISPE HUANCA', NULL, 'ruquispeh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12420, 5910, NULL, NULL, NULL, '20161032', NULL, 'YORISA MARIBEL', NULL, 'QUISPE HUAYLLA', NULL, 'yquispehuay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12421, 5911, NULL, NULL, NULL, '20124117', NULL, 'ANITA LIZ', NULL, 'QUISPE LABRA', NULL, 'aquispelab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12422, 5912, NULL, NULL, NULL, '20174323', NULL, 'CAMILA YASMIN', NULL, 'QUISPE LAYME', NULL, 'cquispelay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12423, 5913, NULL, NULL, NULL, '20162138', NULL, 'KARLA SABINA', NULL, 'QUISPE LOZANO', NULL, 'kquispeloz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12424, 5914, NULL, NULL, NULL, '20164620', NULL, 'NARLY ANALY', NULL, 'QUISPE LUPACA', NULL, 'nquispelu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12425, 5915, NULL, NULL, NULL, '20174291', NULL, 'MILAGROS NORELIA', NULL, 'QUISPE MOLINA', NULL, 'mquispemol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12426, 5916, NULL, NULL, NULL, '20141347', NULL, 'SHARON JAZMIN JOHANA', NULL, 'QUISPE NINA', NULL, 'squispen@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12427, 5917, NULL, NULL, NULL, '20152113', NULL, 'ERIC JOEL', NULL, 'QUISPE PAYEHUANCA', NULL, 'equispepay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12428, 5918, NULL, NULL, NULL, '20174304', NULL, 'LUZ YAHIRA', NULL, 'QUISPE PFOCCO', NULL, 'lquispepf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12429, 5919, NULL, NULL, NULL, '20133684', NULL, 'AQUILINA CANDELARIA', NULL, 'QUISPE QUISPE', NULL, 'aquispequis@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12430, 5920, NULL, NULL, NULL, '20134219', NULL, 'MAGALY SASI', NULL, 'QUISPE QUISPE', NULL, 'mquispequi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12431, 5921, NULL, NULL, NULL, '20164610', NULL, 'MIRIAM YESENIA', NULL, 'QUISPE QUISPE', NULL, 'mquispequispe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12432, 5922, NULL, NULL, NULL, '20142006', NULL, 'ELIDA JOVANNA', NULL, 'QUISPE SOSA', NULL, 'equispes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12433, 5923, NULL, NULL, NULL, '20111480', NULL, 'DARWIN DANIEL', NULL, 'QUISPE SOTO', NULL, 'dquispesot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12434, 5924, NULL, NULL, NULL, '20153969', NULL, 'WILDER IGIDIO', NULL, 'QUISPE SUCLLE', NULL, 'wquispes@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12435, 5925, NULL, NULL, NULL, '20144366', NULL, 'ROXANA ELIZABETH', NULL, 'QUISPE SUMIRE', NULL, 'rquispesumir@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12436, 5926, NULL, NULL, NULL, '20173676', NULL, 'RICARDO JOSE', NULL, 'QUISPE TACO', NULL, 'rquispetaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12437, 5927, NULL, NULL, NULL, '20124165', NULL, 'JESUS KEVIN', NULL, 'QUISPE TAYPE', NULL, 'jquispetay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12438, 5928, NULL, NULL, NULL, '20151511', NULL, 'JUAN CARLOS', NULL, 'QUISPE TICA', NULL, 'jquispeti@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12439, 5929, NULL, NULL, NULL, '20134312', NULL, 'DEISY JUDITH', NULL, 'QUISPE UCHARO', NULL, 'dquispeu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12440, 5930, NULL, NULL, NULL, '20163776', NULL, 'BETSI ODALIS', NULL, 'QUISPE VILLALBA', NULL, 'bquispev@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12441, 5931, NULL, NULL, NULL, '20162145', NULL, 'YAISSLY LIZETH', NULL, 'QUISPE YERBA', NULL, 'yquispey@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12442, 5932, NULL, NULL, NULL, '20134315', NULL, 'OLGA SONIA', NULL, 'QUISPE YUCRA', NULL, 'oquispey@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12443, 5933, NULL, NULL, NULL, '20163777', NULL, 'HILDA DANIELA', NULL, 'QUISPETUPAC PUMACOTA', NULL, 'hquispetupac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12444, 5934, NULL, NULL, NULL, '20174307', NULL, 'JULIO CESAR', NULL, 'RAFAELE MESCCO', NULL, 'jrafaele@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12445, 5935, NULL, NULL, NULL, '20164680', NULL, 'LIDIA LIZ', NULL, 'RAMIREZ CARPIO', NULL, 'lramirezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12446, 5936, NULL, NULL, NULL, '20171004', NULL, 'PAMELA ALESSANDRA', NULL, 'RAMIREZ HUARCA', NULL, 'pramirezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12447, 5937, NULL, NULL, NULL, '20174190', NULL, 'NAYELY DEL CARMEN', NULL, 'RAMIREZ LUPACA', NULL, 'nramirezlu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12448, 5938, NULL, NULL, NULL, '20104068', NULL, 'ROMAN ALEJANDRO', NULL, 'RAMIREZ LUQUE', NULL, 'rramirezlu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12449, 5939, NULL, NULL, NULL, '20144374', NULL, 'LUCY', NULL, 'RAMIREZ MOLINA', NULL, 'lramirezmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12450, 5940, NULL, NULL, NULL, '20134275', NULL, 'ALEJANDRA GERALDINE', NULL, 'RAMIREZ TORRES', NULL, 'aramirezto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12451, 5941, NULL, NULL, NULL, '20174207', NULL, 'HAYLEY FIORELA', NULL, 'RAMIREZ ZUNIGA', NULL, 'hramirezz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12452, 5942, NULL, NULL, NULL, '20164688', NULL, 'JANETH IVANIA', NULL, 'RAMOS ACERO', NULL, 'jramosac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12453, 5943, NULL, NULL, NULL, '20111002', NULL, 'ERNESTO', NULL, 'RAMOS APAZA', NULL, 'eramosap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12454, 5944, NULL, NULL, NULL, '20084430', NULL, 'DIEGO ALFREDO', NULL, 'RAMOS BERNAL', NULL, 'dramosb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12455, 5945, NULL, NULL, NULL, '20171002', NULL, 'CRISTIAN JOANY', NULL, 'RAMOS FLORES', NULL, 'cramosflor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12456, 5946, NULL, NULL, NULL, '20152662', NULL, 'LIDIANA MAGALY', NULL, 'RAMOS GALLEGOS', NULL, 'lramosga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12457, 5947, NULL, NULL, NULL, '20153963', NULL, 'EVANI DEL ROSARIO', NULL, 'RAMOS GUZMAN', NULL, 'eramosg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12458, 5948, NULL, NULL, NULL, '20124094', NULL, 'DAYNYTH MELISSA', NULL, 'RAMOS HUAMANI', NULL, 'dramoshua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12459, 5949, NULL, NULL, NULL, '20173671', NULL, 'GRICEL YASMIN', NULL, 'RAMOS RAMOS', NULL, 'gramosra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12460, 5950, NULL, NULL, NULL, '20144416', NULL, 'DIARELY CELINDA', NULL, 'RAMOS SALCEDO', NULL, 'dramoss@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12461, 5951, NULL, NULL, NULL, '20144427', NULL, 'KAREN JANETH', NULL, 'RAMOS TICONA', NULL, 'kramosti@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12462, 5952, NULL, NULL, NULL, '20151496', NULL, 'JOSE', NULL, 'RAMOS USCA', NULL, 'jramosu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12463, 5953, NULL, NULL, NULL, '20074578', NULL, 'KARLA DANIELA', NULL, 'RAZURI RUIZ', NULL, 'krazuri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12464, 5954, NULL, NULL, NULL, '20174287', NULL, 'KAMILA MISHELL', NULL, 'RECABARREN CARBAJAL', NULL, 'krecabarren@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12465, 5955, NULL, NULL, NULL, '20154608', NULL, 'TATIANA', NULL, 'REGENTE CORNEJO', NULL, 'tregente@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12466, 5956, NULL, NULL, NULL, '20134050', NULL, 'OSCAR ALONSO', NULL, 'REVILLA QUISPE', NULL, 'orevillaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12467, 5957, NULL, NULL, NULL, '20134273', NULL, 'RICHARD BRAYAND DI', NULL, 'REYNA LORENA', NULL, 'rreynal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12468, 5958, NULL, NULL, NULL, '20164661', NULL, 'SHEYLA YZAMAR', NULL, 'RICALDE ESPINOZA', NULL, 'sricalde@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12469, 5959, NULL, NULL, NULL, '20154630', NULL, 'CARMEN PATRICIA', NULL, 'RIEGA CHALCO', NULL, 'criegach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12470, 5960, NULL, NULL, NULL, '20164696', NULL, 'JEHU DAVID', NULL, 'RIOS QUISPE', NULL, 'jrios@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12471, 5961, NULL, NULL, NULL, '20154640', NULL, 'FRANK ZIELKO JULEN', NULL, 'RIVAS ARICA', NULL, 'frivasa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12472, 5962, NULL, NULL, NULL, '20174233', NULL, 'JEAMPIERE JOHANES', NULL, 'RIVERA LAZO', NULL, 'jriveral@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12473, 5963, NULL, NULL, NULL, '20144314', NULL, 'LUIS ALBERTO', NULL, 'RIVERA RAMIREZ', NULL, 'lriverar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12474, 5964, NULL, NULL, NULL, '20171026', NULL, 'KARLA MICHELY', NULL, 'RIVEROS ANCAJIMA', NULL, 'kriveros@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12475, 5965, NULL, NULL, NULL, '20164635', NULL, 'ESTRELLA MILAGROS', NULL, 'RIVEROS DE LA CRUZ', NULL, 'eriverosd@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12476, 5966, NULL, NULL, NULL, '20134232', NULL, 'JEANPIER CARLEZISS', NULL, 'RIVEROS HUARCA', NULL, 'jriveroshu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12477, 5967, NULL, NULL, NULL, '20073828', NULL, 'MAYRA DENISSE', NULL, 'RIVEROS MUNOZ', NULL, 'mriverosmu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12478, 5968, NULL, NULL, NULL, '20174252', NULL, 'JOSE CARLOS', NULL, 'ROCHA TORRES', NULL, 'jrochator@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12479, 5969, NULL, NULL, NULL, '20134298', NULL, 'GRASE DARLY', NULL, 'RODRIGO CAHUI', NULL, 'grodrigoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12480, 5970, NULL, NULL, NULL, '20164596', NULL, 'FRANK JOEL', NULL, 'RODRIGO CHULLUNQUIA', NULL, 'frodrigoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12481, 5971, NULL, NULL, NULL, '20143769', NULL, 'ANGELA JUDITH', NULL, 'RODRIGUEZ AYNAYANQUE', NULL, 'arodriguez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12482, 5972, NULL, NULL, NULL, '20172441', NULL, 'KAREN', NULL, 'RODRIGUEZ CCOSI', NULL, 'krodriguezcc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12483, 5973, NULL, NULL, NULL, '20144368', NULL, 'LESLY ALEXANDRA', NULL, 'RODRIGUEZ CHAVEZ', NULL, 'lrodriguezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12484, 5974, NULL, NULL, NULL, '20174234', NULL, 'MARIA ANGELA', NULL, 'RODRIGUEZ CRUZ', NULL, 'mrodriguezcr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12485, 5975, NULL, NULL, NULL, '20144340', NULL, 'ARCELIA GUADALUPE', NULL, 'RODRIGUEZ GONGORA', NULL, 'arodriguezgo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12486, 5976, NULL, NULL, NULL, '20164717', NULL, 'ZUMICO LISBETH', NULL, 'RODRIGUEZ HUARACA', NULL, 'zrodriguezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12487, 5977, NULL, NULL, NULL, '20064698', NULL, 'HARRY MAXIMILIANO', NULL, 'RODRIGUEZ HUARHUA', NULL, 'hrodriguezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12488, 5978, NULL, NULL, NULL, '20170997', NULL, 'PATRICK BERRY', NULL, 'RODRIGUEZ MARIACA', NULL, 'prodriguezmar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12489, 5979, NULL, NULL, NULL, '20160232', NULL, 'JOSE LUIS', NULL, 'RODRIGUEZ PUMA', NULL, 'jrodriguezpu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12490, 5980, NULL, NULL, NULL, '20143735', NULL, 'FLOR DE LOS ANGELES', NULL, 'RODRIGUEZ RAMOS', NULL, 'frodriguezra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12491, 5981, NULL, NULL, NULL, '20074582', NULL, 'ANTONIO LEONARDO', NULL, 'RODRIGUEZ RODRIGUEZ', NULL, 'arodriguezrod@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12492, 5982, NULL, NULL, NULL, '19992546', NULL, 'JULIO CESAR', NULL, 'RODRIGUEZ YUPANQUI', NULL, 'jrodriguezyu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12493, 5983, NULL, NULL, NULL, '20172424', NULL, 'JAZMIN DANIELA', NULL, 'ROJAS AYALA', NULL, 'jrojasay@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12494, 5984, NULL, NULL, NULL, '20144326', NULL, 'GISELA YESICA', NULL, 'ROJAS CHAVEZ', NULL, 'grojas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12495, 5985, NULL, NULL, NULL, '20174218', NULL, 'EDUARDO ALONSO', NULL, 'ROJAS CHOQUEHUANCA', NULL, 'erojasc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12496, 5986, NULL, NULL, NULL, '20104443', NULL, 'ROGER LUIS', NULL, 'ROJAS LUNA', NULL, 'rrojaslu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12497, 5987, NULL, NULL, NULL, '20163783', NULL, 'CARLOS FERNANDO', NULL, 'ROJAS PALOMINO', NULL, 'crojasp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12498, 5988, NULL, NULL, NULL, '20143751', NULL, 'EDITH', NULL, 'ROMAN HUAYHUAS', NULL, 'eromanh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12499, 5989, NULL, NULL, NULL, '20095355', NULL, 'SANDRO', NULL, 'ROMERO BELLIDO', NULL, 'sromerob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12500, 5990, NULL, NULL, NULL, '20154566', NULL, 'ABEL DIEGO FERNANDO', NULL, 'ROMERO COLONA', NULL, 'aromerocol@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12501, 5991, NULL, NULL, NULL, '20151516', NULL, 'DIANA CAROLINA', NULL, 'ROMERO HUAMANI', NULL, 'dromeroh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12502, 5992, NULL, NULL, NULL, '20131357', NULL, 'GABRIELA ISABEL', NULL, 'RONDAN FUENTES', NULL, 'grondanf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12503, 5993, NULL, NULL, NULL, '20163804', NULL, 'SOLIMAR PAOLA', NULL, 'RONDAN LLANOS', NULL, 'srondan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12504, 5994, NULL, NULL, NULL, '20173666', NULL, 'CRISTHIAN ALEX', NULL, 'RONDON CORTEZ', NULL, 'crondonco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12505, 5995, NULL, NULL, NULL, '20164700', NULL, 'FLAVIO ENRIQUE', NULL, 'RONDON GUERREROS', NULL, 'frondon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12506, 5996, NULL, NULL, NULL, '20134304', NULL, 'YADIRA YOHANA', NULL, 'ROQUE ARENAS', NULL, 'yroquea@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12507, 5997, NULL, NULL, NULL, '20133258', NULL, 'ROSMERY', NULL, 'ROQUE JARA', NULL, 'rroquej@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12508, 5998, NULL, NULL, NULL, '20164718', NULL, 'VAMMER JAMPIER', NULL, 'ROQUE MAMANI', NULL, 'vroque@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12509, 5999, NULL, NULL, NULL, '20134296', NULL, 'DANIELA CLARISA', NULL, 'ROQUE PARANCA', NULL, 'droquepar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12510, 6000, NULL, NULL, NULL, '20070749', NULL, 'DIEGO EDUARDO', NULL, 'ROQUE PARQUE', NULL, 'droquep@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12511, 6001, NULL, NULL, NULL, '20096612', NULL, 'DUAN DERLY', NULL, 'ROSADO VALDIVIA', NULL, 'drosado@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12512, 6002, NULL, NULL, NULL, '20174201', NULL, 'MIRELLA ANGELIK', NULL, 'ROSADO VASQUEZ', NULL, 'mrosadov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12513, 6003, NULL, NULL, NULL, '20134361', NULL, 'MANUEL MASCIEL', NULL, 'ROSALES MARREROS', NULL, 'mrosalesm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12514, 6004, NULL, NULL, NULL, '20164723', NULL, 'KEVIN BRAYAN', NULL, 'ROSAS ESTRADA', NULL, 'krosas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12515, 6005, NULL, NULL, NULL, '20098245', NULL, 'LIDIA ROSAURA', NULL, 'ROSAS LLASA', NULL, 'lrosasl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12516, 6006, NULL, NULL, NULL, '20163792', NULL, 'JESUS', NULL, 'RUIZ CARBAJAL', NULL, 'jruizc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12517, 6007, NULL, NULL, NULL, '20164720', NULL, 'WILLIAN ALEXANDER', NULL, 'RUIZ LLAMOCA', NULL, 'wruizl@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12518, 6008, NULL, NULL, NULL, '20162143', NULL, 'VALERIA ROSA', NULL, 'RUIZ PHOCO', NULL, 'vruiz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12519, 6009, NULL, NULL, NULL, '20141383', NULL, 'ELIZABETH ROCIO', NULL, 'SAANA CONDORI', NULL, 'esaana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12520, 6010, NULL, NULL, NULL, '20164658', NULL, 'KARELY ISABEL', NULL, 'SALAMANCA CORAHUA', NULL, 'ksalamanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12521, 6011, NULL, NULL, NULL, '20121338', NULL, 'DIEGO ANDRE', NULL, 'SALAS AMESQUITA', NULL, 'dsalasam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12522, 6012, NULL, NULL, NULL, '20141384', NULL, 'KARLA ALESSANDRA', NULL, 'SALAS BERROA', NULL, 'ksalas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12523, 6013, NULL, NULL, NULL, '20153962', NULL, 'ALLISON', NULL, 'SALAS IRUS', NULL, 'asalasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12524, 6014, NULL, NULL, NULL, '20072450', NULL, 'JAMES JAVIER', NULL, 'SALAS MANZANO', NULL, 'jsalasman@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12525, 6015, NULL, NULL, NULL, '20174316', NULL, 'MIGUEL ANGEL', NULL, 'SALAS MOTTA', NULL, 'msalasmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12526, 6016, NULL, NULL, NULL, '20098320', NULL, 'MALENA EDITH', NULL, 'SALAS PARODI', NULL, 'msalasp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12527, 6017, NULL, NULL, NULL, '20134269', NULL, 'GERALDINE CHRISTEEL', NULL, 'SALAS VASQUEZ', NULL, 'gsalasva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12528, 6018, NULL, NULL, NULL, '20101068', NULL, 'ROGER FERNANDO', NULL, 'SALAZAR BARRIOS', NULL, 'rsalazarb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12529, 6019, NULL, NULL, NULL, '20174208', NULL, 'DIEGA ALESSANDRA', NULL, 'SALAZAR CHACON', NULL, 'dsalazarch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12530, 6020, NULL, NULL, NULL, '20153968', NULL, 'JENIFFER CARLA', NULL, 'SALAZAR CHAVEZ', NULL, 'jsalazarch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12531, 6021, NULL, NULL, NULL, '20160867', NULL, 'JESUS MANUEL', NULL, 'SALAZAR FERNANDEZ', NULL, 'jsalazarf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12532, 6022, NULL, NULL, NULL, '20134200', NULL, 'ANGELICA LINDAURA', NULL, 'SALAZAR HUAMANI', NULL, 'asalazarhu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12533, 6023, NULL, NULL, NULL, '20123830', NULL, 'JOSH MICHAEL', NULL, 'SALAZAR MEDINA', NULL, 'jsalazarm@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12534, 6024, NULL, NULL, NULL, '20161025', NULL, 'JUAN DENILSON', NULL, 'SALAZAR TORRES', NULL, 'jsalazart@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12535, 6025, NULL, NULL, NULL, '20133672', NULL, 'ESMERALDA JAZMINE', NULL, 'SALAZAR ZUNIGA', NULL, 'esalazarzu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12536, 6026, NULL, NULL, NULL, '20162152', NULL, 'MILAGROS SUSAN', NULL, 'SALINAS CURI', NULL, 'msalinasc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12537, 6027, NULL, NULL, NULL, '20074841', NULL, 'ANGEL FRANCISCO BERNABE', NULL, 'SALINAS PONCE', NULL, 'asalinasp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12538, 6028, NULL, NULL, NULL, '20134224', NULL, 'KARLA ALEJANDRA', NULL, 'SALVADOR RIVERA', NULL, 'ksalvador@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12539, 6029, NULL, NULL, NULL, '20143739', NULL, 'JOSE ARMANDO', NULL, 'SANCHEZ ARCE', NULL, 'jsanchez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12540, 6030, NULL, NULL, NULL, '20144224', NULL, 'EVELYN BRIYITH', NULL, 'SANCHEZ CCOLQQUE', NULL, 'esanchez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12541, 6031, NULL, NULL, NULL, '20144400', NULL, 'PAUL ADOLFO', NULL, 'SANCHEZ CHAVEZ', NULL, 'psanchezc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12542, 6032, NULL, NULL, NULL, '20154714', NULL, 'ANTONIO JERSON', NULL, 'SANCHEZ CONDORI', NULL, 'asanchezco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12543, 6033, NULL, NULL, NULL, '20164666', NULL, 'ALEXI MARCELO', NULL, 'SANCHEZ HANAMPA', NULL, 'asanchezha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12544, 6034, NULL, NULL, NULL, '20144310', NULL, 'CRISTIAN RONALDO', NULL, 'SANCHEZ HUERTA', NULL, 'csanchezh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12545, 6035, NULL, NULL, NULL, '20144378', NULL, 'ROSSMARY ORIELY', NULL, 'SANCHEZ HUERTA', NULL, 'rsanchezhu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12546, 6036, NULL, NULL, NULL, '20154574', NULL, 'KEVIN MICHEL', NULL, 'SANCHEZ JALLO', NULL, 'ksanchez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12547, 6037, NULL, NULL, NULL, '20131376', NULL, 'LOURDES ELIZABETH', NULL, 'SANCHEZ MAMANI', NULL, 'lsanchezma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12548, 6038, NULL, NULL, NULL, '20164630', NULL, 'STEVE ALEXANDER', NULL, 'SANCHEZ PERALTA', NULL, 'ssanchezpe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12549, 6039, NULL, NULL, NULL, '20114195', NULL, 'ROSA IRENE', NULL, 'SANCHEZ SAIRITUPAC', NULL, 'rsanchezsa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12550, 6040, NULL, NULL, NULL, '20112128', NULL, 'MELANNIE JHOAN', NULL, 'SANCHEZ YANCAPALLO', NULL, 'msanchezya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12551, 6041, NULL, NULL, NULL, '20142479', NULL, 'DENNIS IVAN', NULL, 'SANTA CRUZ ALVAREZ', NULL, 'dsantacruza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12552, 6042, NULL, NULL, NULL, '20134313', NULL, 'NAYSHA JUDITH', NULL, 'SANTANA VERA', NULL, 'nsantanav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12553, 6043, NULL, NULL, NULL, '20173678', NULL, 'DIEGO ALONSO', NULL, 'SANTILLANA ESCOBEDO', NULL, 'dsantillanae@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12554, 6044, NULL, NULL, NULL, '20144335', NULL, 'ELSA MAGALI', NULL, 'SARAYASI VIZA', NULL, 'esarayasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12555, 6045, NULL, NULL, NULL, '20131374', NULL, 'PAMELA ANGELA', NULL, 'SAYRA AVALOS', NULL, 'psayra@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12556, 6046, NULL, NULL, NULL, '20154633', NULL, 'OLENNKA GUADALUPE', NULL, 'SEGALES GALLEGOS', NULL, 'osegales@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12557, 6047, NULL, NULL, NULL, '20154651', NULL, 'MARYCARMEN', NULL, 'SEJE BENAVENTE', NULL, 'mseje@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12558, 6048, NULL, NULL, NULL, '20173673', NULL, 'CESAR AUGUSTO', NULL, 'SEQUEIROS ARAPA', NULL, 'csequeiros@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12559, 6049, NULL, NULL, NULL, '20174225', NULL, 'ERICK GABRIEL', NULL, 'SILLOCA CHECCA', NULL, 'esilloca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12560, 6050, NULL, NULL, NULL, '20152659', NULL, 'YURI RUTH', NULL, 'SILVA GARCIA', NULL, 'ysilva@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12561, 6051, NULL, NULL, NULL, '20134301', NULL, 'ARTURO', NULL, 'SIVANA ALVARO', NULL, 'asivana@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12562, 6052, NULL, NULL, NULL, '20144360', NULL, 'NATALY', NULL, 'SIVINCHA LAYME', NULL, 'nsivincha@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12563, 6053, NULL, NULL, NULL, '20144397', NULL, 'CLAUDIO', NULL, 'SOLIS CJURO', NULL, 'csolisc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12564, 6054, NULL, NULL, NULL, '20121343', NULL, 'OLENKA PAOLA', NULL, 'SORIA SONCCO', NULL, 'osorias@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12565, 6055, NULL, NULL, NULL, '20154701', NULL, 'TATIANA FRANSHESKA', NULL, 'SOTO ALARCON', NULL, 'tsoto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12566, 6056, NULL, NULL, NULL, '20096611', NULL, 'ENRIQUE', NULL, 'SOTO MAMANI', NULL, 'esotomam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12567, 6057, NULL, NULL, NULL, '20144341', NULL, 'YANERI BRIGHIT', NULL, 'SOTO MARQUEZ', NULL, 'ysotoma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12568, 6058, NULL, NULL, NULL, '20140384', NULL, 'JUNIOR DAVID', NULL, 'SUAREZ APAZA', NULL, 'jsuarezap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12569, 6059, NULL, NULL, NULL, '20144405', NULL, 'VIKY LAUCATA', NULL, 'SUCA CABANA', NULL, 'vsucac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12570, 6060, NULL, NULL, NULL, '20174268', NULL, 'WILSON', NULL, 'SUCA CONDORI', NULL, 'wsuca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12571, 6061, NULL, NULL, NULL, '20133661', NULL, 'ROLANDA', NULL, 'SUCARI CHOQUEHUANCA', NULL, 'rsucaric@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12572, 6062, NULL, NULL, NULL, '20151489', NULL, 'LUZ KARINA', NULL, 'SUCASAIRE CHUCTAYA', NULL, 'lsucasairec@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12573, 6063, NULL, NULL, NULL, '20142472', NULL, 'XIOMARA VANESA', NULL, 'SULCA MAMANI', NULL, 'xsulca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12574, 6064, NULL, NULL, NULL, '20164687', NULL, 'ESMERALDA GEORGINA', NULL, 'SULLCA HUAMANI', NULL, 'esullcah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12575, 6065, NULL, NULL, NULL, '20170996', NULL, 'LUIS FERNANDO', NULL, 'SULLCA LOPE', NULL, 'lsullcal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12576, 6066, NULL, NULL, NULL, '20164712', NULL, 'ERICKA YESENIA', NULL, 'SULLO QUISPE', NULL, 'esullo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12577, 6067, NULL, NULL, NULL, '20132477', NULL, 'REYMOND DANI', NULL, 'SUNI CALISAYA', NULL, 'rsunica@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12578, 6068, NULL, NULL, NULL, '20162147', NULL, 'KATHERINE MILAGROS', NULL, 'SUNI CUTIPA', NULL, 'ksunic@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12579, 6069, NULL, NULL, NULL, '20143757', NULL, 'ELY ROXANA', NULL, 'SUNI HUANQUI', NULL, 'esuni@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12580, 6070, NULL, NULL, NULL, '20153973', NULL, 'MARYCIELO', NULL, 'SURCO CONDORI', NULL, 'msurcoco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12581, 6071, NULL, NULL, NULL, '20142475', NULL, 'KATHY YUCENIA', NULL, 'SURCO PUMA', NULL, 'ksurcop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12582, 6072, NULL, NULL, NULL, '20164585', NULL, 'ERICK CELSO', NULL, 'SURI QUISPE', NULL, 'esuri@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12583, 6073, NULL, NULL, NULL, '20174321', NULL, 'LIZBETH KAREN', NULL, 'SUYCO PACCO', NULL, 'lsuycop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12584, 6074, NULL, NULL, NULL, '20164578', NULL, 'LORENA LIZBETH', NULL, 'SUYO VELA', NULL, 'lsuyov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12585, 6075, NULL, NULL, NULL, '20134254', NULL, 'JUANA PATRICIA', NULL, 'TACCA ARIAS', NULL, 'jtacca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12586, 6076, NULL, NULL, NULL, '20134234', NULL, 'NADIER YAJAIRA', NULL, 'TACO CHIPANA', NULL, 'ntacoc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12587, 6077, NULL, NULL, NULL, '20162149', NULL, 'JOSE GABRIEL', NULL, 'TACO MAMANI', NULL, 'jtacom@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12588, 6078, NULL, NULL, NULL, '20172420', NULL, 'MONICA', NULL, 'TACO POMA', NULL, 'mtacopo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(12589, 6079, NULL, NULL, NULL, '20131373', NULL, 'YENIT MARGOT', NULL, 'TACUSI YANQUE', NULL, 'ytacusi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12590, 6080, NULL, NULL, NULL, '20131383', NULL, 'MARJORIE ARLEHT', NULL, 'TALAVERA ARANIBAR', NULL, 'mtalaveraa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12591, 6081, NULL, NULL, NULL, '20141372', NULL, 'LESLY DARIT', NULL, 'TALAVERA RAMOS', NULL, 'ltalavera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12592, 6082, NULL, NULL, NULL, '20144308', NULL, 'LUIS ANGEL', NULL, 'TAPARACO HUARACA', NULL, 'ltaparaco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12593, 6083, NULL, NULL, NULL, '20081373', NULL, 'JOEL ANTHONY', NULL, 'TAPIA CRUZ', NULL, 'jtapiacr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12594, 6084, NULL, NULL, NULL, '20152355', NULL, 'BRIYIT KAREN', NULL, 'TAPIA GONZALES', NULL, 'btapia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12595, 6085, NULL, NULL, NULL, '20081844', NULL, 'ABEL', NULL, 'TAPIA MEDRANO', NULL, 'atapiame@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12596, 6086, NULL, NULL, NULL, '20153987', NULL, 'DAVID ULISES', NULL, 'TAPIA QUISPE', NULL, 'dtapiaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12597, 6087, NULL, NULL, NULL, '20144398', NULL, 'CLAUDIA STEPHANIE', NULL, 'TELLEZ TORRES', NULL, 'ctellez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12598, 6088, NULL, NULL, NULL, '20121345', NULL, 'WALTER GUSTAVO', NULL, 'TELLO VELASQUEZ', NULL, 'wtello@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12599, 6089, NULL, NULL, NULL, '20060337', NULL, 'ELFER RAMIRO', NULL, 'TERNERO ALVAREZ', NULL, 'eterneroa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12600, 6090, NULL, NULL, NULL, '20134227', NULL, 'YANIRA DALESKA', NULL, 'TERNERO DIAZ', NULL, 'yternero@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12601, 6091, NULL, NULL, NULL, '20134287', NULL, 'ANDREA', NULL, 'TERRAZAS LAURA', NULL, 'aterrazas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12602, 6092, NULL, NULL, NULL, '20144408', NULL, 'RODOLFO FERNANDO', NULL, 'TICONA ABRIL', NULL, 'rticonaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12603, 6093, NULL, NULL, NULL, '20050820', NULL, 'JEANETH ANALI', NULL, 'TICONA APAZA', NULL, 'jticonaapa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12604, 6094, NULL, NULL, NULL, '20133530', NULL, 'RONALD BRUSSH', NULL, 'TICONA FLORES', NULL, 'rticonaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12605, 6095, NULL, NULL, NULL, '20134288', NULL, 'NARDY LUZ SABI', NULL, 'TICONA LAROTA', NULL, 'nticonal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12606, 6096, NULL, NULL, NULL, '20072588', NULL, 'HELEN YVETT', NULL, 'TICONA PEREZ', NULL, 'hticonap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12607, 6097, NULL, NULL, NULL, '20171019', NULL, 'BLANCA NELIDA', NULL, 'TICONA VELASQUEZ', NULL, 'bticona@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12608, 6098, NULL, NULL, NULL, '20123831', NULL, 'ERIC JOSUE', NULL, 'TIMOTEO RICALDI', NULL, 'etimoteo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12609, 6099, NULL, NULL, NULL, '20091918', NULL, 'NADIA ZULEMA', NULL, 'TINCO GONZALES', NULL, 'ntinco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12610, 6100, NULL, NULL, NULL, '20131356', NULL, 'YANELI MARIEL', NULL, 'TITE VELLANO', NULL, 'ytite@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12611, 6101, NULL, NULL, NULL, '20154620', NULL, 'HERNY ELYOENAY', NULL, 'TITO BAUTISTA', NULL, 'htitob@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12612, 6102, NULL, NULL, NULL, '20163809', NULL, 'GLADYS ALONDRA', NULL, 'TITO CORNEJO', NULL, 'gtitoco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12613, 6103, NULL, NULL, NULL, '20161041', NULL, 'LUIS BERNARDO', NULL, 'TITO TORRES', NULL, 'ltitot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12614, 6104, NULL, NULL, NULL, '20122529', NULL, 'PATRICIA HAYDI', NULL, 'TOHALINO MENDOZA', NULL, 'ptohalino@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12615, 6105, NULL, NULL, NULL, '20091386', NULL, 'SARA BEATRIZ', NULL, 'TOROCAHUA APAZA', NULL, 'storocahua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12616, 6106, NULL, NULL, NULL, '20134209', NULL, 'ADRIANA CAROLINA', NULL, 'TORREBLANCA ROJAS', NULL, 'atorreblanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12617, 6107, NULL, NULL, NULL, '20104507', NULL, 'GRECIA ROCIO', NULL, 'TORREBLANCA ROJAS', NULL, 'gtorreblanca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12618, 6108, NULL, NULL, NULL, '20132482', NULL, 'DARWIN BLADIMIR', NULL, 'TORRES CHACCA', NULL, 'dtorreschac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12619, 6109, NULL, NULL, NULL, '20141385', NULL, 'DANIELA ANTUANE', NULL, 'TORRES CORRALES', NULL, 'dtorresc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12620, 6110, NULL, NULL, NULL, '20161049', NULL, 'RUTH MARIBEL', NULL, 'TORRES HUALLPA', NULL, 'rtorresh@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12621, 6111, NULL, NULL, NULL, '20172426', NULL, 'ROBERT ENRIQUE', NULL, 'TORRES MAMANI', NULL, 'rtorresma@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12622, 6112, NULL, NULL, NULL, '20164625', NULL, 'ROBERTO CARLOS', NULL, 'TORRES MONROY', NULL, 'rtorresmo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12623, 6113, NULL, NULL, NULL, '20161040', NULL, 'LEONARDO ADRIAN', NULL, 'TORRES PALACIOS', NULL, 'ltorrespa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12624, 6114, NULL, NULL, NULL, '20113787', NULL, 'JUAN LUIS', NULL, 'TORRES QUISPESIVANA', NULL, 'jtorresquisp@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12625, 6115, NULL, NULL, NULL, '20041636', NULL, 'KAROL ALFONSO', NULL, 'TORRES RAMOS', NULL, 'ktorresr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12626, 6116, NULL, NULL, NULL, '20154704', NULL, 'MOISES JUNIOR', NULL, 'TORRES TORRES', NULL, 'mtorrest@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12627, 6117, NULL, NULL, NULL, '20174317', NULL, 'BRUNELA ANTUANE', NULL, 'TORRES VALDIVIA', NULL, 'btorresv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12628, 6118, NULL, NULL, NULL, '20131359', NULL, 'JUAN EDUARDO', NULL, 'TORRES VELARDE', NULL, 'jtorresve@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12629, 6119, NULL, NULL, NULL, '20174212', NULL, 'MARIA FLOR TIFANI', NULL, 'TORRES VILCA', NULL, 'mtorresvi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12630, 6120, NULL, NULL, NULL, '20174295', NULL, 'ANA LUCIA', NULL, 'TORRES VILLA', NULL, 'atorresvi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12631, 6121, NULL, NULL, NULL, '20163796', NULL, 'ALONSO ANDRES', NULL, 'TRIGOSO ROJAS', NULL, 'atrigoso@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12632, 6122, NULL, NULL, NULL, '20130891', NULL, 'BRUCE BILL', NULL, 'TTITO ALAYO', NULL, 'bttitoa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12633, 6123, NULL, NULL, NULL, '20143755', NULL, 'JUAN LUIS', NULL, 'TUPA ORTIZ', NULL, 'jtupa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12634, 6124, NULL, NULL, NULL, '20085102', NULL, 'MARIBEL', NULL, 'TURPO CHACNAMA', NULL, 'mturpoch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12635, 6125, NULL, NULL, NULL, '20133670', NULL, 'YANETH ANGIE', NULL, 'TURPO GONZALES', NULL, 'yturpog@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12636, 6126, NULL, NULL, NULL, '20153971', NULL, 'ALEX JHONY', NULL, 'TURPO PACCORI', NULL, 'aturpop@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12637, 6127, NULL, NULL, NULL, '20144347', NULL, 'OLENKA KATTYA', NULL, 'TUYA QUISPE', NULL, 'otuya@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12638, 6128, NULL, NULL, NULL, '20072514', NULL, 'JOSE PEDRO', NULL, 'UMPIRE MARCHAN', NULL, 'jumpirem@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12639, 6129, NULL, NULL, NULL, '20115036', NULL, 'VANESSA LIZBETH', NULL, 'UNGARO CURASI', NULL, 'vungaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12640, 6130, NULL, NULL, NULL, '20100087', NULL, 'MIRIAM VANESSA', NULL, 'URBINA LUQUE', NULL, 'murbinal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12641, 6131, NULL, NULL, NULL, '20132483', NULL, 'MARJORIE CLAUDIA', NULL, 'URDAY CONDORI', NULL, 'murdayc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12642, 6132, NULL, NULL, NULL, '20141381', NULL, 'MARJORIE PAOLA', NULL, 'URDAY CONDORI', NULL, 'murday@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12643, 6133, NULL, NULL, NULL, '20154571', NULL, 'SARA LESLIE', NULL, 'URDAY ROSAS', NULL, 'surday@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12644, 6134, NULL, NULL, NULL, '20134334', NULL, 'GEREMMY ALEJANDRA', NULL, 'URETA ROSAS', NULL, 'gureta@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12645, 6135, NULL, NULL, NULL, '20174213', NULL, 'NOEMI JAEL', NULL, 'USCAMAYTA MAMANI', NULL, 'nuscamaytam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12646, 6136, NULL, NULL, NULL, '20143750', NULL, 'ALEJANDRA JOSELYN', NULL, 'VALDERRAMA PEREZ', NULL, 'avalderramap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12647, 6137, NULL, NULL, NULL, '20163808', NULL, 'LUZ MARIELA', NULL, 'VALDERRAMA VILLAFUERTE', NULL, 'lvalderrama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12648, 6138, NULL, NULL, NULL, '20144383', NULL, 'NOEMI', NULL, 'VALDEZ CONDORI', NULL, 'nvaldez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12649, 6139, NULL, NULL, NULL, '20041549', NULL, 'GISEL NOEMI', NULL, 'VALDIVIA CHOCTAYO', NULL, 'gvaldiviac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12650, 6140, NULL, NULL, NULL, '20143747', NULL, 'BERENICE LIZBETH', NULL, 'VALDIVIA CHOQUE', NULL, 'bvaldiviac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12651, 6141, NULL, NULL, NULL, '20141386', NULL, 'LEYDA AURORA', NULL, 'VALDIVIA FERNANDEZ', NULL, 'lvaldiviaf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12652, 6142, NULL, NULL, NULL, '20143752', NULL, 'DANIEL ELIAZAR', NULL, 'VALDIVIA PANIBRA', NULL, 'dvaldiviapa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12653, 6143, NULL, NULL, NULL, '20124148', NULL, 'FRANCO JAIR', NULL, 'VALDIVIA RODRIGUEZ', NULL, 'fvaldiviarod@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12654, 6144, NULL, NULL, NULL, '20151490', NULL, 'GERILY ALEXANDRA', NULL, 'VALDIVIA ROSADO', NULL, 'gvaldiviaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12655, 6145, NULL, NULL, NULL, '20134213', NULL, 'ELISABETH PATRICIA', NULL, 'VALDIVIA TACO', NULL, 'evaldiviat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12656, 6146, NULL, NULL, NULL, '20121325', NULL, 'JOSE AUGUSTO', NULL, 'VALDIVIA VALDIVIA', NULL, 'jvaldiviavald@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12657, 6147, NULL, NULL, NULL, '20174244', NULL, 'MARIA ALEJANDRA', NULL, 'VALDIVIA VALDIVIA', NULL, 'mvaldiviav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12658, 6148, NULL, NULL, NULL, '20112179', NULL, 'ROBIN FRANCIS', NULL, 'VALDIVIA VALDIVIA', NULL, 'rvaldiviav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12659, 6149, NULL, NULL, NULL, '20124168', NULL, 'DIANA LUCIA', NULL, 'VALDIVIA VASQUEZ', NULL, 'dvaldiviava@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12660, 6150, NULL, NULL, NULL, '20154655', NULL, 'GLORIA MARIA', NULL, 'VALDIVIA VASQUEZ', NULL, 'gvaldiviav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12661, 6151, NULL, NULL, NULL, '20154618', NULL, 'JAIRO JOSE MANUEL', NULL, 'VALENCIA BUSTAMANTE', NULL, 'jvalenciabu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12662, 6152, NULL, NULL, NULL, '20110257', NULL, 'JEAN MARTIN', NULL, 'VALENCIA CCALLO', NULL, 'jvalenciacc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12663, 6153, NULL, NULL, NULL, '20174192', NULL, 'KARLA NICOLL', NULL, 'VALENCIA HEREDIA', NULL, 'kvalenciahe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12664, 6154, NULL, NULL, NULL, '20083749', NULL, 'MARIA CANDELARIA', NULL, 'VALENCIA RODRIGUEZ', NULL, 'mvalenciar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12665, 6155, NULL, NULL, NULL, '20133660', NULL, 'PAULINA', NULL, 'VALERIANO SALCEDO', NULL, 'pvalerianos@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12666, 6156, NULL, NULL, NULL, '20164589', NULL, 'DANIELA JEANNETH', NULL, 'VALERIANO VALERIANO', NULL, 'dvalerianov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12667, 6157, NULL, NULL, NULL, '20114514', NULL, 'DANITZA', NULL, 'VALERO CONDORI', NULL, 'dvaleroc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12668, 6158, NULL, NULL, NULL, '20150903', NULL, 'MERLY FELICITA', NULL, 'VARGAS ABARCA', NULL, 'mvargasab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12669, 6159, NULL, NULL, NULL, '20134317', NULL, 'MILEYDY MELIZA', NULL, 'VARGAS ADRIAN', NULL, 'mvargasad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12670, 6160, NULL, NULL, NULL, '20133690', NULL, 'RUBY YULEIMI', NULL, 'VARGAS ADRIAN', NULL, 'rvargasadr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12671, 6161, NULL, NULL, NULL, '20131360', NULL, 'JULIO CESAR', NULL, 'VARGAS AMADO', NULL, 'jvargasam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12672, 6162, NULL, NULL, NULL, '20134322', NULL, 'DIEGO RODRIGO', NULL, 'VARGAS BRUNO', NULL, 'dvargasbr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12673, 6163, NULL, NULL, NULL, '20144346', NULL, 'LEONARDO RAFAEL', NULL, 'VARGAS BUSTAMANTE', NULL, 'lvargasb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12674, 6164, NULL, NULL, NULL, '20163416', NULL, 'JESSICA ANTONELLA', NULL, 'VARGAS CAHUANA', NULL, 'jvargasca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12675, 6165, NULL, NULL, NULL, '20154676', NULL, 'JIMMY JESUS', NULL, 'VARGAS CRUZ', NULL, 'jvargascr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12676, 6166, NULL, NULL, NULL, '20144412', NULL, 'ANIBAL JONATHAN', NULL, 'VARGAS FERNANDEZ', NULL, 'avargasf@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12677, 6167, NULL, NULL, NULL, '20121317', NULL, 'LUIS ALVERTO', NULL, 'VARGAS GARCIA', NULL, 'lvargasga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12678, 6168, NULL, NULL, NULL, '20154626', NULL, 'ELVIS EDWIN', NULL, 'VARGAS HUACHACA', NULL, 'evargashu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12679, 6169, NULL, NULL, NULL, '20134286', NULL, 'GIANELA BRIGITE', NULL, 'VARGAS IQUIAPAZA', NULL, 'gvargasi@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12680, 6170, NULL, NULL, NULL, '20121895', NULL, 'KAROLAN SIDNEY', NULL, 'VARGAS MAHANEY', NULL, 'kvargasmah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12681, 6171, NULL, NULL, NULL, '20164633', NULL, 'ANYELO ALONSO', NULL, 'VARGAS NINA', NULL, 'avargasn@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12682, 6172, NULL, NULL, NULL, '20153985', NULL, 'BIANCA PAOLA', NULL, 'VARGAS ROJAS', NULL, 'bvargasro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12683, 6173, NULL, NULL, NULL, '20134339', NULL, 'VALERIA HILDA', NULL, 'VARGAS ROSAS', NULL, 'vvargas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12684, 6174, NULL, NULL, NULL, '20163805', NULL, 'NATHALY NIXA', NULL, 'VARGAS SARAVIA', NULL, 'nvargassa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12685, 6175, NULL, NULL, NULL, '20151508', NULL, 'KIMBERLY ANDREA', NULL, 'VARGAS TICLLA', NULL, 'kvargast@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12686, 6176, NULL, NULL, NULL, '20112112', NULL, 'RHUSBELL ANDRE', NULL, 'VARGAS TORRES', NULL, 'rvargasto@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12687, 6177, NULL, NULL, NULL, '20151499', NULL, 'SERGIO ALEJANDRO KELVIN', NULL, 'VARGAS ZAVALETA', NULL, 'svargasz@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12688, 6178, NULL, NULL, NULL, '20113375', NULL, 'SUSAN MERCEDES', NULL, 'VASQUEZ BERNAOLA', NULL, 'svasquezb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12689, 6179, NULL, NULL, NULL, '20134320', NULL, 'LEYDI YUDITH', NULL, 'VASQUEZ RAMOS', NULL, 'lvasquezr@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12690, 6180, NULL, NULL, NULL, '20172439', NULL, 'AMET JOEL', NULL, 'VASQUEZ SAAVEDRA', NULL, 'avasquezs@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12691, 6181, NULL, NULL, NULL, '20161121', NULL, 'YANETH ELIZABETH', NULL, 'VASQUEZ YANA', NULL, 'yvasquezy@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12692, 6182, NULL, NULL, NULL, '20101340', NULL, 'PERCY GABRIEL', NULL, 'VEGA CALLO', NULL, 'pvegac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12693, 6183, NULL, NULL, NULL, '20133541', NULL, 'LESLIE RAQUEL', NULL, 'VELARDE BENAVENTE', NULL, 'lvelardebe@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12694, 6184, NULL, NULL, NULL, '20164710', NULL, 'JOSE CESAR', NULL, 'VELARDE CAMPOS', NULL, 'jvelardecam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12695, 6185, NULL, NULL, NULL, '20154690', NULL, 'JOSE RODOLFO', NULL, 'VELARDE CONTRERAS', NULL, 'jvelardecon@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12696, 6186, NULL, NULL, NULL, '20154628', NULL, 'ROGER RICARDO', NULL, 'VELASQUEZ ARAPA', NULL, 'rvelasqueza@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12697, 6187, NULL, NULL, NULL, '20174241', NULL, 'LEONARDO STEVEN', NULL, 'VELASQUEZ GONZALES', NULL, 'lvelasquezg@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12698, 6188, NULL, NULL, NULL, '20113793', NULL, 'AMBAR XIOMARA', NULL, 'VELASQUEZ HUANCACHOQUE', NULL, 'avelasquezhua@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12699, 6189, NULL, NULL, NULL, '20174327', NULL, 'LESLY RAQUEL', NULL, 'VELASQUEZ HURTADO', NULL, 'lvelasquezhu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12700, 6190, NULL, NULL, NULL, '20013434', NULL, 'MARCO ANTONIO', NULL, 'VELASQUEZ SALAZAR', NULL, 'mvelasquezsa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12701, 6191, NULL, NULL, NULL, '20163779', NULL, 'MIGUEL ANGEL', NULL, 'VELASQUEZ TICONA', NULL, 'mvelasquezt@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12702, 6192, NULL, NULL, NULL, '20151518', NULL, 'JENNY MILAGROS', NULL, 'VELASQUEZ VASQUEZ', NULL, 'jvelasquezvas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12703, 6193, NULL, NULL, NULL, '20174265', NULL, 'GERSON ALDAIR', NULL, 'VELASQUEZ VILLALTA', NULL, 'gvelasquezv@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12704, 6194, NULL, NULL, NULL, '20121306', NULL, 'LIS', NULL, 'VELAZCO TORRES', NULL, 'lvelazcot@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12705, 6195, NULL, NULL, NULL, '20151481', NULL, 'MARILEY', NULL, 'VELEZ HUAHUACHAMBI', NULL, 'mvelez@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12706, 6196, NULL, NULL, NULL, '20085295', NULL, 'LEONORA YESENIA', NULL, 'VENEGAS CASA', NULL, 'lvenegasc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12707, 6197, NULL, NULL, NULL, '20134257', NULL, 'ALBA CAROLINA', NULL, 'VENTURA INFANTES', NULL, 'aventurai@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12708, 6198, NULL, NULL, NULL, '20174281', NULL, 'ALEXANDRA STHEFANIA', NULL, 'VERA BRICENO', NULL, 'averab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12709, 6199, NULL, NULL, NULL, '20164705', NULL, 'MARTIN ANGEL', NULL, 'VERA CARDENAS', NULL, 'mveracar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12710, 6200, NULL, NULL, NULL, '20121315', NULL, 'VLADIMIR LEONCIO', NULL, 'VERA CHURA', NULL, 'vverac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12711, 6201, NULL, NULL, NULL, '20134233', NULL, 'MILIET VANESA', NULL, 'VERA QUINONES', NULL, 'mveraqu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12712, 6202, NULL, NULL, NULL, '20154697', NULL, 'GIANELLA IRIS', NULL, 'VERA SALINAS', NULL, 'gvera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12713, 6203, NULL, NULL, NULL, '20174315', NULL, 'JUDITH SAMIRA', NULL, 'VILCA ALEJOS', NULL, 'jvilcaale@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12714, 6204, NULL, NULL, NULL, '20154694', NULL, 'JOSSELYN ABIGAIL', NULL, 'VILCA ARAPA', NULL, 'jvilcaar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12715, 6205, NULL, NULL, NULL, '20143737', NULL, 'KARINA EVELY', NULL, 'VILCA ARI', NULL, 'kvilcaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12716, 6206, NULL, NULL, NULL, '20134246', NULL, 'MILAGROS MARIBEL', NULL, 'VILCA ARIAS', NULL, 'mvilcaar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12717, 6207, NULL, NULL, NULL, '20163811', NULL, 'DEYSI DORIS', NULL, 'VILCA CAYLLAHUA', NULL, 'dvilcaca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12718, 6208, NULL, NULL, NULL, '20131340', NULL, 'FRANKLIN ALEXIS', NULL, 'VILCA CHUSI', NULL, 'fvilcach@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12719, 6209, NULL, NULL, NULL, '20143909', NULL, 'ALDO WILFREDO', NULL, 'VILCA DIAZ', NULL, 'avilcad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12720, 6210, NULL, NULL, NULL, '20152661', NULL, 'KATHERINE MILAGROS', NULL, 'VILCA FLORES', NULL, 'kvilcaflor@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12721, 6211, NULL, NULL, NULL, '20103805', NULL, 'CARLOS ALONSO', NULL, 'VILCA HUARCA', NULL, 'cvilcah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12722, 6212, NULL, NULL, NULL, '20164736', NULL, 'VICTOR HUGO', NULL, 'VILCA HUARCAYA', NULL, 'vvilcah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12723, 6213, NULL, NULL, NULL, '20164656', NULL, 'ALBERT LUISINO', NULL, 'VILCA MAMANI', NULL, 'avilcam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12724, 6214, NULL, NULL, NULL, '20174308', NULL, 'MARLENY ROCISELA', NULL, 'VILCA MAMANI', NULL, 'mvilcama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12725, 6215, NULL, NULL, NULL, '20154572', NULL, 'PATRICIA FIORELA', NULL, 'VILCA MAMANI', NULL, 'pvilcam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12726, 6216, NULL, NULL, NULL, '20164668', NULL, 'RUTH LAURA', NULL, 'VILCA MERCADO', NULL, 'rvilcam@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12727, 6217, NULL, NULL, NULL, '20164605', NULL, 'ODELY YDELSA', NULL, 'VILCA PANTIGOSO', NULL, 'ovilcap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12728, 6218, NULL, NULL, NULL, '20174253', NULL, 'SAUL RUBEN', NULL, 'VILCA QUISPE', NULL, 'svilcaq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12729, 6219, NULL, NULL, NULL, '20164571', NULL, 'ANDRE FRANCISCO', NULL, 'VILCA ROMAN', NULL, 'avilcar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12730, 6220, NULL, NULL, NULL, '20104473', NULL, 'BRIGHIT FABIOLA', NULL, 'VILCA ROMAN', NULL, 'bvilcaro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12731, 6221, NULL, NULL, NULL, '20144350', NULL, 'ABEL FRANCISCO', NULL, 'VILCA TACO', NULL, 'avilcataco@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12732, 6222, NULL, NULL, NULL, '20153965', NULL, 'IVETTE MARLENY', NULL, 'VILCA TICONA', NULL, 'ivilcati@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12733, 6223, NULL, NULL, NULL, '20151474', NULL, 'JULIANA CAROLINA', NULL, 'VILCA TURPO', NULL, 'jvilcatu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12734, 6224, NULL, NULL, NULL, '20096439', NULL, 'LIA KATHERINE', NULL, 'VILLAJUAN DELGADILLO', NULL, 'lvillajuan@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12735, 6225, NULL, NULL, NULL, '20133692', NULL, 'BRITANY', NULL, 'VILLALTA BELLIDO', NULL, 'bvillaltab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12736, 6226, NULL, NULL, NULL, '20144431', NULL, 'JOSUE JUAN', NULL, 'VILLALTA CABALLERO', NULL, 'jvillaltac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12737, 6227, NULL, NULL, NULL, '20074596', NULL, 'EVERLY JHOAN', NULL, 'VILLEGAS MOSTAJO', NULL, 'evillegas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12738, 6228, NULL, NULL, NULL, '20133691', NULL, 'ANABEL FERNANDA', NULL, 'VILLENA RIVERA', NULL, 'avillenari@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12739, 6229, NULL, NULL, NULL, '20164622', NULL, 'ALEX REYNALDO', NULL, 'VIZCARDO LIPE', NULL, 'avizcardo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12740, 6230, NULL, NULL, NULL, '20164657', NULL, 'CARMEN MIRELLA', NULL, 'VIZCARRA BOZA', NULL, 'cvizcarrab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12741, 6231, NULL, NULL, NULL, '20134338', NULL, 'YOSMARY LUCERO', NULL, 'WISA QUISPE', NULL, 'ywisa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12742, 6232, NULL, NULL, NULL, '20131345', NULL, 'LUIS ANGEL', NULL, 'YANA HUAMANI', NULL, 'lyanah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12743, 6233, NULL, NULL, NULL, '20133696', NULL, 'VALESKA', NULL, 'YANA VELA', NULL, 'vyanav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12744, 6234, NULL, NULL, NULL, '20174283', NULL, 'MILAGROS PAOLA', NULL, 'YANCAYA BENAVENTE', NULL, 'myancayab@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12745, 6235, NULL, NULL, NULL, '20133668', NULL, 'DEYSI PAMELA', NULL, 'YAULI VILCA', NULL, 'dyauli@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12746, 6236, NULL, NULL, NULL, '20164602', NULL, 'GIELVY LADY', NULL, 'YAURI BAUTISTA', NULL, 'gyaurib@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12747, 6237, NULL, NULL, NULL, '20171011', NULL, 'YIMMY YAMPHIERE', NULL, 'YAURI CHURA', NULL, 'yyauric@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12748, 6238, NULL, NULL, NULL, '20141364', NULL, 'LESLIE ABIGAIL', NULL, 'YDME PAYE', NULL, 'lydmep@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12749, 6239, NULL, NULL, NULL, '20174299', NULL, 'FATIMA ESTEFANY', NULL, 'YERBA CHAMBI', NULL, 'fyerbac@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12750, 6240, NULL, NULL, NULL, '20174288', NULL, 'ROGER AUGUSTO', NULL, 'YNCA HUAMAN', NULL, 'rynca@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12751, 6241, NULL, NULL, NULL, '20122426', NULL, 'LUIS ALFREDO', NULL, 'YONG SAN MIGUEL', NULL, 'lyong@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12752, 6242, NULL, NULL, NULL, '20163787', NULL, 'FRANCHESCA NICOLI', NULL, 'YOVERA CHOQUE', NULL, 'fyovera@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12753, 6243, NULL, NULL, NULL, '20144311', NULL, 'FLORA VANESSA', NULL, 'YUCRA CURO', NULL, 'fyucracu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12754, 6244, NULL, NULL, NULL, '20134262', NULL, 'NATIVIDAD FLORIDA', NULL, 'YUCRA PAREDES', NULL, 'nyucrap@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12755, 6245, NULL, NULL, NULL, '20144339', NULL, 'MARYORI YOLANDA', NULL, 'YUCRA TUNQUI', NULL, 'myucratu@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12756, 6246, NULL, NULL, NULL, '20154688', NULL, 'LESLY SHIOMARA', NULL, 'YUNGA PAZ', NULL, 'lyunga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12757, 6247, NULL, NULL, NULL, '20163782', NULL, 'NAYSHA JAQUELINY', NULL, 'YUPANQUI PUMA', NULL, 'nyupanqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12758, 6248, NULL, NULL, NULL, '20171001', NULL, 'LUIS FERNANDO', NULL, 'YUPANQUI TACO', NULL, 'lyupanqui@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');
INSERT INTO `bitacorausuario` (`BitUsuId`, `UsuId`, `UsuDniAnt`, `UsuDniNue`, `UsuCuiAnt`, `UsuCuiNue`, `UsuNomAnt`, `UsuNomNue`, `UsuApeAnt`, `UsuApeNue`, `UsuCorEleAnt`, `UsuCorEleNue`, `UsuTelAnt`, `UsuTelNue`, `UsuDirAnt`, `UsuDirNue`, `UsuNumProAnt`, `UsuNumProNue`, `UsuGenAnt`, `UsuGenNue`, `UsuFKTip1Ant`, `UsuFKTip1Nue`, `UsuFKTip2Ant`, `UsuFKTip2Nue`, `UsuFKCatAnt`, `UsuFKCatNue`, `UsuNomUsuAnt`, `UsuNomUsuNue`, `UsuConUsuAnt`, `UsuConUsuNue`, `UsuFKEstRegAnt`, `UsuFKEstRegNue`, `usuario`, `fecha`, `operacion`, `host`) VALUES
(12759, 6249, NULL, NULL, NULL, '20164600', NULL, 'THALIA MERY', NULL, 'ZAMATA CCAMAQQUE', NULL, 'tzamata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12760, 6250, NULL, NULL, NULL, '20153220', NULL, 'SAMUEL JESUS', NULL, 'ZAMATA QQUELCCA', NULL, 'szamataq@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12761, 6251, NULL, NULL, NULL, '20154315', NULL, 'KEVIN GERSON', NULL, 'ZAMATELO HILACAMA', NULL, 'kzamatelo@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12762, 6252, NULL, NULL, NULL, '20140376', NULL, 'JOSE ENRIQUE', NULL, 'ZAMBRANO VARGAS', NULL, 'jzambrano@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12763, 6253, NULL, NULL, NULL, '20151487', NULL, 'LUIS DAVID', NULL, 'ZAMBRANO VARGAS', NULL, 'lzambranov@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12764, 6254, NULL, NULL, NULL, '20144409', NULL, 'YOIS ANDREA', NULL, 'ZAPANA APAZA', NULL, 'yzapanaa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12765, 6255, NULL, NULL, NULL, '20133688', NULL, 'PRISCILA ALONDRA', NULL, 'ZAPANA DIAZ', NULL, 'pzapanad@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12766, 6256, NULL, NULL, NULL, '20132479', NULL, 'OSCAR ANTONIO', NULL, 'ZAPANA HUACARPUMA', NULL, 'ozapanah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12767, 6257, NULL, NULL, NULL, '20171005', NULL, 'FRANNKLIN BRAYAN', NULL, 'ZAPANA HUANCA', NULL, 'fzapanah@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12768, 6258, NULL, NULL, NULL, '20144338', NULL, 'HENRY', NULL, 'ZAPATA CANALES', NULL, 'hzapata@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12769, 6259, NULL, NULL, NULL, '20132204', NULL, 'ALEXIO', NULL, 'ZARABIA GAMONAL', NULL, 'azarabia@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12770, 6260, NULL, NULL, NULL, '20091148', NULL, 'CHRISTIAN RODOLFO', NULL, 'ZARATE MIRANDA', NULL, 'czaratem@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12771, 6261, NULL, NULL, NULL, '20154613', NULL, 'BROSWI ALFREDO', NULL, 'ZAVALA GRANADA', NULL, 'bzavalag@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12772, 6262, NULL, NULL, NULL, '20172440', NULL, 'CLAUDIA CECILIA', NULL, 'ZAVALA SALINAS', NULL, 'czavalas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12773, 6263, NULL, NULL, NULL, '20164642', NULL, 'VERONICA', NULL, 'ZAVALA TAPARACO', NULL, 'vzavalat@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12774, 6264, NULL, NULL, NULL, '20144306', NULL, 'BRANDON LUCIANO', NULL, 'ZAVALA VELASQUEZ', NULL, 'bzavalav@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12775, 6265, NULL, NULL, NULL, '20141335', NULL, 'YAMILEYDI JIMENA', NULL, 'ZAVALAGA APAZA', NULL, 'yzavalaga@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12776, 6266, NULL, NULL, NULL, '20170999', NULL, 'CLAUDIA VICTORIA', NULL, 'ZEBALLOS BRUNA', NULL, 'czeballosb@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12777, 6267, NULL, NULL, NULL, '20171021', NULL, 'JUAN EDUARDO', NULL, 'ZEBALLOS CHALLCO', NULL, 'jzeballosch@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12778, 6268, NULL, NULL, NULL, '20174305', NULL, 'JUNETH NAYELI', NULL, 'ZEGARRA MAMANI', NULL, 'jzegarrama@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12779, 6269, NULL, NULL, NULL, '20171009', NULL, 'ORFILIA PAOLA', NULL, 'ZEGARRA NINA', NULL, 'ozegarran@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12780, 6270, NULL, NULL, NULL, '20154596', NULL, 'KATHERINE SUGEY', NULL, 'ZEVALLOS AMAT Y LEON', NULL, 'kzevallosa@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12781, 6271, NULL, NULL, NULL, '19973343', NULL, 'ZULY ANANI', NULL, 'ZEVALLOS COSI', NULL, 'zzevallosc@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12782, 6272, NULL, NULL, NULL, '20174306', NULL, 'JOYSSE JISSELA', NULL, 'ZEVALLOS RODRIGUEZ', NULL, 'jzevallosro@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12783, 6273, NULL, NULL, NULL, '20171028', NULL, 'GLENIER FRYDA', NULL, 'ZUNIGA ARREDONDO', NULL, 'gzunigaar@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12784, 6274, NULL, NULL, NULL, '20098334', NULL, 'MARYORI YOMAIRA', NULL, 'ZUNIGA GOMEZ', NULL, 'mzunigago@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12785, 6275, NULL, NULL, NULL, '20134282', NULL, 'ESTEFANY', NULL, 'ZUNIGA LAYME', NULL, 'ezunigal@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12786, 6276, NULL, NULL, NULL, '20120868', NULL, 'JAROL BERLY', NULL, 'ZUNIGA SALVADOR', NULL, 'jzunigas@unsa.edu.pe', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12787, 6277, NULL, '10824353', NULL, NULL, NULL, 'YESSIKA MADELAINE', NULL, 'ABARCA ARIAS', NULL, 'yabarca@unsa.edu.pe', NULL, '959390011', NULL, ' URBQUINTA GAMERO  A-12 3ER PISO', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'yabarca@unsa.edu.pe', NULL, '10824353', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12788, 6278, NULL, '29297296', NULL, NULL, NULL, 'CARPIO JACINTA MAYRENE', NULL, 'ABARCA DEL', NULL, 'cabarcad@unsa.edu.pe', NULL, '205817', NULL, '  VALLECITO  PJ OLAYA206', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'cabarcad@unsa.edu.pe', NULL, '29297296', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12789, 6279, NULL, '29221806', NULL, NULL, NULL, 'ROBERTO CESAR AUGUSTO', NULL, 'ABARCA FERNANDEZ', NULL, 'rabarcaf@unsa.edu.pe', NULL, '959834113', NULL, '  PUERTA VERDE   F-4', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'rabarcaf@unsa.edu.pe', NULL, '29221806', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12790, 6280, NULL, '29237688', NULL, NULL, NULL, 'NELLY LUCY', NULL, 'ABARCA RIVERA', NULL, '', NULL, '958305517', NULL, '  URBSAN MARTIN  CA URUBAMBA221', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29237688', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12791, 6283, NULL, '43542764', NULL, NULL, NULL, 'DANNY YSMAEL', NULL, 'ABRIL GUILLEN', NULL, '', NULL, '', NULL, '', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '43542764', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12792, 6284, NULL, '29203446', NULL, NULL, NULL, 'ALVARO BERNABE', NULL, 'ABRILL BUSCH', NULL, '', NULL, '285984', NULL, '    AV TARAPACA1515', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29203446', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12793, 6285, NULL, '29530850', NULL, NULL, NULL, 'JUANA ELIZABETH', NULL, 'ACO LINARES', NULL, '', NULL, '988844983', NULL, '  URB SAN COSME   F-12', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '29530850', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12794, 6286, NULL, '00791088', NULL, NULL, NULL, 'OLGER NICOLAS', NULL, 'ACOSTA ANGULO', NULL, 'oacostaa@unsa.edu.pe', NULL, '958899966', NULL, '    AV LIMA305', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'oacostaa@unsa.edu.pe', NULL, '00791088', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12795, 6287, NULL, '44862366', NULL, NULL, NULL, 'SILVA LORENA TEREZINHA', NULL, 'ACOSTA LOPEZ', NULL, '', NULL, '959341605', NULL, '  COOPVICTOR ANDRES BELAUNDE   G-14', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '44862366', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12796, 6288, NULL, '29575769', NULL, NULL, NULL, 'RITTER BERNABE', NULL, 'ACOSTA MENDOZA', NULL, 'racostam@unsa.edu.pe', NULL, '465668', NULL, '  URBJESUS MARIA  CA BOLOGNESI110', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'racostam@unsa.edu.pe', NULL, '29575769', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12797, 6289, NULL, '30645477', NULL, NULL, NULL, 'ALEJANDRO NESTOR PORFIRIO', NULL, 'ADRIAN CONDORI', NULL, '', NULL, '586620', NULL, '  PAMPAS DE POLANCO   VISTA ALEGREMZJ LT20', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '30645477', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12798, 6290, NULL, '29525780', NULL, NULL, NULL, 'PASCUAL HERADIO', NULL, 'ADRIAZOLA CORRALES', NULL, 'padriazola@unsa.edu.pe', NULL, '959890261', NULL, '    CA CORTADERAS109', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'padriazola@unsa.edu.pe', NULL, '29525780', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12799, 6291, NULL, '04426578', NULL, NULL, NULL, 'SULEMA JUSTINA', NULL, 'ADRIAZOLA CORRALES', NULL, 'sadriazola@unsa.edu.pe', NULL, '959201740', NULL, '  URBBUENA VISTA   A-34', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'sadriazola@unsa.edu.pe', NULL, '04426578', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12800, 6292, NULL, '40454472', NULL, NULL, NULL, 'ELIANA MARIA', NULL, 'ADRIAZOLA HERRERA', NULL, 'eadriazola@unsa.edu.pe', NULL, '958309006', NULL, '  GARAYCOCHEA   104', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'eadriazola@unsa.edu.pe', NULL, '40454472', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12801, 6293, NULL, '30676007', NULL, NULL, NULL, 'CARLOS ANTONIO', NULL, 'ADRIAZOLA SANTILLANA', NULL, '', NULL, '959719670', NULL, '    PJ TASAHUAYOSN', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '30676007', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12802, 6294, NULL, '29655334', NULL, NULL, NULL, 'MARCO WILFREDO', NULL, 'AEDO LOPEZ', NULL, 'maedol@unsa.edu.pe', NULL, '973181870', NULL, '    CA MISTI423', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'maedol@unsa.edu.pe', NULL, '29655334', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12803, 6295, NULL, '29446491', NULL, NULL, NULL, 'EDUARDO', NULL, 'AGUILAR AITA', NULL, '', NULL, '486269', NULL, '  URBSANTA CLARA   B-11', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29446491', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12804, 6296, NULL, '41834001', NULL, NULL, NULL, 'JORGE DAVID', NULL, 'AGUILAR CARDENAS', NULL, '', NULL, '339772', NULL, '     CIUDAD MUNICIPAL VIA-15', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '41834001', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12805, 6297, NULL, '29268611', NULL, NULL, NULL, 'GENY VIRGINIA', NULL, 'AGUILAR CORNEJO', NULL, 'jaguilarc@unsa.edu.pe', NULL, '423518', NULL, '  URBLA ESTRELLA   H-1', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'jaguilarc@unsa.edu.pe', NULL, '29268611', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12806, 6298, NULL, '41076736', NULL, NULL, NULL, 'CARPIO CARLOS ILICH', NULL, 'AGUILAR DEL', NULL, 'caguilarde@unsa.edu.pe', NULL, '256479', NULL, '  UMACOLLO  CA LAZO DE LOS RIOS310', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'caguilarde@unsa.edu.pe', NULL, '41076736', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12807, 6299, NULL, '41051830', NULL, NULL, NULL, 'JOSE LUIS', NULL, 'AGUILAR GONZALES', NULL, 'jaguilargo@unsa.edu.pe', NULL, '766572', NULL, '  URBBARTOLOME HERRERA   D-16', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'jaguilargo@unsa.edu.pe', NULL, '41051830', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12808, 6300, NULL, '29274083', NULL, NULL, NULL, 'LEONIDAS', NULL, 'AGUILAR PERCA', NULL, '', NULL, '959870925', NULL, '    CA PUENTE GRAU207', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29274083', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12809, 6301, NULL, '29343393', NULL, NULL, NULL, 'EMETERIO VICTOR', NULL, 'AGUILAR PURUHUAYA', NULL, '', NULL, '445219', NULL, '  URBMARISCAL CASTILLA   GONZALES PRADA410', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29343393', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12810, 6302, NULL, '29650614', NULL, NULL, NULL, 'ROXANA', NULL, 'AGUIRRE CANSAYA', NULL, '', NULL, '940183664', NULL, '  URBVILLA HERMOSA   MZG  LT8', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '29650614', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12811, 6303, NULL, '43816580', NULL, NULL, NULL, 'LUCY MARIBEL', NULL, 'AGUIRRE LEANDRO', NULL, '', NULL, '942072100', NULL, '  VILLA CONTINENTAL   MZO-LT6', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '43816580', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12812, 6304, NULL, '44879133', NULL, NULL, NULL, 'MARIA DEL CARMEN', NULL, 'AIMA CHILE', NULL, '', NULL, '993316757', NULL, '  URBJORGE CHAVEZ  AV LAS MALVINASMZ42-LT11', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '44879133', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12813, 6305, NULL, '29597056', NULL, NULL, NULL, 'MIGUEL ANGEL', NULL, 'ALARCON CARRASCO', NULL, 'malarcon1@unsa.edu.pe', NULL, '957691', NULL, '  URBCAMPO VERDE   C-19', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'malarcon1@unsa.edu.pe', NULL, '29597056', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12814, 6306, NULL, '29408651', NULL, NULL, NULL, 'JAVIER GUIDO', NULL, 'ALARCON CONDORI', NULL, 'jalarconc@unsa.edu.pe', NULL, '462965', NULL, '  URBDOLORES   F-5', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'jalarconc@unsa.edu.pe', NULL, '29408651', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12815, 6307, NULL, '30419796', NULL, NULL, NULL, 'BRADLEY FITZERALD', NULL, 'ALARCON MANRIQUE', NULL, '', NULL, '266534', NULL, '    AV MISTI102', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '30419796', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12816, 6308, NULL, '29483852', NULL, NULL, NULL, 'VERONICA ASUNCION', NULL, 'ALARCON PAREDES', NULL, '', NULL, '959787072', NULL, '    CA ANTIQUILLA353-INTERIOR', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '29483852', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12817, 6309, NULL, '29206819', NULL, NULL, NULL, 'HERBERT ALBERTO', NULL, 'ALARCON VILCA', NULL, '', NULL, '959372365', NULL, '  PACHACUTEC  JR HUAMACHUCO109', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29206819', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12818, 6310, NULL, '40466116', NULL, NULL, NULL, 'ARTURO', NULL, 'ALATRISTA CORRALES', NULL, '', NULL, '958954749', NULL, '  JOSE SANTOS CHOCANO   249', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, '', NULL, '40466116', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12819, 6311, NULL, '40920936', NULL, NULL, NULL, 'MANUEL ALFREDO', NULL, 'ALCAZAR HOLGUIN', NULL, '', NULL, '959338844', NULL, '    JR TUCUMAN212', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '40920936', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12820, 6312, NULL, '70003175', NULL, NULL, NULL, 'ALBERT JHON', NULL, 'ALDONATES MOLINA', NULL, 'aaldonatesmo@unsa.edu.pe', NULL, '956335729', NULL, '  MALECON ZOLEZZI   325', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'aaldonatesmo@unsa.edu.pe', NULL, '70003175', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12821, 6313, NULL, '29639015', NULL, NULL, NULL, 'OLGA MELINA', NULL, 'ALEJANDRO OVIEDO', NULL, 'oalejandro@unsa.edu.pe', NULL, '958896480', NULL, '  URBPEDRO DIEZ CANSECO   A-14', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'oalejandro@unsa.edu.pe', NULL, '29639015', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12822, 6314, NULL, '29371284', NULL, NULL, NULL, 'FRANCISCO DOMINGO', NULL, 'ALEJO ZAPATA', NULL, '', NULL, '427203', NULL, '  URBRESMONTERRICO   H-1B', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, '', NULL, '29371284', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12823, 6315, NULL, '29509359', NULL, NULL, NULL, 'LUIS GERVACIO', NULL, 'ALEMAN ABAD', NULL, 'lalemanab@unsa.edu.pe', NULL, '959327767', NULL, '  URBSTATERESA   A-113 DPTO201', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'lalemanab@unsa.edu.pe', NULL, '29509359', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12824, 6316, NULL, '29390456', NULL, NULL, NULL, 'JUAN MANUEL', NULL, 'ALEMAN ARANIBAR', NULL, '', NULL, '281460', NULL, '     JORGE CHAVEZ736', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29390456', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12825, 6317, NULL, '29426631', NULL, NULL, NULL, 'NOEMI VIVIANA', NULL, 'ALEMAN CORDOVA', NULL, '', NULL, '425643', NULL, '    CA SANTA ANA101', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29426631', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12826, 6318, NULL, '29505336', NULL, NULL, NULL, 'OSCAR EDUARDO', NULL, 'ALEMAN DELGADO', NULL, '', NULL, '258875', NULL, '    CA BOLOGNESI302', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29505336', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12827, 6319, NULL, '29609704', NULL, NULL, NULL, 'ROXANA GABRIELA', NULL, 'ALEMAN DELGADO', NULL, 'ralemand@unsa.edu.pe', NULL, '959312053', NULL, '  TINGO  AV ALFONSO UGARTE550-A', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'ralemand@unsa.edu.pe', NULL, '29609704', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12828, 6320, NULL, '29308632', NULL, NULL, NULL, 'MARIA DEL PILAR', NULL, 'ALEMAN HERRERA', NULL, '', NULL, '405367', NULL, '  URB AURORA   H-5-C', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29308632', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12829, 6321, NULL, '70289798', NULL, NULL, NULL, 'JORDAN', NULL, 'ALEMAN ROMAN', NULL, '', NULL, '959305839', NULL, '    CA PROGRESO202', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '70289798', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12830, 6322, NULL, '40661268', NULL, NULL, NULL, 'YANETH', NULL, 'ALEMAN VILCA', NULL, 'yaleman@unsa.edu.pe', NULL, '958258176', NULL, '  URB13 DE ENERO  AV GRACILAZO DE LA VEGA120', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 12, NULL, 'yaleman@unsa.edu.pe', NULL, '40661268', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12831, 6323, NULL, '09648109', NULL, NULL, NULL, 'YURI AUGUSTO', NULL, 'ALENCASTRE MEDRANO', NULL, 'yalencastre@unsa.edu.pe', NULL, '959754673', NULL, '  IV CENTENARIO   GUTIERREZ DE LA FUENTE312', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'yalencastre@unsa.edu.pe', NULL, '09648109', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12832, 6324, NULL, '29223587', NULL, NULL, NULL, 'LUIS ALBERTO', NULL, 'ALFARO CASAS', NULL, 'casas@unsa.edu.pe', NULL, '054-426800', NULL, '  VILLA MEDICA   TORRE 51202', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'casas@unsa.edu.pe', NULL, '29223587', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12833, 6325, NULL, '29675045', NULL, NULL, NULL, 'MICHAEL JOHANN', NULL, 'ALFARO GOMEZ', NULL, 'malfaro@unsa.edu.pe', NULL, '951441020', NULL, '  PJVILLA DEL MAR   19-A', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'malfaro@unsa.edu.pe', NULL, '29675045', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12834, 6326, NULL, '29530070', NULL, NULL, NULL, 'PEDRO ALFONSO', NULL, 'ALI CHAVEZ', NULL, '', NULL, '', NULL, '    CA BUENOS AIRES143', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29530070', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12835, 6327, NULL, '29289758', NULL, NULL, NULL, 'ARMANDO REYNALDO', NULL, 'ALI GOMEZ', NULL, '', NULL, '', NULL, '  LA ESTANCIA   C-I', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29289758', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12836, 6328, NULL, '29213628', NULL, NULL, NULL, 'GONZALO ANDRES', NULL, 'ALIAGA BARREDA', NULL, '', NULL, '215252', NULL, '     STACATALINA312', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29213628', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12837, 6329, NULL, '06292515', NULL, NULL, NULL, 'DE BOHORQUEZ IRIS ELENA', NULL, 'ALIAGA VILLAFUERTE', NULL, 'ialiaga@unsa.edu.pe', NULL, '974446176', NULL, '  URBLA NEGRITA  CA REPUBLICA ARGENTINA107', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'ialiaga@unsa.edu.pe', NULL, '06292515', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12838, 6330, NULL, '29691968', NULL, NULL, NULL, 'YELKA LEYLA', NULL, 'ALLASI MALAGA', NULL, '', NULL, '959148926', NULL, '  COOPVICTOR ABELAUNDE   E-22', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '29691968', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12839, 6331, NULL, '29230555', NULL, NULL, NULL, 'SOFIA LOURDES', NULL, 'ALLASI SORIA', NULL, '', NULL, '959589334', NULL, '  URBCAMPO VERDE   MZO LT4', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29230555', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12840, 6332, NULL, '29445739', NULL, NULL, NULL, 'CARLOTA', NULL, 'ALMIRON BACA', NULL, 'calmiron@unsa.edu.pe', NULL, '959591223', NULL, '     E-9-B VINA DEL MAR', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 12, NULL, 'calmiron@unsa.edu.pe', NULL, '29445739', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12841, 6333, NULL, '29591854', NULL, NULL, NULL, 'HENRRY WILFREDO', NULL, 'ALMIRON BACA', NULL, '', NULL, '959609015', NULL, '     COMPHABDEAN VALDIVIAMZL 10 LT13', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29591854', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12842, 6334, NULL, '29386170', NULL, NULL, NULL, 'ALBERTO', NULL, 'ALMIRON EHUI', NULL, 'aalmiron@unsa.edu.pe', NULL, '263176', NULL, '  COOPLA ESTRELLA   MZAII LT10', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'aalmiron@unsa.edu.pe', NULL, '29386170', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12843, 6335, NULL, '30855195', NULL, NULL, NULL, 'DORIS MARINA', NULL, 'ALMONTE FLORES', NULL, '', NULL, '209540', NULL, '  SANTA RITA DE CASIA   C-6', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '30855195', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12844, 6336, NULL, '29568476', NULL, NULL, NULL, 'ABDON', NULL, 'ALMONTE MAMANI', NULL, 'aalmonte@unsa.edu.pe', NULL, '9948165', NULL, '  ASOCLOS OLIVOS   A-4', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'aalmonte@unsa.edu.pe', NULL, '29568476', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12845, 6337, NULL, '29416865', NULL, NULL, NULL, 'RALPH RENATO', NULL, 'ALMONTE VELARDE', NULL, 'ralmonte@unsa.edu.pe', NULL, '270730', NULL, '  UMACOLLO   BBALLON FARFAN602', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'ralmonte@unsa.edu.pe', NULL, '29416865', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12846, 6338, NULL, '29562783', NULL, NULL, NULL, 'JUAN CHRISTIAM', NULL, 'ALMUELLE ANGLES', NULL, '', NULL, '214618', NULL, '     RIVERO534', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29562783', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12847, 6339, NULL, '29304008', NULL, NULL, NULL, 'ROSA GREGORIA', NULL, 'ALPACA ARCE', NULL, '', NULL, '211102', NULL, '  URBMUNICIPAL  CA OTERO53', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29304008', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12848, 6340, NULL, '29478844', NULL, NULL, NULL, 'MARIZELA AGAPITA', NULL, 'ALPACA CHAVEZ', NULL, 'malpacac@unsa.edu.pe', NULL, '995369200', NULL, '  URBTAHUAYCANI   D-36', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'malpacac@unsa.edu.pe', NULL, '29478844', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12849, 6341, NULL, '29722312', NULL, NULL, NULL, 'CRISTIAN PABEL', NULL, 'ALTUNA SOTOMAYOR', NULL, 'caltuna@unsa.edu.pe', NULL, '340275', NULL, '  URBQUINTA EL SOL   A-48', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'caltuna@unsa.edu.pe', NULL, '29722312', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12850, 6342, NULL, '29549713', NULL, NULL, NULL, 'HUMBERTO DENNY', NULL, 'ALTUNA SOTOMAYOR', NULL, 'haltuna@unsa.edu.pe', NULL, '', NULL, '    CA MELGAR320', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'haltuna@unsa.edu.pe', NULL, '29549713', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12851, 6343, NULL, '29544435', NULL, NULL, NULL, '', NULL, 'ALVAREZ ALVAREZ', NULL, '', NULL, '', NULL, '  URB15 DE AGOSTO   BBALLON FARFAN304', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29544435', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12852, 6344, NULL, '29533509', NULL, NULL, NULL, 'MARIA DEL CARMEN', NULL, 'ALVAREZ ARDILES', NULL, '', NULL, '436990', NULL, '  CIUDAD MI TRABAJO   JOSE OLAYA125', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29533509', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12853, 6345, NULL, '29478838', NULL, NULL, NULL, 'MELECIO', NULL, 'ALVAREZ CALCINA', NULL, 'malvarezcal@unsa.edu.pe', NULL, '953919', NULL, '  TIO GRANDE  AV KENNEDYN-3', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'malvarezcal@unsa.edu.pe', NULL, '29478838', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12854, 6346, NULL, '29381276', NULL, NULL, NULL, 'NADYA LUZGARDA', NULL, 'ALVAREZ CALVO', NULL, '', NULL, '454565', NULL, '  STAROSA   JUAN MANUEL POLAR609', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29381276', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12855, 6347, NULL, '29525752', NULL, NULL, NULL, 'JOSE ALEJANDRO', NULL, 'ALVAREZ CHAVEZ', NULL, 'jalvarezc@unsa.edu.pe', NULL, '054-266973', NULL, '  URBGRAFICOS  CA MARIATEGUI500', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'jalvarezc@unsa.edu.pe', NULL, '29525752', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12856, 6348, NULL, '29295854', NULL, NULL, NULL, 'SANDRA MARIETTA', NULL, 'ALVAREZ CHAVEZ', NULL, '', NULL, '285912', NULL, '  URBLOS ANGELES   SAN JORGEI-2', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29295854', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12857, 6349, NULL, '29669154', NULL, NULL, NULL, 'ANGEL MAXIMO', NULL, 'ALVAREZ ESCARCENA', NULL, '', NULL, '945369596', NULL, '  AHHORACIO ZEVALLOS GAMEZ  AV 9 DE DICIEMBREMZ23-LT12-ZN', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '29669154', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12858, 6350, NULL, '42783022', NULL, NULL, NULL, 'DE DIAZ MARIA DENISSE', NULL, 'ALVAREZ HUANCA', NULL, 'malvarezhu@unsa.edu.pe', NULL, '992597537', NULL, '  LOS ROSALES   C2', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 12, NULL, 'malvarezhu@unsa.edu.pe', NULL, '42783022', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12859, 6351, NULL, '29422079', NULL, NULL, NULL, 'VICTOR FERNANDO', NULL, 'ALVAREZ HUANQUI', NULL, '', NULL, '959372706', NULL, '  URBJUAN EL BUENO   MZJ-LT6', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29422079', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12860, 6352, NULL, '29272339', NULL, NULL, NULL, 'CARMEN GRACIELA', NULL, 'ALVAREZ LEWIS', NULL, '', NULL, '959743377', NULL, '    Av Tarapaca413', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '29272339', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12861, 6353, NULL, '30825902', NULL, NULL, NULL, 'MOISES', NULL, 'ALVAREZ MAMANI', NULL, '', NULL, '532651', NULL, '  CATARINDO   INDEHISN', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '30825902', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12862, 6354, NULL, '29236736', NULL, NULL, NULL, 'MIGUEL FRANCISCO', NULL, 'ALVAREZ POLAR', NULL, '', NULL, '958708681', NULL, '  LA CAMPINA  PJ LOS SAFIROSA-6', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29236736', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12863, 6355, NULL, '42577303', NULL, NULL, NULL, 'DE ROZAN MARLA', NULL, 'ALVAREZ PRUDENCIO', NULL, 'malvarezpr@unsa.edu.pe', NULL, '956380374', NULL, '  URBAMAUTA   B-11', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 12, NULL, 'malvarezpr@unsa.edu.pe', NULL, '42577303', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12864, 6356, NULL, '30416379', NULL, NULL, NULL, 'JOSE LINO', NULL, 'ALVAREZ RIVERA', NULL, 'jalvarezr@unsa.edu.pe', NULL, '454990', NULL, '  ALTO SAN MARTIN  AV NICOLAS DE PIEROLA731', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'jalvarezr@unsa.edu.pe', NULL, '30416379', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12865, 6357, NULL, '29656430', NULL, NULL, NULL, 'LUIS ENRIQUE', NULL, 'ALVAREZ RODRIGUEZ', NULL, '', NULL, '336472', NULL, '  GRAFICOS  AV EL SOL103', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29656430', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12866, 6358, NULL, '29641961', NULL, NULL, NULL, 'MARIEL VERONICA', NULL, 'ALVAREZ RODRIGUEZ', NULL, 'malvarezro@unsa.edu.pe', NULL, '957871518', NULL, '  COOPDE INGENIEROS   MZ CLT15', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'malvarezro@unsa.edu.pe', NULL, '29641961', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12867, 6359, NULL, '29512202', NULL, NULL, NULL, 'LILIANA ROSARIO', NULL, 'ALVAREZ SALINAS', NULL, 'lalvarezsal@unsa.edu.pe', NULL, '966580055', NULL, '    CA BOLOGNESI217', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'lalvarezsal@unsa.edu.pe', NULL, '29512202', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12868, 6360, NULL, '29602206', NULL, NULL, NULL, 'ANA MARIA', NULL, 'ALVAREZ SANZ', NULL, 'aalvarezsa@unsa.edu.pe', NULL, '958910122', NULL, '  URBMIRASOL   C 5 y 6', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'aalvarezsa@unsa.edu.pe', NULL, '29602206', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12869, 6361, NULL, '29636733', NULL, NULL, NULL, 'PEDRO PABLO', NULL, 'ALVAREZ SANZ', NULL, 'palvarezs@unsa.edu.pe', NULL, '959755753', NULL, '  LA LIBERTAD  av ALFONSO UGARTE722', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'palvarezs@unsa.edu.pe', NULL, '29636733', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12870, 6362, NULL, '29282203', NULL, NULL, NULL, 'LUIS ALBERTO', NULL, 'ALVAREZ SOTO', NULL, 'lalvarezs@unsa.edu.pe', NULL, '', NULL, '  URBSAN LORENZO   MALECON ZOLLEZI316', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'lalvarezs@unsa.edu.pe', NULL, '29282203', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12871, 6363, NULL, '29256776', NULL, NULL, NULL, 'VICTOR LUDGARDO', NULL, 'ALVAREZ TOHALINO', NULL, 'vtohalino@unsa.edu.pe', NULL, '994325620', NULL, '  LOS PORTALES   ZAMACOLA209', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'vtohalino@unsa.edu.pe', NULL, '29256776', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12872, 6364, NULL, '40459915', NULL, NULL, NULL, 'ALEXANDER RAFAEL', NULL, 'ALVAREZ TURPO', NULL, '', NULL, '958985408', NULL, '  URB15 DE AGOSTO  CA BBFARFAN304', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '40459915', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12873, 6365, NULL, '06445947', NULL, NULL, NULL, 'ROSA ROSARIO', NULL, 'ALVIZ ALVARO', NULL, 'ralviz@unsa.edu.pe', NULL, '268201', NULL, '  URBVILLA AREQPAMPAS POLANCO   G-7', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 11, NULL, 'ralviz@unsa.edu.pe', NULL, '06445947', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12874, 6366, NULL, '29590657', NULL, NULL, NULL, 'JAIME RAFAEL', NULL, 'AMADO PINTO', NULL, 'jamado@unsa.edu.pe', NULL, '486486', NULL, '  RESMONTERRICO   E-6', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 12, NULL, 'jamado@unsa.edu.pe', NULL, '29590657', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12875, 6367, NULL, '29306887', NULL, NULL, NULL, 'HUGO ALFREDO', NULL, 'AMANQUE CHAINA', NULL, '', NULL, '', NULL, '  VILLA HERMOZA  CA 12 DE OCTUBRE702', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29306887', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12876, 6368, NULL, '29691652', NULL, NULL, NULL, 'KARLA CECILIA', NULL, 'AMESQUITA VERA', NULL, '', NULL, '998050577', NULL, '  FRANCISCO MOSTAJO   402 EDIF12', NULL, 0, NULL, 2, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '29691652', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12877, 6369, NULL, '29231113', NULL, NULL, NULL, 'ANGEL FELIPE', NULL, 'AMEZQUITA CHIRINOS', NULL, 'aamezquita@unsa.edu.pe', NULL, '999921112', NULL, '  LA CAMPINA  AV LOS JARDINESC-1', NULL, 0, NULL, 1, NULL, 3, NULL, NULL, NULL, 11, NULL, 'aamezquita@unsa.edu.pe', NULL, '29231113', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12878, 6370, NULL, '29669548', NULL, NULL, NULL, 'LUIS ALBERTO', NULL, 'AMPUERO CHORA', NULL, '', NULL, '507110', NULL, '    PJ FERRE105', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 11, NULL, '', NULL, '29669548', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12879, 6371, NULL, '29273482', NULL, NULL, NULL, 'SILVIA DIANA', NULL, 'AMPUERO ESPINOZA', NULL, 'sampueroe@unsa.edu.pe', NULL, '959370536', NULL, '  VILLA MEDICA   EDIF2 DPTO204', NULL, 0, NULL, 2, NULL, 3, NULL, NULL, NULL, 12, NULL, 'sampueroe@unsa.edu.pe', NULL, '29273482', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12880, 6372, NULL, '29535450', NULL, NULL, NULL, 'VICTOR ENRIQUE', NULL, 'AMPUERO PAZ', NULL, '', NULL, '233413', NULL, '  URBLOS ANGELES  CA SAN VICENTE104', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 12, NULL, '', NULL, '29535450', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12881, 6373, NULL, '29372188', NULL, NULL, NULL, 'ROBERTO HUGO', NULL, 'ANCA COA', NULL, '', NULL, '953646769', NULL, '  HORACIO ZEBALLOS GAMEZ   MZ6 LT16-ZN12', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '29372188', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12882, 6374, NULL, '80614119', NULL, NULL, NULL, 'IRVING ENRIQUE', NULL, 'ANCI RAMOS', NULL, '', NULL, '974584203', NULL, '  URBLA LIBERTAD  CA TUPAC AMARU620', NULL, 0, NULL, 1, NULL, NULL, NULL, 4, NULL, 31, NULL, '', NULL, '80614119', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost'),
(12883, 6375, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, 'OPERADOR1', NULL, 'OPERADOR1', NULL, 18, 'admin', '2018-10-28 12:49:44', 'ELIMINAR', 'localhost');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `CarId` int(8) NOT NULL COMMENT 'Clave primaria',
  `CarFKTipUsu` int(8) DEFAULT NULL,
  `CarDes` varchar(100) DEFAULT NULL,
  `CarCom` varchar(10) DEFAULT NULL COMMENT 'cargo de comisiones',
  `CarFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`CarId`, `CarFKTipUsu`, `CarDes`, `CarCom`, `CarFKEstReg`) VALUES
(1, 3, 'SUPERVISOR', 'NO', 18),
(2, 3, 'TECNICO', 'NO', 18),
(3, 3, 'CONTROLADOR', 'NO', 18),
(4, 20, 'AYUDANTE TECNICO', 'NO', 18),
(5, 4, 'GUARDERO', 'NO', 18),
(6, 4, 'COORDINACION DE REFRIGERIO', 'NO', 18),
(7, 4, 'APOYO CONTEO', 'NO', 18),
(8, 4, 'APOYO REFRIGERIO', 'NO', 18),
(9, 4, 'ASESOR LEGAL', 'NO', 18),
(10, 4, 'CONTEO', 'NO', 18),
(11, 4, 'ESTADISTICA', 'NO', 18),
(12, 4, 'MANTENIMIENTO', 'NO', 18),
(13, 4, 'OFICINA DUA', 'NO', 18),
(14, 4, 'ORIENTACION', 'NO', 18),
(15, 4, 'PERIMETRO', 'NO', 18),
(16, 4, 'REVISION', 'NO', 18),
(17, 4, 'VIGILANTE', 'NO', 18),
(18, 4, 'ENTREGA DE REFRIGERIOS', 'NO', 18),
(19, 4, 'REFRIGERIO', 'NO', 18),
(20, 4, 'APOYO ADMINISTRATIVO A COORDINACION AREAS', 'NO', 18),
(21, 3, 'COMISION DE ADMISION', 'SI', 18),
(22, 3, 'DIRECCION UNIVERSITARIA DE ADMISION', 'SI', 18),
(23, 3, 'COMISION SUPERVISORA - CONSEJO UNIVERSITARIO', 'SI', 18),
(24, 3, 'COORDINADOR', 'SI', 18),
(25, 3, 'PLANIFICACION GENERAL', 'SI', 18),
(26, 3, 'SECRETARIO GENERAL', 'SI', 18),
(27, 3, 'APOYO TECNICO A COMISION', 'SI', 18),
(28, 3, 'FORMULADORES', 'SI', 18),
(29, 4, 'MEDICO(A)', 'SI', 18),
(30, 4, 'ENFERMERO(A)', 'SI', 18),
(31, 4, 'PERSONAL DIRECCION UNIVERSITARIA DE ADMISION', 'SI', 18),
(32, 4, 'APOYO COMISION DE ADMISION', 'SI', 18),
(33, 4, 'DIGITADORES', 'SI', 18),
(34, 4, 'IMPRESOR', 'SI', 18),
(35, 4, 'LIMPIEZA', 'SI', 18),
(36, 4, 'OFICINA UNIVERSITARIA DE INFORMATICA Y SISTEMAS', 'SI', 18),
(37, 4, 'TRANSPORTES', 'SI', 18),
(38, 4, 'VIGILANTES', 'SI', 18),
(39, 4, 'MANTENIMIENTO', 'SI', 18),
(40, 4, 'DEFENSA CIVIL', 'NO', 18),
(41, 4, 'SUPERVISOR', 'NO', 18),
(42, 4, 'ARCOS DE SEGURIDAD', 'NO', 18),
(43, 4, 'DETECTOR DE SENALES', 'NO', 18),
(44, 4, 'GARRET MANUAL', 'NO', 18),
(45, 4, 'SUPERVISORES PUERTAS', 'NO', 18),
(46, 4, 'CONTEO COORDINACION ARQUITECTURA', 'NO', 18),
(47, 4, 'CONTEO COORDINACION CIVIL', 'NO', 18),
(50, 4, 'CONTEO COORDINACION GEOLOGIA', 'NO', 18),
(51, 4, 'APOYO ADMINISTRATIVO', 'NO', 18),
(52, 4, 'RESPONSABLE CONTEO', 'NO', 18),
(54, 4, 'ORGANIZACION Y COORDINACION REFRIGERIOS / SECTOR', 'NO', 18),
(55, 4, 'CONTROL PUERTAS INGRESO', 'NO', 18),
(56, 4, 'ORIENTACION AL POSTULANTE Y ATENCION REFRIGERIOS', 'NO', 18),
(57, 4, 'CONTROL SERVICIOS INGENICOS', 'NO', 18),
(59, 4, 'CONTROL ORDEN COLAS DE INGRESO', 'NO', 18),
(62, 4, 'SUPERVISOR DETECTOR DE SEÑANES', 'NO', 18),
(63, 4, 'SS.HH', 'NO', 18),
(64, 4, 'CONTROL MESAS', 'NO', 18),
(65, 4, 'IMAGEN INSTITUCIONAL', 'NO', 18),
(66, 3, 'COORDINADOR GENERAL', 'SI', 18),
(67, 3, 'SEGURIDAD', 'NO', 18),
(68, 3, 'APOYO TECNICO DOCENTE', 'NO', 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `DepId` int(8) NOT NULL COMMENT 'Clave primaria',
  `DepDes` varchar(40) DEFAULT NULL,
  `DepFKFac` int(8) DEFAULT NULL,
  `DepFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`DepId`, `DepDes`, `DepFKFac`, `DepFKEstReg`) VALUES
(1, 'ADMINISTRACION', 1, 18),
(2, 'MARKETING', 1, 18),
(3, 'GESTION', 1, 18),
(4, 'TRABAJO SOCIAL', 2, 18),
(5, 'ANTROPOLOGIA', 2, 18),
(6, 'TURISMO Y HOTELERIA', 2, 18),
(7, 'SOCIOLOGIA', 3, 18),
(8, 'HISTORIA', 3, 18),
(9, 'PSICOLOGIA', 3, 18),
(10, 'RELACIONES INDUSTRIALES', 3, 18),
(11, 'CIENCIAS DE LA COMUNICACION', 3, 18),
(12, 'DERECHO', 4, 18),
(13, 'ARTES', 5, 18),
(14, 'FILOSOFIA', 5, 18),
(15, 'LITERATURA Y LINGUISTICA', 5, 18),
(16, 'EDUCACION', 6, 18),
(17, 'CONTABILIDAD', 7, 18),
(18, 'FINANZAS', 7, 18),
(19, 'ECONOMIA', 8, 18),
(20, 'INGENIERIA QUIMICA', 9, 18),
(21, 'INGENIERIA AMBIENTAL', 9, 18),
(22, 'INGENIERIA DE MATERIALES', 9, 18),
(23, 'INGENIERIA METALURGICA', 9, 18),
(24, 'INGENIERIA DE INDUSTRIAS ALIMENTARIAS', 9, 18),
(25, 'INGENIERIA DE SISTEMAS', 10, 18),
(26, 'INGENIERIA ELECTRICA', 10, 18),
(27, 'INGENIERIA ELECTRONICA', 10, 18),
(28, 'INGENIERIA MECANICA', 10, 18),
(29, 'INGENIERIA INDUSTRIAL', 10, 18),
(30, 'CIENCIAS DE LA COMPUTACION', 10, 18),
(31, 'INGENIERIA DE TELECOMUNICACIONES', 10, 18),
(32, 'INGENIERIA GEOFISICA', 11, 18),
(33, 'INGENIERIA GEOLOGICA', 11, 18),
(34, 'INGENIERIA DE MINAS', 11, 18),
(35, 'INGENIERIA CIVIL', 12, 18),
(36, 'INGENIERIA SANITARIA', 12, 18),
(37, 'FISICA', 13, 18),
(38, 'MATEMATICAS', 13, 18),
(39, 'QUIMICA', 13, 18),
(40, 'ARQUITECTURA', 14, 18),
(41, 'BIOLOGIA', 15, 18),
(42, 'CIENCIAS DE LA NUTRICION', 15, 18),
(43, 'INGENIERIA PESQUERA', 15, 18),
(44, 'MEDICINA', 16, 18),
(45, 'ENFERMERIA', 17, 18),
(46, 'AGRONOMIA', 18, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facultad`
--

CREATE TABLE `facultad` (
  `FacId` int(8) NOT NULL COMMENT 'Clave primaria',
  `FacDes` varchar(200) DEFAULT NULL,
  `FacFKAre` int(8) DEFAULT NULL,
  `FacFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facultad`
--

INSERT INTO `facultad` (`FacId`, `FacDes`, `FacFKAre`, `FacFKEstReg`) VALUES
(1, 'FACULTAD DE ADMINISTRACION', 3, 18),
(2, 'FACULTAD DE CIENCIAS HISTORICO SOCIALES', 3, 18),
(3, 'FACULTAD DE PSICOLOGIA RRII CS. DE LA COMUNICACION', 3, 18),
(4, 'FACULTAD DE DERECHO', 3, 18),
(5, 'FACULTAD DE FILOSOFIA Y HUMANIDADES', 3, 18),
(6, 'FACULTAD DE EDUCACION', 3, 18),
(7, 'FACULTAD DE CIENCIAS CONTABLES Y FINANCIERAS', 3, 18),
(8, 'FACULTAD DE ECONOMIA', 3, 18),
(9, 'FACULTAD DE INGENIERIA DE PROCESOS', 2, 18),
(10, 'FACULTAD DE INGENIERIA DE PRODUCCION Y SERVICIOS', 2, 18),
(11, 'FACULTAD DE GEOLOGIA, GEOFISICA Y MINAS', 2, 18),
(12, 'FACULTAD DE INGENIERIA CIVIL', 2, 18),
(13, 'FACULTAD DE CIENCIAS NATURALES Y FORMALES', 2, 18),
(14, 'FACULTAD DE ARQUITECTURA', 2, 18),
(15, 'FACULTAD DE CIENCIAS BIOLOGICAS', 1, 18),
(16, 'FACULTAD DE MEDICINA', 1, 18),
(17, 'FACULTAD DE ENFERMERIA', 1, 18),
(18, 'FACULTAD DE AGRONOMIA', 1, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

CREATE TABLE `general` (
  `GenId` int(8) NOT NULL COMMENT 'Clave primaria',
  `GenCat` varchar(20) DEFAULT NULL,
  `GenSubCat` varchar(30) DEFAULT NULL,
  `GenCodMos` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `general`
--

INSERT INTO `general` (`GenId`, `GenCat`, `GenSubCat`, `GenCodMos`) VALUES
(1, 'GENERO', 'MASCULINO', 'M'),
(2, 'GENERO', 'FEMENINO', 'F'),
(3, 'TIPO DE USUARIO', 'DOCENTE', 'USUDOC'),
(4, 'TIPO DE USUARIO', 'ADMINISTRATIVO', 'USUADM'),
(5, 'ESTADO DE REGISTRO', 'SORTEADO', 'SOR'),
(6, 'ESTADO DE REGISTRO', 'INSCRIPCION CANCELADA', 'INSCAN'),
(7, 'ESTADO DE REGISTRO', 'INSCRITO', 'INS'),
(8, 'CATEGORIA', 'PRINCIPAL', 'CATDOCPRI'),
(9, 'CATEGORIA', 'ASOCIADO', 'CATDOCASO'),
(10, 'CATEGORIA', 'AUXILIAR', 'CATDOCAUX'),
(11, 'CATEGORIA', 'NOMBRADO', 'CATADMNOM'),
(12, 'CATEGORIA', 'CONTRATADO', 'CATADMCON'),
(13, 'TIPO DE PROCESO', 'ORDINARIO', 'PROORD'),
(14, 'TIPO DE PROCESO', 'EXTRAORDINARIO', 'PROEXT'),
(15, 'TIPO DE PROCESO', 'CEPRUNSA', 'PROCEP'),
(16, 'ESTADO DE REGISTRO', 'CONFIRMADO', 'CON'),
(17, 'ESTADO DE REGISTRO', 'ASIGNADO', 'ASI'),
(18, 'ESTADO DE REGISTRO', 'ACTIVO', 'ACT'),
(19, 'ESTADO DE REGISTRO', 'INACTIVO', 'INA'),
(20, 'TIPO DE USUARIO', 'ESTUDIANTE', 'USUEST'),
(21, 'TIPO DE PROCESO', 'PRE QUINTOS', 'PROPREQUI'),
(22, 'TIPO DE USUARIO', 'ADMINISTRADOR', 'ADMIN'),
(23, 'TIPO DE USUARIO', 'OPERADOR', 'OPERA'),
(25, 'TURNO DE PROCESO', 'UNICO', 'TURUNI'),
(26, 'TURNO DE PROCESO', 'MANANA', 'TURMAN'),
(27, 'TURNO DE PROCESO', 'TARDE', 'TURTAR'),
(28, 'ESTADO DE REGISTRO', 'SORTEO CANCELADO', 'SORCAN'),
(29, 'ESTADO DE REGISTRO', 'ASIGNACION CANCELADA', 'ASICAN'),
(30, 'ESTADO DE REGISTRO', 'REEMPLAZADO', 'REE'),
(31, 'ESTADO DE PROCESO', 'PASADO', 'ESTPROPAS'),
(32, 'ESTADO DE PROCESO', 'ACTUAL', 'ESTPROACT'),
(33, 'ESTADO DE PROCESO', 'FUTURO', 'ESTPROFUT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pabellon`
--

CREATE TABLE `pabellon` (
  `PabId` int(8) NOT NULL COMMENT 'Clave primaria',
  `PabIdLog` varchar(5) DEFAULT NULL,
  `PabDes` varchar(200) DEFAULT NULL,
  `PabFKAre` int(8) DEFAULT NULL,
  `PabFKDep` int(8) DEFAULT NULL,
  `PabFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pabellon`
--

INSERT INTO `pabellon` (`PabId`, `PabIdLog`, `PabDes`, `PabFKAre`, `PabFKDep`, `PabFKEstReg`) VALUES
(1, NULL, 'MEDICINA', 1, 44, 18),
(2, NULL, 'MEDICINA POSGRADO', 1, 44, 18),
(3, NULL, 'CS. DE LA NUTRICION', 1, 42, 18),
(4, NULL, 'PESQUERIA', 1, 43, 18),
(5, NULL, 'BIOLOGIA', 1, 41, 18),
(6, NULL, 'ENFERMERIA', 1, 45, 18),
(7, NULL, 'RETEN', 1, 44, 18),
(8, NULL, 'APOYO COORDINACION', 1, 44, 18),
(9, '05', 'INGENIERIA INDUSTRIAL', 2, 29, 18),
(10, '02', 'MATEMATICAS', 2, 38, 18),
(11, '03', 'FISICA', 2, 37, 18),
(12, '04', 'QUIMICA', 2, 39, 18),
(13, '01', 'INGENIERIA CIVIL', 2, 35, 18),
(14, '33', 'LABORATORIO DE CONCRETO (INGENIERIA CIVIL)', 2, 35, 18),
(15, '34', 'INGENIERIA QUIMICA', 2, 20, 18),
(16, '37', 'INGENIERIA METALURGICA', 2, 23, 18),
(17, '36', 'INGENIERIA ELECTRICA', 2, 26, 18),
(18, '35', 'INGENIERIA MECANICA', 2, 28, 18),
(19, NULL, 'INGENIERIA GEOLOGICA, GEOFISICA Y MINAS (NUEVO)', 2, 32, 18),
(20, NULL, 'INGENIERIA GEOLOGICA, GEOFISICA Y MINAS (ANTIGUO)\n', 2, 32, 18),
(21, NULL, 'INGENIERIA GEOLOGICA, GEOFISICA Y MINAS (WILLIAM JENKS)', 2, 32, 18),
(22, '27', 'INGENIERIA DE SISTEMAS', 2, 25, 18),
(23, '28', 'INGENIERIA ELECTRONICA', 2, 27, 18),
(24, '26', 'ARQUITECTURA', 2, 40, 18),
(25, '31', 'INGENIERIA DE INDUSTRIAS ALIMENTARIAS', 2, 24, 18),
(26, '29', 'FAC. DE ADMINISTRACION', 2, 1, 18),
(27, '30', 'ARTES', 2, 13, 18),
(28, '32', 'INGENIERIA DE MATERIALES', 2, 22, 18),
(29, NULL, 'RETEN', 2, 29, 18),
(30, NULL, 'APOYO DE COORDINACION', 2, 29, 18),
(32, NULL, 'CONTABILIDAD ANTIGUO', 3, 17, 18),
(33, NULL, 'CONTABILIDAD NUEVO', 3, 17, 18),
(34, NULL, 'DERECHO (PABELLON A)', 3, 12, 18),
(35, NULL, 'DERECHO (PABELLON B)', 3, 12, 18),
(36, NULL, 'DERECHO (NUEVO 3)', 3, 12, 18),
(37, NULL, 'ECONOMIA', 3, 19, 18),
(38, NULL, 'CS. DE LA COMUNICACION', 3, 11, 18),
(39, NULL, 'EDUCACION (PABELLON ANTIGUO)', 3, 16, 18),
(40, NULL, 'EDUCACION (PABELLON NUEVO)', 3, 16, 18),
(41, NULL, 'PSICOLOGIA', 3, 12, 18),
(42, NULL, 'RELACIONES INDUSTRIALES', 3, 10, 18),
(43, NULL, 'SERVICIOS MULTIPLES', 3, 6, 18),
(44, NULL, 'CS. HISTORICO SOCIALES', 3, 8, 18),
(45, NULL, 'RETEN', 3, 17, 18),
(46, NULL, 'APOYO COORDINACION', 3, 17, 18),
(47, NULL, 'AGRONOMIA', 5, 46, 18),
(48, NULL, 'RETEN', 5, 46, 18),
(49, NULL, 'APOYO COORDINACION', 5, 46, 18),
(50, NULL, 'RETEN', 4, NULL, 18),
(51, NULL, 'APOYO COORDINACION', 4, NULL, 18),
(52, NULL, 'RETEN', 6, NULL, 18),
(53, NULL, 'APOYO COORDINACION', 6, NULL, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `UsuId` int(8) NOT NULL COMMENT 'Clave primaria',
  `UsuDni` varchar(10) DEFAULT NULL,
  `UsuCui` varchar(10) DEFAULT NULL,
  `UsuNom` varchar(100) NOT NULL,
  `UsuApe` varchar(100) NOT NULL,
  `UsuCorEle` varchar(100) DEFAULT NULL,
  `UsuTel` varchar(100) DEFAULT NULL,
  `UsuDir` varchar(200) DEFAULT NULL,
  `UsuFot` blob,
  `UsuDepAca` varchar(150) DEFAULT NULL COMMENT 'departamento academico',
  `UsuFKAre` int(8) DEFAULT NULL,
  `UsuNumPro` int(4) DEFAULT '0',
  `UsuFun` varchar(100) DEFAULT NULL,
  `UsuFKGen` int(8) DEFAULT NULL,
  `UsuFKTip` int(8) DEFAULT NULL,
  `UsuFKTip2` int(8) DEFAULT NULL,
  `UsuDep` varchar(150) DEFAULT NULL COMMENT 'dependencia',
  `UsuFKCat` int(8) DEFAULT NULL,
  `UsuNomUsu` varchar(100) DEFAULT NULL,
  `UsuConUsu` varchar(100) DEFAULT NULL,
  `UsuFKEstReg` int(8) DEFAULT '18'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `usuario`
--
DELIMITER $$
CREATE TRIGGER `bitacorausuarioactualizar` AFTER UPDATE ON `usuario` FOR EACH ROW INSERT bitacorausuario (bitacorausuario.UsuId, bitacorausuario.UsuDniAnt, bitacorausuario.UsuDniNue, bitacorausuario.UsuCuiAnt, bitacorausuario.UsuCuiNue, bitacorausuario.UsuNomAnt, bitacorausuario.UsuNomNue, bitacorausuario.UsuApeAnt, bitacorausuario.UsuApeNue, bitacorausuario.UsuCorEleAnt, bitacorausuario.UsuCorEleNue, bitacorausuario.UsuTelAnt, bitacorausuario.UsuTelNue, bitacorausuario.UsuDirAnt, bitacorausuario.UsuDirNue, bitacorausuario.UsuNumProAnt, bitacorausuario.UsuNumProNue, bitacorausuario.UsuGenAnt, bitacorausuario.UsuGenNue, bitacorausuario.UsuFKTip1Ant, bitacorausuario.UsuFKTip1Nue, bitacorausuario.UsuFKTip2Ant, bitacorausuario.UsuFKTip2Nue, bitacorausuario.UsuFKCatAnt, bitacorausuario.UsuFKCatNue, bitacorausuario.UsuNomUsuAnt, bitacorausuario.UsuNomUsuNue, bitacorausuario.UsuConUsuAnt, bitacorausuario.UsuConUsuNue, bitacorausuario.UsuFKEstRegAnt, bitacorausuario.UsuFKEstRegNue, bitacorausuario.usuario, bitacorausuario.fecha, bitacorausuario.operacion, bitacorausuario.host)
VALUES (NEW.UsuId, OLD.UsuDni, NEW.UsuDni, OLD.UsuCui, NEW.UsuCui, OLD.UsuNom, NEW.UsuNom, OLD.UsuApe, NEW.UsuApe, OLD.UsuCorEle, NEW.UsuCorEle, OLD.UsuTel, NEW.UsuTel, OLD.UsuDir, NEW.UsuDir, OLD.UsuNumPro, NEW.UsuNumPro, OLD.UsuFKGen, NEW.UsuFKGen, OLD.UsuFKTip, NEW.UsuFKTip, OLD.UsuFKTip2, NEW.UsuFKTip2, OLD.UsuFKCat, NEW.UsuFKCat, OLD.UsuNomUsu, NEW.UsuNomUsu, OLD.UsuConUsu, NEW.UsuConUsu, OLD.UsuFKEstReg, NEW.UsuFKEstReg, substring(user(),1,instr(user(),'@')-1), now(), 'ACTUALIZAR',substring(user(),(instr(user(),'@')+1)) )
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bitacorausuarioeliminar` BEFORE DELETE ON `usuario` FOR EACH ROW INSERT bitacorausuario (bitacorausuario.UsuId, bitacorausuario.UsuDniNue, bitacorausuario.UsuCuiNue, bitacorausuario.UsuNomNue, bitacorausuario.UsuApeNue, bitacorausuario.UsuCorEleNue, bitacorausuario.UsuTelNue, bitacorausuario.UsuDirNue, bitacorausuario.UsuNumProNue, bitacorausuario.UsuGenNue, bitacorausuario.UsuFKTip1Nue, bitacorausuario.UsuFKTip2Nue, bitacorausuario.UsuFKCatNue, bitacorausuario.UsuNomUsuNue, bitacorausuario.UsuConUsuNue, bitacorausuario.UsuFKEstRegNue, bitacorausuario.usuario, bitacorausuario.fecha, bitacorausuario.operacion, bitacorausuario.host)
VALUES (OLD.UsuId, OLD.UsuDni, OLD.UsuCui, OLD.UsuNom, OLD.UsuApe, OLD.UsuCorEle, OLD.UsuTel, OLD.UsuDir, OLD.UsuNumPro, OLD.UsuFKGen, OLD.UsuFKTip, OLD.UsuFKTip2, OLD.UsuFKCat, OLD.UsuNomUsu, OLD.UsuConUsu, OLD.UsuFKEstReg, substring(user(),1,instr(user(),'@')-1), now(), 'ELIMINAR',substring(user(),(instr(user(),'@')+1)))
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bitacorausuarioinsertar` AFTER INSERT ON `usuario` FOR EACH ROW INSERT bitacorausuario (bitacorausuario.UsuId, bitacorausuario.UsuDniNue, bitacorausuario.UsuCuiNue, bitacorausuario.UsuNomNue, bitacorausuario.UsuApeNue, bitacorausuario.UsuCorEleNue, bitacorausuario.UsuTelNue, bitacorausuario.UsuDirNue, bitacorausuario.UsuNumProNue, bitacorausuario.UsuGenNue, bitacorausuario.UsuFKTip1Nue, bitacorausuario.UsuFKTip2Nue, bitacorausuario.UsuFKCatNue, bitacorausuario.UsuNomUsuNue, bitacorausuario.UsuConUsuNue, bitacorausuario.UsuFKEstRegNue, bitacorausuario.usuario, bitacorausuario.fecha, bitacorausuario.operacion, bitacorausuario.host)
VALUES (NEW.UsuId, NEW.UsuDni, NEW.UsuCui, NEW.UsuNom, NEW.UsuApe, NEW.UsuCorEle, NEW.UsuTel, NEW.UsuDir, NEW.UsuNumPro, NEW.UsuFKGen, NEW.UsuFKTip, NEW.UsuFKTip2, NEW.UsuFKCat, NEW.UsuNomUsu, NEW.UsuConUsu, NEW.UsuFKEstReg, substring(user(),1,instr(user(),'@')-1), now(), 'INSERTAR',substring(user(),(instr(user(),'@')+1)) )
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariorol`
--

CREATE TABLE `usuariorol` (
  `UsuRolId` int(8) NOT NULL COMMENT 'Clave primaria',
  `UsuRolFKTipUsu` int(8) DEFAULT NULL,
  `UsuRolFKRol` int(8) DEFAULT NULL,
  `UsuRolFKEstReg` int(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admisionarea`
--
ALTER TABLE `admisionarea`
  ADD PRIMARY KEY (`AdmAreId`),
  ADD KEY `AdmAreFKAreId` (`AdmAreFKAreId`),
  ADD KEY `AdmAreFKAdmCabId` (`AdmAreFKAdmCabId`),
  ADD KEY `AdmAreFKEstReg` (`AdmAreFKEstReg`);

--
-- Indices de la tabla `admisionaula`
--
ALTER TABLE `admisionaula`
  ADD PRIMARY KEY (`AdmAulId`),
  ADD KEY `AdmAulFKAulId` (`AdmAulFKAulId`),
  ADD KEY `AdmAulFKAdmPabId` (`AdmAulFKAdmPabId`),
  ADD KEY `AdmAulFKEstReg` (`AdmAulFKEstReg`);

--
-- Indices de la tabla `admisioncabecera`
--
ALTER TABLE `admisioncabecera`
  ADD PRIMARY KEY (`AdmCabId`),
  ADD KEY `AdmCabFKTipPro` (`AdmCabFKTipPro`),
  ADD KEY `AdmCabFKEstReg` (`AdmCabFKEstReg`),
  ADD KEY `AdmCabTur` (`AdmCabTur`);

--
-- Indices de la tabla `admisiondetalle`
--
ALTER TABLE `admisiondetalle`
  ADD PRIMARY KEY (`AdmDetId`),
  ADD KEY `AdmDetFKAdmCabId` (`AdmDetFKAdmCabId`),
  ADD KEY `AdmDetFKUsu` (`AdmDetFKUsu`),
  ADD KEY `AdmDetFKCar` (`AdmDetFKCar`),
  ADD KEY `AdmDetFKCar2` (`AdmDetFKCar2`),
  ADD KEY `AdmDetFKAul` (`AdmDetFKAul`),
  ADD KEY `AdmDetFKPab` (`AdmDetFKPab`),
  ADD KEY `AdmDetFKPab2` (`AdmDetFKPab2`),
  ADD KEY `AdmDetFKAre` (`AdmDetFKAre`),
  ADD KEY `AdmDetFKAre2` (`AdmDetFKAre2`),
  ADD KEY `AdmDetFKArePue` (`AdmDetFKArePue`),
  ADD KEY `AdmDetFKArePue2` (`AdmDetFKArePue2`),
  ADD KEY `AdmDetFKDepTec` (`AdmDetFKDepTec`),
  ADD KEY `AdmDetFKEstReg` (`AdmDetFKEstReg`);

--
-- Indices de la tabla `admisionpabellon`
--
ALTER TABLE `admisionpabellon`
  ADD PRIMARY KEY (`AdmPabId`),
  ADD KEY `AdmPabFKPabId` (`AdmPabFKPabId`),
  ADD KEY `AdmPabFKAdmAreId` (`AdmPabFKAdmAreId`),
  ADD KEY `AdmPabFKAreIdLog` (`AdmPabFKAreIdLog`),
  ADD KEY `AdmPabFKEstReg` (`AdmPabFKEstReg`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`AreId`),
  ADD KEY `AreFKEstReg` (`AreFKEstReg`);

--
-- Indices de la tabla `areapuerta`
--
ALTER TABLE `areapuerta`
  ADD PRIMARY KEY (`ArePueId`),
  ADD KEY `ArePueFKAre` (`ArePueFKAre`),
  ADD KEY `ArePueFKEstReg` (`ArePueFKEstReg`);

--
-- Indices de la tabla `aula`
--
ALTER TABLE `aula`
  ADD PRIMARY KEY (`AulId`),
  ADD KEY `AulFKPab` (`AulFKPab`),
  ADD KEY `AulFKEstReg` (`AulFKEstReg`);

--
-- Indices de la tabla `bitacoraadmisiondetalle`
--
ALTER TABLE `bitacoraadmisiondetalle`
  ADD PRIMARY KEY (`BitAdmDetId`);

--
-- Indices de la tabla `bitacorausuario`
--
ALTER TABLE `bitacorausuario`
  ADD PRIMARY KEY (`BitUsuId`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`CarId`),
  ADD KEY `CarFKTipUsu` (`CarFKTipUsu`),
  ADD KEY `CarFKEstReg` (`CarFKEstReg`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`DepId`),
  ADD KEY `DepFKFac` (`DepFKFac`),
  ADD KEY `DepFKEstReg` (`DepFKEstReg`);

--
-- Indices de la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`FacId`),
  ADD KEY `FacFKAre` (`FacFKAre`),
  ADD KEY `FacFKEstReg` (`FacFKEstReg`);

--
-- Indices de la tabla `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`GenId`);

--
-- Indices de la tabla `pabellon`
--
ALTER TABLE `pabellon`
  ADD PRIMARY KEY (`PabId`),
  ADD KEY `PabFKAre` (`PabFKAre`),
  ADD KEY `PabFKDep` (`PabFKDep`),
  ADD KEY `PabFKEstReg` (`PabFKEstReg`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`UsuId`),
  ADD KEY `UsuFKAre` (`UsuFKAre`),
  ADD KEY `UsuFKGen` (`UsuFKGen`),
  ADD KEY `UsuFKTip` (`UsuFKTip`),
  ADD KEY `UsuFKCat` (`UsuFKCat`),
  ADD KEY `UsuFKEstReg` (`UsuFKEstReg`),
  ADD KEY `UsuFKTip2` (`UsuFKTip2`);

--
-- Indices de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD PRIMARY KEY (`UsuRolId`),
  ADD KEY `UsuRolFKTipUsu` (`UsuRolFKTipUsu`),
  ADD KEY `UsuRolFKRol` (`UsuRolFKRol`),
  ADD KEY `UsuRolFKEstReg` (`UsuRolFKEstReg`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admisionarea`
--
ALTER TABLE `admisionarea`
  MODIFY `AdmAreId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `admisionaula`
--
ALTER TABLE `admisionaula`
  MODIFY `AdmAulId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT de la tabla `admisioncabecera`
--
ALTER TABLE `admisioncabecera`
  MODIFY `AdmCabId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `admisiondetalle`
--
ALTER TABLE `admisiondetalle`
  MODIFY `AdmDetId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `admisionpabellon`
--
ALTER TABLE `admisionpabellon`
  MODIFY `AdmPabId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `AreId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `areapuerta`
--
ALTER TABLE `areapuerta`
  MODIFY `ArePueId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `aula`
--
ALTER TABLE `aula`
  MODIFY `AulId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=279;
--
-- AUTO_INCREMENT de la tabla `bitacoraadmisiondetalle`
--
ALTER TABLE `bitacoraadmisiondetalle`
  MODIFY `BitAdmDetId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT de la tabla `bitacorausuario`
--
ALTER TABLE `bitacorausuario`
  MODIFY `BitUsuId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12884;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `CarId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `DepId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `facultad`
--
ALTER TABLE `facultad`
  MODIFY `FacId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `general`
--
ALTER TABLE `general`
  MODIFY `GenId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `pabellon`
--
ALTER TABLE `pabellon`
  MODIFY `PabId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `UsuId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria', AUTO_INCREMENT=6376;
--
-- AUTO_INCREMENT de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  MODIFY `UsuRolId` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Clave primaria';
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admisionarea`
--
ALTER TABLE `admisionarea`
  ADD CONSTRAINT `admisionarea_ibfk_1` FOREIGN KEY (`AdmAreFKAreId`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `admisionarea_ibfk_2` FOREIGN KEY (`AdmAreFKAdmCabId`) REFERENCES `admisioncabecera` (`AdmCabId`),
  ADD CONSTRAINT `admisionarea_ibfk_3` FOREIGN KEY (`AdmAreFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `admisionaula`
--
ALTER TABLE `admisionaula`
  ADD CONSTRAINT `admisionaula_ibfk_1` FOREIGN KEY (`AdmAulFKAulId`) REFERENCES `aula` (`AulId`),
  ADD CONSTRAINT `admisionaula_ibfk_2` FOREIGN KEY (`AdmAulFKAdmPabId`) REFERENCES `admisionpabellon` (`AdmPabId`),
  ADD CONSTRAINT `admisionaula_ibfk_3` FOREIGN KEY (`AdmAulFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `admisioncabecera`
--
ALTER TABLE `admisioncabecera`
  ADD CONSTRAINT `admisioncabecera_ibfk_1` FOREIGN KEY (`AdmCabFKTipPro`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `admisioncabecera_ibfk_2` FOREIGN KEY (`AdmCabFKEstReg`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `admisioncabecera_ibfk_3` FOREIGN KEY (`AdmCabTur`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `admisiondetalle`
--
ALTER TABLE `admisiondetalle`
  ADD CONSTRAINT `admisiondetalle_ibfk_1` FOREIGN KEY (`AdmDetFKCar`) REFERENCES `cargo` (`CarId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_10` FOREIGN KEY (`AdmDetFKPab2`) REFERENCES `admisionpabellon` (`AdmPabId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_11` FOREIGN KEY (`AdmDetFKAre2`) REFERENCES `admisionarea` (`AdmAreId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_12` FOREIGN KEY (`AdmDetFKEstReg`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_13` FOREIGN KEY (`AdmDetFKAdmCabId`) REFERENCES `admisioncabecera` (`AdmCabId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_2` FOREIGN KEY (`AdmDetFKUsu`) REFERENCES `usuario` (`UsuId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_3` FOREIGN KEY (`AdmDetFKCar2`) REFERENCES `cargo` (`CarId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_4` FOREIGN KEY (`AdmDetFKArePue`) REFERENCES `areapuerta` (`ArePueId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_5` FOREIGN KEY (`AdmDetFKArePue2`) REFERENCES `areapuerta` (`ArePueId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_6` FOREIGN KEY (`AdmDetFKDepTec`) REFERENCES `admisiondetalle` (`AdmDetId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_7` FOREIGN KEY (`AdmDetFKAre`) REFERENCES `admisionarea` (`AdmAreId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_8` FOREIGN KEY (`AdmDetFKAul`) REFERENCES `admisionaula` (`AdmAulId`),
  ADD CONSTRAINT `admisiondetalle_ibfk_9` FOREIGN KEY (`AdmDetFKPab`) REFERENCES `admisionpabellon` (`AdmPabId`);

--
-- Filtros para la tabla `admisionpabellon`
--
ALTER TABLE `admisionpabellon`
  ADD CONSTRAINT `admisionpabellon_ibfk_1` FOREIGN KEY (`AdmPabFKPabId`) REFERENCES `pabellon` (`PabId`),
  ADD CONSTRAINT `admisionpabellon_ibfk_2` FOREIGN KEY (`AdmPabFKAdmAreId`) REFERENCES `admisionarea` (`AdmAreId`),
  ADD CONSTRAINT `admisionpabellon_ibfk_3` FOREIGN KEY (`AdmPabFKAreIdLog`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `admisionpabellon_ibfk_4` FOREIGN KEY (`AdmPabFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `area_ibfk_1` FOREIGN KEY (`AreFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `areapuerta`
--
ALTER TABLE `areapuerta`
  ADD CONSTRAINT `areapuerta_ibfk_1` FOREIGN KEY (`ArePueFKAre`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `areapuerta_ibfk_2` FOREIGN KEY (`ArePueFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `aula`
--
ALTER TABLE `aula`
  ADD CONSTRAINT `aula_ibfk_1` FOREIGN KEY (`AulFKPab`) REFERENCES `pabellon` (`PabId`),
  ADD CONSTRAINT `aula_ibfk_2` FOREIGN KEY (`AulFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD CONSTRAINT `cargo_ibfk_1` FOREIGN KEY (`CarFKTipUsu`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `cargo_ibfk_2` FOREIGN KEY (`CarFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD CONSTRAINT `departamento_ibfk_1` FOREIGN KEY (`DepFKFac`) REFERENCES `facultad` (`FacId`),
  ADD CONSTRAINT `departamento_ibfk_2` FOREIGN KEY (`DepFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `facultad`
--
ALTER TABLE `facultad`
  ADD CONSTRAINT `facultad_ibfk_1` FOREIGN KEY (`FacFKAre`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `facultad_ibfk_2` FOREIGN KEY (`FacFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `pabellon`
--
ALTER TABLE `pabellon`
  ADD CONSTRAINT `pabellon_ibfk_1` FOREIGN KEY (`PabFKAre`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `pabellon_ibfk_2` FOREIGN KEY (`PabFKDep`) REFERENCES `departamento` (`DepId`),
  ADD CONSTRAINT `pabellon_ibfk_3` FOREIGN KEY (`PabFKEstReg`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`UsuFKAre`) REFERENCES `area` (`AreId`),
  ADD CONSTRAINT `usuario_ibfk_3` FOREIGN KEY (`UsuFKGen`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuario_ibfk_4` FOREIGN KEY (`UsuFKTip`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuario_ibfk_5` FOREIGN KEY (`UsuFKCat`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuario_ibfk_6` FOREIGN KEY (`UsuFKEstReg`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuario_ibfk_7` FOREIGN KEY (`UsuFKTip2`) REFERENCES `general` (`GenId`);

--
-- Filtros para la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD CONSTRAINT `usuariorol_ibfk_1` FOREIGN KEY (`UsuRolFKTipUsu`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuariorol_ibfk_2` FOREIGN KEY (`UsuRolFKRol`) REFERENCES `general` (`GenId`),
  ADD CONSTRAINT `usuariorol_ibfk_3` FOREIGN KEY (`UsuRolFKEstReg`) REFERENCES `general` (`GenId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
