<html>
<head>
	<title>Pruebas de Software</title>
	<!-- ESTILOS -->
	<link href="../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
</head>
<body>
	<?php
	include("../conexion/conexion.php");
		if(DatosBD::nombreBD!="DuaPrueba"){
			echo "<h1>PRIMERO CAMBIAR EL NOMBRE DE LA BD POR LA DE PRUEBA</h1>";
			die();
		}
	?>
	<div>
		<ul class="list-group">
			<li class="list-group-item active">
			<h4>BACKUP</h4>
			<a type="button" class="btn btn-default" href="Backup/copiarBD.php" target="_blank">Copiar BD a BD de PRUEBA</a>
			</li>

		  <li class="list-group-item">
		  	<h4>CP01: Verificar la correcta carga de docentes y administrativos</h4>
		  	<?php include ("CP01_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP02: Verificar la correcta carga de estudiantes</h4>
		  	<?php include ("CP02_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP03: Verificar el reemplazo exitoso de usuarios en cualquier etapa del proceso</h4>
		  	<?php include ("CP03_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP04: Verificar la correcta gestión de cargos</h4>
		  	<?php include ("CP04_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP05: Verificar el eliminado de participantes en el proceso de admisión	</h4>
		  	<?php include ("CP05_Ejecucion/index.php")?>
		  </li>
		  <!--li class="list-group-item">
		  	<h4>CP06</h4>
		  	<?php include ("CP06_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP07</h4>
		  	<?php include ("CP07_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP08</h4>
		  	<?php include ("CP08_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP09</h4>
		  	<?php include ("CP09_Ejecucion/index.php")?>
		  </li-->
		  <li class="list-group-item">
		  	<h4>CP10: Verificar el reporte válido de pabellones y áreas participantes. (aulas)</h4>
		  	<?php include ("CP10_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP11: Verificar el reporte válido de administrativos por estado de participación ( registrado, sorteado, participante, por cargo) con firma</h4>
		  	<?php include ("CP11_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP12: Verificar el reporte válido de docente por estado de participación ( registrado, sorteado, participante) con firma</h4>
		  	<?php include ("CP12_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP13: Verificar el reporte válido de estudiantes por estado de participación ( registrado, sorteado, participante) poner firma</h4>
		  	<?php include ("CP13_Ejecucion/index.php")?>
		  </li>
		  <!--li class="list-group-item">
		  	<h4>CP14: Verificar el reporte válido de asistencias por pabellón</h4>
		  	<?php include ("CP14_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP15: Verificar el reporte válido de recepción de refrigerios</h4>
		  	<?php include ("CP15_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP16: Verificar el reporte válido de cantidades de postulantes, controladores, técnicos, administrativos puertas, etc (mas info ver reportes)</h4>
		  	<?php include ("CP16_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP17: Verificar el reporte válido de capacidades de las áreas</h4>
		  	<?php include ("CP17_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP18: Verificar el reporte válido de distribución para informática</h4>
		  	<?php include ("CP18_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP19: Verificar el reporte válido de comisión de proceso de admisión de administrativos</h4>
		  	<?php include ("CP19_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP20: Verificar el reporte válido de comisión de proceso de admisión de docentes</h4>
		  	<?php include ("CP20_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP21: Verificar la correcta generación de credencial por búsqueda</h4>
		  	<?php include ("CP21_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP22: Verificar la correcta generación de credencial por cargos</h4>
		  	<?php include ("CP22_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP23: Verificar la correcta generación de credencial por ayudantes técnicos</h4>
		  	<?php include ("CP23_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP24: Verificar envío de emails</h4>
		  	<?php include ("CP24_Ejecucion/index.php")?>
		  </li>
		  <li class="list-group-item">
		  	<h4>CP25: Verificar la correcta asignación de docentes </h4>
		  	<?php include ("CP25_Ejecucion/index.php")?>
		  </li-->
		</ul>
	</div>
</body>

</html>