<!-- Navigation -->
<?php
  session_start();
  if($_SESSION["idUsuario"] == NULL && $_SESSION["tipo"] == NULL){
    echo "<script>location.href = '../../logout.php'</script>";
    die();
    return;
  }
  $estadoPro = NULL;
  if($_SESSION["idProceso"] != NULL){
    $estadoPro = $_SESSION["estadoProceso"];
  }

  require_once("modelo/usuario/util.php");

  $tipo =  $_SESSION["tipo"];

?>
<nav class="navbar navbar-default navbar-static-top" style="border-color: #5e151d; background-color:#141e42; color: #fff;" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header" >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>

        </button>
        <a  class="navbar-brand"  style=" background-color:#141e42; color: #fff; font-size: 23px;"><strong>OFICINA DE ADMISIÓN - UNIVERSIDAD NACIONAL DE SAN AGUSTÍN DE AREQUIPA </strong> </a>
    </div>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" style="background-color:#5e151d;" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php  
                if($_SESSION["idProceso"] != NULL) {?>
                <li>
                    <a style="color: #ffffff"  href="../inicio/inicio.php"><i class="fa" style="font-size:18px">&#xf004;</i>  Inicio</a>
                </li>
              <?php }else{?>
                <li>
                    <a style="color: #ffffff"  href="../inicio/inicio.php"><i class="fa" style="font-size:18px">&#xf004;</i>  Seleccionar Proceso</a>
                </li>
                <?php} 
                if((in_array(Codigos::codAdministrativo, $tipo) || in_array(Codigos::codDocente, $tipo)) && $estadoPro != NULL){?>
                <li>
                    <a style="color: #ffffff" href="../miPerfil/miPerfil.php"><i class="fa" style="font-size:18px">&#xf007;</i>  Mi Perfil</a>
                </li>
                <?php }?>
                <?php if((in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)) && (Codigos::codEstadoProcesoActual == $estadoPro || Codigos::codEstadoProcesoFuturo == $estadoPro)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i class="fa fa-cog fa-spin" style="font-size:22px"></i> Proceso<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <?php if(Codigos::codEstadoProcesoActual == $estadoPro){ ?>
                        <li><a style="color: #ffffff" href="../proceso/postulantes.php">Postulantes</a></li>
                        <li><a style="color: #ffffff" href="../proceso/distribucion.php">Distribucion</a></li>
                        <li><a style="color: #ffffff" href="../proceso/capacidades.php">Capacidades</a></li>
                        <?php if (in_array(Codigos::codAdministrador, $tipo)) {?>
                          <li><a style="color: #ffffff" href="../proceso/finalizarProceso.php">Finalizar Proceso</a></li>
                        <?php } ?>
                      <?php } else if(Codigos::codEstadoProcesoFuturo == $estadoPro && in_array(Codigos::codAdministrador, $tipo)){?>
                        <li><a style="color: #ffffff" href="../proceso/iniciarProceso.php">Iniciar Proceso</a></li>
                      <?php } ?>
                    </ul>
                </li>
                <?php }

                 if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:22px" class="fa">&#xf21d;</i> Usuario<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../usuario/docentesAdministrativos.php">Docentes / Administrativos</a></li>
                        <li><a style="color: #ffffff" href="../usuario/estudiantes.php">Estudiantes</a></li>
                        <?php if(in_array(Codigos::codAdministrador, $tipo)){ ?>
                        <li><a style="color: #ffffff" href="../usuario/operadores.php">Operadores</a></li>
                        <li><a style="color: #ffffff" href="../usuario/administradores.php">Administradores</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php }
                if ($_SESSION["idProceso"] != NULL && Codigos::codEstadoProcesoActual == $estadoPro) { 
                  if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i class="fa" style="font-size:20px">&#xf0c0;</i> Participantes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../participantes/docentes.php">Docentes</a></li>
                        <li><a style="color: #ffffff" href="../participantes/administrativos.php">Administrativos</a></li>
                        <li><a style="color: #ffffff" href="../participantes/ayudantes.php">Ayudantes</a></li>
                        <li><a style="color: #ffffff" href="../participantes/capacidades.php">Capacidades</a></li>
                        <li><a style="color: #ffffff" href="../participantes/reemplazo.php">Reemplazo</a></li>
                        <li><a style="color: #ffffff" href="../participantes/eliminar.php">Eliminar</a></li>
                    </ul>
                </li>
                <?php }?>
                <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0e8;</i> Sorteo<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../sorteo/sortAdmVar.php">Sorteo Administrativo Varones</a></li>
                        <li><a style="color: #ffffff" href="../sorteo/sortAdmMuj.php">Sorteo Administrativo Mujeres</a></li>
                        <li><a style="color: #ffffff" href="../sorteo/controlador.php">Sorteo Controladores</a></li>
                    </ul>
                </li>
                <?php }?>
                <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0e3;</i>       Asignacion<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../asignacion/docentes.php">Docentes</a></li>
                        <li><a style="color: #ffffff" href="../asignacion/administrativos.php">Administrativos</a></li>
                        <li><a style="color: #ffffff" href="../asignacion/com.php">Comision</a></li>
                    </ul>
                </li>
                <?php }?>
                <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:22px" class="fa">&#xf1ea;</i> Credenciales<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                       <li><a style="color: #ffffff" href="../credenciales/busqueda.php">Busqueda</a></li>
                       <li><a style="color: #ffffff" href="../credenciales/tipo.php">Por Cargos</a></li>
                       <li><a style="color: #ffffff" href="../credenciales/ayudantes.php">Ayudantes Tecnicos</a></li>
                       <li><a style="color: #ffffff" href="../credenciales/envEmails.php">Envio De Emails</a></li>
                   </ul>
               </li>
                <?php }?>
                <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
               <li>
                  <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0b1;</i>  Abastecimiento<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                      <li><a style="color: #ffffff" href="../abastecimiento/material.php">Material</a></li>
                      <li><a style="color: #ffffff" href="../abastecimiento/examenes.php">Examenes</a></li>
                  </ul>
              </li>
                <?php } }?>
              <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo) ){?>
             <li>
              <a style="color: #ffffff" href="#"><i class="fa fa-refresh fa-spin" style="font-size:20px"></i>  Mantenimiento<span class="fa arrow"></span></a>
              <ul style="color: #ffffff" class="nav nav-second-level">
                <?php if (in_array(Codigos::codAdministrador, $tipo)) {?>
                  <li><a style="color: #ffffff" href="../mantenimiento/procesos.php">Procesos de Admision</a></li>
                <?php  } ?>
                  <li><a style="color: #ffffff" href="../mantenimiento/areas.php">Areas</a></li>
                  <li><a style="color: #ffffff" href="../mantenimiento/facultades.php">Pabellones</a></li>
                  <!--<li><a style="color: #ffffff" href="../mantenimiento/departamentos.php">Departamentos</a></li>-->
                  <!-- <li><a style="color: #ffffff" href="../mantenimiento/pabellones.php">Pabellones</a></li>-->
                  <li><a style="color: #ffffff" href="../mantenimiento/aulas.php">Aulas</a></li>
                  <li><a style="color: #ffffff" href="../mantenimiento/cargos.php">Cargos</a></li>
              </ul>
          </li>
          <?php} if(in_array(Codigos::codAdministrador, $tipo)){?>
             <li>
              <a style="color: #ffffff" href="#"><i class="fa" style="font-size:20px">&#xf15c;</i>  Bitácora<span class="fa arrow"></span></a>
              <ul style="color: #ffffff" class="nav nav-second-level">
                  <li><a style="color: #ffffff" href="../bitacora/usuario.php">Usuario</a></li>
                  <li><a style="color: #ffffff" href="../bitacora/procesoAdmision.php">Procesos de Admisión</a></li>
              </ul>
          </li>
          <?php }
          if ($_SESSION["idProceso"] != NULL) {
          ?>
          <?php if((in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)) && Codigos::codEstadoProcesoActual == $estadoPro){?>
          <li>
              <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf15c;</i> Anexos<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a style="color: #ffffff" href="../anexos/administrativos.php">Administrativos</a></li>
                  <li><a style="color: #ffffff" href="../anexos/tecnicos.php">Tecnicos</a></li>
                  <li><a style="color: #ffffff" href="../anexos/controladores.php">Controladores</a></li>
              </ul>
          </li>
          <?php }?>
           <?php if((in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)) && (Codigos::codEstadoProcesoActual == $estadoPro || Codigos::codEstadoProcesoPasado == $estadoPro)){?>
          <li>
              <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf02d;</i> Reportes<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a style="color: #ffffff" href="../reportes/general.php">General</a></li>
                  <li><a style="color: #ffffff" href="../reportes/pabArePart.php">Pabellones y Areas Participantes</a></li>
                  <li><a style="color: #ffffff" href="../reportes/admEstPart.php">Administrativos por Estado de Participación</a></li>
                  <li><a style="color: #ffffff" href="../reportes/docEstPart.php">Docente Por Estado de Participación</a></li>
                  <li><a style="color: #ffffff" href="../reportes/estEstPart.php">Estudiantes Por Estado de Participación </a></li>
                  <li><a style="color: #ffffff" href="../reportes/asiPab.php">Asistencias por Pabellon</a></li>
                  <li><a style="color: #ffffff" href="../reportes/recRef.php">Recepción de Refrigerios</a></li>
                  <li><a style="color: #ffffff" href="../reportes/capAre.php">Capacidades de las Areas</a></li>
                  <li><a style="color: #ffffff" href="../reportes/distInf.php">Distribución para Informática</a></li>
                  <li><a style="color: #ffffff" href="../reportes/comAdmAdm.php">Comisión de Admisión Administrativos</a></li>
                  <li><a style="color: #ffffff" href="../reportes/comAdmDoc.php">Comisión de Admisión Docentes</a></li>
                  <li><a style="color: #ffffff" href="../reportes/remplazo.php">Reemplazo</a></li>
              </ul>
          </li>
          <?php }}?>
          <?php if(in_array(Codigos::codAdministrativo, $tipo) || in_array(Codigos::codDocente, $tipo)){?>
          <li>
              <a style="color: #ffffff" href="../incripcion/incripcion.php"><i style="font-size:24px" class="fa">&#xf1fc;</i>Incripcion</a>
          </li>
          
          <li>
              <a style="color: #ffffff" href="../archivos/archivos.php"><i style="font-size:24px" class="fa">&#xf07c;</i>Archivos</a>
          </li>
          <?php }
          if ($_SESSION["idProceso"] != NULL && Codigos::codEstadoProcesoActual == $estadoPro) {
          ?>
          <?php if(in_array(Codigos::codAdministrador, $tipo) || in_array(Codigos::codOperador, $tipo)){?>
          <li>
              <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf044;</i>Editar<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a style="color: #ffffff" href="../editar/tipPar.php">Tipos de Participantes</a></li>
              </ul>
          </li>
          <?php }
          if(in_array(Codigos::codAdministrador, $tipo)){ ?>
          <li>
              <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf044;</i>Carga de Usuarios<span class="fa arrow"></span></a>
              <ul class="nav nav-second-level">
                  <li><a style="color: #ffffff" href="../carga/cgDoc-Adm.php">Carga de Docentes y Administrativos</a></li>
                  <li><a style="color: #ffffff" href="../carga/cgEst.php">Carga de Estudiantes</a></li>
              </ul>
          </li>
         <?php }} ?>
         <?php if ($_SESSION["idProceso"] != NULL){ ?>
          <li>
              <a style="color: #ffffff" href="../../salirProceso.php"><i style="font-size:20px" class="fa">&#xf122;</i> SALIR PROCESO</a>
          </li>
          <?php } ?>
          <li>
              <a style="color: #ffffff" href="../../logout.php"><i style="font-size:20px" class="fa">&#xf122;</i> SALIR</a>
          </li>

    </ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
