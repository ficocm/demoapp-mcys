/** Codigos */
var codAdministrativo = 4;
var codDocente = 3;
var codEstudiante = 20;

var codInscrito = 7;
var codInscritoCancelado = 6;
var codSorteado = 5;
var codSorteadoCancelado = 28;
var codAsignado = 17;

/** Cargos **/
var codControladorDocente = 3;
var codSeguridad = 67;

//Expresiones regulares
expresionCorreo = /^[_A-Za-z0-9-]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(.[A-Za-z0-9-]+)*(.[A-Za-z]{2,4})$/;

function ponerMayusculasYMinusculas(){
	$('input[type=text]').val (function () {
		return this.value.toUpperCase();
	});
	$('input[type=email]').val (function () {
		return this.value.toUpperCase();
	});
}

var timeoutError;
function mostrarError(mensaje){
	clearTimeout(timeoutError);
	$('.error').text(mensaje.toUpperCase());
	$('.error').slideDown('slow');
	timeoutError = setTimeout(function(){
        $('.error').slideUp('slow');
	}, 5000);
}

var timeoutCorrecto;
function mostrarCorrecto(mensaje){
	clearTimeout(timeoutCorrecto);
	$('.correcto').text(mensaje.toUpperCase());
	$('.correcto').slideDown('slow');
	timeoutCorrecto = setTimeout(function(){
        $('.correcto').slideUp('slow');
	}, 3000);
}

function formularioValido(){
	var nombre = $("#nombres").val();
	var apellidos = $("#apellidos").val();

	if(nombre == null || nombre.length==0){
		mostrarError("Ingrese un nombre valido");
		$("#nombres").focus();
		return false;
	}
	if(apellidos == null || apellidos.length==0){
		mostrarError("Ingrese apellidos validos");
		$("#apellidos").focus();
		return false;
	}
	return true;
}

function formularioValidoDocente(){
	var res = formularioValido();
	if(res == false) return false;
	var dni = $("#dni").val();
	var correo = $("#correo").val();

	if(dni == null || dni.length == '0'){
		mostrarError("Ingrese DNI valido");
		$("#dni").focus();
		return false;
	}

	if(correo == null || correo.length == '0'){
		mostrarError("Ingrese CORREO valido");
		$("#correo").focus();
		return false;
	}
	return true;
}

function formularioValidoAdministrativo(){
	var res = formularioValido();
	if(res == false) return false;
	var dni = $("#dni").val();
	var genero = $("#genero").val();
	var tipo1 = $('#tipo1').is(':checked');
	var tipo2 = $('#tipo2').is(':checked');
	var correo = $("#correo").val();

	if(dni == null || dni.length == '0'){
		mostrarError("Ingrese DNI valido");
		$("#dni").focus();
		return false;
	}

	if(correo == null || correo.length < 3 || !expresionCorreo.test(correo)) {
		mostrarError("Ingrese CORREO valido");
		$("#correo").focus();
		return false;
	}

	if(tipo1 == false && tipo2 == false){
		mostrarError("Seleccione un TIPO (DOCENTE o ADMINISTRATIVO)");
		$("#tipo1").focus();
		return false;
	}

	if(genero == null || genero.length == '0' || genero == -1){
		mostrarError("Seleccione un GÉNERO");
		$("#genero").focus();
		return false;
	}
	return true;
}

function formularioValidoEstudiante(){
	var res = formularioValido();
	if(res == false) return false;

	var cui = $("#cui").val();
	if(cui == null || cui.length == '0'){
		mostrarError("Ingrese CUI valido");
		$("#cui").focus();
		return false;
	}

	var correo = $("#correo").val();
	if(correo != null && correo.length > 0 && !expresionCorreo.test(correo)){
		mostrarError("Ingrese CORREO valido");
		$("#correo").focus();
		return false;
	}
	return true;
}

function precesarDniCorreo(idUsuario,callback, objectClick, tipo){
	verificarDniRepetido(idUsuario,callback, objectClick, tipo);
}

function precesarCuiCorreo(idUsuario,callback, objectClick, tipo){
	verificarCuiRepetido(idUsuario,callback, objectClick, tipo);
}

function verificarDniRepetido(idUsuario,callback, objectClick, tipo){
	var dni = $("#dni").val();
	var parametros = {"tipo":tipo,"dni":dni,"id":idUsuario};
	var url = "../../controlador/usuario/usuarioControlador.php?accion=verificarDniRepetido";
  $.ajax({
	  type: "POST",
	  url: url,
	  data: parametros, // serializes the form's elements.
	  beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Verificando DNI...");
		},
	  success: function(data){
  		$("#loader").hide();
		 	try{
		 		var res = JSON.parse(data);
				if(res.error == false){
					verificarCorreoRepetido(idUsuario,callback, objectClick,tipo);
				}else{
					mostrarError(res.mensaje);
					$("#dni").focus();
				}
		 	}catch(err){
		 		mostrarError("Error Casteo "+err+" data:"+data);
		 	}
		},
  	error: function(result) {
  		$("#loader").hide();
      mostrarError("Error Server result data:"+result);
    } 
	});
}

function verificarCuiRepetido(idUsuario,callback, objectClick, tipo){
	var cui = $("#cui").val();
	var parametros = {"tipo":tipo,"cui":cui,"id":idUsuario};
	var url = "../../controlador/usuario/usuarioControlador.php?accion=verificarCuiRepetido";
  $.ajax({
	  type: "POST",
	  url: url,
	  data: parametros, // serializes the form's elements.
	  beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Verificando CUI...");
		},
	  success: function(data){
  		$("#loader").hide();
		 	try{
		 		var res = JSON.parse(data);
				if(res.error == false){
					verificarCorreoRepetido(idUsuario,callback, objectClick,tipo);
				}else{
					mostrarError(res.mensaje);
					$("#CUI").focus();
				}
		 	}catch(err){
		 		mostrarError("Error Casteo "+err+" data:"+data);
		 	}
		},
  	error: function(result) {
  		$("#loader").hide();
      mostrarError("Error Server result data:"+result);
    } 
	});
}

function verificarCorreoRepetido(idUsuario,callback, objectClick,tipo){
	var correo = $("#correo").val();
	var parametros = {"tipo":tipo,"correo":correo,"id":idUsuario};

	var url = "../../controlador/usuario/usuarioControlador.php?accion=verificarCorreoRepetido";
  $.ajax({
	  type: "POST",
	  url: url,
	  data: parametros, // serializes the form's elements.
	  beforeSend: function(objeto){
	  	$("#loader").html("Verificando CORREO...");
			$("#loader").show();
		},
	  success: function(data){
  		$("#loader").hide();
		 	try{
		 		var res = JSON.parse(data);
				if(res.error == false){
					callback();
					objectClick.prev().click();
				}else{
					mostrarError(res.mensaje);
					$("#correo").focus();
				}
		 	}catch(err){
		 		mostrarError("Error Casteo "+err+" data:"+data);
		 	}
		},
  	error: function(result) {
  		$("#loader").hide();
      mostrarError("Error Server result data:"+result);
    } 
	});
}

$( document ).ready(function() {
  	$('#cui').on('input', function () { 
    	this.value = this.value.replace(/[^0-9]/g,'');
	});
	$('#dni').on('input', function () { 
    	this.value = this.value.replace(/[^0-9]/g,'');
	});
});