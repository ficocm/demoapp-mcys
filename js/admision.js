var admi;
function cargarAdmi(){	

	var parametros = {"action":"ajax"};
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'../../controlador/usuario/usuarioVisualizar.php?accion=mostrarAdmi',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").html("Cargando...");
		},
		success:function(result){
			var resultado = JSON.parse(result);
			if(resultado.error == false){
				admi = resultado.mensaje;
				$("#usuarios").find("tr:gt(0)").remove();
				ponerAdmi(0);	
			}else{
				ponerMensaje();
				alert(result);
			}
		}
	})
}


function ponerAdmi($id){
	var lon=admi.length;
	
	if(lon==0){
		ponerMensaje();
	}

	else if($id<lon){

	  	usuario = admi[$id];
	    
	    var row = document.getElementById("usuarios").insertRow(-1);

	    var cell1 = row.insertCell(0);//Fecha
	    var cell2 = row.insertCell(1);//Tipo
	    var cell3 = row.insertCell(2);//Fase
	    var cell4 = row.insertCell(3);//Estado
	    var cell5 = row.insertCell(4);//Comentario


	   	$idAdm= usuario.AdmCabId;
	    //console.log($idAdm);
	    cell1.innerHTML = usuario.AdmCabAno;
	    cell2.innerHTML = usuario.GenSubCat;
	    cell3.innerHTML = usuario.AdmCabNumFas;
	    
	    $("#idAdm").val($idAdm);
	    
	    
	    $busqueda=document.getElementById("id").value;

		if(typeof busqueda !== 'undefined' && busqueda !== null) {
	    	$busqueda=document.getElementById("id").value;
	  	}
	  	
	   
	  	//ponerCargo($busqueda,$idAdm,cell4);


		var parametros = {"action":"ajax",'busqueda':$busqueda,'id':$idAdm};

		
		$.ajax({
			url:'../../controlador/usuario/usuarioVisualizar.php?accion=verifUsuAdmi',
			data: parametros,
			type: "POST",
			beforeSend: function(objeto){
				$("#loader").html("Cargando...");
			},
			success:function(result){

				var resultado = JSON.parse(result);
				//alert(resultado.mensaje);
				
				if(resultado.error == false){
					cell4.innerHTML= resultado.mensaje;
					cell5.innerHTML= "<p>Usted está registrado. Puede <a href='#' onclick='borrar("+$idAdm+"); return false'>Borrar</a> su inscripción</p>";
					//$("#idAdm1").val(resultado.datos);
				}else if(resultado.error == true){
					cell4.innerHTML = resultado.mensaje;
					cell5.innerHTML = "<p>Usted puede <a href='#' onclick='putValueId("+$idAdm+")' id='submit' value='AGREGAR DOCENTE' data-toggle='modal' data-target='#myModal'>Registrarse</a> en este proceso.</p>";
					//$("#idAdm1").val(resultado.datos);
				}
				ponerAdmi($id+1);
			}
		})
	  }  
	}

	function putValueId(idAdm) {
		$idAdm = idAdm
		$("#idAdm").val($idAdm);
}


function ponerCargo(busqueda,idAdm,cell4){

	$busqueda=busqueda;
	$idAdm=idAdm;
	var cell4=cell4;

	var parametros1 = {"action":"ajax",'busqueda':$busqueda,'idAdm':idAdm};

  	$("#loader").fadeIn('slow');
	$.ajax({
		url:'../../controlador/usuario/usuarioVisualizar.php?accion=getCargo',
		data: parametros1,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").html("Cargando...");
		},
		success:function(result){
			//alert(result);
			var resultado = JSON.parse(result);
			
			if(resultado.error == false){
				var cargo=resultado.mensaje;
				var ncargo=cargo[0].AdmDetFKCar;
				//alert(cargo);


				if(ncargo==4){
					cell4.innerHTML = "ADMINISTRATIVO";
				}else{
					cell4.innerHTML = cargo[0].CarDes;	
				}
				
			}else{
				cell4.innerHTML = "Ninguno";
			}
		}
	})
}

function ponerMensaje(){
	$("#paginacion").html("No hay procesos de adminisión disponibles");
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

cargarAdmi();