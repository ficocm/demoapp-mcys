
var administrativosInscritosSorteados;
var generos;
var categorias;
var iconOK = "<span id='ok' class='glyphicon glyphicon-ok'></span>";
var iconUnCheck = "<span id='un' class='glyphicon glyphicon-unchecked'></span>";
var iconNo = "<span class='glyphicon glyphicon-ban-circle'></span>";
var cargoCategorias = false;
var cargoGeneros = false;

	$( document ).ready(function() {
        console.log( "document loaded" );
        postulantes();
        //capacidadAsignada();
    });

    function capacidadAsignada(){

    	$.ajax({
			url:'../../controlador/proceso/actualizarCantidad.php?accion=capacidadAsignada',
			type: "POST",
			beforeSend: function(objeto){
				$("#loader").show();
				$("#loader").html("Cargando...");
			},
			success: function(result){
				$("#loader").hide();
				try{
					var res = JSON.parse(result);	
					var cantidades=res.mensaje[0];

					var row = document.getElementById("postulantes").insertRow(-1);
				    var cell1 = row.insertCell(0);
				    var cell2 = row.insertCell(1);//Ingenierias
				    var cell3 = row.insertCell(2);//Sociales
				    var cell4 = row.insertCell(3);//Biomedicas
				    var cell5 = row.insertCell(4);//Total

				    var cantidadIngenierias = cambiarNumero(cantidades.cantidadIngenierias);
				    var cantidadSociales = cambiarNumero(cantidades.cantidadSociales);
				    var cantidadBiomedicas = cambiarNumero(cantidades.cantidadBiomedicas);

					cell1.innerHTML = "CAPACIDAD ASIGNADA";
				    cell2.innerHTML = cantidadIngenierias;
				    cell3.innerHTML = cantidadSociales;
				    cell4.innerHTML = cantidadBiomedicas;
				    cell5.innerHTML = cantidadIngenierias+cantidadSociales+cantidadBiomedicas;

				    cell2.setAttribute("style","text-align: center;");
				    cell3.setAttribute("style","text-align: center;");
				    cell4.setAttribute("style","text-align: center;");
				    cell5.setAttribute("style","text-align: center;");

				}catch(err){
						mostrarError("Error Casteo data:"+result);
				}

			}
		})
    }

    function cambiarNumero(number){
    	if (number == null){
			return 0;
		}
		else{
			return parseInt(number);
		}
    }

    function postulantes(){

    	$.ajax({
			url:'../../controlador/proceso/actualizarCantidad.php?accion=cantidad',
			type: "POST",
			beforeSend: function(objeto){
				$("#loader").show();
				$("#loader").html("Cargando...");
			},
			success: function(result){
				$("#loader").hide();
				try{
					var res = JSON.parse(result);	
					var cantidades=res.mensaje[0];

					$("#postulantes").find("tr:gt(0)").remove();

					var row = document.getElementById("postulantes").insertRow(-1);
				    var cell1 = row.insertCell(0);
				    var cell2 = row.insertCell(1);//Ingenierias
				    var cell3 = row.insertCell(2);//Sociales
				    var cell4 = row.insertCell(3);//Biomedicas
				    var cell5 = row.insertCell(4);//Total

				    var cantidadIngenierias = cambiarNumero(cantidades.AdmCabPosIng);
				    var cantidadSociales = cambiarNumero(cantidades.AdmCabPosSoc);
				    var cantidadBiomedicas = cambiarNumero(cantidades.AdmCabPosBio);

					cell1.innerHTML = "CANTIDAD DE POSTULANTES";
				    cell2.innerHTML = cantidadIngenierias;
				    cell3.innerHTML = cantidadSociales;
				    cell4.innerHTML = cantidadBiomedicas;
				    cell5.innerHTML = cantidadIngenierias+cantidadSociales+cantidadBiomedicas;

				    cell2.setAttribute("style","text-align: center;");
				    cell3.setAttribute("style","text-align: center;");
				    cell4.setAttribute("style","text-align: center;");
				    cell5.setAttribute("style","text-align: center;");

				    capacidadAsignada();

				}catch(err){
						mostrarError("Error Casteo data:"+result);
				}

			}
		})
    }

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 			  categorias = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
	  		cargoCategorias = true;
			}
	  }})
	  ).done(function(ajaxAreas){
	  	afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarGeneros", success: function(result){
			try {
 			  generos = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
	  		cargoGeneros = true;
			}
	  }})
	  ).done(function(ajaxAreas){
	  	afterLoad();
	});

var ultimaPaginaCargada;
function cargar(pagina){
	ultimaPaginaCargada = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=10;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};

	$.ajax({
		url:'../../controlador/proceso/admisionPabellonControlador.php?accion=mostrarPabellones',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					administrativosInscritosSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerAdministrativosInscritosSorteados();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function ponerAdministrativosInscritosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

  for (i in administrativosInscritosSorteados){
  	usuario = administrativosInscritosSorteados[i];
    
    var row = document.getElementById("usuarios").insertRow(-1);
    var cell1 = row.insertCell(0);//DNI
    var cell2 = row.insertCell(1);//Apellidos
    var cell3 = row.insertCell(2);//Nombres
    var cell4 = row.insertCell(3);//Genero
    var cell5 = row.insertCell(4);//Categoria
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = usuario.area;
    cell2.innerHTML = usuario.nombre;
    cell3.innerHTML = usuario.capacidadTotal;
    cell4.innerHTML = usuario.capacidadAsignada;


	if(usuario.areaLogica == 2){//INGENIERIAS
		cell5.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+0+")'>"+iconOK+"</button>";
		cell6.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+3+")'>"+iconUnCheck+"</button>";
		cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+1+")'>"+iconUnCheck+"</button>";
	}else if(usuario.areaLogica==3){//SOCIALES
		cell5.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+2+")'>"+iconUnCheck+"</button>";
		cell6.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+0+")'>"+iconOK+"</button>";
		cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+1+")'>"+iconUnCheck+"</button>";
	}else if(usuario.areaLogica	==1){//BIOMEDICAS
		cell5.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+2+")'>"+iconUnCheck+"</button>";
		cell6.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+3+")'>"+iconUnCheck+"</button>";
		cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+0+")'>"+iconOK+"</button>";
	}
	else{
		cell5.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+2+")'>"+iconUnCheck+"</button>";
		cell6.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+3+")'>"+iconUnCheck+"</button>";
		cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarAreaLogica("+usuario.AdmPabId+","+1+")'>"+iconUnCheck+"</button>";
	}
    
    cell3.setAttribute("style","text-align: center;");
    cell4.setAttribute("style","text-align: center;");
    cell5.setAttribute("style","text-align: center;");
    cell6.setAttribute("style","text-align: center;");
    cell7.setAttribute("style","text-align: center;");

  }
  
}

function crearParticipacion(id, participacion){
	var url = "../../controlador/participantes/admisionDetalleControlador.php?accion=crearParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");

	$.ajax({
	  type: "POST",
	  url: url,
	  data: $form.serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Guardando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

function modificarParticipacionDocente(id, participacion){

	var url= "../../controlador/participantes/admisionDetalleControlador.php?accion=modificarParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");
		
	$.ajax({
	  type: "POST",
	  url: url,
	  data: $form.serialize(), // serializes the form's elements.
	  beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Modificando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

function modificarAreaLogica(id, areaLogica){

	var url= "../../controlador/proceso/admisionPabellonControlador.php?accion=modificarAreaLogica";

	//if(areaLogica ===0 )areaLogica="NULL";
	$form = $("<form></form>");
	$form.append("<input type='text' name='pabellonId' value='"+id+"'>");
	$form.append("<input type='text' name='areaLogica' value='"+areaLogica+"'>");
		
	$.ajax({
	  type: "POST",
	  url: url,
	  data: $form.serialize(), // serializes the form's elements.
	  beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Modificando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					postulantes();
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}


function afterLoad(){
	if(cargoCategorias == true && cargoGeneros == true){
		cargar(1);	
	}
}
