	    $(function(){
	    $("#formulario").submit(function(event){
	    var url = $(this).attr("action");
	    //var url = "progressControlador.php"; // El script a dónde se realizará la petición.
	    //alert($("#formulario").serialize());
	    if(validateForm()){
	        $.ajax({
	            type: "POST",
	            url: url,
	            data: $("#formulario").serialize(), // Adjuntar los campos del formulario eniado.
	            
	            beforeSend: function(objeto){
	                $("#send").attr('disabled',true);
	                $("#loader").show();
					$("#loader").html("Cargando...");
	                
	            },
	            success: function(result)
	            {
	            	$("#loader").hide();
	                $('#send').removeAttr('disabled');

	                try{
						var res = JSON.parse(result);
						if(res.error == false){
							mostrarCorrecto(res.mensaje);
							//cargar();
								
						}else{
							mostrarError(res.mensaje);
						}
					}catch(err){
						mostrarError("Error Casteo data:"+result);
					}
				}
	        });
	    }

	    return false; // Evitar ejecutar el submit del formulario.
	    });
	});

	$( document ).ready(function() {
        console.log( "document loaded" );
        cargar();
    });


    function validateForm() {
        var cantidadIngenierias = document.forms["formulario"]["cantidadIngenierias"].value;
        var cantidadSociales = document.forms["formulario"]["cantidadSociales"].value;
        var cantidadBiomedicas = document.forms["formulario"]["cantidadBiomedicas"].value;
        if (cantidadIngenierias == "" || cantidadSociales=="" || cantidadBiomedicas=="") {
            alert("Completar todos los campos");
            return false;
        }
        return true;
    }

    function cargar(){

    	$.ajax({
			url:'../../controlador/proceso/actualizarCantidad.php?accion=cantidad',
			type: "POST",
			beforeSend: function(objeto){
				$("#loader").show();
				$("#loader").html("Cargando...");
			},
			success: function(result){
				$("#loader").hide();
				try{
					var res = JSON.parse(result);	
					var cantidades=res.mensaje[0];
					$("#cantidadIngenierias").val(cantidades.AdmCabPosIng);
					$("#cantidadSociales").val(cantidades.AdmCabPosSoc);
					$("#cantidadBiomedicas").val(cantidades.AdmCabPosBio);
				}catch(err){
						mostrarError("Error Casteo data:"+result);
				}

			}
		})
    }
