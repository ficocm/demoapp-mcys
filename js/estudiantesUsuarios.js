var estudiantes;
var categorias;

var cargoCategorias = false;
var seEdita = false;
var seAgrega = false;

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 				categorias = JSON.parse(result);
			}catch(err) {
				mostrarError("No se pudo cargar las categorias");
			}finally {
				cargoCategorias = true;
			}
		}})
	).done(function(ajaxAreas){
		afterLoad();
	});

var ultimaPagina = 1;
function cargar(pagina){
	ultimaPagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=8;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};

	$.ajax({
		url:'../../controlador/usuario/usuarioControlador.php?accion=mostrarEstudiantes',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					estudiantes = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerEstudiantes();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function borrarOpciones(opcion){
	$(opcion).find('option').remove().end()
		.append('<option value="-1">Seleccione</option>');
}

function cargarSelects(){
	borrarOpciones("#categoria");

	for(i in categorias){
		$('#categoria').append($('<option>', {
			value: categorias[i].GenId,
			text: categorias[i].GenSubCat
		}));
	}
}

function ponerEstudiantes(){
	$("#usuarios").find("tr:gt(0)").remove();

	for (i in estudiantes){
		usuario = estudiantes[i];
		var categoria = "";

		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
				break;
			}
		}
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//EDITAR
		var cell2 = row.insertCell(1);//DNI
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Apellidos
		var cell5 = row.insertCell(4);//Correo
		var cell6 = row.insertCell(5);//Telefono
		var cell7 = row.insertCell(6);//Direccion
		var cell8 = row.insertCell(7);//Categoria

		cell1.innerHTML = "<input type='image' onclick='editable("+usuario.UsuId+");' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuCui;
		cell3.innerHTML = usuario.UsuApe;
		cell4.innerHTML = usuario.UsuNom;
		cell5.innerHTML = usuario.UsuCorEle;
		cell6.innerHTML = usuario.UsuTel;
		cell7.innerHTML = usuario.UsuDir;
		cell8.innerHTML = categoria;
		cell1.setAttribute("style","text-align: center;");
	}
	
}

function agregable(){
	$("#save").text("AGREGAR");
	$(".modal-title").text("NUEVO ESTUDIANTE");
	seAgrega = true;
	seEdita = false;
	idUsuario = null;
	resetForm();
}

var idUsuario;
function editable(id){
	$("#save").text("GUARDAR");
	$(".modal-title").text("EDITAR ESTUDIANTE");
	seEdita = true;
	seAgrega = false;
	idUsuario = id;

	resetForm();
	for (i in estudiantes){
		usuario = estudiantes[i];
		if(usuario.UsuId == id){
			$("#nombres").val(usuario.UsuNom);
			$("#apellidos").val(usuario.UsuApe);
			$("#cui").val(usuario.UsuCui);
			$("#telefono").val(usuario.UsuTel);
			$("#correo").val(usuario.UsuCorEle);
			$("#direccion").val(usuario.UsuDir);
			$('#categoria :nth-child(1)').prop('selected', true);
		}
	}
}

function guardar(){
	ponerMayusculasYMinusculas();

	var url = "../../controlador/usuario/usuarioControlador.php?accion=modificarEstudiante";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize()+"&id="+idUsuario, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Guardando...");
		},
		success: function(data){
			$("#loader").hide();
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
		 	}catch(err){
		 		mostrarError("Error Casteo data:"+data);
		 	}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
	
}

function agregar(){
	ponerMayusculasYMinusculas();

	var url = "../../controlador/usuario/usuarioControlador.php?accion=crearEstudiante";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Agregando...");
		},
		success: function(data){
			$("#loader").hide();
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
		 	}catch(err){
		 		mostrarError("Error Casteo data:"+data);
		 	}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
	
}

function resetForm(){
	$("#nombres").val('');
	$("#apellidos").val('');
	$("#cui").val('');
	$("#telefono").val('');
	$("#correo").val('');
	$("#direccion").val('');
	$('#categoria :nth-child(1)').prop('selected', true);
}

$( document ).ready(function() {
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(formularioValidoEstudiante()==true){
			if(seEdita==true){
				precesarCuiCorreo(idUsuario,guardar,$(this),codEstudiante);//Tambien verifica correo electronico
				//guardar();
			}else if(seAgrega==true){
				precesarCuiCorreo('NULL',agregar,$(this),codEstudiante);
			}
			//$(this).prev().click();
		}
	});
});

var cargo = false;
function afterLoad(){
	if(!cargo && cargoCategorias){
		cargo = true;
		cargarSelects();
		cargar(1);
	}
}
