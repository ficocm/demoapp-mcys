var areas, pabellones, dpto,areas2, areIdLogic,checkArea,reemplazos,cargos,usuarios, aulas,guarderos, comAdm;
var dptoN = "Sin pabellón";

var nombreCargo,nombrePabAula;
function cargarAreas(){
  $.when(
    $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leer", success: function(result){

      areas = JSON.parse(result);
    }})
    ).done(function(ajaxAreas){
      ponerListaAreas();
      llenarAreasCheck();
    });
  }

  function cargarUsuarios(){
    $.when(
      $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=listarUsuarios", success: function(result){


        usuarios = JSON.parse(result);

      }})
      ).done(function(ajaxAreas){
      });
    }

    function cargaCargos(){
      $.when(
        $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=listarCargos", success: function(result){
          cargos = JSON.parse(result);
        }})
        ).done(function(ajaxAreas){
          llenarCargosCheckEst();
        });
      }
      function cargarReemplazos(){
        $.when(
          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=listarReemplazos", success: function(result){
            
            reemplazos = JSON.parse(result);
          }})
          ).done(function(ajaxAreas){
            llenarReemplazos();
          });
        }
        function cargarAreas2(){
          $.when(
            $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leer", success: function(result){
              areas2 = JSON.parse(result);
            }})
            ).done(function(ajaxAreas){

            });
          }

          function cargarPabellones(){
            $.when(
              $.ajax({url: "../../controlador/reportes/pabellonControlador.php?accion=leer", success: function(result){

                pabellones = JSON.parse(result);
              }})
              ).done(function(ajaxAreas){
              });
            }

            function borrarOpciones(opcion){
              $(opcion).find('option').remove().end()
              .append('<option disabled selected value="-1">Eliga una opción</option>');
            }

            function ponerListaAreas(){
              var idProceso=(document.getElementById("idProceso").innerHTML ).trim();
              borrarOpciones("#area");
              borrarOpciones("#pabellon");

              for(i in areas){
                if (areas[i].AdmAreFKAdmCabId==idProceso) {
                  $('#area').append($('<option>', {
                    value: areas[i].AdmAreId,
                    text: areas[i].AreDes,
                  }));

                }
              }

            }

            function llenarReemplazos(){
             $(document).ready(function(){
              $('#reemplazosT').DataTable({
               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            }); 
             var n=1;
             var html="";
             html+="<table id='reemplazosT' class='table table-hover'>";
             html+="<thead><tr><th>#</th><th>Nombre</th><th>Dni</th><th>Correo</th><th>Telefono</th><th>Dirección</th><th>Cargo 1</th><th>Cargo 2</th><th>Observación</th></tr></thead><tbody>";
             for(i in reemplazos){
              html+="<tr>";
              html+="<td>";   
              html+=n;
              html+="</td>";
              html+="<td>"; 
              html+=reemplazos[i].nombre; 
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].dni==null) {
                html+="El usuario no tiene DNI";  
              }else{
                html+=reemplazos[i].dni;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].correo==null) {
                html+="El usuario no tiene Correo";  
              }else{
                html+=reemplazos[i].correo;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].telefono==null) {
                html+="El usuario no tiene Telefono";  
              }else{
                html+=reemplazos[i].telefono;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].direccion==null) {
                html+="El usuario no tiene Dirección ";  
              }else{
                html+=reemplazos[i].direccion;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].cargo1==null) {
                html+="El usuario no tiene Cargo";  
              }else{
                html+=reemplazos[i].cargo1;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].cargo2==null) {
                html+="El usuario no tiene Cargo";  
              }else{
                html+=reemplazos[i].cargo2;  
              }
              html+="</td>";
              html+="<td>";
              if (reemplazos[i].obs==null) {
                html+="El usuario no tiene Observación ";  
              }else{
                html+=reemplazos[i].obs;  
              }
              html+="</td>";
              html+="</tr>";
              n++;
            }
            html+="</table>";
            $('#reemplazos').html(html);
          }

          function llenarAreas(){
           var a=1;
           html="<table class='table table-hover'>";
           html+="<thead><tr><th>#</th><th>Nombre de Area</th><th>Reporte</th></tr></thead><tbody>";
           for(i in areas2){
            html+="<tr>";
            html+="<td>";   
            html+=a;
            html+="<input type='hidden' name='admAreId' type='text' value= '' >"; 
            html+=areas2[i].AdmAreId;  
            html+="</td>";
            html+="</td>";
            html+="<td name='areNombre' type='text' value= '' >";   
            html+=areas2[i].AreDes;
            html+="</td>";
            html+="<td>";   
            html+="<a  class='btn btn-warning' role='button'href='Excel/reporteAreApoyo.php?AdmAreId=";
            html+=areas2[i].AdmAreId;
            html+="&areNombre=";
            html+=areas2[i].AreDes;
            html+="'>GENERAR REPORTE DE APOYOS ADMINISTRATIVOS</a>";
            html+="</td>";
            html+="</tr>";
            a++;
          }

          html+="</table>";
          $('#areas22').html(html);
        }

        function llenarAreasCheck(){
          var idProceso=(document.getElementById("idProceso").innerHTML ).trim();
          html="";
          for(i in areas){  
            if (areas[i].AdmAreFKAdmCabId==idProceso) {
              html+="<input type='radio'  name='areas'";
              //html+=areas[i].AreDes+'check';
              html+=" id=";
              html+=areas[i].AreDes;
              html+=" onclick="
              html+="llenarPab("
              html+="'"+areas[i].AreDes+"'";
              html+=",";
              html+="'"+areas[i].AdmAreId+"'";
              html+=  ") >";
              //html+=areas[i].AreDes;
              //html+=" ')'>"; 
              html+="<font SIZE=3>";
              html+="  "+areas[i].AreDes; 
              html+="</font> ";
              html+="<br><br>";
            }
          }
          $('#checkAreas').html(html);
        }


          //<button type="button" class="btn btn-danger">Danger</button>
          function llenarPab(a,b){

            $(document).ready(function(){
              $('#pabT').DataTable({

               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            }); 


            var nomArea = a;
            html2="";

            html2+="<a  class='btn btn-danger' role='button' href='Excel/reportePabArePart.php?admAreId=";
            html2+=b;
            html2+="&areaNom=";
            html2+=a;
            html2+="'>GENERAR REPORTE</a>";
            $('#reporte').html(html2);

            $("#nomArea").text(nomArea);

            html="";
            var checkBox = document.getElementById(a);            
            var text = document.getElementById(a);
            
            if (checkBox.checked == true){
              var n=1;
              html+="<table id='pabT' class='table table-hover'>";
              html+="<thead><tr><th>#</th><th>Nombre de Pabellon</th><th>Area</th></tr></thead><tbody>";
              for(i in pabellones){
                if(pabellones[i].AdmPabFKAdmAreId == b){
                  html+="<tr>";
                  html+="<td>";   
                  html+=n;
                  html+="</td>";
                  html+="<td>";
                  html+="<input type='hidden' name='admAreId' type='text' value= '' >"; 
                  html+=pabellones[i].PabDes;  
                  html+="</td>";
                  html+="</td>";
                  html+="<td name='areNombre' type='text' value= '' >";   
                  html+=a;
                  html+="</td>";      
                  html+="</tr>";
                  n++;
                }
              }

            } else{
             html+=""
           }
           html+="</table>";
           $('#pab1').html(html);
         }

         function llenarCargosCheckEst(){

           $(document).ready(function(){
            $('#cargosT').DataTable({
             "scrollY": "150px", 
             "language":{
              "lengthMenu": "Se muestran _MENU_ registros por pagina",
              "info": "Mostrando pagina _PAGE_ de _PAGES_",
              "infoEmpty": "No hay registros disponibles",
              "infoFiltered": "(filtrada de _MAX_ registros)",
              "loadingRecords": "Cargando...",
              "processing":     "Procesando...",
              "search": "Buscar:  ",
              "zeroRecords":    "No se encontraron registros coincidentes",
              "paginate": {
                "next":       "Siguiente",
                "previous":   "Anterior"
              },          
            }
          }); 
          }); 

           var html="";
           var cargo=(document.getElementById("cargo").innerHTML ).trim();
           html+="<table id='cargosT' class='table table-hover'>";
           html+="<thead><tr><th>Cargo</th></tr></thead><tbody>";
           for(i in cargos){
            if (cargos[i].CarFKTipUsu==cargo) {
              nombreCargo=(cargos[i].CarDes).replace(/ /g, "-");
              html+="<tr>";
              html+="<td>"; 
              html+="<input type='radio'  name='areas'";
              html+=" id=";
              html+=cargos[i].CarId;
              html+=" onclick="

              html+="llenarEs("
              html+="'"+cargos[i].CarId+"'";
              html+=",";
              html+="'"+nombreCargo+"'";
              html+=  ") >";
              html+="<font SIZE=3>";
              html+="  "+cargos[i].CarDes; 
              html+="</font> ";
              html+="</td>";      
              html+="</tr>";
            }
          }
          $('#checkCargosEst').html(html);
        }


        function llenarEs(a,b){
          $(document).ready(function(){
            $('#estudiantesT').DataTable({

             "language":{
              "lengthMenu": "Se muestran _MENU_ registros por pagina",
              "info": "Mostrando pagina _PAGE_ de _PAGES_",
              "infoEmpty": "No hay registros disponibles",
              "infoFiltered": "(filtrada de _MAX_ registros)",
              "loadingRecords": "Cargando...",
              "processing":     "Procesando...",
              "search": "Buscar:  ",
              "zeroRecords":    "No se encontraron registros coincidentes",
              "paginate": {
                "next":       "Siguiente",
                "previous":   "Anterior"
              },          
            }
          }); 
          }); 

          var idEstado = $("#estados option:selected").val();
          var idCargo= a;
          var idProceso=(document.getElementById("idProceso").innerHTML ).trim();
          var nombreEstado=$("#estados option:selected").text();
          var nombreCargoS=b;

          html2="";
          html2+="<div class='row'>";
          html2+="<div class='col-lg-8'>";
          html2+="<h1 class='page-header' style='color:#333;font-size:24px;'><strong>"
          html2+="Usuarios con cargo "+nombreCargoS+" con estado de participacion "+nombreEstado+"</strong>";
          html2+="</h1>";
          html2+="</div>";
          html2+="<div class='col-lg-3'>";
          html2+="<br><br>";
          if(idEstado==17){
            html2+="<a  class='btn btn-danger' role='button' href='Excel/reporteEstPartAsig.php?idEstado=";
          }else{
            html2+="<a  class='btn btn-danger' role='button' href='Excel/reporteEstPart.php?idEstado=";
          }    
          html2+=idEstado;
          html2+="&idCargo=";
          html2+=idCargo;
          html2+="&idProceso=";
          html2+=idProceso;
          html2+="&nombreEstado=";
          html2+=nombreEstado;
          html2+="&nombreCargoS=";
          html2+=nombreCargoS;
          html2+="'>GENERAR REPORTE EN EXCEL</a>";
          html2+="</div>";
          html2+="</div>";
          $('#reporteEst').html(html2);
          //$("#nomArea").text(nomArea);
          var html="";
          var checkBox = document.getElementById(a);

          if (checkBox.checked == true){
            var n=1;
            html+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3>Información de usuarios</font><br></div><br><div class='panel-body'>";
            html+="<table id='estudiantesT' class='table table-hover' style='width:100%'>";   
            if(idEstado==17){
              html+="<thead><tr><th>#</th><th>Nombre</th><th>Dni</th><th>Correo</th><th>telefono</th><th>Pabellon</th></tr></thead>";
            }else{
              html+="<thead><tr><th>#</th><th>Nombre</th><th>Dni</th><th>Correo</th><th>telefono</th></tr></thead>";
            }
            
            for(i in usuarios){    
              if (idEstado==7) {
               if( (usuarios[i].AdmDetFKEstReg == 7 || usuarios[i].AdmDetFKEstReg == 17 || usuarios[i].AdmDetFKEstReg == 5)&& (usuarios[i].AdmDetFKCar == idCargo || usuarios[i].AdmDetFKCar2 == idCargo) && usuarios[i].AdmDetFKAdmCabId == idProceso ){

                html+="<tr>";
                html+="<td>";   
                html+=n;
                html+="</td>";
                html+="<td>"; 
                html+=usuarios[i].nombre; 
                html+="</td>";
                html+="<td>";
                if (usuarios[i].dni==null) {
                  html+="El usuario no tiene DNI";  
                }else{
                  html+=usuarios[i].dni;  
                }
                html+="</td>";
                html+="<td>";
                if (usuarios[i].correo==null) {
                  html+="El usuario no tiene Correo";  
                }else{
                  html+=usuarios[i].correo;  
                }
                html+="</td>";
                html+="<td>";
                if (usuarios[i].telefono==null) {
                  html+="El usuario no tiene Telefono";  
                }else{
                  html+=usuarios[i].telefono;  
                }
                html+="</td>";
                html+="</tr>";
                n++;
              }
            }else if (idEstado==17) {
              if( usuarios[i].AdmDetFKEstReg == idEstado && (usuarios[i].AdmDetFKCar == idCargo || usuarios[i].AdmDetFKCar2 == idCargo) && usuarios[i].AdmDetFKAdmCabId == idProceso ){
               html+="<tr>";
               html+="<td>";   
               html+=n;
               html+="</td>";
               html+="<td>"; 
               html+=usuarios[i].nombre; 
               html+="</td>";
               html+="<td>";
               if (usuarios[i].dni==null) {
                html+="El usuario no tiene DNI";  
              }else{
                html+=usuarios[i].dni;  
              }
              html+="</td>";
              html+="<td>";
              if (usuarios[i].correo==null) {
                html+="El usuario no tiene Correo";  
              }else{
                html+=usuarios[i].correo;  
              }
              html+="</td>";
              html+="<td>";
              if (usuarios[i].telefono==null) {
                html+="El usuario no tiene Telefono";  
              }else{
                html+=usuarios[i].telefono;  
              }
              html+="</td>";
              html+="<td>"; 
              if (usuarios[i].pabellon==null) {
                html+=usuarios[i].pabellon2 ;   
              }else if (usuarios[i].pabellon2==null){
                html+=usuarios[i].pabellon;
              }else{
                html+=usuarios[i].pabellon+ " - "+usuarios[i].pabellon2 ; 
              }
              html+="</td>";
              html+="</tr>";
              n++;
            }
          }else{
            if( usuarios[i].AdmDetFKEstReg == idEstado && (usuarios[i].AdmDetFKCar == idCargo || usuarios[i].AdmDetFKCar2 == idCargo) && usuarios[i].AdmDetFKAdmCabId == idProceso ){
              html+="<tr>";
              html+="<td>";   
              html+=n;
              html+="</td>";
              html+="<td>"; 
              html+=usuarios[i].nombre; 
              html+="</td>";
              html+="<td>";
              if (usuarios[i].dni==null) {
                html+="El usuario no tiene DNI";  
              }else{
                html+=usuarios[i].dni;  
              }
              html+="</td>";
              html+="<td>";
              if (usuarios[i].correo==null) {
                html+="El usuario no tiene Correo";  
              }else{
                html+=usuarios[i].correo;  
              }
              html+="</td>";
              html+="<td>";
              if (usuarios[i].telefono==null) {
                html+="El usuario no tiene Telefono";  
              }else{
                html+=usuarios[i].telefono;  
              }
              html+="</td>";
              html+="</tr>";
              n++;
            }
          }


        }
      } else{
       html+=""
     }
     html+="</table>";
     $('#estudiantes').html(html);
   }

   function seleccionarArea(){

    $(document).ready(function(){
      $('#pabTAsis').DataTable({
       "language":{
        "lengthMenu": "Se muestran _MENU_ registros por pagina",
        "info": "Mostrando pagina _PAGE_ de _PAGES_",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtrada de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "search": "Buscar:  ",
        "zeroRecords":    "No se encontraron registros coincidentes",
        "paginate": {
          "next":       "Siguiente",
          "previous":   "Anterior"
        },          
      }
    }); 
    }); 


    var areaSeleccionada = $("#area option:selected").val();
    var nombreAreaSeleccionada = $("#area option:selected").text();

    for(i in areas){
      if(areas[i].AreDes==nombreAreaSeleccionada){
        areIdLogic=areas[i].AreIdLog;
        break;
      }
    }

    $("#nombreAreaSeleccionada").text(nombreAreaSeleccionada);
          //$("#areaSeleccionada").text(nombreAreaSeleccionada);
          borrarOpciones("#pabellon");
          $(document).ready(function() {
            $("#pabelloness").find("tr:gt(0)").remove();
          });
          html="<table id ='pabTAsis' class='table table-hover'>";
          html+="<thead><tr><th>#</th><th>Nombre de Pabellon</th><th>Reporte</th></tr></thead><tbody>";
          var c =1;
          for(i in pabellones){
            if(pabellones[i].AdmPabFKAdmAreId == areaSeleccionada){

              $('#pabellon').append($('<option>', {
                value: pabellones[i].AdmPabId,
                text: pabellones[i].PabDes
              }));
              html+="<tr>";
              html+="<td>";   
              html+=c;
              html+="</td>";
              html+="<td name='pabNombre' type='text' value= '' >";   
              html+=pabellones[i].PabDes;
              html+="</td>";
              html+="<td>";   
              html+="<a  class='btn btn-warning' role='button' href='Excel/reportePabAsis.php?pabId=";
              html+=pabellones[i].AdmPabId;
              html+="&pabNombre=";
              html+=pabellones[i].PabDes;
              html+="&areIdLogic=";
              html+=areIdLogic;
              html+="'>GENERAR REPORTE DE ASISTENCIA POR PABELLON</a>";
              html+="</td>";
              html+="</tr>";
              c++;
            }
          }
          html+="</table>";
          $('#pabellonesS').html(html);
        }
        
        function seleccionarAreaR(){
          $(document).ready(function(){
            $('#pabTAsis').DataTable({
             "language":{
              "lengthMenu": "Se muestran _MENU_ registros por pagina",
              "info": "Mostrando pagina _PAGE_ de _PAGES_",
              "infoEmpty": "No hay registros disponibles",
              "infoFiltered": "(filtrada de _MAX_ registros)",
              "loadingRecords": "Cargando...",
              "processing":     "Procesando...",
              "search": "Buscar:  ",
              "zeroRecords":    "No se encontraron registros coincidentes",
              "paginate": {
                "next":       "Siguiente",
                "previous":   "Anterior"
              },          
            }
          }); 
          }); 

          var areaSeleccionada = $("#area option:selected").val();
          var nombreAreaSeleccionada = $("#area option:selected").text();
          for(i in areas){
            if(areas[i].AreDes==nombreAreaSeleccionada){
              areIdLogic=areas[i].AreIdLog;
              break;
            }
          }

          $("#nombreAreaSeleccionada").text(nombreAreaSeleccionada);
          //$("#areaSeleccionada").text(nombreAreaSeleccionada);
          borrarOpciones("#pabellon");
          $(document).ready(function() {
            $("#pabelloness").find("tr:gt(0)").remove();
          });
          html="<table id ='pabTAsis' class='table table-hover'>";
          html+="<thead><tr><th>#</th><th>Nombre de Pabellon</th><th>Reporte</th></tr></thead><tbody>";
          var c =1;
          for(i in pabellones){
            if(pabellones[i].AdmPabFKAdmAreId == areaSeleccionada){

              $('#pabellon').append($('<option>', {
                value: pabellones[i].AdmPabId,
                text: pabellones[i].PabDes
              }));
              html+="<tr>";
              html+="<td>";   
              html+=c;
              html+="</td>";
              html+="<td name='pabNombre' type='text' value= '' >";   
              html+=pabellones[i].PabDes;
              html+="</td>";
              html+="<td>";   
              html+="<a  class='btn btn-warning' role='button' href='Excel/reporteRecRef.php?pabId=";
              html+=pabellones[i].AdmPabId;
              html+="&pabNombre=";
              html+=pabellones[i].PabDes;
              html+="&areIdLogic=";
              html+=areIdLogic;
              html+="'>GENERAR REPORTE DE REFRIGERIO POR PABELLON</a>";
              html+="</td>";
              html+="</tr>";
              c++;
            }
          }
          html+="</table>";
          $('#pabellonesS').html(html);
        }

        function seleccionarAreaInfor(){
          $(document).ready(function(){
            $('#pabellonesT').DataTable({
              "scrollY": "150px", 
              "language":{
                "lengthMenu": "Se muestran _MENU_ registros",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
          }); 
          
          var areaSeleccionada = $("#area option:selected").val();
          var nombreAreaSeleccionada = $("#area option:selected").text();

          $("#nombreAreaSeleccionada").text("Generar reporte para "+nombreAreaSeleccionada);
          $("#nombreAreaSeleccionada2").text("Seleecion un pabellon de "+nombreAreaSeleccionada+" para ver información");
          
          //$("#areaSeleccionada").text(nombreAreaSeleccionada);
          var idProceso=(document.getElementById("idProceso").innerHTML ).trim();

          html="<a  class='btn btn-danger' role='button' href='Excel/reporteDisInf.php?admAreId=";
          html+=areaSeleccionada;
          html+="&areaNom=";
          html+=nombreAreaSeleccionada;
          html+="'>GENERAR REPORTE EN EXCEL</a>";
          $('#reporteInfo').html(html);

          html2="<table id='pabellonesT' class='table table-hover'>";
          html2+="<thead><tr><th>CargoPabellón</th></tr></thead><tbody>";
          for(i in pabellones){
            if (pabellones[i].AdmPabFKAdmAreId ==areaSeleccionada) {
              nombrePabAula=(pabellones[i].PabDes).replace(/ /g, "-");
              html2+="<tr>";
              html2+="<td>"; 
              html2+="<input type='radio'  name='areas'";
              html2+=" id=";
              html2+=pabellones[i].AdmPabId;
              html2+=" onclick="
              html2+="llenarAul("
              html2+="'"+pabellones[i].AdmPabId+"'";
              html2+=",";
              html2+="'"+areaSeleccionada+"'";
              html2+=",";
              html2+="'"+nombrePabAula+"'";
              html2+=  ") >";
              html2+="<font SIZE=3>";
              html2+="  "+pabellones[i].PabDes; 
              html2+="</font> ";
              html2+="</td>";      
              html2+="</tr>";
            }
          }
          $('#pabellonesS').html(html2);
        }

        function llenarAul(a,b,c){ 
          var checkBox = document.getElementById(a);
          var nC;
          var nombreAreaSeleccionada = $("#area option:selected").text();
          var htmlAula="";
          var n=0;
          var aforoT=0;
          var capacidadT=0;
          var controladoresT=0;
          var infoTitulo="<h1  ALIGN='justify' style='font-size:21px;'>Visualización de información de distribuición para informaticca en "+nombreAreaSeleccionada+" / "+c+"</h1><br>";
          $('#infoTitulo ').html(infoTitulo);
          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leerAulas&idArea="+b+"&idPab="+a+"", success: function(result){

            $(document).ready(function(){
              $('#aulasT').DataTable({
               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            });
            aulas = JSON.parse(result);
            if (checkBox.checked == true){             

             htmlAula+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3> Distribuición de aulas </font><br></div><br><div class='panel-body'>";
             htmlAula+="<table id='aulasT' class='table table-hover'>";
             htmlAula+="<thead><tr><th>#</th><th>Aula del pabellon</th><th>Aforo</th><th>Piso</th><th>Fila</th><th>Columna</th><th>Capacidad</th></tr></thead><tbody>";
             for(i in aulas){
              n++;
              nC=0;
              aforoT=aforoT+parseInt(aulas[i].aforo);
              capacidadT=capacidadT+parseInt(aulas[i].capacidad);
              htmlAula+="<tr>";
              htmlAula+="<td>";   
              htmlAula+=n;
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+="Aula N° "; 
              htmlAula+=aulas[i].numero; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].aforo; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].piso; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].fila; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].columna; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].capacidad; 
              htmlAula+="</td>";
              htmlAula+="</tr>";
            }
          }else{
           htmlAula+=""
         }

         $('#aulasPab').html(htmlAula);
         var totales="<h1  ALIGN='justify' style='font-size:17px;'><strong>Totales:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aulas: </strong>"+n+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Aforo: </strong>"+aforoT+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Capacidad: </strong>"+capacidadT+"</h1><br>";
         $('#totales').html(totales);
       }})

        }


        function seleccionarAreaCap(){
          $(document).ready(function(){
            $('#pabellonesT').DataTable({
              "scrollY": "150px", 
              "language":{
                "lengthMenu": "Se muestran _MENU_ registros",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
          }); 
          
          var areaSeleccionada = $("#area option:selected").val();
          var nombreAreaSeleccionada = $("#area option:selected").text();

          $("#nombreAreaSeleccionada").text("Generar reporte para "+nombreAreaSeleccionada);
          $("#nombreAreaSeleccionada2").text("Seleecion un pabellon de "+nombreAreaSeleccionada+" para ver información");
          
          //$("#areaSeleccionada").text(nombreAreaSeleccionada);
          var idProceso=(document.getElementById("idProceso").innerHTML ).trim();

          html="<a  class='btn btn-danger' role='button' href='Excel/reporteCapAre.php?admAreId=";
          html+=areaSeleccionada;
          html+="&areaNom=";
          html+=nombreAreaSeleccionada;
          html+="'>GENERAR REPORTE EN EXCEL</a>";
          $('#reporteInfo').html(html);

          html2="<table id='pabellonesT' class='table table-hover'>";
          html2+="<thead><tr><th>Pabellón</th></tr></thead><tbody>";
          for(i in pabellones){
            if (pabellones[i].AdmPabFKAdmAreId ==areaSeleccionada) {
              nombrePabAula=(pabellones[i].PabDes).replace(/ /g, "-");
              html2+="<tr>";
              html2+="<td>"; 
              html2+="<input type='radio'  name='areas'";
              html2+=" id=";
              html2+=pabellones[i].AdmPabId;
              html2+=" onclick="
              html2+="llenarAulCap("
              html2+="'"+pabellones[i].AdmPabId+"'";
              html2+=",";
              html2+="'"+areaSeleccionada+"'";
              html2+=",";
              html2+="'"+nombrePabAula+"'";
              html2+=  ") >";
              html2+="<font SIZE=3>";
              html2+="  "+pabellones[i].PabDes; 
              html2+="</font> ";
              html2+="</td>";      
              html2+="</tr>";
            }
          }
          $('#pabellonesS').html(html2);
        }

        function llenarAulCap(a,b,c){ 
          var checkBox = document.getElementById(a);
          var nC;
          var nombreAreaSeleccionada = $("#area option:selected").text();
          var htmlAula="";
          var n=0;
          var aforoT=0;
          var capacidadT=0;
          var controladoresT=0;
          var infoTitulo="<h1  ALIGN='justify' style='font-size:21px;'>Visualización de información de distribuición para informaticca en "+nombreAreaSeleccionada+" / "+c+"</h1><br>";
          $('#infoTitulo ').html(infoTitulo);
          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leerAulas&idArea="+b+"&idPab="+a+"", success: function(result){

            $(document).ready(function(){
              $('#aulasT').DataTable({
               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            });
            
            aulas = JSON.parse(result);
            if (checkBox.checked == true){             

             htmlAula+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3> Distribuición de aulas </font><br></div><br><div class='panel-body'>";
             htmlAula+="<table id='aulasT' class='table table-hover'>";
             htmlAula+="<thead><tr><th>#</th><th>Aula del pabellon</th><th>Aforo</th><th>Piso</th><th>Fila</th><th>Columna</th><th>Capacidad</th><th>controladores</th></tr></thead><tbody>";
             for(i in aulas){
              n++;
              nC=0;
              aforoT=aforoT+parseInt(aulas[i].aforo);
              capacidadT=capacidadT+parseInt(aulas[i].capacidad);
              htmlAula+="<tr>";
              htmlAula+="<td>";   
              htmlAula+=n;
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+="Aula N° "; 
              htmlAula+=aulas[i].numero; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].aforo; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].piso; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].fila; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].columna; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              htmlAula+=aulas[i].capacidad; 
              htmlAula+="</td>";
              htmlAula+="<td>"; 
              if(aulas[i].capacidad<50){
                nC=2;
              }else{
                nC=3;                
              }
              controladoresT=controladoresT+nC;

              htmlAula+=nC;
              htmlAula+="</td>";
              htmlAula+="</tr>";
            }
          }else{
           htmlAula+=""
         }

         $('#aulasPab').html(htmlAula);
         var totales="<h1  ALIGN='justify' style='font-size:17px;'><strong>Totales:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Aulas: </strong>"+n+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Aforo: </strong>"+aforoT+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Capacidad: </strong>"+capacidadT+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong> Controladores: </strong>"+controladoresT+"</h1><br>";
         $('#totales').html(totales);
       }})

          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leerGuraderos&idPabG="+a+"", success: function(result){
            var htmlGuar="";
            htmlGuar+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3> Distribuición de guarderos </font><br></div><br><div class='panel-body'>";
            guarderos = JSON.parse(result);
            htmlGuar+="<label class='control-label col-sm-5'>Apellidos y Nombres/ Telefono :</label>";
            htmlGuar+="<div class='col-lg-7'>";
            
            for(i in guarderos){
              htmlGuar+=guarderos[i].nombreG;
              htmlGuar+="<br><br>";
            }
            htmlGuar+="</div";
            htmlGuar+="</div>";
            htmlGuar+="</div>";
            $('#Guarderos').html(htmlGuar);
          }})
        }

        function doSearch(){
          var tableReg = document.getElementById('estudiantesTa');
          var searchText = document.getElementById('searchTerm').value.toLowerCase();
          var cellsOfRow="";
          var found=false;
          var compareWith="";

          for (var i = 1; i < tableReg.rows.length; i++){
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            found = false;

            for (var j = 0; j < cellsOfRow.length && !found; j++){
              compareWith = cellsOfRow[j].innerHTML.toLowerCase();

              if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1)){
                found = true;
              }
            }
            if(found){
              tableReg.rows[i].style.display = '';
            } else {
              tableReg.rows[i].style.display = 'none';
            }
          }
        }

        function selectComAdm(){

          var n=0;
          var areaSeleccionada = $("#area option:selected").val();
          
          var nombreAreaSeleccionada = $("#area option:selected").text();

          $("#nombreAreaSeleccionada").text("Generar reporte para "+nombreAreaSeleccionada);
          $("#nombreAreaSeleccionada2").text("Seleecion un pabellon de "+nombreAreaSeleccionada+" para ver información");

          html="<a  class='btn btn-danger' role='button' href='Excel/reporteComAdm.php?admAreId=";
          html+=areaSeleccionada;
          html+="&areaNom=";
          html+=nombreAreaSeleccionada;
          html+="'>GENERAR REPORTE EN EXCEL</a>";
          $('#reporteComAdm').html(html);

          var infoTitulo="<h1  ALIGN='justify' style='font-size:21px;'>Visualización de información de comision de ADMINISTRATIVOS "+nombreAreaSeleccionada+"</h1><br>";
          $('#infoTitulo ').html(infoTitulo);
          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leerAdmComAdm&idAreaAdm="+areaSeleccionada+"", success: function(result){
            var htmlAdmComAdm="";

            $(document).ready(function(){
              $('#comAdmT').DataTable({
               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            });
            comAdm = JSON.parse(result);
            htmlAdmComAdm+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3> Adminisitrativos con cargos de comisión en el área de "+nombreAreaSeleccionada+" </font><br></div><br><div class='panel-body'>";
            htmlAdmComAdm+="<table id='comAdmT' class='table table-hover'>";
            htmlAdmComAdm+="<thead><tr><th>#</th><th>Apellidos y Nombres</th><th>Dni</th><th>Cargo Comision 1</th><th>Cargo Comision 2</th></tr></thead><tbody>";
            for(i in comAdm){
              if (comAdm[i].comision1=="SI" || comAdm[i].comision2=="SI") {
               n++;
               nC=0;
               htmlAdmComAdm+="<tr>";
               htmlAdmComAdm+="<td>";   
               htmlAdmComAdm+=n;
               htmlAdmComAdm+="</td>";
               htmlAdmComAdm+="<td>"; 
               htmlAdmComAdm+=comAdm[i].nombre; 
               htmlAdmComAdm+="</td>";
               htmlAdmComAdm+="<td>"; 
               if (comAdm[i].dni==null) {
                htmlAdmComAdm+="El usuario no tiene DNI";  
              }else{
                htmlAdmComAdm+=comAdm[i].dni;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="<td>";
              if (comAdm[i].cargo1==null) {
                htmlAdmComAdm+="El usuario no tiene cargo";  
              }else{
                htmlAdmComAdm+=comAdm[i].cargo1;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="<td>"; 
              if (comAdm[i].cargo2==null) {
                htmlAdmComAdm+="El usuario no tiene cargo";  
              }else{
                htmlAdmComAdm+=comAdm[i].cargo2;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="</tr>";
            }
          }

          $('#comisionAdm').html(htmlAdmComAdm);
        }})
        }
        function selectComDoc(){

          var n=0;
          var areaSeleccionada = $("#area option:selected").val();
          
          var nombreAreaSeleccionada = $("#area option:selected").text();

          $("#nombreAreaSeleccionada").text("Generar reporte para "+nombreAreaSeleccionada);
          $("#nombreAreaSeleccionada2").text("Seleecion un pabellon de "+nombreAreaSeleccionada+" para ver información");

          html="<a  class='btn btn-danger' role='button' href='Excel/reporteComDoc.php?admAreId=";
          html+=areaSeleccionada;
          html+="&areaNom=";
          html+=nombreAreaSeleccionada;
          html+="'>GENERAR REPORTE EN EXCEL</a>";
          $('#reporteComAdm').html(html);

          var infoTitulo="<h1  ALIGN='justify' style='font-size:21px;'>Visualización de información de comision de ADMINISTRATIVOS "+nombreAreaSeleccionada+"</h1><br>";
          $('#infoTitulo ').html(infoTitulo);
          $.ajax({url: "../../controlador/reportes/reporteControlador.php?accion=leerAdmComDoc&idAreaDoc="+areaSeleccionada+"", success: function(result){
            var htmlAdmComAdm="";

            $(document).ready(function(){
              $('#comAdmT').DataTable({
               "language":{
                "lengthMenu": "Se muestran _MENU_ registros por pagina",
                "info": "Mostrando pagina _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrada de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search": "Buscar:  ",
                "zeroRecords":    "No se encontraron registros coincidentes",
                "paginate": {
                  "next":       "Siguiente",
                  "previous":   "Anterior"
                },          
              }
            }); 
            });
            comAdm = JSON.parse(result);
            htmlAdmComAdm+="<div class='panel panel-default'> <div class='panel-heading'><font SIZE=3> Adminisitrativos con cargos de comisión en el área de "+nombreAreaSeleccionada+" </font><br></div><br><div class='panel-body'>";
            htmlAdmComAdm+="<table id='comAdmT' class='table table-hover'>";
            htmlAdmComAdm+="<thead><tr><th>#</th><th>Apellidos y Nombres</th><th>Dni</th><th>Cargo Comision 1</th><th>Cargo Comision 2</th></tr></thead><tbody>";
            for(i in comAdm){
              if (comAdm[i].comision1=="SI" || comAdm[i].comision2=="SI") {
               n++;
               nC=0;
               htmlAdmComAdm+="<tr>";
               htmlAdmComAdm+="<td>";   
               htmlAdmComAdm+=n;
               htmlAdmComAdm+="</td>";
               htmlAdmComAdm+="<td>"; 
               htmlAdmComAdm+=comAdm[i].nombre; 
               htmlAdmComAdm+="</td>";
               htmlAdmComAdm+="<td>"; 
               if (comAdm[i].dni==null) {
                htmlAdmComAdm+="El usuario no tiene DNI";  
              }else{
                htmlAdmComAdm+=comAdm[i].dni;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="<td>";
              if (comAdm[i].cargo1==null) {
                htmlAdmComAdm+="El usuario no tiene cargo";  
              }else{
                htmlAdmComAdm+=comAdm[i].cargo1;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="<td>"; 
              if (comAdm[i].cargo2==null) {
                htmlAdmComAdm+="El usuario no tiene cargo";  
              }else{
                htmlAdmComAdm+=comAdm[i].cargo2;  
              }
              htmlAdmComAdm+="</td>";
              htmlAdmComAdm+="</tr>";
            }
          }

          $('#comisionAdm').html(htmlAdmComAdm);
        }})
        }

        function seleccionarPabellon(){
          var nombrePabellonSeleccionado = $("#pabellon option:selected").text();
          $("#pabellonSeleccionado").text(nombrePabellonSeleccionado);
        }

        function init(){
          cargarReemplazos();
          cargarAreas2();
          cargarAreas();
          cargarPabellones();
          cargaCargos();
          cargarUsuarios();
        }
