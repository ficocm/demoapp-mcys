
var administrativosInscritosSorteados;
var generos;
var categorias;
var iconOK = "<span id='ok' class='glyphicon glyphicon-ok'></span>";
var iconUnCheck = "<span id='un' class='glyphicon glyphicon-unchecked'></span>";
var iconNo = "<span class='glyphicon glyphicon-ban-circle'></span>";
var cargoCategorias = false;
var cargoGeneros = false;

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 				categorias = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCategorias = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarGeneros", success: function(result){
			try {
 				generos = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoGeneros = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

var ultimaPaginaCargada;
function cargar(pagina){
	ultimaPaginaCargada = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=7;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};

	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=mostrarAdministrativosInscritosSorteados',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					administrativosInscritosSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerAdministrativosInscritosSorteados();
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function ponerAdministrativosInscritosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in administrativosInscritosSorteados){
		usuario = administrativosInscritosSorteados[i];
		var genero = "";
		var categoria = "";
		for(j in generos){
			if(generos[j].GenId==usuario.UsuFKGen){
				genero = generos[j].GenSubCat;
				break;
			}
		}
		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
				break;
			}
		}
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//DNI
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombres
		var cell4 = row.insertCell(3);//Genero
		var cell5 = row.insertCell(4);//Categoria
		var cell6 = row.insertCell(5);//Número de procesos
		var cell7 = row.insertCell(6);
		var cell8 = row.insertCell(7);
		var cell9 = row.insertCell(8);
		cell1.innerHTML = usuario.UsuDni;
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = genero;
		cell5.innerHTML = categoria;
		cell6.innerHTML = usuario.UsuNumPro;

		if(usuario.idDetalle==-1){
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='crearParticipacion("+usuario.UsuId+","+codInscrito+")'>"+iconUnCheck+"</button>";
			cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='crearParticipacion("+usuario.UsuId+","+codSorteado+")'>"+iconUnCheck+"</button>";
			cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
		}else{
			if(usuario.estadoRegistro == codInscrito){//INSCRITO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='confirmarAccion("+usuario.idDetalle+","+codInscritoCancelado+", "+i+")'>"+iconOK+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarParticipacionAdministrativo("+usuario.idDetalle+","+codSorteado+")'>"+iconUnCheck+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
			}else if(usuario.estadoRegistro==codSorteado){//SORTEADO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarParticipacionAdministrativo("+usuario.idDetalle+","+codInscrito+")'>"+iconUnCheck+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='confirmarAccion("+usuario.idDetalle+","+codSorteadoCancelado+", "+i+")'>"+iconOK+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
			}else if(usuario.estadoRegistro==codAsignado){//ASIGNADO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' disabled>"+iconNo+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' disabled>"+iconNo+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='.informativoAsignacion'>SI</button>";
			}
		}
		cell6.setAttribute("style","text-align: center;");
		cell7.setAttribute("style","text-align: center;");
		cell8.setAttribute("style","text-align: center;");
		cell9.setAttribute("style","text-align: center;");

	}
	
}

function crearParticipacion(id, participacion){
	var url = "../../controlador/participantes/admisionDetalleControlador.php?accion=crearParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");

	$.ajax({
		type: "POST",
		url: url,
		data: $form.serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
}

function modificarParticipacionAdministrativo(id, participacion){

	var url= "../../controlador/participantes/admisionDetalleControlador.php?accion=modificarParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");
		
	$.ajax({
		type: "POST",
		url: url,
		data: $form.serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
}

function afterLoad(){
	if(cargoCategorias == true && cargoGeneros == true){
		cargar(1);	
	}
}

function confirmarAccion(id, participacion, posicion){
	usuario =	administrativosInscritosSorteados[posicion];
	if(participacion==codInscritoCancelado)
		$("#mensajeConfirmacion").html("DESEAR CANCELAR LA <b>INSCRIPCIÓN</b> AL PROCESO DE ADMISIÓN DE:<br> <b>	[" +usuario.UsuApe+" "+usuario.UsuNom+"]</b>");
	else if(participacion==codSorteadoCancelado)
		$("#mensajeConfirmacion").html("DESEAR CANCELAR EL <b>SORTEO</b> PARA EL PROCESO DE ADMISIÓN DE:<br> <b>[" +usuario.UsuApe+" "+usuario.UsuNom+"]</b>");
	var clicks = 0;
	$('.confirmacion').modal('show');
	$("#cancelar, #confirmar").on("click", function(e){
		clicks += 1;
		if(clicks>1)return;
		e.preventDefault();
		var opcion = $(this).attr('id');
		if(opcion=='cancelar'){
			$(this).prev().click();
		}else if(opcion=='confirmar'){
			modificarParticipacionAdministrativo(id, participacion);
			$(this).prev().click();
		}
	});
}