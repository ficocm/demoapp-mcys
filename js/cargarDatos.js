
var busqueda;

function cargar(){
	busqueda=document.getElementById("id").value;
	
	
	if(typeof busqueda !== 'undefined' && busqueda !== null) {
    busqueda=document.getElementById("id").value;
  	}
  	
	var parametros = {"action":"ajax",'busqueda':busqueda};


	$("#loader").fadeIn('slow');
	$.ajax({
		url:'../../controlador/usuario/usuarioVisualizar.php?accion=mostrarDatos',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").html("Cargando...");
		},
		success:function(result){
			var resultado = JSON.parse(result);
			if(resultado.error == false){
				dUsuario = resultado.mensaje;
				//alert(dUsuario[0].UsuTel);
				ponerDatos();
			}else{
				alert(result);
			}
		}
	})
}

function ponerDatos(){

	

	//var nom=document.getElementById("nombres");
	//var x=nom.val("");
	var nom=document.getElementById("nombres");
	if(typeof nom !== 'undefined' && nom !== null) {
		//alert(dUsuario[0].UsuNom);
    	document.getElementById("nombres").value=dUsuario[0].UsuNom;
  	}
	
	var ape=document.getElementById("apellidos");
	if(typeof ape !== 'undefined' && ape !== null) {
    	document.getElementById("apellidos").value=dUsuario[0].UsuApe;
  	}
  	var dni=document.getElementById("dni");
	if(typeof dni !== 'undefined' && dni !== null) {
    	document.getElementById("dni").value=dUsuario[0].UsuDni;
  	}
  	var correo=document.getElementById("correo");
	if(typeof correo !== 'undefined' && correo !== null) {
    	document.getElementById("correo").value=dUsuario[0].UsuCorEle;
  	}
  	var celular=document.getElementById("celular");
	if(typeof celular !== 'undefined' && celular !== null) {
    	document.getElementById("celular").value=dUsuario[0].UsuTel;
  	}
}

function editar(id){
	resetForm();
}

function agregar(){
	$('input[type=text]').val (function () {
    return this.value.toUpperCase();
	})

	var url = "../../controlador/usuario/usuarioControlador.php?accion=crearUsuario";
  $.ajax({
	  type: "POST",
	  url: url,
	  data: $("#formulario").serialize()+"&tipo=3", // serializes the form's elements.
	  success: function(data){
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
				cargar(1);
			}else{
				alert("Error resultado"+ data);
			}
		 	}catch(err){
		 		alert("Error casteo"+ data);
		 	}
		},
  	error: function(result) {
      alert("Error server"+ result);
    } 
	});
	
}

function resetForm(){
	$("#nombres").val('');
	$("#apellidos").val('');
	$("#dni").val('');
	$("#telefono").val('');
	$("#correo").val('');
	$("#numeroProcesos").val('');
	$("#direccion").val('');
	$("#funcion").val('');
	$("#dependencia").val('');
  $('#area :nth-child(1)').prop('selected', true);
  $('#departamento :nth-child(1)').prop('selected', true);
  $('#genero :nth-child(1)').prop('selected', true);
  $('#categoria :nth-child(1)').prop('selected', true);
}

cargar();