var docentesSorteados;
var areasParticipantes;
var pabellonesParticipantes;
var cargosDocentesAdministrativos;

var cargosAdministrativosComision;
var cargosDocentesComision;

var cargoAreasParticipantes = false;
var cargoPabellonesParticipantes = false;
var cargoCargosDocentes = false;
var cargoCargosComision = false;

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAreasParticipantes", success: function(result){
			try {
 				areasParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoAreasParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			ponerListaAreas();
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarPabellonesParticipantes", success: function(result){
			try {
 				pabellonesParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoPabellonesParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosDocentesComision", success: function(result){
			try {
 				cargosDocentesComision = JSON.parse(result);
 			 
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosDocentesComision = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosAdministrativosComision", success: function(result){
			try {
 				cargosAdministrativosComision = JSON.parse(result);
 			 
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosAdministrativosComision = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosDocentesAdministrativos", success: function(result){
			try {
 				cargosDocentesAdministrativos = JSON.parse(result);
 			 
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosDocentes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

var ultimaPagina = 1;
var mostrarDocentes = true;

function cargar(pagina){
	ultimaPagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=5;

	var URL = "../../controlador/asignacion/asignacionControlador.php?accion=mostrarDocentesSorteadosSinTipo";
	if(mostrarDocentes == true){
		URL = "../../controlador/asignacion/asignacionControlador.php?accion=mostrarDocentesSorteadosSinTipo";
	}else{
		URL = "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAdministrativosSorteados";
	}

	var cargo = $("#cargo1").val();
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:URL,
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					docentesSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDocentesSorteados();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function borrarOpciones(opcion){
	$(opcion).find('option').remove().end()
		.append('<option value="-1">Seleccione</option>');
}

function ponerListaAreas(){
	borrarOpciones("#area");
	borrarOpciones("#pabellon1");
	borrarOpciones("#pabellon2");
	for(i in areasParticipantes){
		$('#area').append($('<option>', {
			value: areasParticipantes[i].AdmAreId,
			text: areasParticipantes[i].AreDes
		}));
	}
}

function ponerListaCargos(nombre){
	borrarOpciones(nombre);
	for(i in cargosDocentesAdministrativos){
		$(nombre).append($('<option>', {
			value: cargosDocentesAdministrativos[i].CarId,
			text: cargosDocentesAdministrativos[i].CarDes
		}));
	}
}

function ponerListaCargosComision(nombre){
	borrarOpciones(nombre);
	for(i in cargosDocentesComision){
		$(nombre).append($('<option>', {
			value: cargosDocentesComision[i].CarId,
			text: "Docente: "+cargosDocentesComision[i].CarDes
		}));
	}
	for(i in cargosAdministrativosComision){
		$(nombre).append($('<option>', {
			value: cargosAdministrativosComision[i].CarId,
			text: "Administrativo: "+cargosAdministrativosComision[i].CarDes
		}));
	}
}

function seleccionarArea1(){
	var areaSeleccionada = $("#area option:selected").val();
	borrarOpciones("#pabellon1");

	for(i in pabellonesParticipantes){
		if(pabellonesParticipantes[i].AdmPabFKAdmAreId == areaSeleccionada){
			$('#pabellon1').append($('<option>', {
				value: pabellonesParticipantes[i].AdmPabId,
				text: pabellonesParticipantes[i].PabDes
			}));
		}
	}
	for(i in pabellonesParticipantes){
		if(pabellonesParticipantes[i].AdmPabFKAdmAreId == areaSeleccionada){
			$('#pabellon2').append($('<option>', {
				value: pabellonesParticipantes[i].AdmPabId,
				text: pabellonesParticipantes[i].PabDes
			}));
		}
	}
}


function ponerDocentesSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in docentesSorteados){
		usuario = docentesSorteados[i];
		var cargo1 = "";
		var cargo2 = "";
		var pabellon1 = "";
		var pabellon2 = "";
		var area = "";
		var primerCargo = "nada";
		var segundoCargo = "nada";

		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//Editar
		var cell2 = row.insertCell(1);//Dni
		var cell3 = row.insertCell(2);//Apellidos
		var cell4 = row.insertCell(3);//Nombres
		var cell5 = row.insertCell(4);//Cargo1
		var cell6 = row.insertCell(5);//Area1
		var cell7 = row.insertCell(6);//pabellon1
		var cell8 = row.insertCell(7);//Cargo2
		var cell9 = row.insertCell(8);//pabellon2
		var cell10 = row.insertCell(9);cell10.style.display="none";//Experiencia

		for(j in pabellonesParticipantes){
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab){
				pabellon1 = pabellonesParticipantes[j].PabDes;
			}
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab2){
				pabellon2 = pabellonesParticipantes[j].PabDes;
			}
		}
		for(j in cargosDocentesAdministrativos){
			if(cargosDocentesAdministrativos[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosDocentesAdministrativos[j].CarDes;
				primerCargo = "normal";
			}
			if(cargosDocentesAdministrativos[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosDocentesAdministrativos[j].CarDes;
				segundoCargo = "normal";
			}
		}
		for(j in cargosDocentesComision){
			if(cargosDocentesComision[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosDocentesComision[j].CarDes;
				primerCargo = "comisionDocente";
			}
			if(cargosDocentesComision[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosDocentesComision[j].CarDes;
				segundoCargo = "comisionDocente";
			}
		}
		for(j in cargosAdministrativosComision){
			if(cargosAdministrativosComision[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosAdministrativosComision[j].CarDes;
				primerCargo = "comisionAdministrativo";
			}
			if(cargosAdministrativosComision[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosAdministrativosComision[j].CarDes;
				segundoCargo = "comisionAdministrativo";
			}
		}
		for(j in areasParticipantes){
			if(areasParticipantes[j].AdmAreId == usuario.AdmDetFKAre){
				area = areasParticipantes[j].AreDes;
			}
		}

		cell1.innerHTML = "<input type='image' onclick='editar("+usuario.AdmDetId+",\""+primerCargo+"\",\""+segundoCargo+"\");' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuDni;
		cell3.innerHTML = usuario.UsuApe;
		cell4.innerHTML = usuario.UsuNom;
		cell5.innerHTML = cargo1;
		cell6.innerHTML = area;
		cell7.innerHTML = pabellon1;
		cell8.innerHTML = cargo2;
		cell9.innerHTML = pabellon2;
		cell10.innerHTML = usuario.UsuNumPro;
		cell1.setAttribute("style","text-align: center;");
	}
	
}

var idDocente = -1;
function editar(idParticipante, primerCargo, segundoCargo){
	$("#cargo1").attr('disabled', false);
	$("#area").attr('disabled', false);
	$("#pabellon1").attr('disabled', false);
	$("#cargo2").attr('disabled', false);
	$("#pabellon2").attr('disabled', false);
	idDocente = idParticipante;
	ponerDatos(idDocente, primerCargo, segundoCargo);
}

function ponerDatos(idParticipante, primerCargo, segundoCargo){
	if(primerCargo == "normal"){
		$("#mensaje1").text("Este cargo no es de comisión");
		ponerListaCargos("#cargo1");
		$("#cargo1").attr('disabled', 'disabled');
		$("#area").attr('disabled', 'disabled');
		$("#pabellon1").attr('disabled', 'disabled');
	}else{
		$("#mensaje1").text("Ingrese cargo de comisión");
		ponerListaCargosComision("#cargo1");
	}
	if(segundoCargo == "normal"){
		$("#mensaje2").text("Este cargo no es de comisión");
		ponerListaCargos("#cargo2");
		$("#cargo2").prop('disabled', 'disabled');
		$("#pabellon2").prop('disabled', 'disabled');
	}else{
		$("#mensaje2").text("Ingrese cargo de comisión");
		ponerListaCargosComision("#cargo2");
	}
	for (i in docentesSorteados){
		var usuario = docentesSorteados[i];
		if(usuario.AdmDetId == idParticipante){
			$("#cargo1").val(usuario.AdmDetFKCar);
			$("#cargo2").val(usuario.AdmDetFKCar2);
			$("#area").val(usuario.AdmDetFKAre);
			seleccionarArea1();
			$("#pabellon1").val(usuario.AdmDetFKPab);
		}
	}
}

function asignar(idParticipante){

	$("#cargo1").attr('disabled', false);
	$("#area").attr('disabled', false);
	$("#pabellon1").attr('disabled', false);
	$("#cargo2").attr('disabled', false);
	$("#pabellon2").attr('disabled', false);

	var url = "../../controlador/asignacion/asignacionControlador.php?accion=asignarLugaresYCargosComision";
	
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize() + "&id=" + idParticipante, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Asignando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

var cargoListado = false;
function loadUsuario(){
	if(!cargoListado && cargoAreasParticipantes == true && cargoPabellonesParticipantes==true && cargoCargosDocentes==true && cargoCargosDocentesComision==true){
		cargar(1);
		cargoListado = true;
	}
}

$( document ).ready(function() {
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(validarFormulario()==true){
			asignar(idDocente);
			$(this).prev().click();
		}else{
			mostrarError("NO TIENE NINGUNA ASIGNACIÓN PARA GUARDAR");
			$('#area').focus();
		}
	});

	$("#botonResumen").on("click", function(e){
			e.preventDefault(); // prevent de default action, which is to submit
			// save your value where you want
			if(mostrarDocentes==true){
				mostrarDocentes = false;
				$("#botonResumen").text("Mostrar Docentes");
				$("#mensajeListado").text("LISTADO DE ADMINISTRATIVOS");
			}else{
				mostrarDocentes = true;
				$("#botonResumen").text("Mostrar Administrativos");
				$("#mensajeListado").text("LISTADO DE DOCENTES");
			}
			cargar(1);
			$(this).prev().click();
	});
});

function validarFormulario(){
	if(($("#area").val()==null || $("#area").val() < 1) && 
		($("#pabellon1").val()==null || $("#pabellon1").val() < 1) && 
		($("#pabellon2").val()==null || $("#pabellon2").val() < 1) && 
		($("#cargo1").val()==null || $("#cargo1").val() < 1) && 
		($("#cargo2").val()==null || $("#cargo2").val() < 1)){
		return false;
	}
	return true;
}