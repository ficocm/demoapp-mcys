
var administrativosInscritosSorteados;
var generos;
var categorias;

var cargoCategorias = false;
var cargoGeneros = false;

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 			  categorias = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
	  		cargoCategorias = true;
			}
	  }})
	  ).done(function(ajaxAreas){
	  	afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarGeneros", success: function(result){
			try {
 			  generos = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
	  		cargoGeneros = true;
			}
	  }})
	  ).done(function(ajaxAreas){
	  	afterLoad();
	});

function cargar(pagina){
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=10;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'../../controlador/credenciales/admisionDetalle.php?accion=mostrarAdministrativosInscritosSorteados',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").html("Cargando...");
		},
		success:function(result){
			var resultado = JSON.parse(result);
			if(resultado.error == false){
				administrativosInscritosSorteados = resultado.mensaje;
				$("#paginacion").html(resultado.datos);
				$("#loader").html("");
				ponerAdministrativosInscritosSorteados();	
			}else{
				alert(result);
			}
		}
	})
}

function ponerAdministrativosInscritosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

  for (i in administrativosInscritosSorteados){
  	usuario = administrativosInscritosSorteados[i];
  	var genero = "";
  	var categoria = "";
    
    var row = document.getElementById("usuarios").insertRow(-1);
    var cell1 = row.insertCell(0);//DNI
    var cell2 = row.insertCell(1);//Nombre
    var cell3 = row.insertCell(2);//cargo
    var cell4 = row.insertCell(3);//button
 
    //cell1.innerHTML = usuario.UsuId;
    cell1.innerHTML = usuario.UsuDni;
    cell2.innerHTML = usuario.UsuApe+", "+usuario.UsuNom;
    cell3.innerHTML = usuario.cargo;
    cell4.innerHTML = "<a href=\"../../controlador/credenciales/unitaria.php?id="+usuario.AdmDetId+"\" target=\"_blank\" >Generar</a>"
    
    //alert(usuario.inscrito+ ", "+usuario.sorteado +", "+usuario.noInscrito);
    
    //cell7.innerHTML = "<input type='checkbox' class='radio' onclick='modificarParticipacionDocente(true, 12, this)' name='participacion["+i+"][]'>";
		cell1.setAttribute("style","text-align: center;");
		cell3.setAttribute("style","text-align: center;");
		cell4.setAttribute("style","text-align: center;");
   // cell3.setAttribute("style","text-align: center;");
    
   // cell4.setAttribute("style","text-align: center;");
  }
  
}


function afterLoad(){
	if(cargoCategorias == true && cargoGeneros == true){
		cargar(1);	
	}
}

