var docentesAdministrativos;
var categorias;
var generos;
var areas;
var tipos; //Docentes y Administrativos
var cargoCategorias = false;
var cargoGeneros = false;
var cargoAreas = false;
var cargoTipos = false;

var seEdita = false;
var seAgrega = false;

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 				categorias = JSON.parse(result);
			}catch(err) {
				mostrarError("No se pudo cargar las categorias");
			}finally {
				cargoCategorias = true;
			}
		}})
	).done(function(ajaxAreas){
		afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarGeneros", success: function(result){
			try {
 				generos = JSON.parse(result);
			}catch(err) {
				mostrarError("No se pudo cargar los generos");
			}finally {
				cargoGeneros = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarAreas", success: function(result){
			try {
 				areas = JSON.parse(result);
			}catch(err) {
				mostrarError("No se pudo cargar las areas");
			}finally {
				cargoAreas = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarTiposDocentesAdministrativos", success: function(result){
			try {
 				tipos = JSON.parse(result);
			}catch(err) {
				mostrarError("No se pudo cargar los tipos");
			}finally {
				cargoTipos = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

var ultimaPagina;
function cargar(pagina){
	ultimaPagina = pagina;
	var tipo = $("#fitroTipo").val();

	var URL = '../../controlador/usuario/usuarioControlador.php?accion=mostrarAdministrativosDocentes';
	if(tipo=="TODOS"){
		URL = '../../controlador/usuario/usuarioControlador.php?accion=mostrarAdministrativosDocentes';
	}else if(tipo=="DOCENTE"){
		URL = '../../controlador/usuario/usuarioControlador.php?accion=mostrarDocentes';
	}else if(tipo=="ADMINISTRATIVO"){
		URL = '../../controlador/usuario/usuarioControlador.php?accion=mostrarAdministrativos';
	}
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=5;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:URL,
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					docentesAdministrativos = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDocentesAdministrativos();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo ("+err+") data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function borrarOpciones(opcion){
	$(opcion).find('option').remove().end()
		.append('<option value="-1">Seleccione</option>');
}

function cargarSelects(){
	borrarOpciones("#genero");
	borrarOpciones("#categoria");
	borrarOpciones("#area");

	for(i in generos){
		$('#genero').append($('<option>', {
			value: generos[i].GenId,
			text: generos[i].GenSubCat
		}));
	}
	for(i in categorias){
		$('#categoria').append($('<option>', {
			value: categorias[i].GenId,
			text: categorias[i].GenSubCat
		}));
	}
	for(i in areas){
		$('#area').append($('<option>', {
			value: areas[i].AreId,
			text: areas[i].AreDes
		}));
	}
}

function ponerDocentesAdministrativos(){
	$("#usuarios").find("tr:gt(0)").remove();

	for (i in docentesAdministrativos){
		usuario = docentesAdministrativos[i];

		var genero = "";
		var categoria = "";
		var area = "";
		var tipo1 = "";
		var tipo2 = "";

		for(j in generos){
			if(generos[j].GenId==usuario.UsuFKGen){
				genero = generos[j].GenSubCat;
				break;
			}
		}
		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
				break;
			}
		}

		for(j in areas){
			if(areas[j].AreId==usuario.UsuFKAre){
				area = areas[j].AreDes;
				break;
			}
		}

		for(j in tipos){
			if(tipos[j].GenId == usuario.UsuFKTip){
				tipo1 = tipos[j].GenSubCat;
			}
			if(tipos[j].GenId == usuario.UsuFKTip2){
				if(tipo1!="")
					tipo2=" - "
				tipo2 = tipo2+tipos[j].GenSubCat;
			}
		}

		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//EDITAR
		var cell2 = row.insertCell(1);//DNI
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Apellidos
		var cell5 = row.insertCell(4);//Tipos
		var cell6 = row.insertCell(5);//Correo
		var cell7 = row.insertCell(6);//Telefono
		var cell8 = row.insertCell(7);//Direccion
		var cell9 = row.insertCell(8);//Categoria
		var cell10 = row.insertCell(9);//Genero
		var cell11 = row.insertCell(10);//Numero de Procesos
		var cell12 = row.insertCell(11);//Departamento Academico
		var cell13 = row.insertCell(12);//Area
		var cell14 = row.insertCell(13);//Funcion
		var cell15 = row.insertCell(14);//Dependencia
		if(administrador)
			var cell16 = row.insertCell(15);//Eliminar

		cell1.innerHTML = "<input type='image' onclick='editable("+usuario.UsuId+");' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuDni;
		cell3.innerHTML = usuario.UsuApe;
		cell4.innerHTML = usuario.UsuNom;
		cell5.innerHTML = tipo1+tipo2;
		cell6.innerHTML = usuario.UsuCorEle;
		cell7.innerHTML = usuario.UsuTel;
		cell8.innerHTML = usuario.UsuDir;
		cell9.innerHTML = categoria;
		cell10.innerHTML = genero;
		cell11.innerHTML = usuario.UsuNumPro;
		cell12.innerHTML = usuario.UsuDepAca;
		cell13.innerHTML = area;
		cell14.innerHTML = usuario.UsuFun;
		cell15.innerHTML = usuario.UsuDep;
		if(administrador)
			cell16.innerHTML = "<button type='button' class='btn btn-danger' onclick='confirmarAccion("+usuario.UsuId+")'>ELIMINAR</button>"
		cell1.setAttribute("style","text-align: center;");
		cell11.setAttribute("style","text-align: center;");
	}
}

function agregable(){
	$("#save").text("AGREGAR");
	$(".modal-title").text("NUEVO DOCENTE / ADMINISTRATIVO");
	seAgrega = true;
	seEdita = false;
	idUsuario = null;
	resetForm();
}

var idUsuario;
function editable(id){
	$("#save").text("GUARDAR");
	$(".modal-title").text("EDITAR DOCENTE / ADMINISTRATIVO");
	seEdita = true;
	seAgrega = false;
	idUsuario = id;

	resetForm();
	for (i in docentesAdministrativos){
		usuario = docentesAdministrativos[i];
		if(usuario.UsuId == id){
			$("#nombres").val(usuario.UsuNom);
			$("#apellidos").val(usuario.UsuApe);
			$("#dni").val(usuario.UsuDni);
			$("#telefono").val(usuario.UsuTel);
			$("#correo").val(usuario.UsuCorEle);
			$("#numeroProcesos").val(usuario.UsuNumPro);
			$("#direccion").val(usuario.UsuDir);
			$("#funcion").val(usuario.UsuFun);
			$("#dependencia").val(usuario.UsuDep);
			$('#area').val(usuario.UsuFKAre);
			$('#departamento').val(usuario.UsuDepAca);
			$('#genero').val(usuario.UsuFKGen);
			$('#categoria').val(usuario.UsuFKCat);
			if(usuario.UsuFKTip==codDocente){
				$("#tipo1").prop("checked", true);
			}
			if(usuario.UsuFKTip2==codAdministrativo){
				$("#tipo2").prop("checked", true);
			}
		}
	}
}

function guardar(){
	ponerMayusculasYMinusculas();
	var url = "../../controlador/usuario/usuarioControlador.php?accion=modificarUsuario";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize()+"&id="+idUsuario, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Guardando...");
		},
		success: function(data){
			$("#loader").hide();
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
		 	}catch(err){
		 		mostrarError("Error Casteo data:"+data);
		 	}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
	
}

function agregar(){
	ponerMayusculasYMinusculas();

	var url = "../../controlador/usuario/usuarioControlador.php?accion=crearUsuario";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Guardando...");
		},
		success: function(data){
			$("#loader").hide();
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(1);
				}else{
					mostrarError(res.mensaje);
				}
		 	}catch(err){
		 		mostrarError("Error Casteo data:"+data);
		 	}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
	
}

function confirmarAccion(id){
	var clicks = 0;
	$('.confirmacion').modal('show');
	$("#cancelar, #confirmar").on("click", function(e){
		clicks += 1;
		if(clicks>1)return;
		e.preventDefault();
		var opcion = $(this).attr('id');
		if(opcion=='cancelar'){
			$(this).prev().click();
		}else if(opcion=='confirmar'){
			eliminar(id);
			$(this).prev().click();
		}
	});
}


function eliminar(id){
	var parametros = {"id":id};
	var url = "../../controlador/usuario/usuarioControlador.php?accion=eliminarDocenteAdministrativo";
	$.ajax({
		type: "POST",
		url: url,
		data: parametros, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Eliminando...");
		},
		success: function(data){
			$("#loader").hide();
		 	try{
		 		res = JSON.parse(data);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
		 	}catch(err){
		 		mostrarError("Error Casteo data:"+data);
		 	}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
	
}

function resetForm(){
	$("#nombres").val('');
	$("#apellidos").val('');
	$("#dni").val('');
	$("#telefono").val('');
	$("#correo").val('');
	$("#numeroProcesos").val('');
	$("#direccion").val('');
	$("#funcion").val('');
	$("#dependencia").val('');
	$("#tipo1").prop("checked", false);
	$("#tipo2").prop("checked", false);
	$('#area :nth-child(1)').prop('selected', true);
	$("#departamento").val('');
	$('#genero :nth-child(1)').prop('selected', true);
	$('#categoria :nth-child(1)').prop('selected', true);
}

$( document ).ready(function() {
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(formularioValidoAdministrativo()==true){
			if(seEdita==true){
				precesarDniCorreo(idUsuario,guardar,$(this),codAdministrativo);//Tambien verifica correo electronico
				//guardar();
			}else if(seAgrega==true){
				precesarDniCorreo('NULL',agregar,$(this),codAdministrativo);
			}
			//$(this).prev().click();
		}
	});
});


var cargo = false;
function afterLoad(){
	if(!cargo && cargoCategorias && cargoGeneros && cargoAreas && cargoTipos){
		cargo = true;
		cargarSelects();
		cargar(1);	
	}
}