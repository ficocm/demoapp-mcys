var administrativosSorteados;
var areasParticipantes;
var pabellonesParticipantes;
var areasPuertas;
var cargosAdministrativos;
var cargosDocentes;
var cargosComision;

var cargoAreasParticipantes = false;
var cargoPabellonesParticipantes = false;
var cargoAreasPuertas = false;
var cargoCargosAdministrativos = false;
var cargoCargosDocentes = false;
var cargoCargosComision = false;

$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAreasParticipantes", success: function(result){
			try {
 				areasParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoAreasParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			ponerListaAreas();
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarPabellonesParticipantes", success: function(result){
			try {
 				pabellonesParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoPabellonesParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAreasPuertas", success: function(result){
			try {
 				areasPuertas = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoAreasPuertas = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosAdministrativos", success: function(result){
			try {
 				cargosAdministrativos = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosAdministrativos = true;
			}
		}})
		).done(function(ajaxAreas){
			ponerCargos();
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosDocentes", success: function(result){
			try {
 				cargosDocentes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosDocentes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosComision", success: function(result){
			try {
 				cargosComision = JSON.parse(result);
 			 
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosComision = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

var ultimaPagina = 1;
function cargar(pagina){
	ultimaPagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=5;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/asignacion/asignacionControlador.php?accion=mostrarAdministrativosSorteados',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					administrativosSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerAdministrativosSorteados();
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function borrarOpciones(opcion){
	$(opcion).find('option').remove().end()
		.append('<option value="-1">Seleccione</option>');
}

function ponerListaAreas(){
	borrarOpciones("#area");
	borrarOpciones("#pabellon1");
	borrarOpciones("#pabellon2");
	for(i in areasParticipantes){
		$('#area').append($('<option>', {
			value: areasParticipantes[i].AdmAreId,
			text: areasParticipantes[i].AreDes
		}));
	}
}

function seleccionarArea(){
	var areaSeleccionada = $("#area option:selected").val();

	borrarOpciones("#pabellon1");
	borrarOpciones("#pabellon2");
	borrarOpciones("#puerta1");
	borrarOpciones("#puerta2");
	for(i in pabellonesParticipantes){
		if(pabellonesParticipantes[i].AdmPabFKAdmAreId == areaSeleccionada){
			$('#pabellon1').append($('<option>', {
				value: pabellonesParticipantes[i].AdmPabId,
				text: pabellonesParticipantes[i].PabDes
			}));
			$('#pabellon2').append($('<option>', {
				value: pabellonesParticipantes[i].AdmPabId,
				text: pabellonesParticipantes[i].PabDes
			}));
		}
	}
	for(i in areasPuertas){
	 	if(areasPuertas[i].AdmAreId == areaSeleccionada){
			$('#puerta1').append($('<option>', {
				value: areasPuertas[i].ArePueId,
				text: areasPuertas[i].ArePueDes
			}));
			$('#puerta2').append($('<option>', {
				value: areasPuertas[i].ArePueId,
				text: areasPuertas[i].ArePueDes
			}));
	 	}
	}
}

function ponerCargos(){
	borrarOpciones("#cargo1");
	borrarOpciones("#cargo2");
	for(i in cargosAdministrativos){
		$('#cargo1').append($('<option>', {
			value: cargosAdministrativos[i].CarId,
			text: cargosAdministrativos[i].CarDes
		}));
		$('#cargo2').append($('<option>', {
			value: cargosAdministrativos[i].CarId,
			text: cargosAdministrativos[i].CarDes
		}));
	}
}

function ponerAdministrativosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in administrativosSorteados){
		usuario = administrativosSorteados[i];
		var puerta1 = "";
		var puerta2 = "";
		var cargo1 = "";
		var cargo2 = "";
		var pabellon1 = "";
		var pabellon2 = "";
		var area = "";
		var cargo1Tipo = "nada";
		var cargo2Tipo = "nada";

		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//AsignarCargp
		var cell2 = row.insertCell(1);//Nombre
		var cell3 = row.insertCell(2);//Area
		var cell4 = row.insertCell(3);//Pabellon1
		var cell5 = row.insertCell(4);//cargo1
		var cell6 = row.insertCell(5);//puerta1
		var cell7 = row.insertCell(6);//lugar1
		var cell8 = row.insertCell(7);//Pabellon2
		var cell9 = row.insertCell(8);//cargo2
		var cell10 = row.insertCell(9);//puerta2
		var cell11 = row.insertCell(10);//lugar2
		var cell12 = row.insertCell(11);cell12.style.display="none";//Experiencia

		cell6.className = 'resumen';
		cell7.className = 'resumen';
		cell10.className = 'resumen';
		cell11.className = 'resumen';

		for(i in areasPuertas){
	 		if(areasPuertas[i].ArePueId == usuario.AdmDetFKArePue){
	 			puerta1 = areasPuertas[i].ArePueDes;
	 		}
	 		if(areasPuertas[i].ArePueId == usuario.AdmDetFKArePue2){
	 			puerta2 = areasPuertas[i].ArePueDes;
	 		}
		}
		for(j in pabellonesParticipantes){
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab){
				pabellon1 = pabellonesParticipantes[j].PabDes;
			}
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab2){
				pabellon2 = pabellonesParticipantes[j].PabDes;
			}
		}
		for(j in cargosAdministrativos){
			if(cargosAdministrativos[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosAdministrativos[j].CarDes;
				cargo1Tipo = "administrativo";
			}
			if(cargosAdministrativos[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosAdministrativos[j].CarDes;
				cargo2Tipo = "administrativo";
			}
		}

		for(j in cargosComision){
			if(cargosComision[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosComision[j].CarDes;
				cargo1Tipo = "comision";
			}
			if(cargosComision[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosComision[j].CarDes;
				cargo1Tipo = "comision";
			}
		}

		for(j in cargosDocentes){
			if(cargosDocentes[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosDocentes[j].CarDes;
				cargo1Tipo = "docente";
			}
			if(cargosDocentes[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosDocentes[j].CarDes;
				cargo1Tipo = "docente";
			}
		}

		for(j in areasParticipantes){
			if(areasParticipantes[j].AdmAreId == usuario.AdmDetFKAre){
				area = areasParticipantes[j].AreDes;
				break;
			}
		}
		cell1.innerHTML = "<input type='image' onclick='editar("+usuario.AdmDetId+", \""+cargo1Tipo+"\", \""+cargo2Tipo+"\")';' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuApe+", "+usuario.UsuNom;
		cell3.innerHTML = area;
		cell4.innerHTML = pabellon1;
		cell5.innerHTML = cargo1;
		cell6.innerHTML = puerta1;
		cell7.innerHTML = usuario.AdmDetLug1;
		cell8.innerHTML = pabellon2;
		cell9.innerHTML = cargo2;
		cell10.innerHTML = puerta2;
		cell11.innerHTML = usuario.AdmDetLug2;
		cell12.innerHTML = usuario.UsuNumPro;

		cell1.setAttribute("style","text-align: center;");
	}
	mostrarCampos($.cookie("resumen"));
}

function ponerListaCargosComision(nombre){
	borrarOpciones(nombre);
	for(i in cargosComision){
		$(nombre).append($('<option>', {
			value: cargosComision[i].CarId,
			text: cargosComision[i].CarDes
		}));
	}
}


function ponerListaCargosDocente(nombre){
	borrarOpciones(nombre);
	for(i in cargosDocentes){
		$(nombre).append($('<option>', {
			value: cargosDocentes[i].CarId,
			text: cargosDocentes[i].CarDes
		}));
	}
}

var idAdministrativo = -1;
function editar(idParticipante, primerCargo, segundoCargo){
	ponerCargos();
	habilitarSelects();
	if(primerCargo=="comision"){//Primer cargo es de comisión
		$("#mensaje1").text("Este cargo es de comisión");
		ponerListaCargosComision("#cargo1");
		$("#cargo1").attr('disabled', 'disabled');
		$("#area").attr('disabled', 'disabled');
		$("#pabellon1").attr('disabled', 'disabled');
		$("#lugar1").attr('disabled', 'disabled');
		$("#puerta1").attr('disabled', 'disabled');
	}else if(primerCargo=="docente"){
		$("#mensaje1").text("Este cargo es de docente");
		ponerListaCargosDocente("#cargo1");
		$("#cargo1").attr('disabled', 'disabled');
		$("#area").attr('disabled', 'disabled');
		$("#pabellon1").attr('disabled', 'disabled');
		$("#lugar1").attr('disabled', 'disabled');
		$("#puerta1").attr('disabled', 'disabled');
	}else{
		$("#mensaje1").text("Seleccione el cargo");
	}
	if(segundoCargo=="comision"){//Primer cargo es de comisión
		$("#mensaje2").text("Este cargo es de comisión");
		ponerListaCargosComision("#cargo2");
		$("#cargo2").attr('disabled', 'disabled');
		$("#area2").attr('disabled', 'disabled');
		$("#pabellon2").attr('disabled', 'disabled');
		$("#lugar2").attr('disabled', 'disabled');
		$("#puerta2").attr('disabled', 'disabled');
	}else if(segundoCargo=="docente"){//Primer cargo es de comisión
		$("#mensaje2").text("Este cargo es de docente");
		ponerListaCargosDocente("#cargo2");
		$("#cargo2").attr('disabled', 'disabled');
		$("#area2").attr('disabled', 'disabled');
		$("#pabellon2").attr('disabled', 'disabled');
		$("#lugar2").attr('disabled', 'disabled');
		$("#puerta2").attr('disabled', 'disabled');
	}else{
		$("#mensaje2").text("Seleccione el cargo");
	}
	idAdministrativo = idParticipante;
	ponerDatos(idParticipante);
}

function habilitarSelects(){
	$("#cargo1").attr('disabled', false);
	$("#area").attr('disabled', false);
	$("#pabellon1").attr('disabled', false);
	$("#lugar1").attr('disabled', false);
	$("#puerta1").attr('disabled', false);
	$("#cargo2").attr('disabled', false);
	$("#area2").attr('disabled', false);
	$("#pabellon2").attr('disabled', false);
	$("#lugar2").attr('disabled', false);
	$("#puerta2").attr('disabled', false);
}

function ponerDatos(idParticipante){
	for (i in administrativosSorteados){
		var usuario = administrativosSorteados[i];
		if(usuario.AdmDetId == idParticipante){
			$("#area").val(usuario.AdmDetFKAre);
			seleccionarArea();
			$("#pabellon1").val(usuario.AdmDetFKPab);
			$("#pabellon2").val(usuario.AdmDetFKPab2);
			$("#cargo1").val(usuario.AdmDetFKCar);
			$("#cargo2").val(usuario.AdmDetFKCar2);
			$("#puerta1").val(usuario.AdmDetFKArePue);
			$("#puerta2").val(usuario.AdmDetFKArePue2);
			$("#lugar1").val(usuario.AdmDetLug1);
			$("#lugar2").val(usuario.AdmDetLug2);
		}
	}
}

function asignar(idParticipante){
	habilitarSelects();
	var url = "../../controlador/asignacion/asignacionControlador.php?accion=asignarLugaresYCargos";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize() + "&id=" + idParticipante, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
}

function resetForm(){
	$('#area :nth-child(1)').prop('selected', true);
	$('#pabellon1 :nth-child(1)').prop('selected', true); 
	$('#pabellon2 :nth-child(1)').prop('selected', true); 
	$('#cargo1 :nth-child(1)').prop('selected', true); 
	$('#cargo2 :nth-child(1)').prop('selected', true);
	$('#puerta1 :nth-child(1)').prop('selected', true); 
	$('#puerta2 :nth-child(1)').prop('selected', true);
	$("#lugar1").val(null);
	$("#lugar2").val(null);
}

var cargo = false;
function loadUsuario(){
	if(!cargo && cargoAreasParticipantes && cargoPabellonesParticipantes && cargoAreasPuertas && cargoCargosAdministrativos && cargoCargosComision){
		cargo = true;
		cargar(1);	
	}
}

function mostrarCampos(opcion){
	if(opcion=="false"){
		$("#botonResumen").text("Mostrar pocos campos");
		var column = "#usuarios .resumen";
		$(column).show();
	}else{
		$("#botonResumen").text("Mostrar todos los campos");
		var column = "#usuarios .resumen";
		$(column).hide();
	}
}

$( document ).ready(function() {
	//Para mostrar el formulario completo
	if($.cookie("resumen")==null)$.cookie("resumen", true);
	
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(validarFormulario()==true){
			asignar(idAdministrativo);
			$(this).prev().click();
		}
	});
	$("#botonResumen").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		var resumen = $.cookie("resumen");
		if(resumen=="true")$.cookie("resumen", false);
		if(resumen=="false")$.cookie("resumen", true);
		
		mostrarCampos($.cookie("resumen"));
		$(this).prev().click();
	});
});


function validarFormulario(){
	if(($("#area").val()==null || $("#area").val() < 1) && 
		($("#pabellon1").val()==null || $("#pabellon1").val() < 1) && 
		($("#pabellon2").val()==null || $("#pabellon2").val() < 1) && 
		($("#cargo1").val()==null || $("#cargo1").val() < 1) && 
		($("#cargo2").val()==null || $("#cargo2").val() < 1) && 
		($("#puerta1").val()==null || $("#puerta1").val() < 1) && 
		($("#puerta2").val()==null || $("#puerta2").val() < 1) && 
		($("#lugar1").val()==null || $("#lugar1").val().length == 0) && 
		($("#lugar2").val()==null || $("#lugar2").val().length == 0)){
		
		mostrarError("NO TIENE NINGUNA ASIGNACIÓN PARA GUARDAR");
		$('#area').focus();
		return false;

	}else{
		var cantidad1 = 0;
		var primero = ["", "", ""];
		var segundo = ["", "", ""];
		var cantidad2 = 0;
		if($("#pabellon1").val()!=null && $("#pabellon1").val() >= 1){
			cantidad1 += 1;
			primero[0] = "#pabellon1";
		}
		if($("#puerta1").val()!=null && $("#puerta1").val() >= 1){
			cantidad1 += 1;
			primero[1] = "#puerta1";
		}
		if($("#lugar1").val()!=null && $("#lugar1").val().length >= 1){
			cantidad1 += 1;
			primero[2] = "#lugar1";
		}
		if($("#pabellon2").val()!=null && $("#pabellon2").val() >= 1){
			cantidad2 += 1;
			segundo[0] = "#pabellon2";
		}
		if($("#puerta2").val()!=null && $("#puerta2").val() >= 1){
			cantidad2 += 1;
			segundo[1] = "#puerta2";
		}
		if($("#lugar2").val()!=null && $("#lugar2").val().length >= 1){
			cantidad2 += 1;
			segundo[2] = "#lugar2";
		}
		if(cantidad1>1){
			mostrarError("SOLO PUEDE LLENAR UN CAMPO DE (PABELLON, PUERTA, INFORMACIÓN ADICIONAL) EN CADA COLUMNA");
			for(i in primero){
				$(primero[i]).css({'background-color' : '#FF988A'});
			}
			var timeoutError = setTimeout(function(){
				for(i in primero){
					$(primero[i]).css({'background-color' : ''});
				}
			}, 2000);
			return false;
		}
		if(cantidad2>1){
			mostrarError("SOLO PUEDE LLENAR UN CAMPO DE (PABELLON, PUERTA, INFORMACIÓN ADICIONAL) EN CADA COLUMNA");
			for(i in segundo){
				$(segundo[i]).css({'background-color' : '#FF988A'});
			}
			var timeoutError = setTimeout(function(){
				for(i in segundo){
					$(segundo[i]).css({'background-color' : ''});
				}
			}, 2000);
			return false;
		}
		return true;
	}
	
}