$(function(){
    $("#formulario").submit(function(event){
        var url = $(this).attr("action");
        //var url = "progressControlador.php"; // El script a dónde se realizará la petición.
        //alert($("#formulario").serialize());
        if(validateForm()){
            $.ajax({
                type: "POST",
                url: url,
                data: $("#formulario").serialize(), // Adjuntar los campos del formulario eniado.
                
                beforeSend: function(objeto){
                    $("#send").attr('disabled',true);
                    $("#loader").show();
                    
                },
                success: function(data)
                {
                    $("#loader").hide();
                    $('#send').removeAttr('disabled');
                    var resultado = JSON.parse(data);
                    alert(resultado.message);
                    if(resultado.result == 1){
                        document.getElementById('formulario').reset();
                    }
                    //$("#respuesta").html(data);
                    // Mostrar la respuestas del script PHP.
                }
            });
        }

        return false; // Evitar ejecutar el submit del formulario.
    });
});

    function allTipos(){
        var array = document.forms["formulario"]["optionsRadios"];
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if(element.checked) return "checked";
        }
        return "";
    }


    function validateForm() {
        // var tipo = document.forms["formulario"]["optionsRadios"].value;
        var tipo = allTipos();
        var asunto = document.forms["formulario"]["asunto"].value;
        var mensaje = document.forms["formulario"]["mensaje"].value;
        var correo = document.forms["formulario"]["correo"].value;
        var contrasena = document.forms["formulario"]["contrasena"].value;

        if (tipo == "" || asunto=="" || mensaje=="" || correo=="" || contrasena=="") {
            alert("Completar todos los campos");
            return false;
        }
        return true;
    } 
    
    function init(){
        $("#loader").hide();
    }

    function selectAll(){
        $(".checkSendEmail").prop("checked", true);
    }

    function unSelectAll(){
        $(".checkSendEmail").prop("checked", false);
    }