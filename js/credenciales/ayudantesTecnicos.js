
var tecnicos;
var generos;
var categorias;

var cargoCategorias = false;
var cargoGeneros = false;

console.log( "document loaded" );

	$( document ).ready(function() {
        console.log( "document loaded" );
        cargar(1);
        //capacidadAsignada();
    });

var ultimaPaginaCargada;
function cargar(pagina){
	console.log( "document loaded" );
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=10;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	$("#loader").fadeIn('slow');
	$.ajax({
		url:'../../controlador/credenciales/admisionDetalle.php?accion=mostrarTecnicos',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").html("Cargando...");
		},
		success:function(result){
			var resultado = JSON.parse(result);
			if(resultado.error == false){
				tecnicos = resultado.mensaje;
				$("#paginacion").html(resultado.datos);
				$("#loader").html("");
				ponerTecnicos();	
			}else{
				alert(result);
			}
		}
	})
}

function ponerTecnicos(){
 	$("#usuarios").find("tr:gt(0)").remove();

  for (i in tecnicos){
  	usuario = tecnicos[i];
  	var genero = "";
  	var categoria = "";
    
    var row = document.getElementById("usuarios").insertRow(-1);
    var cell1 = row.insertCell(0);//DNI
    var cell2 = row.insertCell(1);//Nombre
    var cell3 = row.insertCell(2);//button
 
    //cell1.innerHTML = usuario.UsuId;
    cell1.innerHTML = usuario.UsuDni;
    cell2.innerHTML = usuario.UsuApe+", "+usuario.UsuNom;
    cell3.innerHTML = "<a href=\"../../controlador/credenciales/ayudantes.php?id="+usuario.AdmDetId+"\" target=\"_blank\" >Generar</a>"
    
	cell1.setAttribute("style","text-align: center;");
	cell3.setAttribute("style","text-align: center;");

  }
  
}
