var usuarios;
var detalles;


function cargarUsuarios(pagina){
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=10;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/bitacora/bitacoraControlador.php?accion=mostrarBitacoraUsuario',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					usuarios = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerUsuario();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function cargarDetalles(pagina){
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=10;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/bitacora/bitacoraControlador.php?accion=mostrarBitacoraAdmisionDetalle',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					detalles = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDetalle();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}



function ponerUsuario(){
	$("#usuarios").find("tr:gt(0)").remove();

	for (i in usuarios){
		usuario = usuarios[i];
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//EDITAR
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Correo
		var cell5 = row.insertCell(4);//Telefono
		var cell6 = row.insertCell(5);//Direccion
		var cell7 = row.insertCell(6);//Usuario
		var cell8 = row.insertCell(7);//Contraseña
		var cell9 = row.insertCell(8);//EDITAR
		var cell10 = row.insertCell(9);//Apellidos
		var cell11 = row.insertCell(10);//Nombre
		var cell12 = row.insertCell(11);//Correo
		var cell13 = row.insertCell(12);//Telefono
		var cell14 = row.insertCell(13);//Direccion
		var cell15 = row.insertCell(14);//Usuario
		var cell16 = row.insertCell(15);//Contraseña
		var cell17 = row.insertCell(16);//EDITAR
		var cell18 = row.insertCell(17);//Apellidos
		var cell19 = row.insertCell(18);//Nombre
		var cell20 = row.insertCell(19);//Correo
		var cell21 = row.insertCell(20);//Telefono
		var cell22 = row.insertCell(21);//Direccion
		var cell23 = row.insertCell(22);//Usuario
		var cell24 = row.insertCell(23);//Contraseña
		var cell25 = row.insertCell(24);//EDITAR
		var cell26 = row.insertCell(25);//Apellidos
		var cell27 = row.insertCell(26);//Nombre
		var cell28 = row.insertCell(27);//Correo
		var cell29 = row.insertCell(28);//Telefono
		var cell30 = row.insertCell(29);//Direccion
		var cell31 = row.insertCell(30);//Usuario
		var cell32 = row.insertCell(31);//Contraseña
		var cell33 = row.insertCell(32);//Correo
		var cell34 = row.insertCell(33);//Telefono
		var cell35 = row.insertCell(34);//Direccion
		var cell36 = row.insertCell(35);//Usuario
		var cell37 = row.insertCell(36);//Contraseña

		cell1.innerHTML = usuario.BitUsuId;
		cell2.innerHTML = usuario.operacion;
		cell3.innerHTML = usuario.ResCod;
		cell4.innerHTML = usuario.ResNom;
		cell5.innerHTML = usuario.fecha;
		cell6.innerHTML = usuario.host;
		cell7.innerHTML = usuario.UsuId;
		cell8.innerHTML = usuario.UsuDniAnt;
		cell9.innerHTML = usuario.UsuDniNue;
		cell10.innerHTML = usuario.UsuCuiAnt;
		cell11.innerHTML = usuario.UsuCuiNue;
		cell12.innerHTML = usuario.UsuNomAnt;
		cell13.innerHTML = usuario.UsuNomNue;
		cell14.innerHTML = usuario.UsuApeAnt;
		cell15.innerHTML = usuario.UsuApeNue;
		cell16.innerHTML = usuario.UsuCorEleAnt;
		cell17.innerHTML = usuario.UsuCorEleNue;
		cell18.innerHTML = usuario.UsuTelAnt;
		cell19.innerHTML = usuario.UsuTelNue;
		cell20.innerHTML = usuario.UsuDirAnt;
		cell21.innerHTML = usuario.UsuDirNue;
		cell22.innerHTML = usuario.UsuNumProAnt;
		cell23.innerHTML = usuario.UsuNumProNue;
		cell24.innerHTML = usuario.GenAnt;
		cell25.innerHTML = usuario.GenNue;
		cell26.innerHTML = usuario.Tip1Ant;
		cell27.innerHTML = usuario.Tip1Nue;
		cell28.innerHTML = usuario.Tip2Ant;
		cell29.innerHTML = usuario.Tip2Nue;
		cell30.innerHTML = usuario.CatAnt;
		cell31.innerHTML = usuario.CatNue;
		cell32.innerHTML = usuario.UsuNomUsuAnt;
		cell33.innerHTML = usuario.UsuNomUsuNue;
		cell34.innerHTML = usuario.UsuConUsuAnt;
		cell35.innerHTML = usuario.UsuConUsuNue;
		cell36.innerHTML = usuario.EstRegAnt;
		cell37.innerHTML = usuario.EstRegNue;
		//cell1.setAttribute("style","text-align: center;");
	}
	
}

function ponerDetalle(){
	$("#usuarios").find("tr:gt(0)").remove();

	for (i in detalles){
		usuario = detalles[i];
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//EDITAR
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Correo
		var cell5 = row.insertCell(4);//Telefono
		var cell6 = row.insertCell(5);//Direccion
		var cell7 = row.insertCell(6);//Usuario
		var cell8 = row.insertCell(7);//Contraseña
		var cell9 = row.insertCell(8);//EDITAR
		var cell10 = row.insertCell(9);//Apellidos
		var cell11 = row.insertCell(10);//Nombre
		var cell12 = row.insertCell(11);//Correo
		var cell13 = row.insertCell(12);//Telefono
		var cell14 = row.insertCell(13);//Direccion
		var cell15 = row.insertCell(14);//Usuario
		var cell16 = row.insertCell(15);//Contraseña
		var cell17 = row.insertCell(16);//EDITAR
		var cell18 = row.insertCell(17);//Apellidos
		var cell19 = row.insertCell(18);//Nombre
		var cell20 = row.insertCell(19);//Correo
		var cell21 = row.insertCell(20);//Telefono
		var cell22 = row.insertCell(21);//Direccion
		var cell23 = row.insertCell(22);//Usuario
		var cell24 = row.insertCell(23);//Contraseña
		var cell25 = row.insertCell(24);//EDITAR
		var cell26 = row.insertCell(25);//Apellidos
		var cell27 = row.insertCell(26);//Nombre
		var cell28 = row.insertCell(27);//Correo
		var cell29 = row.insertCell(28);//Telefono
		var cell30 = row.insertCell(29);//Direccion
		var cell31 = row.insertCell(30);//Usuario
		var cell32 = row.insertCell(31);//Contraseña
		var cell33 = row.insertCell(32);//Correo
		var cell34 = row.insertCell(33);//Telefono
		var cell35 = row.insertCell(34);//Direccion
		var cell36 = row.insertCell(35);//Usuario
		var cell37 = row.insertCell(36);//Contraseña
		var cell38 = row.insertCell(37);//Contraseña
		var cell39 = row.insertCell(38);//Contraseña
		var cell40 = row.insertCell(39);//Contraseña
		var cell41 = row.insertCell(40);//Contraseña

		cell1.innerHTML = usuario.BitAdmDetId;
		cell2.innerHTML = usuario.operacion;
		cell3.innerHTML = usuario.ResCod;
		cell4.innerHTML = usuario.ResNom;
		cell5.innerHTML = usuario.fecha;
		cell6.innerHTML = usuario.host;
		cell7.innerHTML = usuario.AdmDetId;
		cell8.innerHTML = usuario.AdmDetFKAdmCabIdAnt;
		cell9.innerHTML = usuario.AdmDetFKAdmCabIdNue;
		cell10.innerHTML = usuario.UsuDniAnt;
		cell11.innerHTML = usuario.UsuDniNue;
		cell12.innerHTML = usuario.CarAnt;
		cell13.innerHTML = usuario.CarNue;
		cell14.innerHTML = usuario.Car2Ant;
		cell15.innerHTML = usuario.Car2Nue;
		cell16.innerHTML = usuario.AreAnt;
		cell17.innerHTML = usuario.AreNue;
		cell18.innerHTML = usuario.Are2Ant;
		cell19.innerHTML = usuario.Are2Nue;
		cell20.innerHTML = usuario.Pab1Ant;
		cell21.innerHTML = usuario.Pab1Nue;
		cell22.innerHTML = usuario.Pab2Ant;
		cell23.innerHTML = usuario.Pab2Nue;
		cell24.innerHTML = usuario.AdmDetFKAulAnt;
		cell25.innerHTML = usuario.AdmDetFKAulNue;
		cell26.innerHTML = usuario.ArePueAnt;
		cell27.innerHTML = usuario.ArePueNue;
		cell28.innerHTML = usuario.ArePue2Ant;
		cell29.innerHTML = usuario.ArePue2Nue;
		cell30.innerHTML = usuario.AdmDetLug1Ant;
		cell31.innerHTML = usuario.AdmDetLug1Nue;
		cell32.innerHTML = usuario.AdmDetLug2Ant;
		cell33.innerHTML = usuario.AdmDetLug2Nue;
		cell34.innerHTML = usuario.AdmDetDetAsiAnt;
		cell35.innerHTML = usuario.AdmDetDetAsiNue;
		cell36.innerHTML = usuario.AdmDetCorEnvAnt;
		cell37.innerHTML = usuario.AdmDetCorEnvNue;
		cell38.innerHTML = usuario.DepTecAnt;
		cell39.innerHTML = usuario.DepTecNue;
		cell40.innerHTML = usuario.EstRegAnt;
		cell41.innerHTML = usuario.EstRegNue;
		//cell1.setAttribute("style","text-align: center;");
	}
	
}