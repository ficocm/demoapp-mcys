var administradores;

var seEdita = false;
var seAgrega = false;

var ultimaPagina = 1;
function cargar(pagina){
	ultimaPagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=8;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/usuario/usuarioControlador.php?accion=mostrarAdministradores',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					administradores = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerAdministradores();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}


function ponerAdministradores(){
	$("#usuarios").find("tr:gt(0)").remove();

	for (i in administradores){
		usuario = administradores[i];
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//EDITAR
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Correo
		var cell5 = row.insertCell(4);//Telefono
		var cell6 = row.insertCell(5);//Direccion
		var cell7 = row.insertCell(6);//Usuario
		var cell8 = row.insertCell(7);//Contraseña

		cell1.innerHTML = "<input type='image' onclick='editable("+usuario.UsuId+");' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = usuario.UsuCorEle;
		cell5.innerHTML = usuario.UsuTel;
		cell6.innerHTML = usuario.UsuDir;
		cell7.innerHTML = usuario.UsuNomUsu;
		cell8.innerHTML = usuario.UsuConUsu;
		cell1.setAttribute("style","text-align: center;");
	}
	
}

function agregable(){
	$("#save").text("AGREGAR");
	$(".modal-title").text("NUEVO ADMINISTRADOR");
	seAgrega = true;
	seEdita = false;
	idUsuario = null;
	resetForm();
}

var idUsuario;
function editable(id){
	$("#save").text("GUARDAR");
	$(".modal-title").text("EDITAR ADMINISTRADOR");
	seEdita = true;
	seAgrega = false;
	idUsuario = id;

	resetForm();
	for (i in administradores){
		usuario = administradores[i];
		if(usuario.UsuId == id){
			$("#nombres").val(usuario.UsuNom);
			$("#apellidos").val(usuario.UsuApe);
			$("#correo").val(usuario.UsuCorEle);
			$("#telefono").val(usuario.UsuTel);
			$("#direccion").val(usuario.UsuDir);
			$("#usunom").val(usuario.UsuNomUsu);
			$("#usucon").val(usuario.UsuConUsu);
			break;
		}
	}
}

function guardar(){
	ponerMayusculasYMinusculas();

	var url = "../../controlador/usuario/usuarioControlador.php?accion=modificarAdministrador";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize()+"&id="+idUsuario, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		} 
	});
	
}

function agregar(){
	ponerMayusculasYMinusculas();

	var url = "../../controlador/usuario/usuarioControlador.php?accion=crearAdministrador";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Conectando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
	
}

function resetForm(){
	$("#nombres").val('');
	$("#apellidos").val('');
	$("#telefono").val('');
	$("#correo").val('');
	$("#direccion").val('');
	$('#usunom').val('');
	$("#usucon").val('');
}

function formularioValido(){
	var nombres = $("#nombres").val();
	var apellidos = $("#apellidos").val();
	var usunom = $("#usunom").val();
	var usucon = $("#usucon").val();

	if(nombres == null || nombres.length == '0'){
		mostrarError("Ingrese Nombres valido");
		$("#nombres").focus();
		return false;
	}

	if(apellidos == null || apellidos.length == '0'){
		mostrarError("Ingrese APELLIDOS valido");
		$("#apellidos").focus();
		return false;
	}

	if(usunom == null || usunom.length < 4){
		mostrarError("Ingrese nombre de usuario valido (mayor o igual a 4 letras)");
		$("#usunom").focus();
		return false;
	}

	if(usucon == null || usucon.length < 8){
		mostrarError("Ingrese contraseña de usuario valido (mayor o igual a 8 letras)");
		$("#usucon").focus();
		return false;
	}
	return true;
}

$( document ).ready(function() {
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(formularioValido()==true){
			if(seEdita==true){
				guardar();//Tambien verifica correo electronico
				//guardar();
			}else if(seAgrega==true){
				agregar();
			}
			$(this).prev().click();
		}
	});
});

cargar(1);