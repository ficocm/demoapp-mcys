
	function enviarFormulario(){
		$('input[type=text]').val (function () {
			return this.value.toUpperCase();
		});
		$('input[type=email]').val (function () {
			return this.value.toLowerCase();
		});

		var url = "controlador/usuario/usuarioControlador.php?accion=iniciarSesion";

		$.ajax({
			type: "POST",
			url: url,
			processData: false,
			data: $(".login-form").serialize(), // serializes the form's elements.
			beforeSend: function(objeto){
				$("#loader").show();
				$("#loader").html("Conectando...");
			},
			success: function(result){
				$("#loader").hide();
				try{
					var res = JSON.parse(result);
					if(res.error == false){
						location.href = 'vista/inicio/inicio.php';	
					}else{
						mostrarError(res.mensaje);
					}
				}catch(err){
					mostrarError("Error Casteo data:"+result);
				}
			},
			error: function(result) {
				$("#loader").hide();
				mostrarError("Error Server result data:"+result);
			}
		});
	}
	$("#send").click(enviarFormulario);

	$(".confirm").hide();

	$('.message a').click(function(){
		$('form').animate({height: "toggle", opacity: "toggle"}, "slow");
	});

$( document ).ready(function() {
	$("#user").keypress(function(e) {
		if(e.which == 13) {
			if($("#pass").val() == null || $("#pass").val().length==0){
				$("#pass").focus();
			}else{
				enviarFormulario();
			}
		}
	});

	$("#pass").keypress(function(e) {
		if(e.which == 13) {
			enviarFormulario();
		}
	});
});

