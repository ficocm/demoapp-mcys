var docentesInscritosSorteados;
var generos;
var categorias;
var cargosDocente;
var selectCargos;
var iconOK = "<span id='ok' class='glyphicon glyphicon-ok'></span>";
var iconUnCheck = "<span id='un' class='glyphicon glyphicon-unchecked'></span>";
var iconNo = "<span class='glyphicon glyphicon-ban-circle'></span>";

var cargoCategorias = false;
var cargoGeneros = false;
var cargoCargos = false;

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
			try {
 				categorias = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCategorias = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarGeneros", success: function(result){
			try {
 				generos = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoGeneros = true;
			}
		}})
		).done(function(ajaxAreas){
			afterLoad();
	});

	$.when(
		$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCargosDocente", success: function(result){
			try {
 				cargosDocente = JSON.parse(result);
				selectCargos = $('<select>').appendTo('body');
				for(j in cargosDocente){
					if(cargosDocente[j].CarId != codSeguridad){
						selectCargos.append($("<option>").attr('value',cargosDocente[j].CarId).text(cargosDocente[j].CarDes));
					}
				}
				selectCargos.append($("<option>").attr('value',-2).text("PARA COMISIÓN"));
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargos = true;
			}
			
		}})
		).done(function(ajaxAreas){
			afterLoad();
		});

var ultimaPaginaCargada= 1;
function cargar(pagina){
	ultimaPaginaCargada = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=7;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=mostrarDocentesInscritosSorteados',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					docentesInscritosSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDocentesInscritosSorteados();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function ponerDocentesInscritosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in docentesInscritosSorteados){
		usuario = docentesInscritosSorteados[i];
		var genero = "";
		var categoria = "";
		for(j in generos){
			if(generos[j].GenId==usuario.UsuFKGen){
				genero = generos[j].GenSubCat;
				break;
			}
		}
		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
				break;
			}
		}
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//DNI
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombres
		var cell4 = row.insertCell(3);//Categoria
		var cell5 = row.insertCell(4);//Procesos
		var cell6 = row.insertCell(5);//Tipo cell5.style.display="none";//Experiencia
		var cell7 = row.insertCell(6);
		var cell8 = row.insertCell(7);
		var cell9 = row.insertCell(8);
		cell1.innerHTML = usuario.UsuDni;
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = categoria;
		cell5.innerHTML = usuario.UsuNumPro;
		cell6.innerHTML = "<select id='cargo"+i+"' class='form-control' required>"+selectCargos.html()+"</select>";
		//alert(usuario.inscrito+ ", "+usuario.sorteado +", "+usuario.noInscrito);

		if(usuario.idDetalle==-1){
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='crearParticipacion("+usuario.UsuId+","+codInscrito+", "+i+")'>"+iconUnCheck+"</button>";
			cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='crearParticipacion("+usuario.UsuId+","+codSorteado+", "+i+")'>"+iconUnCheck+"</button>";
			cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
		}else{
			if(usuario.estadoRegistro == codInscrito){//INSCRITO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='confirmarAccion("+usuario.idDetalle+","+codInscritoCancelado+", "+i+")'>"+iconOK+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarParticipacionDocente("+usuario.idDetalle+","+codSorteado+", "+i+")'>"+iconUnCheck+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
			}else if(usuario.estadoRegistro==codSorteado){//SORTEADO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' onclick='modificarParticipacionDocente("+usuario.idDetalle+","+codInscrito+", "+i+")'>"+iconUnCheck+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' onclick='confirmarAccion("+usuario.idDetalle+","+codSorteadoCancelado+", "+i+")'>"+iconOK+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-default' disabled>NO</button>";
			}else if(usuario.estadoRegistro==codAsignado){//ASIGNADO
			cell7.innerHTML = "<button type='button' class='btn btn-secondary' disabled>"+iconNo+"</button>";
				cell8.innerHTML = "<button type='button' class='btn btn-secondary' disabled>"+iconNo+"</button>";
				cell9.innerHTML = "<button type='button' class='btn btn-danger' data-toggle='modal' data-target='.informativoAsignacion'>SI</button>";
			}
			$("#cargo"+i).prop('disabled', 'disabled');
		}

		cell5.setAttribute("style","text-align: center;");
		cell7.setAttribute("style","text-align: center;");
		cell8.setAttribute("style","text-align: center;");
		cell9.setAttribute("style","text-align: center;");
		
		var esCargoDocente = false;
		for(j in cargosDocente){
			if(usuario.cargo==cargosDocente[j].CarId){
				$("#cargo"+i+"").val(usuario.cargo);
				esCargoDocente = true;
				break;
			}
		}
		if(esCargoDocente==false && usuario.cargo == null && usuario.cargo != -1){
			$("#cargo"+i+"").val(-2);
		}else if(usuario.cargo==-1){
			$("#cargo"+i+"").val(null);
		}
		

	}
	
}

function eliminarDelProceso(id){
	var parametros = {"id":id};

	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=eliminarDelProceso',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Eliminando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function crearParticipacion(id, participacion, posicion){
	var cargo = $("#cargo"+posicion).val();

	if(cargo == null || cargo.length==0){
		mostrarError("Seleccione un cargo");
		$("#cargo"+posicion).focus();
		return;
	}else if(cargo == -2){
		cargo = -1;
	}

	var url = "../../controlador/participantes/admisionDetalleControlador.php?accion=crearParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");
	$form.append("<input type='text' name='cargo' value='"+cargo+"'>");

	$.ajax({
		type: "POST",
		url: url,
		data: $form.serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Guardando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

function modificarParticipacionDocente(id, participacion, posicion){

	var cargo = $("#cargo"+posicion).val();
	
	if(cargo == null || cargo.length==0){
		mostrarError("Seleccione un cargo");
		$("#cargo"+posicion).focus();
		return;
	}else if(cargo == -2){
		cargo = -1;
	}

	var url= "../../controlador/participantes/admisionDetalleControlador.php?accion=modificarParticipacion";

	$form = $("<form></form>");
	$form.append("<input type='text' name='usuarioId' value='"+id+"'>");
	$form.append("<input type='text' name='participacion' value='"+participacion+"'>");
	$form.append("<input type='text' name='cargo' value='"+cargo+"'>");

	$.ajax({
		type: "POST",
		url: url,
		data: $form.serialize(), // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Modificando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPaginaCargada);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});	
}

function afterLoad(){
	if(cargoCategorias == true && cargoGeneros == true && cargoCargos == true){
		cargar(1);	
	}
}

function confirmarAccion(id, participacion, posicion){
	usuario =	docentesInscritosSorteados[posicion];
	if(participacion==codInscritoCancelado)
		$("#mensajeConfirmacion").html("DESEAR CANCELAR LA INSCRIPCIÓN AL PROCESO DE ADMISIÓN DE:<br> <b>	[" +usuario.UsuApe+" "+usuario.UsuNom+"]</b>");
	else if(participacion==codSorteadoCancelado)
		$("#mensajeConfirmacion").html("DESEAR CANCELAR EL SORTEO PARA EL PROCESO DE ADMISIÓN DE:<br> <b>[" +usuario.UsuApe+" "+usuario.UsuNom+"]</b>");
	var clicks = 0;
	$('.confirmacion').modal('show');
	$("#cancelar, #confirmar").on("click", function(e){
		clicks += 1;
		if(clicks>1)return;
		e.preventDefault();
		var opcion = $(this).attr('id');
		if(opcion=='cancelar'){
			$(this).prev().click();
		}else if(opcion=='confirmar'){
			modificarParticipacionDocente(id, participacion, posicion);
			$(this).prev().click();
		}
	});
}
