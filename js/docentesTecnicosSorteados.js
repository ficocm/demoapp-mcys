var docentesTecnicosSorteados;
var ayudantesTecnicos;
var estudiantes;

var categorias;
$.when(
	$.ajax({url: "../../controlador/usuario/usuarioControlador.php?accion=mostrarCategorias", success: function(result){
		try {
			categorias = JSON.parse(result);
		}catch(err) {
			//Mensaje de error
		}finally {
			cargarEstudiantes(1);
		}
	}})
	).done(function(ajaxAreas){
		//afterLoad();
});

var ultimapagina;
function cargar(pagina){
	ultimapagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=7;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=mostrarDocentesTecnicos',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando Técnicos...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					docentesTecnicosSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDocentesTecnicosSorteados();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

var ultimapaginaestudiates = 1;
function cargarEstudiantes(pagina){
	ultimapaginaestudiates = pagina;
	var busqueda=$("#busquedaEstudiantes").val();
	var registrosPorPagina=4;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=mostrarEstudiantes',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando Estudiantes...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					estudiantes = res.mensaje;
					$("#paginacionEstudiantes").html(res.datos);
					ponerEstudiantes();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function ponerDocentesTecnicosSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in docentesTecnicosSorteados){
		usuario = docentesTecnicosSorteados[i];
		
		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//DNI
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Cantidad ayudantes
		var cell5 = row.insertCell(4);//Ver Agregar
		cell1.innerHTML = usuario.UsuDni;
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = usuario.ayudantes;
		cell5.innerHTML = "<a onclick='mostrarAyudantes("+usuario.AdmDetId+");' style='cursor: pointer;' data-toggle='modal' data-target='#myModal'>Agregar o Ver Ayudates</a>"
		
		//cell7.innerHTML = "<input type='checkbox' class='radio' onclick='modificarParticipacionDocente(true, 12, this)' name='participacion["+i+"][]'>";
		cell1.setAttribute("style","text-align: center;");
		cell2.setAttribute("style","text-align: left;");
		cell3.setAttribute("style","text-align: left;");
		cell4.setAttribute("style","text-align: center;");
		cell5.setAttribute("style","text-align: center;");
	}
	
}

var docenteTecnicoId;
function mostrarAyudantes(id){
	docenteTecnicoId = id;
	$("#ayudantesAsignados").find("tr:gt(0)").remove();
	var parametros = {"docenteTecnicoId":id};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=mostrarAyudantesTecnicos',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					ayudantesTecnicos = res.mensaje;
					ponerAyudantesTecnicos();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function ponerAyudantesTecnicos(){

	for (i in ayudantesTecnicos){
		usuario = ayudantesTecnicos[i];
		var categoria = "";

		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
			}
		}
		
		var row = document.getElementById("ayudantesAsignados").insertRow(-1);
		var cell1 = row.insertCell(0);//CUI
		var cell2 = row.insertCell(1);//Apelldios
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Categoria
		var cell5 = row.insertCell(4);//Accion
		cell1.innerHTML = usuario.UsuCui;
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = categoria;
		cell5.innerHTML = "<a onclick='confirmarAccion("+usuario.AdmDetId+", "+docenteTecnicoId+", "+i+");' style='cursor: pointer;' >Eliminar</a>"
		
		//cell7.innerHTML = "<input type='checkbox' class='radio' onclick='modificarParticipacionDocente(true, 12, this)' name='participacion["+i+"][]'>";
		cell1.setAttribute("style","text-align: center;");
		cell2.setAttribute("style","text-align: left;");
		cell3.setAttribute("style","text-align: left;");
		cell4.setAttribute("style","text-align: center;");
		cell5.setAttribute("style","text-align: center;");
	}
}

function ponerEstudiantes(){
	$("#listaEstudiantes").find("tr:gt(0)").remove();

	for (i in estudiantes){
		usuario = estudiantes[i];
		var categoria = "";

		for(j in categorias){
			if(categorias[j].GenId==usuario.UsuFKCat){
				categoria = categorias[j].GenSubCat;
			}
		}
		
		var row = document.getElementById("listaEstudiantes").insertRow(-1);
		var cell1 = row.insertCell(0);//CUI
		var cell2 = row.insertCell(1);//Apellidos
		var cell3 = row.insertCell(2);//Nombre
		var cell4 = row.insertCell(3);//Categoria
		var cell5 = row.insertCell(4);//Docente Asignado
		var cell6 = row.insertCell(5);//Accion
		cell1.innerHTML = usuario.UsuCui;
		cell2.innerHTML = usuario.UsuApe;
		cell3.innerHTML = usuario.UsuNom;
		cell4.innerHTML = categoria;
		cell5.innerHTML = usuario.docente;
		if(usuario.docente == null || usuario.docente.lenght <= 2)
			cell6.innerHTML = "<a onclick='agregarAyudante("+usuario.UsuId+");' style='cursor: pointer;' >Agregar</a>"
		else
			cell6.innerHTML = "";
		
		cell1.setAttribute("style","text-align: center;");
		cell2.setAttribute("style","text-align: left;");
		cell3.setAttribute("style","text-align: left;");
		cell4.setAttribute("style","text-align: center;");
		cell5.setAttribute("style","text-align: left;");
		cell6.setAttribute("style","text-align: center;");
	}
}

function eliminarAyudanteTecnico(ayudanteId){
	var parametros = {"estudianteId":ayudanteId};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=eliminarAyudanteTecnico',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Eliminando Ayudante...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarAyudantes(docenteTecnicoId);
					cargar(ultimapagina);
					cargarEstudiantes(ultimapaginaestudiates)
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}

function agregarAyudante(estudianteId){

	var parametros = {"docenteTecnicoId":docenteTecnicoId, "estudianteId":estudianteId};
	
	$.ajax({
		url:'../../controlador/participantes/admisionDetalleControlador.php?accion=crearAyudanteTecnico',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Agregando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarAyudantes(docenteTecnicoId);
					cargar(ultimapagina);
					cargarEstudiantes(ultimapaginaestudiates)
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	})
}


function confirmarAccion(idAyudante, idDocenteTecnico, posAyudante){
	ayudante =	ayudantesTecnicos[posAyudante];
	docente	=	docentesTecnicosSorteados[idDocenteTecnico];
	$("#mensajeConfirmacion").html("¿DESEAR ELIMINAR <br>EL AYUDANTE TÉCNICO: <b>"+ayudante.UsuApe+" "+ayudante.UsuNom+"</b><br>DEL PROCESO DE ADMISIÓN?");
	
	var clicks = 0;
	$('.confirmacion').modal('show');
	$("#cancelar, #confirmar").on("click", function(e){
		clicks += 1;
		if(clicks>1)return;
		e.preventDefault();
		var opcion = $(this).attr('id');
		if(opcion=='cancelar'){
			$(this).prev().click();
		}else if(opcion=='confirmar'){
			eliminarAyudanteTecnico(idAyudante);
			$(this).prev().click();
		}
	});
}

afterLoad();

function afterLoad(){
	cargar(1);
	
}
