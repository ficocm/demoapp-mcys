var procesosAdmision;

function cargar(pagina){
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=8;
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina};
	$("#section-4").fadeIn('slow');
	$.ajax({
		url:'../../controlador/inicio/procesosControlador.php?accion=mostrarProcesosDeAdmision',
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#section-4").html("Cargando...");
		},
		success:function(result){
			//console.log("params "+parametros.action + " - "+parametros.pagina+" - "+parametros.busqueda+" - "+registrosPorPagina);
			//console.log("resultado "+result);
			var resultado = JSON.parse(result);
			if(resultado.error == false){
				procesosAdmision = resultado.mensaje;
				//$("#paginacion").html(resultado.datos);
				$("#section-4").html("");
				ponerProcesos();	
			}else{
				alert(result);
			}
		}
	})
}

function ponerProcesos(){
	//$("#usuarios").find("tr:gt(0)").remove();
	for(var obj in procesosAdmision){
		if(procesosAdmision.hasOwnProperty(obj)){
			var propId = 'AdmCabId';
			var propFecha = 'AdmCabFec';
			var propAnio = 'AdmCabAno';
			var propDes = 'GenSubCat';
			var propFas = 'AdmCabNumFas';
			var propNumEva = 'AdmCabNumEva';
			var propEstReg = 'AdmCabFKEstReg';
			if(procesosAdmision[obj].hasOwnProperty(propId)){
				var idProceso = procesosAdmision[obj][propId];
				var nombreProceso = procesosAdmision[obj][propDes];
				var faseProceso = procesosAdmision[obj][propFas];
				var fechaProceso = procesosAdmision[obj][propFecha];
				var anioProceso = procesosAdmision[obj][propAnio];
				var numEvaProceso = procesosAdmision[obj][propNumEva];
				var estadoProceso = procesosAdmision[obj][propEstReg];
				var datosProceso = [idProceso, nombreProceso, faseProceso, fechaProceso, anioProceso, numEvaProceso, estadoProceso];
				var variablesProceso = ['idProceso', 'nombreProceso', 'faseProceso', 'fechaProceso', 'anioProceso', 'numEvaProceso', 'estadoProceso'];
				var datosCompletosProceso = [];
				datosCompletosProceso[0] = datosProceso;
				datosCompletosProceso[1] = variablesProceso;
				var button = document.createElement('button');
				button.id = 'seleccionar';
				button.className = 'seleccionar seleccionar-4 seleccionar-4c icon-arrow-right';
				button.value = datosCompletosProceso;
				//button.innerHTML = 'PROCESO '+ nombreProceso + ' '+ anioProceso +' Fase N° ' + faseProceso + ' ('+fechaProceso+') [' + estadoProceso +']';
				if(estadoProceso == 31){
					button.innerHTML = ''+ nombreProceso + ' '+ anioProceso +' Fase N° ' + faseProceso + ' - Examen N° '+ numEvaProceso +' ('+fechaProceso+') [PROCESO PASADO]';
				}else if(estadoProceso == 32){
					button.innerHTML = ''+ nombreProceso + ' '+ anioProceso +' Fase N° ' + faseProceso + ' - Examen N° '+ numEvaProceso +' ('+fechaProceso+') [PROCESO EN CURSO]';
				}else if(estadoProceso == 33){
					button.innerHTML = ''+ nombreProceso + ' '+ anioProceso +' Fase N° ' + faseProceso + ' - Examen N° '+ numEvaProceso +' ('+fechaProceso+') [PROCESO FUTURO]';
				}

				
				button.onclick = function(){
					//alert('here be dragons');return false;
					redirectPost("../../vista/inicio/inicio.php", this.value)
					
				};
				//alert(prop + ':' + procesosAdmision[obj][prop]);
				document.getElementById('section-4').appendChild(button);
			}
		}
	}
}

function redirectPost(url, data) {
    var form = document.createElement('form');
    document.body.appendChild(form);
    form.method = 'post';
    form.action = url;

    var arrayData = data.split(",");

    for (var i = 0; i < arrayData.length/2; i++) {
    	var input = document.createElement('input');
    	
    	input.type = 'hidden';
    	input.value = arrayData[i];
    	input.name = arrayData[i+7];
    	form.appendChild(input);
    }
    form.submit();
}

cargar(1);