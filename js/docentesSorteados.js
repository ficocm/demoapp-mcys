var docentesSorteados;
var areasParticipantes;
var pabellonesParticipantes;
var aulasParticipantes;
var cargosDocentes;

var cargoAreasParticipantes = false;
var cargoPabellonesParticipantes = false;
var cargoAulasParticipantes = false;
var cargoCargosDocentes = false;

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAreasParticipantes", success: function(result){
			try {
 				areasParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoAreasParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			ponerListaAreas();
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarPabellonesParticipantes", success: function(result){
			try {
 				pabellonesParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoPabellonesParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarAulasParticipantes", success: function(result){
			try {
 				aulasParticipantes = JSON.parse(result);
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoAulasParticipantes = true;
			}
		}})
		).done(function(ajaxAreas){
			loadUsuario();
	});

	$.when(
		$.ajax({url: "../../controlador/asignacion/asignacionControlador.php?accion=mostrarCargosDocentes", success: function(result){
			try {
 				cargosDocentes = JSON.parse(result);
 			 
			}catch(err) {
				//Mensaje de error
			}finally {
				cargoCargosDocentes = true;
			}
		}})
		).done(function(ajaxAreas){
			ponerListaCargos();
			loadUsuario();
	});

var ultimaPagina = 1;
function cargar(pagina){
	ultimaPagina = pagina;
	var busqueda=$("#busqueda").val();
	var registrosPorPagina=5;

	var cargo = $("#cargo1").val();
	var parametros = {"action":"ajax","pagina":pagina,'busqueda':busqueda,'registrosPorPagina':registrosPorPagina, 'tipo':cargo};

	var URL = '../../controlador/asignacion/asignacionControlador.php?accion=mostrarDocentesSorteadosConCargo';
	if(cargo==-3){
		URL = '../../controlador/asignacion/asignacionControlador.php?accion=mostrarDocentesSorteadosConCargo';
	}else{
		URL = '../../controlador/asignacion/asignacionControlador.php?accion=mostrarDocentesSorteados';
	}
	$.ajax({
		url:URL,
		data: parametros,
		type: "POST",
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Cargando Docentes...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					docentesSorteados = res.mensaje;
					$("#paginacion").html(res.datos);
					ponerDocentesSorteados();	
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}	
	})
}

function borrarOpciones(opcion){
	$(opcion).find('option').remove().end()
		.append('<option value="-1">Seleccione</option>');
}

function ponerListaAreas(){
	borrarOpciones("#area");
	borrarOpciones("#pabellon1");
	borrarOpciones("#pabellon2");
	for(i in areasParticipantes){
		$('#area').append($('<option>', {
			value: areasParticipantes[i].AdmAreId,
			text: areasParticipantes[i].AreDes
		}));
	}
}

function ponerListaCargos(){
	for(i in cargosDocentes){
		if(cargosDocentes[i].CarId != codSeguridad){
			$('#cargo1').append($('<option>', {
				value: cargosDocentes[i].CarId,
				text: cargosDocentes[i].CarDes
			}));
		}
	}
}

function seleccionarArea(){
	var areaSeleccionada = $("#area option:selected").val();
	borrarOpciones("#pabellon1");

	for(i in pabellonesParticipantes){
		if(pabellonesParticipantes[i].AdmPabFKAdmAreId == areaSeleccionada){
			$('#pabellon1').append($('<option>', {
				value: pabellonesParticipantes[i].AdmPabId,
				text: pabellonesParticipantes[i].PabDes
			}));
		}
	}
}

function seleccionarPabellon(){
	var pabellonSeleccionado = $("#pabellon1 option:selected").val();
	borrarOpciones("#aula1");
	for(i in aulasParticipantes){
		if(aulasParticipantes[i].AdmAulFKAdmPabId == pabellonSeleccionado){
			$('#aula1').append($('<option>', {
				value: aulasParticipantes[i].AdmAulId,
				text: aulasParticipantes[i].AulNum
			}));
		}
	}
}

function ponerDocentesSorteados(){
 	$("#usuarios").find("tr:gt(0)").remove();

	for (i in docentesSorteados){
		usuario = docentesSorteados[i];
		var cargo1 = "";
		var cargo2 = "";
		var aula = "";
		var pabellon1 = "";
		var pabellon2 = "";
		var area = "";

		var row = document.getElementById("usuarios").insertRow(-1);
		var cell1 = row.insertCell(0);//Editar
		var cell2 = row.insertCell(1);//Dni
		var cell3 = row.insertCell(2);//Apellidos
		var cell4 = row.insertCell(3);//Nombres
		var cell5 = row.insertCell(4);//Cargos
		var cell6 = row.insertCell(5);//Area
		var cell7 = row.insertCell(6);//pabellon1
		var cell8 = row.insertCell(7);//aula
		var cell9 = row.insertCell(8);cell9.style.display="none";//Experiencia
		for(j in pabellonesParticipantes){
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab){
				pabellon1 = pabellonesParticipantes[j].PabDes;
			}
			if(pabellonesParticipantes[j].AdmPabId == usuario.AdmDetFKPab2){
				pabellon2 = pabellonesParticipantes[j].PabDes;
			}
		}
		for(j in cargosDocentes){
			if(cargosDocentes[j].CarId == usuario.AdmDetFKCar){
				cargo1 = cargosDocentes[j].CarDes;
			}
			if(cargosDocentes[j].CarId == usuario.AdmDetFKCar2){
				cargo2 = cargosDocentes[j].CarDes;
			}
		}
		for(j in areasParticipantes){
			if(areasParticipantes[j].AdmAreId == usuario.AdmDetFKAre){
				area = areasParticipantes[j].AreDes;
				break;
			}
		}
		if(usuario.AdmDetFKAul!= null){
			for(j in aulasParticipantes){
				if(aulasParticipantes[j].AdmAulId == usuario.AdmDetFKAul){
					aula = aulasParticipantes[j].AulNum;
					break;
				}
			}
		}
		cell1.innerHTML = "<input type='image' onclick='editar("+usuario.AdmDetId+");' src='../../img/editar.png' data-toggle='modal' data-target='#myModal' class='botonEditar' value='Editar'>";
		cell2.innerHTML = usuario.UsuDni;
		cell3.innerHTML = usuario.UsuApe;
		cell4.innerHTML = usuario.UsuNom;
		cell5.innerHTML = cargo1;
		cell6.innerHTML = area;
		cell7.innerHTML = pabellon1;
		cell8.innerHTML = aula;
		//cell7.innerHTML = "<a onclick='eliminarPuestos("+usuario.AdmDetId+")' style='cursor: pointer'>Eliminar</a>";
		cell9.innerHTML = usuario.UsuNumPro;
		cell1.setAttribute("style","text-align: center;");
	}
	
}

var idDocente = -1;
function editar(idParticipante){
	idDocente = idParticipante;
	ponerDatos(idDocente);
}

function ponerDatos(idParticipante){
	$("#seguridad2").prop("checked", true);
	$("#mensajeSeguridad").text("¿Es de Seguridad?:");
	$("#seguridad1").attr('disabled', false);
	$("#seguridad2").attr('disabled', false);
	for (i in docentesSorteados){
		var usuario = docentesSorteados[i];
		if(usuario.AdmDetId == idParticipante){
			$("#area").val(usuario.AdmDetFKAre);
			seleccionarArea();
			$("#pabellon1").val(usuario.AdmDetFKPab);
			seleccionarPabellon();
			$("#aula1").val(usuario.AdmDetFKAul);

			if(usuario.AdmDetFKCar2==codSeguridad){
				$("#seguridad1").prop("checked", true);
			}else if(usuario.AdmDetFKCar2>1){
				$("#mensajeSeguridad").text("SE LE ASIGNO OTRO CARGO QUE NO ES SEGURIDAD");
				$("#seguridad1").prop("checked", false);
				$("#seguridad2").prop("checked", false);
				$("#seguridad1").attr('disabled', true);
				$("#seguridad2").attr('disabled', true);
			}

			if(usuario.AdmDetFKCar == codControladorDocente){
				$("#seguridad").show();
			}else{
				$("#seguridad").hide();
			}
			break;
		}
	}
}

function asignar(idParticipante){
	var url = "../../controlador/asignacion/asignacionControlador.php?accion=asignarLugaresYCargosDocentes";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#formulario").serialize() + "&id=" + idParticipante, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Asignando...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

function eliminarPuestos(idUsuario){
	var url = "../../controlador/asignacion/asignacionControlador.php?accion=eliminarLugaresDocentes";
	var parametros = {'id':idUsuario};
	$.ajax({
		type: "POST",
		url: url,
		data: parametros, // serializes the form's elements.
		beforeSend: function(objeto){
			$("#loader").show();
			$("#loader").html("Eliminando puestos...");
		},
		success: function(result){
			$("#loader").hide();
			try{
				var res = JSON.parse(result);
				if(res.error == false){
					mostrarCorrecto(res.mensaje);
					cargar(ultimaPagina);
				}else{
					mostrarError(res.mensaje);
				}
			}catch(err){
				mostrarError("Error Casteo data:"+result);
			}
		},
		error: function(result) {
			$("#loader").hide();
			mostrarError("Error Server result data:"+result);
		}
	});
}

function resetForm(){
	$('#area :nth-child(1)').prop('selected', true);
	$('#pabellon1 :nth-child(1)').prop('selected', true); 
	$('#aula1 :nth-child(1)').prop('selected', true); 
}

var cargo = false;
function loadUsuario(){
	if(!cargo && cargoAreasParticipantes && cargoPabellonesParticipantes && cargoAulasParticipantes && cargoCargosDocentes){
		cargo = false;
		cargar(1);	
	}
}

function validarFormulario(){
	if(($("#area").val()==null || $("#area").val() < 1) && ($("#pabellon1").val()==null || $("#pabellon1").val() < 1) && ($("#aula1").val()==null || $("#aula1").val() < 1)){
		return false;
	}
	return true;
}

$( document ).ready(function() {
	$("#save").on("click", function(e){
		e.preventDefault(); // prevent de default action, which is to submit
		// save your value where you want
		if(validarFormulario()==true){
			asignar(idDocente);
			$(this).prev().click();
		}else{
			mostrarError("NO TIENE NINGUNA ASIGNACIÓN PARA GUARDAR");
			$('#area').focus();
		}
	});
	
});