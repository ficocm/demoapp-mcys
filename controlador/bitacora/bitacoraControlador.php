<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once("../../modelo/usuario/util.php");

//Llamada al modelo
require_once("../../modelo/bitacora/bitacoraUsuarioModelo.php");
require_once("../../modelo/bitacora/bitacoraAdmisionDetalleModelo.php");

include 'paginacionUsuarios.php';


$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

session_start();

if(in_array(Codigos::codAdministrador,$_SESSION["tipo"])) {
	if($accion=='mostrarBitacoraUsuario'){
		$usuarios = new bitacoraUsuarioModelo();
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 10;
		$cantidadRegistros = $usuarios->getCantidadBitacoraUsuario();
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$usarios = $usuarios->getBitacoraUsuario($offset, $registrosPorPagina);
		$pie = paginate($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $usarios, $pie);

	}else if($accion=='mostrarBitacoraAdmisionDetalle'){
		$usuarios = new bitacoraAdmisionDetalleModelo();
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 4;
		$cantidadRegistros = $usuarios->getCantidadBitacoraAdmisionDetalle();
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$detalle = $usuarios->getBitacoraAdmisionDetalle($offset, $registrosPorPagina);
		$pie = paginate2($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $detalle, $pie);
	}

}