<?php
            
    include("sendemail2.php");
    require 'PHPMailer/PHPMailerAutoload.php';
    include("variables.php");
    require_once('../../modelo/usuario/util.php');
    require_once("../../conexion/conexion.php");

    session_start();
    $idProceso = $_SESSION["idProceso"];

    if($idProceso==NULL || $idProceso==""){
        echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
        die();
        return;
    }

    $mysqli = Conectar::conexion();
    $mysqli->set_charset("utf8");

    $asunto = $_POST['asunto'];
    $mensaje = $_POST['mensaje'];
    $tipo_all = $_POST['optionsRadios'];
    $correoSend = $_POST['correo'];
    $contrasenaSend = $_POST['contrasena'];


    //Datos del mensaje

    $txt_message= $mensaje;
    $mail_subject= $asunto;

    $mail = new PHPMailer;
    $mail->isSMTP();                            // Establecer el correo electrónico para utilizar SMTP
    $mail->Host = 'smtp.gmail.com';             // Especificar el servidor de correo a utilizar 
    $mail->SMTPAuth = true;                     // Habilitar la autenticacion con SMTP
    $mail->Username = $correoSend;  
    $mail->Password = $contrasenaSend;          // Correo electronico saliente ejemplo: tucorreo@gmail.com
    /*! jQuery v@1.8.1 jquery.com | jquery.org/license */
    $mail->SMTPSecure = 'tls';                  // Habilitar encriptacion, `ssl` es aceptada
    $mail->Port = 587;                          // Puerto TCP  para conectarse 
    $mail->setFrom($mail_setFromEmail, $mail_setFromName);//Introduzca la dirección de la que debe aparecer el correo electrónico. Puede utilizar cualquier dirección que el servidor SMTP acepte como válida. El segundo parámetro opcional para esta función es el nombre que se mostrará como el remitente en lugar de la dirección de correo electrónico en sí.
    $mail->addReplyTo($mail_setFromEmail, $mail_setFromName);//Introduzca la dirección de la que debe responder. El segundo parámetro opcional para esta función es el nombre que se mostrará para responder

    $mail->Subject = $mail_subject;
    $mail->Body = $txt_message;
    $contador=0;
    
    // echo Util::jsonMensaje(true, $_POST['optionsRadios']);
    // $contador=0;

    foreach ($tipo_all as $tipo) {
        
        if( $tipo=="0"){
            $query = "select AdmDetId, usuario.UsuNom as nombre, usuario.UsuApe as apellido, usuario.UsuCorEle as correo FROM `admisiondetalle` inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId INNER JOIN cargo ON admisiondetalle.AdmDetFKCar=cargo.CarId WHERE AdmDetFKAdmCabId=".$idProceso." AND AdmDetCorEnv=0 and (AdmDetFKEstReg=5 OR AdmDetFKEstReg=17) AND cargo.CarFKTipUsu=4";
        }
        else{
            $query = "select AdmDetId, usuario.UsuNom as nombre, usuario.UsuApe as apellido, usuario.UsuCorEle as correo FROM `admisiondetalle` inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE AdmDetFKAdmCabId=".$idProceso." AND AdmDetCorEnv=0 and (AdmDetFKEstReg=5 OR AdmDetFKEstReg=17) and AdmDetFKCar= ".$tipo;
        }

        $resultado = $mysqli->query($query);

        // $ids="";

        while($row = $resultado->fetch_assoc())
        {
            $contador=$contador+1;
            $AdmDetId = $row['AdmDetId'];
            $apellidos = $row['apellido'];
            $nombres =$row['nombre'];
            $correo = $row['correo'];

            // if($contador==1){
            //     $ids="".$AdmDetId;
            // }
            // else{
            //     $ids=$ids.",".$AdmDetId;
            // }
            $mail->AddBCC($correo);
            // echo " $contador  $AdmDetId  $apellidos  $nombres  $correo \n";
        }

    }   

    if($contador > 0){
        ini_set('max_execution_time',1000); 
        if(!$mail->send()){
            $result = array('result'=> 0, 'message'=>"INTENTELO DE NUEVO.");   
        }
        else{
            $scriptActualizar = "LOS CORREOS FUERON ENVIADOS CORRECTAMENTE";
            $mysqli->query($scriptActualizar);
            $result = array('result'=> 1, 'message'=>$scriptActualizar);   
        }
    }
    else{
        $result = array('result'=> 0, 'message'=>"NO HAY USUARIOS ASIGNADOS PARA EL ENVIO");
    }
    
    echo json_encode($result);
    
?>





