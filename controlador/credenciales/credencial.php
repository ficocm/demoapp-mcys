<?php
	function addCredential($medidaY, $pdf, $apellidos, $nombres, $area, $lugar1, $fecha, $cargo, $cargo2, $lugar2, $tipoUsuario, $tecnico="" ){

		$nombreProceso = $_SESSION["nombreProceso"];
		$faseProceso = $_SESSION["faseProceso"];
		$fechaProceso = $_SESSION["fechaProceso"];
		$anioProceso = $_SESSION["anioProceso"];
		$numEvaProceso = $_SESSION["numEvaProceso"];


		if($lugar2!=''){$local=$lugar1." - ".$lugar2;}
		if($cargo2!=''){$cargo=$cargo." - ".$cargo2;}

		if(strlen($cargo)<=18){
			$tamLetraCargo=20;
		}elseif(strlen($cargo)<=21){
			$tamLetraCargo=18;
		}elseif(strlen($cargo)<=23){
			$tamLetraCargo=16;
		}elseif(strlen($cargo)<=26) {
			$tamLetraCargo=14;
		}else{
			$tamLetraCargo=12;
		}

		//NOMBRE DEL PROCESO
		$procesoNombreCompleto = $nombreProceso." ";

		if($faseProceso!='') $procesoNombreCompleto = $procesoNombreCompleto.$faseProceso." FASE ";
		$procesoNombreCompleto = $procesoNombreCompleto.$anioProceso;

		if($numEvaProceso!=''){
			if($numEvaProceso =='1') $procesoNombreCompleto = $procesoNombreCompleto.", ".$numEvaProceso."ER. EXAMEN";
			if($numEvaProceso =='2') $procesoNombreCompleto = $procesoNombreCompleto.", ".$numEvaProceso."DO. EXAMEN";
			if($numEvaProceso =='3') $procesoNombreCompleto = $procesoNombreCompleto.", ".$numEvaProceso."ER. EXAMEN";
		}

		if(strlen($procesoNombreCompleto)<=18){
			$tamLetraProceso=20;
		}elseif(strlen($procesoNombreCompleto)<=21){
			$tamLetraProceso=18;
		}elseif(strlen($procesoNombreCompleto)<=23){
			$tamLetraProceso=16;
		}elseif(strlen($procesoNombreCompleto)<=26) {
			$tamLetraProceso=14;
		}else{
			$tamLetraProceso=12;
		}


		$pdf->SetFont('times', '', $tamLetraProceso);
		$pdf->SetTextColor(0,0,0);

		$pdf->setCellPaddings(0,0,0,0);
		$pdf->setCellMargins(0,0,0,0);

		$pdf->writeHTMLCell(101, 60, '6', (string)($medidaY+16), $procesoNombreCompleto, 0, 1, 0, true, 'C');


		//CARGO
		$html = "\n".strtoupper($cargo);

		$pdf->SetFont('times', '', $tamLetraCargo);

		$pdf->writeHTMLCell(101, 50, '6', (string)($medidaY+24), $html, 0, 1, 0, true, 'C');


		
		//CREDENCIAL ADELANTE
		// set font
		$pdf->SetFont('times', '', 10);
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);
		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
		// set color for background
		$pdf->SetFillColor(255, 255, 255);
		$pdf->setCellHeightRatio(1.7);



		$txt = '		                                                                Dirección Universitaria
		                                                                de Admisión



		   APELLIDOS: '.$apellidos."
		   NOMBRES: ".$nombres.'
		   PABELLON 1: '.$lugar1;

		if($cargo2!='') $txt = $txt."
		   PABELLON 2: ".str_replace("\n","",$lugar2);
		
		$txt = $txt.'
		   AREA: '.$area;

		// print a blox of text using multicell()
		$pdf->MultiCell(100, 68, $txt."\n", 1, 'L', 1, 1, '5' ,$medidaY, true);

		$pdf->setCellHeightRatio(1.25);

		$txt="";

		if($tipoUsuario == "DOCENTE"){
			$posicion_coincidencia = strrpos($cargo, "CONTROLADOR");
			$aula11 = ($posicion_coincidencia !== false)?"\n			AULA:\n":"";
			$aula1space = ($posicion_coincidencia !== false)?"":"\n";
			$txt = '
			AULA DE COORDINACION:
			'.$aula11.'
			DNI:

			FECHA: '.$fecha.'  
			'.$aula1space.'
 
 
 
			-                                        FIRMA ';
			
			
		}else if($tipoUsuario == "ESTUDIANTE"){
			$txt = '
			AULA DE COORDINACION:
			CUI:
			FECHA: '.$fecha.' 
			TECNICO A CARGO: '.$tecnico.'
 
 
 

 
 
			-                                        FIRMA ';
		}else if($tipoUsuario == "ADMINISTRATIVO"){
			$txt = '
			AULA DE COORDINACION:
			DNI:
			FECHA: '.$fecha.' 
 
 
 
 
 


			-                                        FIRMA ';
		}else{
			$txt = '
			AULA DE COORDINACION:
			DNI:
			FECHA: '.$fecha.' 
 
 
 


		
 
 
			-                                        FIRMA ';
		}



		// $txt = '
		//    '.$CAMPO1.':
		//    AULA:
		//    '.$CAMPO3.':
		//    FECHA: '.$fecha.' 
		//    '.$CAMPO4.'





		//                                             FIRMA
		// ';

		$pdf->MultiCell(100, 68, $txt."\n", 1, 'J', 1, 1, '105', (string)($medidaY), true);

		$pdf->Image('images/unsa.jpg', 9, $medidaY+2, 40, 14, '', '', '', false, 300, '', false, false, '', false, false, false);

		// set color for background
		$pdf->SetFillColor(255, 235, 235);

		// set font
		$pdf->SetFont('helvetica', '', 8);

		// set cell padding
		$pdf->setCellPaddings(2, 4, 6, 8);



		$html = 'Nota: La presente tarjeta certifica su control de asistencia, por lo 
		tanto sírvase devolverla firmada, bajo su responsabilidad.';

		$pdf->SetFont('times', '', 8);
		// print a cell
		$pdf->writeHTMLCell(100, 0, '107', (string)($medidaY+55), $html, 0, 1, 0, true, 'L');


	}
?>