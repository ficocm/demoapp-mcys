<?php

	function addCredential($medidaY, $pdf, $apellidos, $nombres, $area, $local, $proceso, $fecha, $cargo, $cargo2, $lugar2, $tipoUsuario, $tecnico="" ){

		if($lugar2!=''){$local=$local." - ".$lugar2;}
		if($cargo2!=''){$cargo=$cargo."-".$cargo2;}

		if(strlen($cargo)<=18){
			$tamLetraCargo=20;
		}elseif(strlen($cargo)<=21){
			$tamLetraCargo=18;
		}elseif(strlen($cargo)<=23){
			$tamLetraCargo=16;
		}elseif(strlen($cargo)<=26) {
			$tamLetraCargo=14;
		}else{
			$tamLetraCargo=12;
		}
		// set font
		$pdf->SetFont('times', '', 10);
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);
		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		$CAMPO1 = "AULA DE COORDINACION";
		$CAMPO3 = "DNI";
		$CAMPO4 = "";

		if($tipoUsuario == "DOCENTE"){
			$CAMPO1 = "AULA DE COORDINACION";
			$CAMPO3 = "DNI";
		}else if($tipoUsuario == "ESTUDIANTE"){
			$CAMPO1 = "AULA DE COORDINACION";
			$CAMPO3 = "CUI";
			$CAMPO4 = "TECNICO:".$tecnico;
		}else if($tipoUsuario == "ADMINISTRATIVO"){

		}

		$txt = '		                                                                Dirección Universitaria
		                                                                de Admisión



		   

		   APELLIDOS: '.$apellidos.'

		   NOMBRES: '.$nombres.'

		   AREA: '.$area.'

		   LOCAL: '.$local;

		// print a blox of text using multicell()
		$pdf->MultiCell(100, 68, $txt."\n", 0, 'L', 1, 1, '5' ,$medidaY, true);

		$txt = '




		   '.$proceso;

		$pdf->MultiCell(100, 68, $txt."\n", 1, 'C', 1, 1, '5' ,(string)($medidaY), true);

		$txt = '
		   '.$CAMPO1.':
		   AULA:
		   '.$CAMPO3.':
		   FECHA: '.$fecha.' 
		   '.$CAMPO4.'



		                                            FIRMA
		';

		$pdf->MultiCell(100, 68, $txt."\n", 1, 'J', 1, 1, '105', (string)($medidaY), true);

		$pdf->Image('images/unsa.jpg', 9, $medidaY+2, 40, 14, '', '', '', false, 300, '', false, false, '', false, false, false);

		// set color for background
		$pdf->SetFillColor(255, 235, 235);

		// set font
		$pdf->SetFont('helvetica', '', 8);

		// set cell padding
		$pdf->setCellPaddings(2, 4, 6, 8);

		// HTML text with soft hyphens (&shy;)
		$html = "\n\t".strtoupper($cargo);

		$pdf->SetFont('times', '', $tamLetraCargo);
		//$pdf->SetDrawColor(0,0,0);
		$pdf->SetTextColor(0,0,0);

		// print a cell
		$pdf->writeHTMLCell(110, 60, '5', (string)($medidaY+10), $html, 0, 1, 0, true, 'C');

		$html = 'Nota: La presente tarjeta certifica su control de asistencia, por lo 
		tanto sírvase devolverla firmada, bajo su responsabilidad.';

		$pdf->SetFont('times', '', 8);
		// print a cell
		$pdf->writeHTMLCell(100, 0, '107', (string)($medidaY+55), $html, 0, 1, 0, true, 'L');
	}


	$apellidos = strtoupper($_POST['apellidos']);
	$nombres = strtoupper($_POST['nombres']);
	$area = strtoupper($_POST['area']);
	$local = strtoupper($_POST['local']);

	$cargo = strtoupper($_POST['cargo']);
	$fecha = "2018-09-09";
	$proceso = 'ORDINARIO I FASE, 2019';

		// Include the main TCPDF library (search for installation path).
		require_once('config/tcpdf_config.php');
		require_once('tcpdf.php');

		header('Content-type: application/download;filename="reporteAdministrativos.pdf"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Admision');
		$pdf->SetTitle('Credencial');
		$pdf->SetSubject('Credencial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}
			// add a page
			$pdf->AddPage();

				$cargo2 = "";
				$lugar2 = "";


				addCredential(120, $pdf, $apellidos, $nombres, $area, $local, $proceso, $fecha, $cargo, $cargo2, $lugar2);

			
			// move pointer to last page
			$pdf->lastPage();
		

		//Close and output PDF document
		//$pdf->Output($addressBackUpAyudantes."AyudantesTecnicos".$AdmDetId.".pdf", 'F');
		$pdf->Output();
?>