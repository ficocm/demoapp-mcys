<?php
	require_once('../../modelo/usuario/util.php');

	session_start();
	$idProceso = $_SESSION["idProceso"];

	if($idProceso==NULL || $idProceso==""){
		echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
		die();
		return;
	}

	require_once("credencial.php");

	$tipo = $_GET['id'];

	$query = "select AdmDetId, 
AdmDetId as id ,
COALESCE((select area.AreDes from area where admisionarea.AdmAreFKAreId=area.AreId), '') as area ,

COALESCE((select pabellon.PabDes from pabellon inner join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisiondetalle.AdmDetFKPab=admisionpabellon.AdmPabId  where admisiondetalle.AdmDetId=id),(select areapuerta.ArePueDes from areapuerta where areapuerta.ArePueId=admisiondetalle.AdmDetFKArePue),(admisiondetalle.AdmDetLug1),'') as lugar,

COALESCE((select pabellon.PabDes from pabellon inner join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisiondetalle.AdmDetFKPab2=admisionpabellon.AdmPabId  where admisiondetalle.AdmDetId=id),(select areapuerta.ArePueDes from areapuerta where areapuerta.ArePueId=admisiondetalle.AdmDetFKArePue2),(admisiondetalle.AdmDetLug2),'') as lugar2,

admisioncabecera.AdmCabFec as fecha
from admisiondetalle 
inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId 
inner join admisionarea on admisiondetalle.AdmDetFKAre=admisionarea.AdmAreId 
where AdmDetFKAdmCabId=".$idProceso." AND admisiondetalle.AdmDetId=".$tipo;

	require_once("../../conexion/conexion.php");
	$mysqli = Conectar::conexion();
	$mysqli->set_charset("utf8");

	$resultado = $mysqli->query($query);

	while($row = $resultado->fetch_assoc()){

				$AdmDetId = $row['AdmDetId'];
				$area = strtoupper($row['area']);
				$local = strtoupper($row['lugar']);
				$fecha = strtoupper($row['fecha']);
				$proceso = strtoupper($row['proceso']);
				$lugar2 = strtoupper($row['lugar2']);

	}

		$query = "select AdmDetId, 
		AdmDetId as id ,
		usuario.UsuApe as apellidos,
		usuario.UsuNom as nombres,
		usuario.UsuFKTip as usuTipCod,
		COALESCE((SELECT usuario.UsuDni FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetId=$tipo ),'') as dniTecnico,
        COALESCE((SELECT CONCAT(usuario.UsuNom, ' ', usuario.UsuApe) as usuario FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetId=$tipo),'') as nombresTecnico,
		COALESCE((SELECT `GenSubCat` FROM `general` WHERE general.GenId = usuario.UsuFKTip),'') as usuTipDes,
		COALESCE((select cargo.CarDes from cargo where cargo.CarId=admisiondetalle.AdmDetFKCar),'') as cargo,
		COALESCE((select cargo.CarDes from cargo where cargo.CarId=admisiondetalle.AdmDetFKCar2),'') as cargo2

		from admisiondetalle 
		inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId 
		inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId
		inner join cargo on admisiondetalle.AdmDetFKCar=cargo.CarId 
		where admisiondetalle.AdmDetFKDepTec=".$tipo." AND admisioncabecera.AdmCabId=".$idProceso." AND AdmDetFKEstReg=17 order by apellidos, nombres";

	// echo " $query \n";

	$resultado = $mysqli->query($query);



		// Include the main TCPDF library (search for installation path).
		require_once('config/tcpdf_config.php');
		require_once('tcpdf.php');

		header('Content-type: application/download;filename="reporteAdministrativos.pdf"');
		header('Cache-Control: private, max-age=0, must-revalidate');
		header('Pragma: public');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Admision');
		$pdf->SetTitle('Credencial');
		$pdf->SetSubject('Credencial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		while($row = $resultado->fetch_assoc()){
			// add a page
			$pdf->AddPage();

				$AdmDetId = $row['AdmDetId'];
				$apellidos = strtoupper($row['apellidos']);
				$nombres = strtoupper($row['nombres']);
				$cargo = strtoupper($row['cargo']);
				$cargo2 = strtoupper($row['cargo2']);
				$usuTipDes = strtoupper($row['usuTipDes']);
				$nombresTecnico = strtoupper($row['nombresTecnico']);

			addCredential(40, $pdf, $apellidos, $nombres, $area, $local, $fecha, $cargo, $cargo2, $lugar2, $usuTipDes,$nombresTecnico);

			if($row = $resultado->fetch_assoc()){
				$AdmDetId = $row['AdmDetId'];
				$apellidos = strtoupper($row['apellidos']);
				$nombres = strtoupper($row['nombres']);
				$cargo = strtoupper($row['cargo']);
				$cargo2 = strtoupper($row['cargo2']);
				$usuTipDes = strtoupper($row['usuTipDes']);
				$nombresTecnico = strtoupper($row['nombresTecnico']);

				addCredential(115, $pdf, $apellidos, $nombres, $area, $local, $fecha, $cargo, $cargo2, $lugar2, $usuTipDes,$nombresTecnico);
			}

			if($row = $resultado->fetch_assoc()){		
				$AdmDetId = $row['AdmDetId'];
				$apellidos = strtoupper($row['apellidos']);
				$nombres = strtoupper($row['nombres']);
				$cargo = strtoupper($row['cargo']);
				$cargo2 = strtoupper($row['cargo2']);
				$usuTipDes = strtoupper($row['usuTipDes']);
				$nombresTecnico = strtoupper($row['nombresTecnico']);

				addCredential(190, $pdf, $apellidos, $nombres, $area, $local, $fecha, $cargo, $cargo2, $lugar2, $usuTipDes,$nombresTecnico);
			}
			
			// move pointer to last page
			$pdf->lastPage();
		}

		//Close and output PDF document
		//$pdf->Output($addressBackUpAyudantes."AyudantesTecnicos".$AdmDetId.".pdf", 'F');
		$pdf->Output();
?>