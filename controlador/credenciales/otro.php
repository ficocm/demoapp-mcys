<?php


	$apellidos = strtoupper($_POST['apellidos']);
	$nombres = strtoupper($_POST['nombres']);
	$area = strtoupper($_POST['area']);
	$local = strtoupper($_POST['local']);

	$cargo = strtoupper($_POST['cargo']);
	$fecha = "2018-08-19";
	$proceso = 'ORDINARIO I FASE, 2019';
	


		// Include the main TCPDF library (search for installation path).
		require_once('config/tcpdf_config.php');
		require_once('tcpdf.php');

header('Content-type: application/download;filename="reporteAdministrativos.pdf"');
header('Cache-Control: private, max-age=0, must-revalidate');
header('Pragma: public');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Admision');
		$pdf->SetTitle('Credencial');
		$pdf->SetSubject('Credencial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('times', '', 10);

		// add a page
		$pdf->AddPage();

		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);

		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

		$txt = '		                                                        Dirección Universitaria
		                                                        de Admisión



		   

		   APELLIDOS: '.$apellidos.'

		   NOMBRES: '.$nombres.'

		   AREA: '.$area.'

		   LOCAL: '.$local;

		// print a blox of text using multicell()
		$pdf->MultiCell(90, 68, $txt."\n", 1, 'L', 1, 1, '' ,'120', true);

		$txt = '




		   '.$proceso;

		$pdf->MultiCell(90, 68, $txt."\n", 1, 'C', 1, 1, '' ,'120', true);

		$txt = '
		   DEPARTAMENTO ACADÉMICO:

		   AULA: 

		   DNI N°:

		   FECHA: '.$fecha.' 




		                                            FIRMA
		';

		$pdf->MultiCell(90, 68, $txt."\n", 1, 'J', 1, 1, '106','120', true);

		$pdf->Image('images/unsa.jpg', 19, 122, 40, 14, '', '', '', false, 300, '', false, false, '', false, false, false);


		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// AUTO-FITTING

		// set color for background
		$pdf->SetFillColor(255, 235, 235);

		// Fit text on cell by reducing font size
		//$pdf->MultiCell(55, 60, '[FIT CELL] '.$txt."\n", 1, 'J', 1, 1, 125, 145, true, 0, false, true, 60, 'M', true);

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// CUSTOM PADDING

		// set color for background
		//$pdf->SetFillColor(255, 255, 215);

		// set font
		$pdf->SetFont('helvetica', '', 8);

		// set cell padding
		$pdf->setCellPaddings(2, 4, 6, 8);

		//$pdf->MultiCell(55, 5, $txt, 1, 'J', 1, 2, 125, 210, true);


		// HTML text with soft hyphens (&shy;)
		$html = "\n\t".strtoupper($cargo);

		$pdf->SetFont('times', '', 13);
		//$pdf->SetDrawColor(0,0,0);
		$pdf->SetTextColor(0,0,0);

		// print a cell
		$pdf->writeHTMLCell(90, 60, '20', '130', $html, 0, 1, 0, true, 'C');

		$html = 'Nota: La presente tarjeta certifica su control de asistencia, por lo 
		tanto sírvase devolverla firmada, bajo su responsabilidad.';

		$pdf->SetFont('times', '', 8);
		// print a cell
		$pdf->writeHTMLCell(90, 60, '107', '175', $html, 0, 1, 0, true, 'L');


		// move pointer to last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		 $pdf->Output();



	
?>
