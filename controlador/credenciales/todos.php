<?php
	require_once('../../modelo/usuario/util.php');

	session_start();
	$idProceso = $_SESSION["idProceso"];

	if($idProceso==NULL || $idProceso==""){
		echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
		die();
		return;
	}

	require_once("credencial.php");

	

	$query = "select AdmDetId, 
AdmDetId as id , AdmDetFKCar, AdmDetFKDepTec,
usuario.UsuApe as apellidos,
usuario.UsuNom as nombres,
usuario.UsuFKTip as usuTipCod,
COALESCE((SELECT `GenSubCat` FROM `general` WHERE general.GenId = usuario.UsuFKTip),'') as usuTipDes,
COALESCE((select cargo.CarDes from cargo where cargo.CarId=admisiondetalle.AdmDetFKCar),'') as cargo,
COALESCE((select cargo.CarDes from cargo where cargo.CarId=admisiondetalle.AdmDetFKCar2),'') as cargo2,
COALESCE((select area.AreDes from area inner join admisionarea on admisionarea.AdmAreFKAreId=area.AreId inner join admisiondetalle on  admisiondetalle.AdmDetFKAre=admisionarea.AdmAreId  where admisiondetalle.AdmDetId=id) ,'')as area ,

COALESCE((select pabellon.PabDes from pabellon inner join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisiondetalle.AdmDetFKPab=admisionpabellon.AdmPabId  where admisiondetalle.AdmDetId=id),(select areapuerta.ArePueDes from areapuerta where areapuerta.ArePueId=admisiondetalle.AdmDetFKArePue),(admisiondetalle.AdmDetLug1),'') as lugar,

COALESCE((select pabellon.PabDes from pabellon inner join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisiondetalle.AdmDetFKPab2=admisionpabellon.AdmPabId  where admisiondetalle.AdmDetId=id),(select areapuerta.ArePueDes from areapuerta where areapuerta.ArePueId=admisiondetalle.AdmDetFKArePue2),(admisiondetalle.AdmDetLug2),'') as lugar2,
admisioncabecera.AdmCabFec as fecha

from admisiondetalle 
inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId 
inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId 

where AdmDetFKAdmCabId=".$idProceso." AND admisiondetalle.AdmDetFKEstReg=17";


	require_once("../../conexion/conexion.php");
	$mysqli = Conectar::conexion();
	$mysqli->set_charset("utf8");

	$resultado = $mysqli->query($query);

	// Include the main TCPDF library (search for installation path).
	require_once('config/tcpdf_config.php');
	require_once('tcpdf.php');

	header('Content-type: application/download;filename="reporteAdministrativos.pdf"');
	header('Cache-Control: private, max-age=0, must-revalidate');
	header('Pragma: public');

	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Admision');
	$pdf->SetTitle('Credencial');
	$pdf->SetSubject('Credencial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    $pdf->setLanguageArray($l);
	}
	$i = 20;
	$pdf->AddPage();
	while($row = $resultado->fetch_assoc()){
		// add a page
		
		if($i > 160){
			$pdf->lastPage();
			$pdf->AddPage();
			$i = 20;
		}
		$i = $i; // hack de php;

		$AdmDetId = $row['AdmDetId'];
		$apellidos = strtoupper($row['apellidos']);
		$nombres = strtoupper($row['nombres']);
		$cargo = strtoupper($row['cargo']);
		$cargo2 = strtoupper($row['cargo2']);
		$area = strtoupper($row['area']);
		$local = strtoupper($row['lugar']);
		$fecha = strtoupper($row['fecha']);
		$lugar2 = strtoupper($row['lugar2']);
		$usuTipDes = strtoupper($row['usuTipDes']);
			
		if($row['AdmDetFKCar']==4){

			$query = "select AdmDetId, 
			AdmDetId as id ,
			COALESCE((select area.AreDes from area inner join admisionarea on admisionarea.AdmAreFKAreId=area.AreId inner join admisiondetalle on  admisiondetalle.AdmDetFKAre=admisionarea.AdmAreId  where admisiondetalle.AdmDetId=id) ,'')as area ,

			COALESCE((select pabellon.PabDes from pabellon inner join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisiondetalle.AdmDetFKPab=admisionpabellon.AdmPabId  where admisiondetalle.AdmDetId=id),(select areapuerta.ArePueDes from areapuerta where areapuerta.ArePueId=admisiondetalle.AdmDetFKArePue),(admisiondetalle.AdmDetLug1),'') as lugar

			from admisiondetalle 
			inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId 
			inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId 

			where AdmDetFKAdmCabId=".$idProceso." AND admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetId=".$row['AdmDetFKDepTec'];

			$resultado = $mysqli->query($query);

			while($row = $resultado->fetch_assoc()){
				$area = strtoupper($row['area']);
				$local = strtoupper($row['lugar']);
			}

			
		}

		addCredential($i, $pdf, $apellidos, $nombres, $area, $local, $fecha, $cargo, $cargo2, $lugar2, $usuTipDes);
		
		$i = $i + 70;
		
		
	}
		
		// move pointer to last page
		$pdf->lastPage();
	

	//Close and output PDF document
	//$pdf->Output($addressBackUpAyudantes."AyudantesTecnicos".$AdmDetId.".pdf", 'F');
	$pdf->Output();
?>