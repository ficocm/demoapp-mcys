<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/credenciales/admisionDetalleModelo.php");
require_once("../../modelo/usuario/usuarioModelo.php");
include 'paginacion.php';

session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}

$admisionDetalle = new admisionDetalleModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";
	
if($accion == 'mostrarAdministrativosInscritosSorteados'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);
}

if($accion == 'mostrarTecnicos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadTecnicos($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getTecnicos($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);
}

if($accion == 'mostrarCargos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadCargos($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getCargos($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);
}
?>