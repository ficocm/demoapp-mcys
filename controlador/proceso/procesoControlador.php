<?php 

//Archivo de configuracion
require_once('../../conexion/conexion.php');
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/proceso/admisionCabecera.php");

session_start();
$idProceso = $_SESSION["idProceso"];

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie sesion y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}

$admisionCabecera = new admisionCabecera();


if($accion=='iniciarEstadoProceso'){
	$idProceso = Util::getParameterBusqueda("idProceso");

	$codigoRespuesta = $admisionCabecera->iniciarProceso($idProceso);

	if($codigoRespuesta===0){
		$_SESSION["estadoProceso"] = 32;
		echo Util::jsonMensaje(false, "Proceso de admision actualizado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo actualizar el Proceso de Admision (".$codigoRespuesta.")");
	}
}

if($accion=='finalizarEstadoProceso'){
	$idProceso = Util::getParameterBusqueda("idProceso");
	$codigoRespuesta = $admisionCabecera->finalizarProceso($idProceso);

	if($codigoRespuesta===0){
		$_SESSION["estadoProceso"] = 31;
		echo Util::jsonMensaje(false, "Proceso de admision actualizado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo actualizar el Proceso de Admision (".$codigoRespuesta.")");
	}
}

?>