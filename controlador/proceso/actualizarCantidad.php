<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/proceso/admisionCabecera.php");
require_once("../../modelo/proceso/admisionPabellon.php");

session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}


$admisionCabecera = new admisionCabecera();
$admisionPabellon = new admisionPabellon();

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";	
if($accion=='cantidad'){
	$cantidad = $admisionCabecera->getCantidades($idProceso);
	echo Util::jsonMensaje(false, $cantidad, $pie);
}

if($accion=='capacidadAsignada'){
	$capacidadAsignada = $admisionPabellon->getCapacidadAsignada($idProceso);
	echo Util::jsonMensaje(false, $capacidadAsignada, $pie);
}

if($accion=='actualizar'){
	$cantidadIngenierias = Util::getParameterBusqueda("cantidadIngenierias");
	$cantidadSociales = Util::getParameterBusqueda("cantidadSociales");
	$cantidadBiomedicas = Util::getParameterBusqueda("cantidadBiomedicas");

	$codigoRespuesta = "No hay codigo correcto";

	$codigoRespuesta = $admisionCabecera->modificarCantidad($idProceso, $cantidadIngenierias, $cantidadSociales, $cantidadBiomedicas);

	if($codigoRespuesta===0){
		echo Util::jsonMensaje(false, "Datos actualizados correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo actualizar los datos (".$codigoRespuesta.")");
	}
}


?>