<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/asignacion/admisionDetalleModelo.php");
require_once("../../modelo/asignacion/cargoModelo.php");
include 'paginacion.php';

session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}

$admisionDetalle = new admisionDetalleModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";
	
if($accion=='mostrarDocentesSorteados'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$tipo = Util::getParameter("tipo");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadDocentesSorteados($idProceso, $busqueda, $tipo);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentes = $admisionDetalle->getDocentesSorteados($idProceso, $busqueda, $offset, $registrosPorPagina, $tipo);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentes, $pie);

}else if($accion=='mostrarDocentesSorteadosConCargo'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$tipo = Util::getParameter("tipo");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadDocentesSorteadosConCargo($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentes = $admisionDetalle->getDocentesSorteadosConCargo($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentes, $pie);

}else if($accion=='mostrarAdministrativosSorteados'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 2;
	$cantidadRegistros = $admisionDetalle->getCantidadAdministrativosSorteados($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getAdministrativosSorteados($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);

}else if($accion == 'mostrarDocentesTecnicos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$tipo = Util::getParameter("tipo");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;

	$cantidadRegistros = $admisionDetalle->getCantidadDocentesTecnicos($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentesTecnicos = $admisionDetalle->getDocentesTecnicos($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentesTecnicos, $pie);
}

//Para comisión
else if($accion=='mostrarDocentesSorteadosSinTipo'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$tipo = Util::getParameter("tipo");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadDocentesSorteadosSinTipo($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentes = $admisionDetalle->getDocentesSorteadosSinTipo($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentes, $pie);

}

else if($accion == 'mostrarAreasParticipantes'){
	$areasParticipantes = $admisionDetalle->getAreasParticipantes($idProceso);
	echo json_encode($areasParticipantes);
}else if($accion == 'mostrarPabellonesParticipantes'){
	$pabellonesParticipantes = $admisionDetalle->getPabellonesParticipantes($idProceso);
	echo json_encode($pabellonesParticipantes);
}else if($accion == 'mostrarAulasParticipantes'){
	$aulasParticipantes = $admisionDetalle->getAulasParticipantes($idProceso);
	echo json_encode($aulasParticipantes);
}else if($accion == 'mostrarAreasPuertas'){
	$areasPuertas = $admisionDetalle->getPuertasAreas($idProceso);
	echo json_encode($areasPuertas);
}else if($accion == 'mostrarCargosAdministrativos'){
	$cargo = new cargoModelo();
	$cargosAdministrativos = $cargo->getCargosAdministrativos();
	echo json_encode($cargosAdministrativos);
}else if($accion == 'mostrarCargosDocentes'){
	$cargo = new cargoModelo();
	$cargosDocentes = $cargo->getCargosDocentes();
	echo json_encode($cargosDocentes);
}else if($accion == 'mostrarCargosDocentesAdministrativos'){
	$cargo = new cargoModelo();
	$cargosDocentes = $cargo->getCargosDocentesAdministrativos();
	echo json_encode($cargosDocentes);
}else if($accion == 'mostrarCargosDocentesComision'){
	$cargo = new cargoModelo();
	$cargosDocentes = $cargo->getCargosDocentesComision();
	echo json_encode($cargosDocentes);
}else if($accion == 'mostrarCargosAdministrativosComision'){
	$cargo = new cargoModelo();
	$cargosAdministrativos = $cargo->getCargosAdministrativosComision();
	echo json_encode($cargosAdministrativos);
}else if($accion == 'mostrarCargosComision'){
	$cargo = new cargoModelo();
	$cargosAdministrativos = $cargo->getCargosComision();
	echo json_encode($cargosAdministrativos);
}else if($accion == 'asignarLugaresYCargos'){
		$id = Util::getParameter('id');
		$area1 = Util::getParameter('area');
		$pabellon1 = Util::getParameter('pabellon1');
		$cargo1 = Util::getParameter('cargo1');
		$puerta1 = Util::getParameter('puerta1');
		$lugar1 = Util::getParameter('lugar1');
		$pabellon2 = Util::getParameter('pabellon2');
		$cargo2 = Util::getParameter('cargo2');
		$puerta2 = Util::getParameter('puerta2');
		$lugar2 = Util::getParameter('lugar2');

		$area2 = ($pabellon2!="NULL" || $cargo2!="NULL" || $puerta2!="NULL" || $lugar2!="NULL")? $area1 : "NULL";
		$codigoRespuesta = $admisionDetalle->updatePuestosParticipante($id, $area1, $pabellon1, $cargo1, $lugar1, $puerta1, $area2, $pabellon2, $cargo2, $lugar2, $puerta2);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario asignado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo asignar verifique los datos (".$codigoRespuesta.")");
		}
}else if($accion == 'asignarLugaresYCargosDocentes'){
		$id = Util::getParameter('id');
		$area1 = Util::getParameter('area');
		$pabellon1 = Util::getParameter('pabellon1');
		$aula1 = Util::getParameter('aula1');

		$cargo2 = Util::getParameter('seguridad');
		$codigoRespuesta = "NO SE REALIZÓ NINGUN CAMBIO";
		if($cargo2 == "'SI'"){
			$codigoRespuesta = $admisionDetalle->updatePuestosParticipanteDocentesSeguridad($id, $area1, $pabellon1,$aula1, $area1, $pabellon1,  Codigos::codDocenteSeguridad);
		}else if($cargo2 == "'NO'"){
			$codigoRespuesta = $admisionDetalle->updatePuestosParticipanteDocentesSeguridad($id, $area1, $pabellon1, $aula1, "NULL", "NULL", "NULL");
		}else{
			$codigoRespuesta = $admisionDetalle->updatePuestosParticipanteDocentes($id, $area1, $pabellon1, $aula1);
		}

		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Docente asignado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo asignar verifique los datos (".$codigoRespuesta.")");
		}
}else if($accion == 'asignarLugaresYCargosComision'){
		$id = Util::getParameter('id');
		$area1 = Util::getParameter('area');
		$pabellon1 = Util::getParameter('pabellon1');
		$cargo1 = Util::getParameter('cargo1');
		$pabellon2 = Util::getParameter('pabellon2');
		$cargo2 = Util::getParameter('cargo2');

		$area2 = ($pabellon2!="NULL" || $cargo2!="NULL")? $area1 : "NULL";

		$codigoRespuesta = $admisionDetalle->updatePuestosParticipanteComision($id, $area1, $area2, $pabellon1, $pabellon2, $cargo1, $cargo2);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario (Comisión) asignado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo asignar (Comisión) verifique los datos (".$codigoRespuesta.")");
		}
}else if($accion == 'eliminarLugaresDocentes'){
	$id = Util::getParameter('id');
	$codigoRespuesta = $admisionDetalle->eliminarLugaresDocentes($id);
	if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario modificado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo eliminar lugares verifique los datos (".$codigoRespuesta.")");
		}
}else if($accion == 'eliminarCargosLugaresAdministrativos'){
	$id = Util::getParameter('id');
	$codigoRespuesta = $admisionDetalle->eliminarLugaresAdministrativos($id);
	if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario modificado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo eliminar lugares verifique los datos (".$codigoRespuesta.")");
		}
}

?>