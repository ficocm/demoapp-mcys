<?php 
//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/inicio/util.php');

//Llamada al modelo
require_once("../../modelo/inicio/admisionCabeceraModelo.php");

include '../asignacion/paginacion.php';

$procesosAdmision = new admisionCabeceraModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";


if($accion=='mostrarProcesosDeAdmision'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $procesosAdmision->getCantidadProcesos($busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$procesosDeAdmision = $procesosAdmision->getProcesos($busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $procesosDeAdmision, $pie);
}

//Mostrar Areas
else if($accion=='mostrarAreas'){
	$areas = $procesosAdmision->getAreas();
	echo json_encode($areas);
}

?>