<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/participantes/admisionDetalleModelo.php");
require_once("../../modelo/usuario/usuarioModelo.php");
include 'paginacion.php';

session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}

$admisionDetalle = new admisionDetalleModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";
	
if($accion=='mostrarDocentesInscritosSorteados'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadDocentesConSinInscripcionOParticipacion($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentes = $admisionDetalle->getDocentesConSinInscripcionOParticipacion($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentes, $pie);

}else if($accion == 'mostrarAdministrativosInscritosSorteados'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);

}else if($accion == 'mostrarDocentesTecnicos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadDocentesTecnicosSorteados($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$docentesTecnicos = $admisionDetalle->getDocentesTecnicosSorteados($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $docentesTecnicos, $pie);

}else if($accion == 'mostrarAyudantesTecnicos'){
	$docenteTecnicoId = Util::getParameter("docenteTecnicoId");
	$ayudantesTecnicos = $admisionDetalle->getAyudantesTecnicos($idProceso, $docenteTecnicoId);
	echo Util::jsonMensaje(false, $ayudantesTecnicos, $pie);

}else if($accion == 'mostrarEstudiantes'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadEstudiantes($busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$estudiantes = $admisionDetalle->getEstudidantes($busqueda, $offset, $registrosPorPagina, $idProceso);
	$pie = paginateEstudiantes($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $estudiantes, $pie);

}else if($accion == 'crearAyudanteTecnico'){
	$docenteTecnicoId = Util::getParameter("docenteTecnicoId");
	$estudianteId = Util::getParameter("estudianteId");
	$codigoRespuesta = $admisionDetalle->crearAyudanteTecnico($docenteTecnicoId, $estudianteId, $idProceso);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Ayudante Tecnico asignado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo asignar ayudante tecnico verifique los datos (".$codigoRespuesta.")");
	}
}else if($accion == 'eliminarAyudanteTecnico'){
	$estudianteId = Util::getParameter("estudianteId");
	$codigoRespuesta = $admisionDetalle->eliminarAyudanteTecnico($estudianteId);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Ayudante Tecnico eliminado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo eliminar ayudante tecnico verifique los datos (".$codigoRespuesta.")");
	}

}else if($accion == 'crearParticipacion'){
	$usuarioId = Util::getParameter("usuarioId");//Docente o Administrador
	$cargo = Util::getParameter("cargo"); //Solo para docentes
	$participacion = Util::getParameter("participacion");//Inscrito o Sorteado
	$codigoRespuesta = "No hay codigo de participacion correcto";

	$idNuevo = -1;
	if($participacion == Codigos::codInscrito){
		$res = $admisionDetalle->crearInscrito($usuarioId, $idProceso, $cargo);
		$codigoRespuesta = $res[0];
		$idNuevo = $res[1];
	}else if($participacion == Codigos::codSorteado){
		$res = $admisionDetalle->crearSorteado($usuarioId, $idProceso, $cargo);
		$codigoRespuesta = $res[0];
		$idNuevo = $res[1];
	}

	if($codigoRespuesta === 0){
		echo Util::jsonMensaje(false, "Registro en el proceso de admision correcto", $idNuevo);
	}else{
		echo Util::jsonMensaje(true, "No se pudo poner en participacion verifique los datos (".$codigoRespuesta.")");
	}
}else if($accion == 'modificarParticipacion'){
	$usuarioId = Util::getParameter("usuarioId");//Docente o Administrador
	$cargo = Util::getParameter("cargo"); //Solo para docentes
	$participacion = Util::getParameter("participacion");//Inscrito o Sorteado
	$codigoRespuesta = "No hay codigo de participacion correcto";

	if($participacion == Codigos::codInscrito){
		$codigoRespuesta = $admisionDetalle->modificarAInscrito($usuarioId, $cargo);
	}else if($participacion == Codigos::codSorteado){
		$codigoRespuesta = $admisionDetalle->modificarASorteado($usuarioId, $cargo);
	}else if($participacion == Codigos::codInscritoCancelado){
		$codigoRespuesta = $admisionDetalle->modificarANoInscrito($usuarioId, $cargo);
	}else if($participacion == Codigos::codSorteadoCancelado){
		$codigoRespuesta = $admisionDetalle->modificarANoSorteado($usuarioId, $cargo);
	}
	
	if($codigoRespuesta===0){
		echo Util::jsonMensaje(false, "Usuario con participacion modificado correcto");
	}else{
		echo Util::jsonMensaje(true, "No se pudo modificar participacion verifique los datos (".$codigoRespuesta.")");
	}
}else if($accion == 'eliminarDelProceso'){
	$id = Util::getParameter("id");
	$codigoRespuesta = $admisionDetalle->eliminarDelProceso($id);
	
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Usuario eliminado del proceso correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo eliminar participacion verifique los datos (".$codigoRespuesta.")");
	}
}
?>