<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/usuario/areaModelo.php");
require_once("../../modelo/usuario/departamentoModelo.php");
require_once("../../modelo/usuario/generalModelo.php");
require_once("../../modelo/usuario/usuarioModeloVis.php");

$usuarios = new usuarioModeloVis();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";
	
if($accion=='guardar'){
	$id = Util::getParameter("id");
	$celular =  Util::getParameter("celular");

	$usuarios->setDatos($celular,$id);

	echo Util::jsonMensaje(false,"Yep",$usuarios);
}
//MOSTRAR
else if($accion=='mostrarDocentes'){
	$docentes = $usuarios->getDocentes();
	echo json_encode($docentes);

}else if($accion=='mostrarAdministrativos'){
	$administrativos = $usuarios->getAdministrativos();
	echo json_encode($administrativos);

}else if($accion=='mostrarEstudiantes'){
	$estudiantes = $usuarios->getEstudiantes();
	echo json_encode($estudiantes);

}else if($accion=='mostrarOperadores'){
	$operadores = $usuarios->getOperadores();
	echo json_encode($operadores);

}else if($accion=='mostrarAdministradores'){
	$administradores = $usuarios->getAdministradores();
	echo json_encode($administradores);

}else if($accion=='mostrarAreas'){
	$area = new areaModelo();
	$areas = $area->getAreas();
	echo json_encode($areas);

}else if($accion=='mostrarDepartamentos'){
	$departamento = new departamentoModelo();
	$departamentos = $departamento->getDepartamentos();
	echo json_encode($departamentos);

}else if($accion=='mostrarTiposUsuario'){
	$general = new generalModelo();
	$tiposUsuario = $general->getTiposDeUsuario();
	echo json_encode($tiposUsuario);

}else if($accion=='mostrarCategorias'){
	$general = new generalModelo();
	$categorias = $general->getCategorias();
	echo json_encode($categorias);

}else if($accion=='mostrarGeneros'){
	$general = new generalModelo();
	$generos = $general->getGeneros();
	echo json_encode($generos);

}else if($accion=='mostrarCargosDocente'){
	$general = new generalModelo();
	$cargos = $general->getCargosDocente();
	echo json_encode($cargos);

}else if($accion=='mostrarCargosDocenteComision'){
	$general = new generalModelo();
	$cargos = $general->getCargosDocenteComision();
	echo json_encode($cargos);

}
//MODIFICAR
else if($accion=='modificarUsuario'){
	$id = Util::getParameter("id");
	$dni = Util::getParameter("dni");
	$cui = Util::getParameter("cui");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$departamento = Util::getParameter("departamento");
	$area = Util::getParameter("area");
	$numeroProcesos = Util::getParameter("numeroProcesos");
	$funcion = Util::getParameter("funcion");
	$genero = Util::getParameter("genero");
	$tipo = Util::getParameter("tipo");
	$dependencia = Util::getParameter("dependencia");
	$categoria = Util::getParameter("categoria");
	$estadoRegistro = Util::getParameter("estadoRegistro");

	$codigoRespuesta = $usuarios->updateUsuario($id, $dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo, $dependencia, $categoria,$estadoRegistro);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Usuario modificado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo modificar verifica los datos (".$codigoRespuesta.")");
	}
}else if($accion=='crearUsuario'){
	$dni = Util::getParameter("dni");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$departamento = Util::getParameter("departamento");
	$area = Util::getParameter("area");
	$numeroProcesos = Util::getParameter("numeroProcesos");
	$funcion = Util::getParameter("funcion");
	$genero = Util::getParameter("genero");
	$tipo = Util::getParameter("tipo");
	$dependencia = Util::getParameter("dependencia");
	$categoria = Util::getParameter("categoria");
	$estadoRegistro = Util::getParameter("estadoRegistro");

	$codigoRespuesta = $usuarios->crearUsuario($dni, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo, $dependencia, $categoria,$estadoRegistro);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Usuario creado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo crear usuario verifica los datos (".$codigoRespuesta.")");
	}

}else if($accion=='crearEstudiante'){
	$dni = Util::getParameter("dni");
	$cui = Util::getParameter("cui");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$departamento = Util::getParameter("departamento");
	$area = Util::getParameter("area");
	$numeroProcesos = Util::getParameter("numeroProcesos");
	$funcion = Util::getParameter("funcion");
	$genero = Util::getParameter("genero");
	$tipo = Util::getParameter("tipo");
	$dependencia = Util::getParameter("dependencia");
	$categoria = Util::getParameter("categoria");
	$estadoRegistro = Util::getParameter("estadoRegistro");

	$codigoRespuesta = $usuarios->crearEstudiante($dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo, $dependencia, $categoria,$estadoRegistro);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Estudiante creado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo crear estudiante verifica los datos (".$codigoRespuesta.")");
	}
}
/*else if($accion=='leerDocentesSorteados'){
	$usuarios=$admisionDetalle->getUsuariosDocentesSorteados();
	echo json_encode($usuarios);
}else if($accion == 'eliminarCargos'){
	$id = $_GET['id'];
	if(esIdValido($id)){
		$res = $admisionDetalle->eliminarCargosYLugares($id);
		if($res == 0){ //Devuelve error si es 0 no huvo error
			echo jsonMensaje(false, "Se Elimino los lugares y el cargo Correctamente");
		}else{
			echo jsonMensaje(true, "No se pudo eliminar cargos ni lugares");
		}
	}else{
		echo jsonMensaje(true, "Datos inválido");
	}
}else if($accion=="actualizarCargosYLugares"){
	//Se tiene que validar cada entrada del formulario
	$area1 = getPostId('area');
	$pabellon1 = getPostId('pabellon1');
	$cargo1 = getPostId('cargo1');
	$puerta1 = getPostId('puerta1');
	$pabellon2 = getPostId('pabellon2');
	$cargo2 = getPostId('cargo2');
	$puerta2 = getPostId('puerta2');
	$aula = getPostId('aula');
	$area2 = ($pabellon2!="NULL" || $cargo2!="NULL")? $area1 : "NULL";
	$array = array();
	foreach ($_POST['ids'] as $key => $id) {
		$array[] = $admisionDetalle->updateAdmisionDetallePuestos($id, $area1, $pabellon1, $aula, $cargo1, $puerta1, $area2, $pabellon2, $cargo2, $puerta2)." Registro exitoso";
	}
	echo json_encode($array);
}else{
	echo jsonMensaje(true, "Error en URL");
}
*/
?>