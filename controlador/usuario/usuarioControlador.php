<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once("../../modelo/usuario/util.php");

//Llamada al modelo
require_once("../../modelo/usuario/areaModelo.php");
require_once("../../modelo/usuario/departamentoModelo.php");
require_once("../../modelo/usuario/generalModelo.php");
require_once("../../modelo/usuario/usuarioModelo.php");

include '../asignacion/paginacion.php';

$usuarios = new usuarioModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

session_start();

if($accion=='iniciarSesion'){
	$usuario = Util::getParameterNumeroCadena("user");
	$contrasena = Util::getParameterNumeroCadena("pass");

	if($usuario== 'NULL' || $contrasena== 'NULL'){
		echo Util::jsonMensaje(true, "Complete el usuario o Contraseña");
		return;
	}

	$usuario = $usuarios->getUsuarioLogin($usuario, $contrasena);
	$cantidad = count($usuario);
	
	if($cantidad==0){
		echo Util::jsonMensaje(true, "Usuario o Contrasena INVALIDA");
	}else if($cantidad>1){
		echo Util::jsonMensaje(true, "Otro usuario tiene el mismo usuario y contrasena");
	}else if($cantidad==1){
		session_start();
		$_SESSION["idUsuario"]=$usuario[0]["UsuId"];
		$_SESSION["nombres"]=$usuario[0]["UsuNom"];
		$_SESSION["apellidos"]=$usuario[0]["UsuApe"];
		$tipos = array($usuario[0]["UsuFKTip"], $usuario[0]["UsuFKTip2"]);
		$_SESSION["tipo"]=$tipos;
		if(in_array(Codigos::codOperador, $tipos) || in_array(Codigos::codAdministrador, $tipos)){
			$_SESSION["usuarioNombre"]=$usuario[0]["UsuId"];
			$_SESSION["usuarioContraseña"]=md5($usuario[0]["UsuId"]);
		}
		echo Util::jsonMensaje(false, "Inicio de Sesion CORRECTO");
	}
}

if(in_array(Codigos::codOperador,$_SESSION["tipo"])||in_array(Codigos::codAdministrador,$_SESSION["tipo"])) {

	//MOSTRAR
	if($accion=='mostrarDocentes'){
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 4;
		$cantidadRegistros = $usuarios->getCantidadDocentes($busqueda);
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$docentes = $usuarios->getDocentes($busqueda, $offset, $registrosPorPagina);
		$pie = paginate($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $docentes, $pie);

	}else if($accion=='mostrarAdministrativos'){
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 2;
		$cantidadRegistros = $usuarios->getCantidadAdministrativos($busqueda);
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$administrativos = $usuarios->getAdministrativos($busqueda, $offset, $registrosPorPagina);
		$pie = paginate($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $administrativos, $pie);

	}else if($accion=='mostrarAdministrativosDocentes'){
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 2;
		$cantidadRegistros = $usuarios->getCantidadAdministrativosDocentes($busqueda);
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$administrativos = $usuarios->getAdministrativosDocentes($busqueda, $offset, $registrosPorPagina);
		$pie = paginate($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $administrativos, $pie);

	}else if($accion=='mostrarEstudiantes'){
		$busqueda = Util::getParameterBusqueda("busqueda");
		$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
		$registrosPorPagina = Util::getParameter("registrosPorPagina");
		$adyacentes = 4;
		$cantidadRegistros = $usuarios->getCantidadEstudiantes($busqueda);
		$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];

		$offset = ($pagina - 1) * $registrosPorPagina;
		$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

		$estudiantes = $usuarios->getEstudiantes($busqueda, $offset, $registrosPorPagina);
		$pie = paginate($pagina, $totalPaginas, $adyacentes);
		echo Util::jsonMensaje(false, $estudiantes, $pie);

	}else if($accion=='mostrarAreas'){
		$area = new areaModelo();
		$areas = $area->getAreas();
		echo json_encode($areas);

	}else if($accion=='mostrarDepartamentos'){
		$departamento = new departamentoModelo();
		$departamentos = $departamento->getDepartamentos();
		echo json_encode($departamentos);

	}else if($accion=='mostrarTiposUsuario'){
		$general = new generalModelo();
		$tiposUsuario = $general->getTiposDeUsuario();
		echo json_encode($tiposUsuario);

	}else if($accion=='mostrarTiposDocentesAdministrativos'){
		$general = new generalModelo();
		$tiposUsuario = $general->getTiposDeDocentesAdministrativos();
		echo json_encode($tiposUsuario);

	}else if($accion=='mostrarCategorias'){
		$general = new generalModelo();
		$categorias = $general->getCategorias();
		echo json_encode($categorias);

	}else if($accion=='mostrarGeneros'){
		$general = new generalModelo();
		$generos = $general->getGeneros();
		echo json_encode($generos);

	}else if($accion=='mostrarCargosDocente'){
		$general = new generalModelo();
		$cargos = $general->getCargosDocente();
		echo json_encode($cargos);

	}else if($accion=='mostrarCargosDocenteComision'){
		$general = new generalModelo();
		$cargos = $general->getCargosDocenteComision();
		echo json_encode($cargos);

	}
	//MODIFICAR
	else if($accion=='modificarUsuario'){
		$id = Util::getParameter("id");
		$dni = Util::getParameterNumeroCadena("dni");
		$cui = Util::getParameterNumeroCadena("cui");
		$nombres = Util::getParameter("nombres");
		$apellidos = Util::getParameter("apellidos");
		$correo = Util::getParameter("correo");
		$telefono = Util::getParameterNumeroCadena("telefono");
		$direccion = Util::getParameter("direccion");
		$departamento = Util::getParameter("departamento");
		$area = Util::getParameter("area");
		$numeroProcesos = Util::getParameterDefaultZero("numeroProcesos");
		$funcion = Util::getParameter("funcion");
		$genero = Util::getParameter("genero");
		$tipo1 = Util::getParameter("tipo1");
		$tipo2 = Util::getParameter("tipo2");
		$dependencia = Util::getParameter("dependencia");
		$categoria = Util::getParameter("categoria");

		$codigoRespuesta = $usuarios->updateUsuario($id, $dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo1, $tipo2, $dependencia, $categoria);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario modificado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo modificar verifica los datos (".$codigoRespuesta.")");
		}
	}else if($accion=='crearUsuario'){
		$dni = Util::getParameterNumeroCadena("dni");
		$nombres = Util::getParameter("nombres");
		$apellidos = Util::getParameter("apellidos");
		$correo = Util::getParameter("correo");
		$telefono = Util::getParameterNumeroCadena("telefono");
		$direccion = Util::getParameter("direccion");
		$departamento = Util::getParameter("departamento");
		$area = Util::getParameter("area");
		$numeroProcesos = Util::getParameterDefaultZero("numeroProcesos");
		$funcion = Util::getParameter("funcion");
		$genero = Util::getParameter("genero");
		$tipo1 = Util::getParameter("tipo1");
		$tipo2 = Util::getParameter("tipo2");
		$dependencia = Util::getParameter("dependencia");
		$categoria = Util::getParameter("categoria");

		$codigoRespuesta = $usuarios->crearUsuario($dni, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo1,$tipo2, $dependencia, $categoria);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Usuario creado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo crear usuario verifica los datos (".$codigoRespuesta.")");
		}

	}else if($accion=='modificarEstudiante'){
		$id = Util::getParameter("id");
		$dni = Util::getParameterNumeroCadena("dni");
		$cui = Util::getParameterNumeroCadena("cui");
		$nombres = Util::getParameter("nombres");
		$apellidos = Util::getParameter("apellidos");
		$correo = Util::getParameter("correo");
		$telefono = Util::getParameterNumeroCadena("telefono");
		$direccion = Util::getParameter("direccion");
		$categoria = Util::getParameter("categoria");

		$codigoRespuesta = $usuarios->updateEstudiante($id, $dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $categoria);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Estudiante modificado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo modificar estudiante verifica los datos (".$codigoRespuesta.")");
		}

	}else if($accion=='crearEstudiante'){
		$dni = Util::getParameterNumeroCadena("dni");
		$cui = Util::getParameterNumeroCadena("cui");
		$nombres = Util::getParameter("nombres");
		$apellidos = Util::getParameter("apellidos");
		$correo = Util::getParameter("correo");
		$telefono = Util::getParameterNumeroCadena("telefono");
		$direccion = Util::getParameter("direccion");
		$categoria = Util::getParameter("categoria");

		$codigoRespuesta = $usuarios->crearEstudiante($dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $categoria);
		if($codigoRespuesta==0){
			echo Util::jsonMensaje(false, "Estudiante creado correctamente");
		}else{
			echo Util::jsonMensaje(true, "No se pudo crear estudiante verifica los datos (".$codigoRespuesta.")");
		}
	}

	//PARA VERIFICAR CANTIDADES
	else if($accion=='verificarDniRepetido'){
		$id = Util::getParameter("id");
		$dni = Util::getParameterNumeroCadena("dni");
		$tipo = Util::getParameter("tipo");

		$usuarios = $usuarios->getCantidadDni($id, $dni);
		$cantidadRegistros = count($usuarios);

		if($cantidadRegistros==0){
			echo Util::jsonMensaje(false, "DNI no repetido");
		}else{
			echo Util::jsonMensaje(true, "El DNI esta repetido por otro usuario (".$usuarios[0]['UsuApe']." ".$usuarios[0]['UsuNom']." - TIPO: ".$usuarios[0]['GenSubCat'].")");
		}
	}else if($accion=='verificarCuiRepetido'){
		$id = Util::getParameter("id");
		$cui = Util::getParameterNumeroCadena("cui");
		$tipo = Util::getParameter("tipo");

		$usuarios = $usuarios->getCantidadCui($id, $cui, $tipo);
		$cantidadRegistros = count($usuarios);

		if($cantidadRegistros==0){
			echo Util::jsonMensaje(false, "CUI no repetido");
		}else{
			echo Util::jsonMensaje(true, "El CUI esta repetido por otro estudiante (".$usuarios[0]['UsuApe']." ".$usuarios[0]['UsuNom'].")");
		}
		
	}else if($accion=='verificarCorreoRepetido'){
		$id = Util::getParameter("id");
		$correo = Util::getParameter("correo");
		$tipo = Util::getParameter("tipo");

		$usuarios = $usuarios->getCantidadCorreo($id, $correo);
		$cantidadRegistros = count($usuarios);

		if($cantidadRegistros==0){
			echo Util::jsonMensaje(false, "CORREO no repetido");
		}else{
			echo Util::jsonMensaje(true, "El CORREO esta repetido por otro usuario (".$usuarios[0]['UsuApe']." ".$usuarios[0]['UsuNom']." - TIPO: ".$usuarios[0]['GenSubCat'].")");
		}
	}

	if(in_array(Codigos::codAdministrador,$_SESSION["tipo"])) {
		if($accion=='mostrarOperadores'){
			$busqueda = Util::getParameterBusqueda("busqueda");
			$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
			$registrosPorPagina = Util::getParameter("registrosPorPagina");
			$adyacentes = 4;
			$cantidadRegistros = $usuarios->getCantidadOperadores($busqueda);
			$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
			$offset = ($pagina - 1) * $registrosPorPagina;
			$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

			$operadores = $usuarios->getOperadores($busqueda, $offset, $registrosPorPagina);
			$pie = paginate($pagina, $totalPaginas, $adyacentes);
			echo Util::jsonMensaje(false, $operadores, $pie);

		} else if($accion=='modificarOperador'){
			$id = Util::getParameter("id");
			$nombres = Util::getParameter("nombres");
			$apellidos = Util::getParameter("apellidos");
			$correo = Util::getParameter("correo");
			$telefono = Util::getParameterNumeroCadena("telefono");
			$direccion = Util::getParameter("direccion");
			$usuarioNombre = Util::getParameterNumeroCadena("usunom");
			$usuarioContrasena = Util::getParameterNumeroCadena("usucon");

			$usuario = $usuarios->getUsuarioLogin($usuarioNombre, $usuarioContrasena);
			$cantidad = count($usuario);
			if($cantidad == 0 || ($cantidad == 1 && $usuario[0]['UsuId']==$id)){
				$codigoRespuesta = $usuarios->updateUsuarioCuenta($id, $nombres, $apellidos, $correo, $telefono, $direccion, $usuarioNombre, $usuarioContrasena, Codigos::codActivo);
				if($codigoRespuesta==0){
					echo Util::jsonMensaje(false, "Operador modificado correctamente");
				}else{
					echo Util::jsonMensaje(true, "No se pudo modificar operador verifica los datos (".$codigoRespuesta.")");
				}
			}else{
				echo Util::jsonMensaje(true, "Ya existe el nombre de usuario y contraseña por otro usuario");
			}
			
		}else if($accion=='crearOperador'){
			$nombres = Util::getParameter("nombres");
			$apellidos = Util::getParameter("apellidos");
			$correo = Util::getParameter("correo");
			$telefono = Util::getParameterNumeroCadena("telefono");
			$direccion = Util::getParameter("direccion");
			$usuarioNombre = Util::getParameterNumeroCadena("usunom");
			$usuarioContrasena = Util::getParameterNumeroCadena("usucon");

			$usuario = $usuarios->getUsuarioLogin($usuarioNombre, $usuarioContrasena);
			$cantidad = count($usuario);
			if($cantidad == 0){
				$codigoRespuesta = $usuarios->crearOperadorAdministrador($nombres, $apellidos, $correo, $telefono, $direccion, Codigos::codOperador, $usuarioNombre, $usuarioContrasena);
				if($codigoRespuesta==0){
					echo Util::jsonMensaje(false, "Operador creado correctamente");
				}else{
					echo Util::jsonMensaje(true, "No se pudo crear operador verifica los datos (".$codigoRespuesta.")");
				}
			}else{
				echo Util::jsonMensaje(true, "Ya existe el nombre de usuario y contraseña por otro usuario");
			}
			
		}else if($accion=='mostrarAdministradores'){
			$busqueda = Util::getParameterBusqueda("busqueda");
			$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
			$registrosPorPagina = Util::getParameter("registrosPorPagina");
			$adyacentes = 4;
			$cantidadRegistros = $usuarios->getCantidadAdministradores($busqueda);
			$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
			$offset = ($pagina - 1) * $registrosPorPagina;
			$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

			$administradores = $usuarios->getAdministradores($busqueda, $offset, $registrosPorPagina);
			$pie = paginate($pagina, $totalPaginas, $adyacentes);
			echo Util::jsonMensaje(false, $administradores, $pie);

		}else if($accion=='modificarAdministrador'){
			$id = Util::getParameter("id");
			$nombres = Util::getParameter("nombres");
			$apellidos = Util::getParameter("apellidos");
			$correo = Util::getParameter("correo");
			$telefono = Util::getParameterNumeroCadena("telefono");
			$direccion = Util::getParameter("direccion");
			$usuarioNombre = Util::getParameterNumeroCadena("usunom");
			$usuarioContrasena = Util::getParameterNumeroCadena("usucon");

			$usuario = $usuarios->getUsuarioLogin($usuarioNombre, $usuarioContrasena);
			$cantidad = count($usuario);

			if($cantidad == 0 || ($cantidad == 1 && $usuario[0]['UsuId']==$id)){
				$codigoRespuesta = $usuarios->updateUsuarioCuenta($id, $nombres, $apellidos, $correo, $telefono, $direccion, $usuarioNombre, $usuarioContrasena, Codigos::codActivo);
				if($codigoRespuesta==0){
					echo Util::jsonMensaje(false, "Administrador modificado correctamente");
				}else{
					echo Util::jsonMensaje(true, "No se pudo modificar administrador verifica los datos (".$codigoRespuesta.")");
				}
			}else{
				echo Util::jsonMensaje(true, "Ya existe el nombre de usuario y contraseña por otro usuario");
			}
			
		}else if($accion=='crearAdministrador'){
			$nombres = Util::getParameter("nombres");
			$apellidos = Util::getParameter("apellidos");
			$correo = Util::getParameter("correo");
			$telefono = Util::getParameterNumeroCadena("telefono");
			$direccion = Util::getParameter("direccion");
			$usuarioNombre = Util::getParameterNumeroCadena("usunom");
			$usuarioContrasena = Util::getParameterNumeroCadena("usucon");

			$usuario = $usuarios->getUsuarioLogin($usuarioNombre, $usuarioContrasena);
			$cantidad = count($usuario);
			if($cantidad == 0){
				$codigoRespuesta = $usuarios->crearOperadorAdministrador($nombres, $apellidos, $correo, $telefono, $direccion, Codigos::codAdministrador, $usuarioNombre, $usuarioContrasena);
				if($codigoRespuesta==0){
					echo Util::jsonMensaje(false, "Administrador creado correctamente");
				}else{
					echo Util::jsonMensaje(true, "No se pudo crear administrador verifica los datos (".$codigoRespuesta.")");
				}
			}else{
				echo Util::jsonMensaje(true, "Ya existe el nombre de usuario y contraseña por otro usuario");
			}
			
		}else if($accion=='eliminarOperadorAdministrador'){
			$id = Util::getParameter("id");

			$codigoRespuesta = $usuarios->eliminarUsuario($id);
			if($codigoRespuesta==0){
				echo Util::jsonMensaje(false, "Eliminado CORRECTAMENTE");
			}else{
				echo Util::jsonMensaje(true, "No se pudo ELIMINAR verifica los datos (".$codigoRespuesta.")");
			}
			
		}
		else if($accion=='eliminarDocenteAdministrativo'){
			$id = Util::getParameter("id");
			$codigoRespuesta = $usuarios->eliminarDocenteAdministrativo($id);
			if($codigoRespuesta==0){
				echo Util::jsonMensaje(false, "Eliminado CORRECTAMENTE");
			}else if($codigoRespuesta == 1451){
				echo Util::jsonMensaje(true, "Este usuario ya a sido usado en un proceso por lo que no se puede eliminar");
			}else{
				echo Util::jsonMensaje(true, "No se pudo ELIMINAR verifica los datos (".$codigoRespuesta.")");
			}
			
		}
	}
	
}


?>