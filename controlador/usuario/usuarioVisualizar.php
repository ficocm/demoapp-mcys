<?php

//Archivo de configuracion
require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');

//Llamada al modelo
require_once("../../modelo/usuario/areaModelo.php");
require_once("../../modelo/usuario/departamentoModelo.php");
require_once("../../modelo/usuario/generalModelo.php");
require_once("../../modelo/usuario/usuarioModeloVis.php");

//include '../asignacion/paginacion.php';

$usuarios = new usuarioModeloVis();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";
	
if($accion=='iniciarSesion'){
	$usuario = Util::getParameter("user");
	$contrasena = Util::getParameter("pass");
	$usuario = $usuarios->getUsuarioLogin($usuario, $contrasena);
	$cantidad = count($usuario)."\n<br>";
	if($cantidad==0){
		echo Util::jsonMensaje(true, "Usuario o Contrasena INVALIDA");
	}else if($cantidad>1){
		echo Util::jsonMensaje(true, "Otro usuario tiene el mismo usuario y contrasena");
	}else if($cantidad==1){
		session_start();
		$_SESSION["idUsuario"]=$usuario[0]["UsuId"];
		$_SESSION["nombres"]=$usuario[0]["UsuNom"];
		$_SESSION["apellidos"]=$usuario[0]["UsuApe"];
		$_SESSION["tipo"]=$usuario[0]["UsuFKTip"];
		echo Util::jsonMensaje(false, "Inicio de Sesion CORRECTO");
	}
}
//MOSTRAR
else if($accion=='mostrarDatos'){
	$busqueda = Util::getParameterBusqueda("busqueda");

	$docente = $usuarios->getDatos($busqueda);
	//echo $docente;

	echo Util::jsonMensaje(false, $docente);

}else if($accion=='borrarIncrip'){

	$busqueda = Util::getParameterBusqueda("busqueda");
	$idAdm = Util::getParameterBusqueda("idAdm");
	$codCargo=0;
	$resp1=$usuarios->borrarRegistro($busqueda,$idAdm);
	//$resp2 = $usuarios->getDatosAdmiUsu($busqueda);
	//$cantidad = count($resp2);
	if($resp1==0){
		echo Util::jsonMensaje(false, "Usuario borrado");
	}else {
		echo Util::jsonMensaje(true, "Usuario no borrado"+$codCargo);
	}

}else if($accion=='regIncrip'){
	$tipo=Util::getParameter("categoria");
	
	$idAdm=Util::getParameter("idAdm");
	$busqueda = Util::getParameterBusqueda("id");

		$resp1=$usuarios->crearRegistro($busqueda,$tipo,$idAdm);
		if($resp1==0){
			echo Util::jsonMensaje(false, "Usuario creado");
		}else {
			echo Util::jsonMensaje(true, "Usuario no creado");
		}

	//$resp2 = $usuarios->getDatosAdmiUsu($busqueda);
	//$cantidad = count($resp2);
}
else if($accion=='verifUsuAdmi'){

	$busqueda = Util::getParameterBusqueda("busqueda");
	$id=Util::getParameterBusqueda("id");

	$resp = $usuarios->getDatosAdmiUsu($busqueda,$id);
	
	$cantidad = count($resp);

	if($cantidad==0){
		echo Util::jsonMensaje(true, "Ninguno");
	
	}else{
		$estRes=$resp[0]["AdmDetFKEstReg"];	
		$cabId=$resp[0]["AdmDetFKAdmCabId"];
		if($estRes==6){
			echo Util::jsonMensaje(true, "Ninguno", $cabId);	
		}else if($estRes==7){
			$cargo=$resp[0]["CarDes"];
			echo Util::jsonMensaje(false, $cargo, $cabId);
		}
	}
	
	//echo $docente;

	

}else if($accion=='verificarDatos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$docente = $usuarios->verificarDatos($busqueda);

	$cantidad = count($docente);
	if($cantidad==0){
		echo Util::jsonMensaje(true, "No esta inscrito");
	}else if($cantidad>1){
		echo Util::jsonMensaje(true, "Hay un error");
	}else if($cantidad==1){
		echo Util::jsonMensaje(false, $docente);
	}

}else if($accion=='getCargo'){

	$busqueda = Util::getParameterBusqueda("busqueda");
	$idAdm = Util::getParameterBusqueda("idAdm");

	$resp = $usuarios->getDatosAdmiUsu($busqueda,$idAdm);
	
	$cantidad = count($resp);
	if($cantidad==0){
		echo Util::jsonMensaje(true, "No esta en la cabeceraDetalle");
	}else{
		$estRes=$resp[0]["AdmDetFKEstReg"];	
		if($estRes==6){
			echo Util::jsonMensaje(true, "registrado estado 6");	
		}else if($estRes==7){
			$cargo = $usuarios->getCargo($busqueda,$idAdm);
			echo Util::jsonMensaje(false, $cargo);	
		}
	}
}else if($accion=='getEstReg'){

	$busqueda = Util::getParameterBusqueda("busqueda");

	$resp = $usuarios->getDatosAdmiUsu($busqueda);
	
	$cantidad = count($resp);
	if($cantidad==0){
		echo Util::jsonMensaje(true, "No esta en la cabeceraDetalle");
	}else{
		$estRes=$resp[0]["AdmDetFKEstReg"];	
		echo Util::jsonMensaje(false, $estRes);	
	}
}
else if($accion=='mostrarAdmi'){
	$admi = $usuarios->getDatosAdmi();
	//echo $docente;
	echo Util::jsonMensaje(false, $admi);

}else if($accion=='mostrarAdministrativos'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $usuarios->getCantidadAdministrativos($busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $usuarios->getAdministrativos($busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);

}else if($accion=='mostrarEstudiantes'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $usuarios->getCantidadEstudiantes($busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$estudiantes = $usuarios->getEstudiantes($busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $estudiantes, $pie);

}else if($accion=='mostrarOperadores'){
	$operadores = $usuarios->getOperadores();
	echo json_encode($operadores);

}else if($accion=='mostrarAdministradores'){
	$administradores = $usuarios->getAdministradores();
	echo json_encode($administradores);

}else if($accion=='mostrarAreas'){
	$area = new areaModelo();
	$areas = $area->getAreas();
	echo json_encode($areas);

}else if($accion=='mostrarDepartamentos'){
	$departamento = new departamentoModelo();
	$departamentos = $departamento->getDepartamentos();
	echo json_encode($departamentos);

}else if($accion=='mostrarTiposUsuario'){
	$general = new generalModelo();
	$tiposUsuario = $general->getTiposDeUsuario();
	echo json_encode($tiposUsuario);

}else if($accion=='mostrarCategorias'){
	$general = new generalModelo();
	$categorias = $general->getCategorias();
	echo json_encode($categorias);

}else if($accion=='mostrarGeneros'){
	$general = new generalModelo();
	$generos = $general->getGeneros();
	echo json_encode($generos);

}else if($accion=='mostrarCargosDocente'){
	$general = new generalModelo();
	$cargos = $general->getCargosDocente();
	echo json_encode($cargos);

}else if($accion=='mostrarCargosDocenteComision'){
	$general = new generalModelo();
	$cargos = $general->getCargosDocenteComision();
	echo json_encode($cargos);

}
//MODIFICAR
else if($accion=='modificarUsuario'){
	$id = Util::getParameter("id");
	$dni = Util::getParameter("dni");
	$cui = Util::getParameter("cui");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$departamento = Util::getParameter("departamento");
	$area = Util::getParameter("area");
	$numeroProcesos = Util::getParameter("numeroProcesos");
	$funcion = Util::getParameter("funcion");
	$genero = Util::getParameter("genero");
	$tipo = Util::getParameter("tipo");
	$dependencia = Util::getParameter("dependencia");
	$categoria = Util::getParameter("categoria");
	$estadoRegistro = Util::getParameter("estadoRegistro");

	$codigoRespuesta = $usuarios->updateUsuario($id, $dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo, $dependencia, $categoria,$estadoRegistro);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Usuario modificado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo modificar verifica los datos (".$codigoRespuesta.")");
	}
}else if($accion=='crearUsuario'){
	$dni = Util::getParameter("dni");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$departamento = Util::getParameter("departamento");
	$area = Util::getParameter("area");
	$numeroProcesos = Util::getParameter("numeroProcesos");
	$funcion = Util::getParameter("funcion");
	$genero = Util::getParameter("genero");
	$tipo = Util::getParameter("tipo");
	$dependencia = Util::getParameter("dependencia");
	$categoria = Util::getParameter("categoria");

	$codigoRespuesta = $usuarios->crearUsuario($dni, $nombres, $apellidos, $correo, $telefono, $direccion, $departamento, $area, $numeroProcesos, $funcion, $genero, $tipo, $dependencia, $categoria);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Usuario creado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo crear usuario verifica los datos (".$codigoRespuesta.")");
	}

}else if($accion=='crearEstudiante'){
	$dni = Util::getParameter("dni");
	$cui = Util::getParameter("cui");
	$nombres = Util::getParameter("nombres");
	$apellidos = Util::getParameter("apellidos");
	$correo = Util::getParameter("correo");
	$telefono = Util::getParameter("telefono");
	$direccion = Util::getParameter("direccion");
	$tipo = Util::getParameter("tipo");
	$categoria = Util::getParameter("categoria");

	$codigoRespuesta = $usuarios->crearEstudiante($dni, $cui, $nombres, $apellidos, $correo, $telefono, $direccion,  $tipo, $categoria);
	if($codigoRespuesta==0){
		echo Util::jsonMensaje(false, "Estudiante creado correctamente");
	}else{
		echo Util::jsonMensaje(true, "No se pudo crear estudiante verifica los datos (".$codigoRespuesta.")");
	}
}
/*else if($accion=='leerDocentesSorteados'){
	$usuarios=$admisionDetalle->getUsuariosDocentesSorteados();
	echo json_encode($usuarios);
}else if($accion == 'eliminarCargos'){
	$id = $_GET['id'];
	if(esIdValido($id)){
		$res = $admisionDetalle->eliminarCargosYLugares($id);
		if($res == 0){ //Devuelve error si es 0 no huvo error
			echo jsonMensaje(false, "Se Elimino los lugares y el cargo Correctamente");
		}else{
			echo jsonMensaje(true, "No se pudo eliminar cargos ni lugares");
		}
	}else{
		echo jsonMensaje(true, "Datos inválido");
	}
}else if($accion=="actualizarCargosYLugares"){
	//Se tiene que validar cada entrada del formulario
	$area1 = getPostId('area');
	$pabellon1 = getPostId('pabellon1');
	$cargo1 = getPostId('cargo1');
	$puerta1 = getPostId('puerta1');
	$pabellon2 = getPostId('pabellon2');
	$cargo2 = getPostId('cargo2');
	$puerta2 = getPostId('puerta2');
	$aula = getPostId('aula');
	$area2 = ($pabellon2!="NULL" || $cargo2!="NULL")? $area1 : "NULL";
	$array = array();
	foreach ($_POST['ids'] as $key => $id) {
		$array[] = $admisionDetalle->updateAdmisionDetallePuestos($id, $area1, $pabellon1, $aula, $cargo1, $puerta1, $area2, $pabellon2, $cargo2, $puerta2)." Registro exitoso";
	}
	echo json_encode($array);
}else{
	echo jsonMensaje(true, "Error en URL");
}
*/
?>