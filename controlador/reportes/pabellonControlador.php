<?php
//Archivo de configuracion
require_once("../../conexion/db.php");
//require_once("../util/util.php");

//Llamada al modelo
require_once("../../modelo/reportes/pabellonModelo.php");
$pabellon = new pabellonModelo();
$pabellones=$pabellon->getPabellones();

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

if($accion=='leer'){
	$json = json_encode($pabellones);
	echo $json;
}else if($accion=="nombreDpto"){
	$dpto = $pabellon->nombreDpto();
	echo json_encode($dpto);
}else if($accion == 'busquedaPabellon'){
	$busqueda = Util::getParameterBusqueda("busqueda");
	$pagina = Util::getParameter("pagina")=="NULL"? 1 : Util::getParameter("pagina");
	$registrosPorPagina = Util::getParameter("registrosPorPagina");
	$adyacentes = 4;
	$cantidadRegistros = $admisionDetalle->getCantidadAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda);
	$cantidadRegistros = $cantidadRegistros[0]['numeroRegistros'];
	$offset = ($pagina - 1) * $registrosPorPagina;
	$totalPaginas = ceil($cantidadRegistros/$registrosPorPagina);

	$administrativos = $admisionDetalle->getAdministrativosConSinInscripcionOParticipacion($idProceso, $busqueda, $offset, $registrosPorPagina);
	$pie = paginate($pagina, $totalPaginas, $adyacentes);
	echo Util::jsonMensaje(false, $administrativos, $pie);

}else{	
	echo jsonMensaje(true, "Error en URL");
}
?>
