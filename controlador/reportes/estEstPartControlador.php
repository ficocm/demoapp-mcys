<?php
//Archivo de configuracion
require_once("../../conexion/db.php");
//require_once("../util/util.php");

//Llamada al modelo
require_once("../../modelo/reportes/estEstPartModelo.php");
$estudiantes = new estEstPartModelo();
$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

if($accion=='cargarCargos'){
	$cargos = $estudiantes->cargarCargos();
	echo json_encode($cargos);
}else if ($accion=="cargarEst") {
	$est = $estudiantes->cargarEst();
	echo json_encode($est);
}else{	
	echo jsonMensaje(true, "Error en URL");
}
?>
