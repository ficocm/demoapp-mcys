<?php
//Archivo de configuracion
require_once("../../conexion/db.php");
//require_once("../util/util.php");

//Llamada al modelo
require_once("../../modelo/reportes/reporteModelo.php");
session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}


$reporte = new reporteModelo();
$areas=$reporte->getAreas();
$cargos=$reporte->getCargos();
$usuarios=$reporte->getUsuarios();

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

$idArea = isset($_GET['idArea']) ? $_GET['idArea'] : "";
$idPab = isset($_GET['idPab']) ? $_GET['idPab'] : "";
$idPabG = isset($_GET['idPabG']) ? $_GET['idPabG'] : "";
$idAreaAdm = isset($_GET['idAreaAdm']) ? $_GET['idAreaAdm'] : "";
$idAreaDoc = isset($_GET['idAreaDoc']) ? $_GET['idAreaDoc'] : "";


if($accion=='leer'){
	$json = json_encode($areas);
	echo $json;
}else if($accion=="listarCargos"){
	$json = json_encode($cargos);
	echo $json;
}else if($accion=="leerAulas"){
	$aulas=$reporte->getAulas($idArea,$idPab);
	$json = json_encode($aulas);
	echo $json;
}else if($accion=="leerAdmComAdm"){
	$comAdm=$reporte->getComAdm($idAreaAdm,$idProceso);
	$json = json_encode($comAdm);
	echo $json;
}else if($accion=="leerAdmComDoc"){
	$comAdm=$reporte->getComDoc($idAreaDoc,$idProceso);
	$json = json_encode($comAdm);
	echo $json;
}else if($accion=="listarReemplazos"){
	$comAdm=$reporte->getReemplazos($idProceso);
	$json = json_encode($comAdm);
	echo $json;
}else if($accion=="leerGuraderos"){
	$guarderos=$reporte->getGuarderos	($idProceso,$idPabG);
	$json = json_encode($guarderos);
	echo $json;
}else if($accion=="listarUsuarios"){
	$json = json_encode($usuarios);
	echo $json;
}else{
	echo jsonMensaje(true, "Error en URL");
}
?>
