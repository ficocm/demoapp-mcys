<?php
//Archivo de configuracion
require_once("../../conexion/db.php");
//require_once("../util/util.php");

//Llamada al modelo
require_once("../../modelo/reportes/areaModelo.php");
$area = new areaModelo();
$areas=$area->getAreas();

$accion = isset($_GET['accion']) ? $_GET['accion'] : "";

if($accion=='leer'){
	$json = json_encode($areas);
	echo $json;
}else{
	echo jsonMensaje(true, "Error en URL");
}
?>