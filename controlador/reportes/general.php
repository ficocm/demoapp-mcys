<?php

require_once("../../conexion/conexion.php");
require_once('../../modelo/usuario/util.php');
require_once("../../modelo/reportes/general.php");

session_start();
$idProceso = $_SESSION["idProceso"];

if($idProceso==NULL || $idProceso==""){
	echo Util::jsonMensaje(true, "Inicie session y seleccione un proceso de admision", "../../index.php");
	die();
	return;
}

$general = new general();

$pabellones = $general->getPabellones($idProceso);


//include the file that loads the PhpSpreadsheet classes
require 'vendor/autoload.php';
 
//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 
//object of the Spreadsheet class to create the excel data
$spreadsheet = new Spreadsheet();

$spreadsheet->getDefaultStyle()->getFont()->setSize(8);	

$spreadsheet->getActiveSheet()->setCellValue('B1', 'EVALUACION');
$spreadsheet->getActiveSheet()->setCellValue('B2', 'CONSOLIDADO DE DE ORGANIZACION Y RESPONSABLES');

$spreadsheet->getActiveSheet()->mergeCells('B2:N2');

$rowArray = ['AREAS', 'PAB.', 'PABELLON', 'AULAS', 'TOT. AULAS', 'TECNICOS', 'AYUD. TEC.', 'CONTROLADORES', 'SUPERVISOR', 'GUARDEROS', 'AFORO', 'CAPACIDAD', 'CAP. x AREA'];
$spreadsheet->getActiveSheet()
    ->fromArray(
        $rowArray,   // The data to set
        NULL,        // Array values with this value will not be set
        'B3'         // Top left coordinate of the worksheet range where
                     //    we want to set these values (default is A1)
    );


//set style for A1,B1,C1 cells
$cell_st =[
 'font' =>['bold' => true],
 'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
];
$spreadsheet->getActiveSheet()->getStyle("B1:N3")->applyFromArray($cell_st);

$izq="B";
$cont=4;
$contPab=1;
$contTec=0;
$auxArea='';
$auxAreaCont=4;

while($row = $pabellones->fetch_assoc()){

	$tecnicos = $general->getTecnicos($idProceso, $row['AdmPabId']);
	$contTec=$cont;
	while($row2 = $tecnicos->fetch_assoc()){
		$rowArray = [$row['area'], $contPab, $row['nombre'], $row['aulas'], '', $row2['UsuNom'].$row2['UsuApe'], $row['cantAyudantes'], $row['cantControladores'], $row['supervisor'], $row['guardero'], $row['aforo'], $row['capacidadAsignada'], ''];
		$spreadsheet->getActiveSheet()->fromArray($rowArray, NULL, $izq.$cont);
		$cont=$cont+1;

	}
	if($contTec<$cont){
		$spreadsheet->getActiveSheet()->mergeCells('C'.$contTec.':C'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('D'.$contTec.':D'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('E'.$contTec.':E'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('H'.$contTec.':H'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('I'.$contTec.':I'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('K'.$contTec.':K'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('L'.$contTec.':L'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('M'.$contTec.':M'.($cont-1));
	}


	if($row['area']!=$auxArea && $auxArea!='' && $auxAreaCont<$cont){
		$spreadsheet->getActiveSheet()->mergeCells('B'.$auxAreaCont.':B'.($cont-1));
		$spreadsheet->getActiveSheet()->mergeCells('F'.$auxAreaCont.':F'.($cont-1));
		$spreadsheet->getActiveSheet()->setCellValue('F'.$auxAreaCont,'=SUM(E'.$auxAreaCont.':E'.($cont-1).')');
		$spreadsheet->getActiveSheet()->mergeCells('N'.$auxAreaCont.':N'.($cont-1));
		$spreadsheet->getActiveSheet()->setCellValue('N'.$auxAreaCont,'=SUM(M'.$auxAreaCont.':M'.($cont-1).')');
		$auxAreaCont=$cont;
	}
	$auxArea=$row['area'];


	$contPab=$contPab+1;
}

if($auxAreaCont<$cont){
	$spreadsheet->getActiveSheet()->mergeCells('B'.$auxAreaCont.':B'.($cont-1));
	$spreadsheet->getActiveSheet()->mergeCells('F'.$auxAreaCont.':F'.($cont-1));
	$spreadsheet->getActiveSheet()->setCellValue('F'.$auxAreaCont,'=SUM(E'.$auxAreaCont.':E'.($cont-1).')');
	$spreadsheet->getActiveSheet()->mergeCells('N'.$auxAreaCont.':N'.($cont-1));
	$spreadsheet->getActiveSheet()->setCellValue('N'.$auxAreaCont,'=SUM(M'.$auxAreaCont.':M'.($cont-1).')');
}


$spreadsheet->getActiveSheet()->setCellValue('E'.$cont,'=SUM(E4:E'.($cont-1).')');
$spreadsheet->getActiveSheet()->setCellValue('I'.$cont,'=SUM(I4:I'.($cont-1).')');
$spreadsheet->getActiveSheet()->setCellValue('H'.$cont,'=SUM(H4:H'.($cont-1).')');
$spreadsheet->getActiveSheet()->setCellValue('N'.$cont,'=SUM(N4:N'.($cont-1).')');


$cell_st =[
 'alignment' =>['vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER],
 'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
    ]
];
$spreadsheet->getActiveSheet()->getStyle('B3:N'.($cont-1))->applyFromArray($cell_st);

$spreadsheet->getActiveSheet()->getStyle('E'.$cont)->applyFromArray($cell_st);
$spreadsheet->getActiveSheet()->getStyle('H'.$cont)->applyFromArray($cell_st);
$spreadsheet->getActiveSheet()->getStyle('I'.$cont)->applyFromArray($cell_st);
$spreadsheet->getActiveSheet()->getStyle('N'.$cont)->applyFromArray($cell_st);

$styleArray = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => ['argb' => 'FFFF0000'],
        ],
    ],
];

//$spreadsheet->getStyle('B3:N15')->applyFromArray($styleArray);


//set cells' width 
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

$spreadsheet->getActiveSheet()->getPageSetup()->setVerticalCentered(true);




//make object of the Xlsx class to save the excel file
$writer = new Xlsx($spreadsheet);
$fxls ="excel-file_1.xlsx";
header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename="CONSOLIDADO_ DE_ORGANIZACION_Y_RESPONSABLES.xlsx"');
$writer->save('php://output');

?>