<?php
require_once("../../modelo/usuario/util.php");
session_start();
if (!empty($_POST["idProceso"]) || $_POST["idProceso"]!=NULL){
	$_SESSION["idProceso"]= $_POST["idProceso"];
	$_SESSION["nombreProceso"]= $_POST["nombreProceso"];
	$_SESSION["faseProceso"]= $_POST["faseProceso"];
	$_SESSION["fechaProceso"]= $_POST["fechaProceso"];
	$_SESSION["anioProceso"]= $_POST["anioProceso"];
	$_SESSION["estadoProceso"]= $_POST["estadoProceso"];
	$_SESSION["numEvaProceso"]= $_POST["numEvaProceso"];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts 
-->
	<link rel="stylesheet" type="text/css" href="../../css/component.css" />

<!-- SCRIPTS -->
<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		if($_SESSION["tipo"] != NULL || !empty($_SESSION["tipo"])){
			$tipo = $_SESSION["tipo"];
			$userId = $_SESSION["idUsuario"];
			$nombres = $_SESSION["nombres"];
			$apellidos = $_SESSION["apellidos"];
		}
		
		//if($_SESSION["idProceso"]==NULL AND (in_array(Codigos::codAdministrador, $tipo) || In_array(Codigos::codOperador, $tipo))){
			include '../../menu.php';
		//}
		?>
		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h2 class="page-header" style="color:#5e151d;">Bienvenido(a) <?php echo $nombres?></h2>
						
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="80%" height="80%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->
				<?php if($_SESSION["idProceso"] != NULL) { ?>
					<h3 style="color:#5e151d;">Proceso de Admision: <?php echo " ".$_SESSION["nombreProceso"]. " ".$_SESSION["anioProceso"]."  FASE N° ". $_SESSION["faseProceso"]. " (".$_SESSION["fechaProceso"].") ";?> </h3>
                    <a href="../../salirProceso.php" class="btn btn-primary btn-lg" role="button" aria-pressed="true">Salir Proceso</a>
				<?php }else{ if(!in_array(Codigos::codAdministrador, $tipo) && !in_array(Codigos::codOperador, $tipo)) {?>  
					<!-- Contenido para usuarios normales-->
					<h2><p>Sistema de gestión de participantes para la Oficina de Admision UNSA</p></h2>

				<?php }else{?>
					<!-- Contenido para usuarios especiales (administrador, operador)-->
					<h3 style="color:#5e151d;">Seleccione el proceso de admision </h3>
					<div class='col-sm-4 pull-right'>
						<div id="custom-search-input">
							<div class="input-group col-md-12">

								<input type="text" class="form-control" placeholder="Ingrese nombre del proceso o el año"  id="busqueda" onkeyup="cargar(1);" />
								<span class="input-group-btn">
									<button class="btn btn-info" type="button" onclick="cargar(1);">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</span>
							</div>
						</div>
					</div>
					<br>
					<div>
						<section class="color-4" id="section-4">
							<!--
							<p>
								<button id="seleccionar" class="seleccionar seleccionar-4 seleccionar-4c icon-arrow-right" >Proceso de Admision - Ordinario Fase N° 1 (09/09/2018)</button>
							</p>
							-->
						</section>
						<div id="paginacion">
				</div>
					</div>

				<?php } }?>
				
			</div>
			
		</div>
	</div>
	<script src="../../js/procesosAdmisionCabecera.js"></script><!-- Custom Theme JavaScript -->
</body>

</html>
