          <?php
          session_start();
          ?>
          <!DOCTYPE html>
          <html>
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
            <title>OFICINA DE ADMISION</title>

            <!-- ESTILOS -->
            <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
            <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
            <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
            <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
            <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

            <!-- SCRIPTS -->
            <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
            <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
            <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
            <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
            <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
            <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
            <script src="../../js/reporte.js"></script>
            <!--script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
            <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
            <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script-->

              <!--TABLAS -->
              <script type="text/javascript" src="../../css/dataTables.bootstrap.min.css"></script>
              <script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
              <script type="text/javascript" src="../../js/dataTables.bootstrap.min.js"></script>
            </head>
            <body onload="init();">
              <div id="wrapper">
                <!-- Menu -->
                <?php
                include '../../menu.php';
                ?>
                <!-- Header -->
                <div id="page-wrapper">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-lg-8">
                        <h1 class="page-header" style="color:#5e151d;">Reportes / Reemplazos</h1>
                      </div>
                      <div class="col-lg-4">
                        <br>
                        <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
                      </div>
                    </div>  
                    <!-- PAGUE CONTENT -->
                    
                    <h1  ALIGN="justify" style="font-size:23px;">Genere reportes de Reemplazos para el proceso de admisión <?php echo " ".$_SESSION["nombreProceso"]. " ".$_SESSION["anioProceso"]."  FASE N° ".$_SESSION["faseProceso"] ?>  </h1><br>
                    
                    <label id="idProceso" name="idProceso" style='display:none;'> <?php echo $_SESSION["idProceso"]?> </label> 

                    <div class="col-lg-7" align="center" >
                      <div class="panel panel-default">
                        <div class="panel-heading" >
                          <font SIZE=3>Generar Reporte</font><br>
                        </div>
                        <div class="panel-body">
                         <br>
                         <div class="col-lg-6">
                          <label class="control-label col-sm-12"> <font SIZE=5>Generar Reporte:</font></label>
                        </div> 
                        <div class="col-lg-6">
                         <a  class='btn btn-danger' role='button' href='Excel/reporteReemplazados.php'>REPORTE DE REEMPLAZADOS</a>
                       </div>
                       <br> <br> <br> 
                     </div>
                   </div>
                 </div>
                 
                 <div class="col-lg-5" align="center" >
                  <img src="../../img/reemplazo.png" width="60%" height="60%">
                </div>

                <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-heading" >
                      <font SIZE=4>
                        <span >Personal Reemplazado</span>  </font><br>
                      </div>
                      <div class="panel-body">
                       <div id="reemplazos" name="reemplazos" ></div>
                     </div>
                   </div>
                 </div>

               </div>


             </div>
           </div>
         </body>
