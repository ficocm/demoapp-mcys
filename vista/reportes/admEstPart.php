<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
	<script src="../../js/reporte.js"></script>

	<!-- TABLAS -->
	
	
	<script type="text/javascript" src="../../css/dataTables.bootstrap.min.css"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="../../js/dataTables.bootstrap.min.js"></script>
	



</head>

<body onload="init();">
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Reportes / Administrativos Por Estado de Participación</h1>
					</div>

					<label id="idProceso" name="idProceso" style='display:none;'> <?php echo $_SESSION["idProceso"]?> </label> 
					<label id="cargo" name="cargo" style='display:none;'>4</label> 
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  



				<h1  ALIGN="justify" style="font-size:23px;">Genere reportes para comisión de docentes por área del proceso de admisión <?php echo " ".$_SESSION["nombreProceso"]. " ".$_SESSION["anioProceso"]."  FASE N° ".$_SESSION["faseProceso"] ?>  </h1><br>


				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading" >
							<font SIZE=3>
								<span >Seleccione estado de participación</span>  </font><br>
							</div>
							<div class="panel-body">
								<label class="control-label col-sm-5">Estado de participacion:</label>
								<div class="col-sm-7">

									<select class="	form-control" name="estados" id="estados" >
										<option disabled selected value="-1">  Seleccionar  </option>
										<option value="7">  INSCRITOS  </option>
										<option value="5">  SORTEADOS  </option>
										<option value="17"> ASIGNADOS  </option>
									</select>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" >
								<font SIZE=3>Reporte General para Administrativos </font><br>
							</div>
							<div class="panel-body">
								<br>
								<div class="col-lg-4">
									<label class="control-label col-sm-12"> Generar Reporte:</label>
								</div> 
								<div class="col-lg-5">
									<a  class='btn btn-danger' role='button' href='Excel/reporteCargosGeneral.php?idTipoUsu=4&nombreTipo=ADMINISTRATIVOS'>Reporte General</a>
								</div>
								<br> <br> <br> 
							</div>
						</div>
					</div>

					<div class="col-lg-8">
						<div class="panel panel-default">
							<div class="panel-heading" >
								<font SIZE=3>
									<span >Seleccione el cargo de administrativo</span>  </font><br>
								</div>
								<div class="panel-body">
									<div id="checkCargosEst" name="checkCargosEst" ></div>
								</div>
							</div>
						</div>


				<div id="reporteEst" name="reporteEst" >  </div>
				<div id="estudiantes" name="estudiantes" >  </div>

			</div>
		</div>
	</div>

</body>
<br>
