<?php
session_start();


$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$idProceso=$_SESSION["idProceso"];


$admAreId = $_GET['admAreId'];
$areaNom=$_GET['areaNom'];

if (PHP_SAPI == 'cli')
  die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();



// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte administrativos de comision")
->setSubject("Reporte administrativos de comision")
->setDescription("Reporte administrativos de comision")
->setKeywords("Particpantes")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$titulo = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));


//negrita
$objPHPExcel->getActiveSheet()->getStyle('A1:E7')->applyFromArray($boldArray);    

$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($titulo); 
$objPHPExcel->getActiveSheet()->getStyle('A7:E7')->applyFromArray($boldArray);    


//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);



for ($i=1; $i < 8; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(20);
}


for ($i=8; $i < 1000; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(16);
}

$sqlComAdm = "select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as cargo1, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as comision1, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as cargo2, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as comision2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE  admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (usuario.UsuFKTip=4 or usuario.UsuFKTip2=4) and admisiondetalle.AdmDetFKEstReg=17 and (admisiondetalle.AdmDetFKAre=".$admAreId." or admisiondetalle.AdmDetFKAre2=".$admAreId.")";

// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('A2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('A3',"FECHA: ".$fechaProceso)
->setCellValue('A5', " COMISION ADMINISTRATIVOS / ".$areaNom)


->setCellValue('A7', ' # ')
->setCellValue('B7', ' APELLIDOS Y NOMBRES')
->setCellValue('C7', ' DNI ')
->setCellValue('D7', ' CARGO COMISION 1')
->setCellValue('E7', ' CARGO COMISION 2');


$resultado = $mysqli->query($sqlComAdm);
$countCT=1;
$cel=8;
while ($row = $resultado->fetch_assoc()){
  if ($row['comision1']=="SI" || $row['comision2']=="SI") {

    $nombre=$row['nombre'];
    $dni=$row['dni'];  
    $cargo1=$row['cargo1'];  
    $cargo2=$row['cargo2'];  


    $a="A".$cel;
    $b="B".$cel;
    $c="C".$cel;
    $d="D".$cel;
    $e="E".$cel;


    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue($a, $countCT)
    ->setCellValue($b, $nombre)
    ->setCellValue($c, $dni)
    ->setCellValue($d, $cargo1)
    ->setCellValue($e, $cargo2);

    $countCT+=1;
    $cel+=1;
  }
}
if($cel>8){
  /*Fin extracion de datos MYSQL*/
  $rango="A7:$e";
  $styleArray = array('font' => array( 'name' => 'Arial','size' => 9),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
  );
  $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}

// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=AdministrativosComision_$areaNom.xls");
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>  