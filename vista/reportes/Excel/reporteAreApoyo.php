<?php

//$pabId = strtoupper($_POST['pabId']);
//$pabNombre = strtoupper($_POST['pabNombre']);

$areId=$_GET['areId'];
$areNombre=$_GET['areNombre'];

if (PHP_SAPI == 'cli')
	die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();


// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de Asistencias por Pabellon")
->setSubject("Reporte de Asistencias por Pabellon")
->setDescription("Reporte de Asistencias por Pabellon")
->setKeywords("Asistencias por Pabellon")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:D2')->applyFromArray($boldArray);		

//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);  	


$apoyoAdm="select concat(usuario.UsuApe, ', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (AdmDetFKCar = 25 or AdmDetFKCar2 = 25 ) AND (AdmDetFKAre=".$areId ." or AdmDetFKAre=".$areId .")";


// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('B2', "ORDINARIO - I FASE 2019")
->setCellValue('C2', "APOYO ADMINISTRATIVO")
->setCellValue('B3', "9 DE SEPTIEMBRE DEL 2018")
->setCellValue('B4', "LOCAL PABELLON:")
->setCellValue('C4', $areNombre)
->setCellValue('B5', "AREA:")
->setCellValue('B6', "N° DE AULA:")

->setCellValue('A8', ' # ')
->setCellValue('B8', ' APELLIDOS Y NOMBRES ')
->setCellValue('C8', 'DNI')
->setCellValue('D8', 'FIRMA');



$resultado = $mysqli->query($apoyoAdm);


$countCT=1;
$c=0;

$cel=9;
while ($row = $resultado->fetch_assoc()){

	$corTecNom=$row['nombre'];
	$corTecDnoi=$row['dni'];	
	$a="A".$cel;
	$b="B".$cel;
	$c="C".$cel;
	$d="D".$cel;

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue($a, $countCT)
	->setCellValue($b, $corTecNom)
	->setCellValue($c, $corTecDnoi);
	$countCT+=1;
	$cel+=1;
}
$numborder="$cel";
if($cel>9){
	/*Fin extracion de datos MYSQL*/
	$rango="A8:$d";
	$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
		'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
	);
	$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}

// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Reporte de Asistencias');


// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);


// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=NombreDeRporte.xls");
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>