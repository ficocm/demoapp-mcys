<?php

session_start();


$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$idProceso=$_SESSION["idProceso"];


$admAreId = $_GET['admAreId'];
$areaNom=$_GET['areaNom'];

if (PHP_SAPI == 'cli')
  die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();



// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de pabellones participantes por area")
->setSubject("Reporte de pabellones participantes por area")
->setDescription("Reporte de pabellones participantes por area")
->setKeywords("Pabellones participantes")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$titulo = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));


//negrita
$objPHPExcel->getActiveSheet()->getStyle('A1:D7')->applyFromArray($boldArray);    

$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($titulo); 
$objPHPExcel->getActiveSheet()->getStyle('A7:D7')->applyFromArray($boldArray);    


//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(80);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);      


for ($i=1; $i < 8; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(17);
}


for ($i=8; $i < 200; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(25);
}


$sqlPabPart="select AdmPabId, AdmPabFKAdmAreId, pabellon.PabDes, pabellon.PabFKAre FROM admisionpabellon inner JOIN pabellon on AdmPabFKPabId = pabellon.PabId WHERE AdmPabFKEstReg = 18 and AdmPabFKAdmAreId=".$admAreId."";

// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');
$objPHPExcel->setActiveSheetIndex(0)

->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('B2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('B3',"FECHA: ".$fechaProceso)

->setCellValue('B5', "PABELLONES QUE PARTICIPARAN EN EL AREA DE ".$areaNom)

->setCellValue('B7', ' # ')
->setCellValue('C7', ' NOMBRE DE PABELLON ');

$resultado = $mysqli->query($sqlPabPart);
$countCT=1;
$cel=8;
while ($row = $resultado->fetch_assoc()){

  $corTecNom=$row['PabDes'];  
  $b="B".$cel;
  $c="C".$cel;

  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue($b, $countCT)
  ->setCellValue($c, $corTecNom);
  $countCT+=1;
  $cel+=1;
}

if($cel>9){
  /*Fin extracion de datos MYSQL*/
  $rango="B7:$c";
  $styleArray = array('font' => array( 'name' => 'Arial','size' => 11),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
  );
  $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}

// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=Pabellones_$areaNom.xls");
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>
