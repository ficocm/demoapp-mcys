<?php
session_start();

$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$nombreEstado=$_GET['nombreEstado'];
$nombreCargoS=$_GET['nombreCargoS'];

$idCargo=$_GET['idCargo'];
$idProceso= $_GET['idProceso'];



if (PHP_SAPI == 'cli')
  die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();



// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de cargos por area de participacion")
->setSubject("Reporte de cargos por area de participacion")
->setDescription("Reporte de cargos por area de participacion")
->setKeywords("Particpantes")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$titulo = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));


//negrita
$objPHPExcel->getActiveSheet()->getStyle('A1:F7')->applyFromArray($boldArray);    

$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($titulo); 
$objPHPExcel->getActiveSheet()->getStyle('A7:F7')->applyFromArray($boldArray);    


//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(65);      


for ($i=1; $i < 8; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(17);
}


for ($i=8; $i < 1000; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(25);
}


$sqlPabPart="select admisiondetalle.AdmDetId as iddetalle,admisiondetalle.AdmDetFKAdmCabId as idcab , concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, usuario.UsuCorEle as correo, usuario.UsuTel as telefono, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab2 WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon2 FROM usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (AdmDetFKCar=".$idCargo." or AdmDetFKCar2=".$idCargo.") and AdmDetFKEstReg=17 and AdmDetFKAdmCabId=".$idProceso."";



$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('B2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('B3',"FECHA: ".$fechaProceso)
->setCellValue('B5', $nombreCargoS." / ".$nombreEstado)


->setCellValue('A7', ' # ')
->setCellValue('B7', ' NOMBRE ')
->setCellValue('C7', ' DNI ')
->setCellValue('D7', ' CORREO ')
->setCellValue('E7', ' TELEFONO ')
->setCellValue('F7', ' PABELLON ');

$resultado = $mysqli->query($sqlPabPart);
$countCT=1;
$cel=8;
while ($row = $resultado->fetch_assoc()){
  $nombre=$row['nombre'];
  $dni=$row['dni'];  
  $correo=$row['correo'];  
  $telefono=$row['telefono']; 
  $pabellon=$row['pabellon'];  
  $pabellon2=$row['pabellon2'];  
  $asignacion="";
  
  if($pabellon==null){
    $asignacion=$pabellon2;
  }else if($pabellon2==null){
    $asignacion=$pabellon;
  }else{
    $asignacion=$pabellon." - ".$pabellon2; 
  }
  $a="A".$cel;
  $b="B".$cel;
  $c="C".$cel;
  $d="D".$cel;
  $e="E".$cel;
  $f="F".$cel;

  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue($a, $countCT)
  ->setCellValue($b, $nombre)
  ->setCellValue($c, $dni)
  ->setCellValue($d, $correo)
  ->setCellValue($e, $telefono)

  ->setCellValue($f, $asignacion);

  $countCT+=1;
  $cel+=1;
}

if($cel>8){
  /*Fin extracion de datos MYSQL*/
  $rango="A7:$f";
  $styleArray = array('font' => array( 'name' => 'Arial','size' => 11),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
  );
  $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}

// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=$nombreEstado/$nombreCargoS.xls");
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>  