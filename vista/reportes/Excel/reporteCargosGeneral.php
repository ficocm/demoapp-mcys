<?php
session_start();

$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$idProceso=$_SESSION["idProceso"];

$idTipoUsu=$_GET['idTipoUsu'];
$nombreTipo=$_GET['nombreTipo'];

if (PHP_SAPI == 'cli')
  die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();



// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de cargos por area de participacion")
->setSubject("Reporte de cargos por area de participacion")
->setDescription("Reporte de cargos por area de participacion")
->setKeywords("Particpantes")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$titulo = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));


//negrita
$objPHPExcel->getActiveSheet()->getStyle('A1:I7')->applyFromArray($boldArray);    

$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($titulo); 
$objPHPExcel->getActiveSheet()->getStyle('A7:I7')->applyFromArray($boldArray);    


//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(38);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);      
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);    


$objPHPExcel->getActiveSheet()->getStyle('A1:I999')
    ->getAlignment()->setWrapText(true); 

for ($i=1; $i < 8; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(17);
}




//$sqlPabPart="select admisiondetalle.AdmDetId as iddetalle,admisiondetalle.AdmDetFKAdmCabId as idcab , concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, usuario.UsuCorEle as correo, usuario.UsuTel as telefono, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1,(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab2 WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon2, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo2, usuario.UsuFKTip FROM usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE AdmDetFKEstReg=17 and AdmDetFKAdmCabId=".$idProceso." and (usuario.UsuFKTip =".$idTipoUsu." or usuario.UsuFKTip2 =".$idTipoUsu.") ORDER BY concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom)";
$sqlPabPart="select admisiondetalle.AdmDetId as iddetalle,admisiondetalle.AdmDetFKAdmCabId as idcab , concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, usuario.UsuCorEle as correo, usuario.UsuTel as telefono, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1,(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab2 WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon2, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo2, usuario.UsuFKTip FROM usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId left JOIN cargo on cargo.CarId = admisiondetalle.AdmDetFKCar left join general on cargo.CarFKTipUsu = general.GenId WHERE AdmDetFKEstReg=17 and AdmDetFKAdmCabId=".$idProceso." and (usuario.UsuFKTip =".$idTipoUsu." or usuario.UsuFKTip2 =".$idTipoUsu.") and (cargo.CarFKTipUsu = ".$idTipoUsu.") ORDER BY concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom)";


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:D5');


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('B2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('B3',"FECHA: ".$fechaProceso)

->setCellValue('B5',"REPORTE GENERAL DE CARGOS ASIGNADOS PARA ".$nombreTipo)


->setCellValue('A7', ' # ')
->setCellValue('B7', ' NOMBRE ')
->setCellValue('C7', ' DNI ')
->setCellValue('D7', ' CORREO ')
->setCellValue('E7', ' TELEFONO ')
->setCellValue('F7', ' PABELLON1 ')
->setCellValue('G7', ' CARGO1 ');

if($nombreTipo!=="DOCENTES"){
  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue('H7', ' PABELLON2 ')
  ->setCellValue('I7', ' CARGO2 ');
}

$resultado = $mysqli->query($sqlPabPart);
$countCT=1;
$cel=8;
while ($row = $resultado->fetch_assoc()){
  $nombre=$row['nombre'];
  $dni=$row['dni'];  
  $correo=$row['correo'];  
  $telefono=$row['telefono']; 
  $pabellon1=$row['pabellon'];  
  $cargo1=$row['cargo1'];  
  $pabellon2=$row['pabellon2'];  
  $cargo2=$row['cargo2'];


  $a="A".$cel;
  $b="B".$cel;
  $c="C".$cel;
  $d="D".$cel;
  $e="E".$cel;
  $f="F".$cel;
  $g="G".$cel;
  $h="H".$cel;
  $i="I".$cel;

  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue($a, $countCT)
  ->setCellValue($b, $nombre)
  ->setCellValue($c, $dni)
  ->setCellValue($d, $correo)
  ->setCellValue($e, $telefono)
  ->setCellValue($f, $pabellon1)
  ->setCellValue($g, $cargo1)
  ->setCellValue($h, $pabellon2)
  ->setCellValue($i, $cargo2);
  
  $countCT+=1;
  $cel+=1;
}

if($cel>8){
  /*Fin extracion de datos MYSQL*/
  if($nombreTipo=="DOCENTES"){
    $rango="A7:$g";
  }else{
    $rango="A7:$i";
  }
  $styleArray = array('font' => array( 'name' => 'Arial','size' => 9),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
  );
  $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}

// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=ReporteGeneral_$nombreTipo.xls");
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>  