<?php

session_start();

$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$idProceso=$_SESSION["idProceso"];


$admAreId = $_GET['admAreId'];
$areaNom=$_GET['areaNom'];
$limite=0;

if (PHP_SAPI == 'cli')
  die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();



// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de pabellones participantes por area")
->setSubject("Reporte de pabellones participantes por area")
->setDescription("Reporte de pabellones participantes por area")
->setKeywords("Pabellones participantes")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT));
$titulo = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$centro = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A8:Z1000')->applyFromArray($centro); 
//negrita
$objPHPExcel->getActiveSheet()->getStyle('A1:D7')->applyFromArray($boldArray);    

$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($titulo); 
$objPHPExcel->getActiveSheet()->getStyle('A7:D7')->applyFromArray($boldArray);    


//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(16); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);      
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(14);      
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12); 



for ($i=1; $i < 8; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(17);
}


for ($i=8; $i < 200; $i++) { 
  $objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(25);
}


$sqlPab="select AdmPabId, AdmPabFKAdmAreId, pabellon.PabDes, pabellon.PabFKAre FROM admisionpabellon inner JOIN pabellon on AdmPabFKPabId = pabellon.PabId WHERE AdmPabFKEstReg = 18 and AdmPabFKAdmAreId=".$admAreId."";


$objPHPExcel->getActiveSheet()->getStyle('J2:J999')->getAlignment()->setWrapText(true); 
$objPHPExcel->getActiveSheet()->getStyle('A9:B999')->getAlignment()->setWrapText(true); 

// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
$objPHPExcel->setActiveSheetIndex(0)

->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('A2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('A3',"FECHA: ".$fechaProceso)

->setCellValue('A5', "DISTRIBUICION PARA INFORMATICA - AREA ".$areaNom);
$resultado = $mysqli->query($sqlPab);


$cel=8;
$countCT=1;
while ($row = $resultado->fetch_assoc()){

  $limite=0;
  $idPab=$row['AdmPabId'];
  $nomPab=$row['PabDes'];  
  $a="A".$cel;
  $b="B".$cel;
  $c="C".$cel;
  $e="E".$cel;
  $g="G".$cel;
  $f="F".$cel;
  $d="D".$cel;
  $h="H".$cel;
  $i="I".$cel;
  $j="J".$cel;
  $cel+=1;
  $a1="A".$cel;
  $b1="B".$cel;
  $c1="C".$cel;
  $d1="D".$cel;
  $e1="E".$cel;
  $f1="F".$cel;
  $g1="G".$cel;
  $h1="H".$cel;
  $i1="I".$cel;
  $j1="J".$cel;
  $rango2="$a:$j1";
  $rangoLocal="$a:$b";
  
  $rangoPabNom="$d:$g";
  $objPHPExcel->getActiveSheet()->getStyle($rango2)->applyFromArray($boldArray);
  $objPHPExcel->setActiveSheetIndex(0)->mergeCells($rangoLocal);
  
  $objPHPExcel->setActiveSheetIndex(0)->mergeCells($rangoPabNom); 
  $objPHPExcel->setActiveSheetIndex(0)

  
  ->setCellValue($a,'LOCAL N° ')
  
  ->setCellValue($c,'PABELLON')
  ->setCellValue($d,$nomPab)
  ->setCellValue($a1, 'Área')
  ->setCellValue($b1, 'Aula del pabellón')
  ->setCellValue($c1, 'Aforo')
  ->setCellValue($d1, 'Piso')
  ->setCellValue($e1, 'Fila')
  ->setCellValue($f1, 'Columna')
  ->setCellValue($g1, 'Capacidad');

  $sqlPabPart= "select aula.AulNum as numero, admisionaula.AdmAulAfo as aforo, aula.AulPis as piso, admisionaula.AdmAulCol as columna, admisionaula.AdmAulFil as fila, admisionaula.AdmAulCap as capacidad, admisionaula.AdmAulFKAdmPabId as idpabellon, admisionpabellon.AdmPabFKAdmAreId as idArea FROM admisionaula INNER JOIN admisionpabellon ON admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId INNER JOIN aula on admisionaula.AdmAulFKAulId=aula.AulId WHERE admisionaula.AdmAulFKEstReg=18 AND admisionpabellon.AdmPabFKAdmAreId=".$admAreId." and admisionaula.AdmAulFKAdmPabId=".$idPab."";

  
  
  $resultado2 = $mysqli->query($sqlPabPart);

  $cel=$cel+1;
  $nControl=2;
  $tAul=0;
  $tAfo=0;
  $tCap=0;
  $tCon=0;

  while ($row2 = $resultado2->fetch_assoc()){

    $numero=$row2['numero'];
    $aforo=$row2['aforo'];  
    $piso=$row2['piso'];
    $fila=$row2['fila'];     
    $columna=$row2['columna'];  
    $capacidad=$row2['capacidad'];  
    $nC=0;
    
    $tAfo=$tAfo+$aforo;
    $tCap=$tCap+$capacidad;
    

    $a2="A".$cel;
    $b2="B".$cel;
    $c2="C".$cel;
    $d2="D".$cel;
    $e2="E".$cel;
    $f2="F".$cel;
    $g2="G".$cel;
    $h2="H".$cel;
    $i2="I".$cel;
    $j2="J".$cel;

    if($capacidad<50){
      $nC=2;
    }else{
      $nC=3;
    }

    $tCon=$tCon+$nC;

    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue($a2,$areaNom)
    ->setCellValue($b2,"Aula N° ".$numero)
    ->setCellValue($c2,$aforo)
    ->setCellValue($d2,$piso)
    ->setCellValue($e2,$fila)
    ->setCellValue($f2,$columna)
    ->setCellValue($g2,$capacidad);

    
    array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $countCT+=1;
    $resta=$cel-$countCT;

    if($cel>2){
      $rango="A".$resta.":$g2";
      $styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
        'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF'))),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
      $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
    }
    $cel=$cel+1;
    $cel2=$cel+1;
    $countCT=1;
    $tAul+=1;

  }


  $objPHPExcel->getActiveSheet()->getStyle("A".$cel.":J".$cel)->applyFromArray($boldArray);
  $objPHPExcel->setActiveSheetIndex(0)
  ->setCellValue("A".$cel," TOTALES")
  ->setCellValue("B".$cel,$tAul)
  ->setCellValue("C".$cel,$tAfo)
  ->setCellValue("G".$cel,$tCap);
  

  $cel+=1;

  $countCT+=1;
  $resta=$cel-$countCT-1;


  if($cel>2){
    $rango="A".$resta.":G".($cel-1);
    $styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
      'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF'))),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
    $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
  }
  $cel=$cel+2;
  $cel2=$cel+1;
  $countCT=1;
}


// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=Distribuicion_Informatica_$areaNom.xls");
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>