<?php

//$pabId = strtoupper($_POST['pabId']);
//$pabNombre = strtoupper($_POST['pabNombre']);
session_start();


$nombreProceso=$_SESSION["nombreProceso"];
$anioProceso=$_SESSION["anioProceso"];
$faseProceso=$_SESSION["faseProceso"];
$fechaProceso =$_SESSION["fechaProceso"];
$idProceso=$_SESSION["idProceso"];


$pabId=$_GET['pabId'];                      
$pabNombre=$_GET['pabNombre'];
$areIdLogic=$_GET['areIdLogic'];


if (PHP_SAPI == 'cli')
	die('Este ejemplo sólo se puede ejecutar desde un navegador Web');

require_once("../../../conexion/db.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();
$objPHPExcel2 = new PHPExcel();


// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
->setLastModifiedBy("Oficina Admision")
->setTitle("Reporte de resfrigerio por Pabellon")
->setSubject("Reporte de resfrigerio por Pabellon")
->setDescription("Reporte de resfrigerio por Pabellon")
->setKeywords("resfrigerio por Pabellon")
->setCategory("Archivo Excel");

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle('A1:E2')->applyFromArray($boldArray);    

//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);  
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);      


for ($i=7; $i < 200; $i++) { 
	$objPHPExcel->getActiveSheet()->getRowDimension("$i")->setRowHeight(28);
}


$sqlTecnico="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=2 or admisiondetalle.AdmDetFKCar2=2) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";


$sqlCon="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=3 or admisiondetalle.AdmDetFKCar2=3) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";


$sqlApo = "select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuCui as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=4 or admisiondetalle.AdmDetFKCar2=4) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";


$sqlGua="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=5 or admisiondetalle.AdmDetFKCar2=5) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

$apoTecAdm="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=51 or admisiondetalle.AdmDetFKCar2=51) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

$sqlAsisTecnico="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=70 or admisiondetalle.AdmDetFKCar2=70) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

$garrett="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=76 or admisiondetalle.AdmDetFKCar2=76) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

$allAutomatic="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ORDER BY cargo1, cargo2 ASC";


$sqlCargos1="select DISTINCT (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1, admisiondetalle.AdmDetFKCar as idcargo, admisiondetalle.AdmDetFKPab as pabellon1 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and admisiondetalle.AdmDetFKEstReg=17 AND AdmDetFKPab=".$pabId." and admisiondetalle.AdmDetFKCar<>2 and admisiondetalle.AdmDetFKCar<>70 and (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) is not null ORDER BY cargo1 ASC";

$sqlCargos1C="select DISTINCT (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1, admisiondetalle.AdmDetFKCar as idcargo, admisiondetalle.AdmDetFKPab as pabellon1 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and admisiondetalle.AdmDetFKEstReg=17 AND AdmDetFKPab=".$pabId." and (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) is not null ORDER BY cargo1 ASC";


$sqlCargos2="select DISTINCT (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo1, admisiondetalle.AdmDetFKCar2 as idcargo, admisiondetalle.AdmDetFKPab2 as pabellon2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and admisiondetalle.AdmDetFKEstReg=17 AND AdmDetFKPab2=".$pabId." and admisiondetalle.AdmDetFKCar2<>2 and admisiondetalle.AdmDetFKCar2<>70 and (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) is not null ORDER BY cargo1 ASC";

$sqlCargos2C="select DISTINCT (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo1, admisiondetalle.AdmDetFKCar2 as idcargo, admisiondetalle.AdmDetFKPab2 as pabellon2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and admisiondetalle.AdmDetFKEstReg=17 AND AdmDetFKPab2=".$pabId." and (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) is not null ORDER BY cargo1 ASC";


$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
	'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF'))),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

$objPHPExcel->getActiveSheet()->getStyle("A8:E8")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->getStyle("A8:E8")->applyFromArray($boldArray);

// Combino las celdas desde A1 hasta E1


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
$objPHPExcel->getActiveSheet()->getStyle('A7:E7')->applyFromArray($boldArray);  
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:E7');

$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A1', "UNIVERSIDAD NACIONAL DE SAN AGUSTIN ")
->setCellValue('B2', "EXAMEN ".$nombreProceso." FASE N° ".$faseProceso." - ".$anioProceso)
->setCellValue('D2', "RECEPCION DE REFRIGERIO POR PABELLON")
->setCellValue('B3',"FECHA: ".$fechaProceso)
->setCellValue('B4', "LOCAL PABELLON:  ".$pabNombre)
->setCellValue('B5', "AREA:  ".$areIdLogic);



$cel=9;

$tecnico="NO";
$asistente="NO";


$resultado = $mysqli->query($sqlCargos1C);
while ($row = $resultado->fetch_assoc()){
	$idcargo=$row['idcargo'];  
	if ($idcargo==2) {
		$tecnico="SI";
	}
	if ($idcargo==70) {
		$asistente="SI";
	}
}

if ($asistente=="SI" || $tecnico=="SI") {
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A7', ' TECNICO ')
	->setCellValue('A8', ' # ')
	->setCellValue('B8', ' NOMBRES Y APELLIDOS ')
	->setCellValue('C8', 'DNI')
	->setCellValue('D8', 'AULA')
	->setCellValue('E8', 'FIRMA');


	$resultado = $mysqli->query($sqlTecnico);
	$countCT=1;
	$c=0;

	while ($row = $resultado->fetch_assoc()){

		$nombre=$row['nombre'];
		$dni=$row['dni'];  
		$a="A".$cel;
		$b="B".$cel;
		$c="C".$cel;
		$e="E".$cel;


		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;
	}

	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A7:$e";
		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);

		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}


	$cel=$cel+2;

	$objPHPExcel->getActiveSheet()->getStyle("A".($cel-1).":E".($cel-1))->applyFromArray($boldArray);  
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".($cel-1).":E".($cel-1));
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.($cel-1), ' ASISTENTE TECNÍCO ')
	->setCellValue('A'.$cel, ' # ')
	->setCellValue('B'.$cel, ' APELLIDOS Y NOMBRES ')
	->setCellValue('C'.$cel, 'DNI')
	->setCellValue('D'.$cel, 'AULA')
	->setCellValue('E'.$cel, 'FIRMA');

	$resultado = $mysqli->query($sqlAsisTecnico);
	$countCT=1;
	$cel=$cel+1;

	while ($row = $resultado->fetch_assoc()){

		$nombre=$row['nombre'];
		$dni=$row['dni'];  
		$a="A".$cel;
		$b="B".$cel;
		$c="C".$cel;
		$e="E".$cel;


		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;
	}
	$resta=$cel-$countCT-1;
	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A".$resta.":$e";
		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);

		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}


	//Automatico
$resultado = $mysqli->query($sqlCargos1);

while ($row = $resultado->fetch_assoc()){
	$countCT=1;
	$c=0;
	$cargo=$row['cargo1'];
	$nomIden="DNI";
	$idcargo=$row['idcargo'];  
	if ($idcargo==4) {
		$nomIden="CUI";
	}

	$cel=$cel+3;

	$objPHPExcel->getActiveSheet()->getStyle("A".($cel-1).":E".($cel))->applyFromArray($boldArray);  
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".($cel-1).":E".($cel-1));
	
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.($cel-1), $cargo)
	->setCellValue('A'.$cel, ' # ')
	->setCellValue('B'.$cel, ' APELLIDOS Y NOMBRES ')
	->setCellValue('C'.$cel, $nomIden)
	->setCellValue('D'.$cel, 'AULA')
	->setCellValue('E'.$cel, 'FIRMA');

	$sqlPersonal="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=".$idcargo." or admisiondetalle.AdmDetFKCar2=".$idcargo.") and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

	$resultado2 = $mysqli->query($sqlPersonal);
	while ($row2 = $resultado2->fetch_assoc()){
		$nombre=$row2['nombre'];
		$dni=$row2['dni'];  
		$a="A".($cel+1);
		$b="B".($cel+1);
		$c="C".($cel+1);
		$e="E".($cel+1);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;

		$resta=$cel-$countCT;

	}
	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A".$resta.":$e";

		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);
		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}

}

$resultado = $mysqli->query($sqlCargos2);

while ($row = $resultado->fetch_assoc()){
	$countCT=1;

	$cargo=$row['cargo1'];
	$nomIden="DNI";
	$idcargo=$row['idcargo'];  
	if ($idcargo==4) {
		$nomIden="CUI";
	}

	$cel=$cel+3;

	$objPHPExcel->getActiveSheet()->getStyle("A".($cel-1).":E".($cel))->applyFromArray($boldArray);  
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".($cel-1).":E".($cel-1));
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.($cel-1), $cargo)
	->setCellValue('A'.$cel, ' # ')
	->setCellValue('B'.$cel, ' APELLIDOS Y NOMBRES ')
	->setCellValue('C'.$cel, $nomIden)
	->setCellValue('D'.$cel, 'AULA')
	->setCellValue('E'.$cel, 'FIRMA');

	$sqlPersonal="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=".$idcargo." or admisiondetalle.AdmDetFKCar2=".$idcargo.") and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

	$resultado2 = $mysqli->query($sqlPersonal);
	while ($row2 = $resultado2->fetch_assoc()){
		$nombre=$row2['nombre'];
		$dni=$row2['dni'];  
		$a="A".($cel+1);
		$b="B".($cel+1);
		$c="C".($cel+1);
		$e="E".($cel+1);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;

		$resta=$cel-$countCT;

	}
	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A".$resta.":$e";

		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);
		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}

}


}else{

	//Automatico
$resultado = $mysqli->query($sqlCargos1);

while ($row = $resultado->fetch_assoc()){
	$countCT=1;

	$cargo=$row['cargo1'];
	$nomIden="DNI";
	$idcargo=$row['idcargo'];  
	if ($idcargo==4) {
		$nomIden="CUI";
	}

	$cel=$cel;

	$objPHPExcel->getActiveSheet()->getStyle("A".($cel-1).":E".($cel))->applyFromArray($boldArray);  
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".($cel-1).":E".($cel-1));
	
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.($cel-1), $cargo)
	->setCellValue('A'.$cel, ' # ')
	->setCellValue('B'.$cel, ' APELLIDOS Y NOMBRES ')
	->setCellValue('C'.$cel, $nomIden)
	->setCellValue('D'.$cel, 'AULA')
	->setCellValue('E'.$cel, 'FIRMA');

	$sqlPersonal="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=".$idcargo." or admisiondetalle.AdmDetFKCar2=".$idcargo.") and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

	$resultado2 = $mysqli->query($sqlPersonal);
	while ($row2 = $resultado2->fetch_assoc()){
		$nombre=$row2['nombre'];
		$dni=$row2['dni'];  
		$a="A".($cel+1);
		$b="B".($cel+1);
		$c="C".($cel+1);
		$e="E".($cel+1);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;

		$resta=$cel-$countCT;

	}
	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A".$resta.":$e";

		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);
		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}

}

$resultado = $mysqli->query($sqlCargos2);

while ($row = $resultado->fetch_assoc()){
	$countCT=1;

	$cargo=$row['cargo1'];
	$nomIden="DNI";
	$idcargo=$row['idcargo'];  
	if ($idcargo==4) {
		$nomIden="CUI";
	}

	$cel=$cel+3;

	$objPHPExcel->getActiveSheet()->getStyle("A".($cel-1).":E".($cel))->applyFromArray($boldArray);  
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A".($cel-1).":E".($cel-1));
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.($cel-1), $cargo)
	->setCellValue('A'.$cel, ' # ')
	->setCellValue('B'.$cel, ' APELLIDOS Y NOMBRES ')
	->setCellValue('C'.$cel, $nomIden)
	->setCellValue('D'.$cel, 'AULA')
	->setCellValue('E'.$cel, 'FIRMA');

	$sqlPersonal="select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=".$idProceso." and (admisiondetalle.AdmDetFKCar=".$idcargo." or admisiondetalle.AdmDetFKCar2=".$idcargo.") and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=".$pabId." OR AdmDetFKPab2=".$pabId.") ";

	$resultado2 = $mysqli->query($sqlPersonal);
	while ($row2 = $resultado2->fetch_assoc()){
		$nombre=$row2['nombre'];
		$dni=$row2['dni'];  
		$a="A".($cel+1);
		$b="B".($cel+1);
		$c="C".($cel+1);
		$e="E".($cel+1);

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue($a, $countCT)
		->setCellValue($b, $nombre)
		->setCellValue($c, $dni);

		$countCT+=1;
		$cel+=1;

		$resta=$cel-$countCT;

	}
	if($cel>9){
		/*Fin extracion de datos MYSQL*/
		$rango="A".$resta.":$e";

		$objPHPExcel->getActiveSheet()->getStyle($rango)->getAlignment()->setWrapText(true);
		$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
			'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
		);
		$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
	}

}


}



// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Reporte de Refrigerio');



// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=REFRIGERIO_$pabNombre.xls");
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');

// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;

?>