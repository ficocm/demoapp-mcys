<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
  <title>OFICINA DE ADMISION</title>

  <!-- ESTILOS -->
  <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
  <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
  <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
  <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->


  <!-- SCRIPTS -->
  <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
  <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
  <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
  <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
  <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
  <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
  <script src="../../js/reporte.js"></script>

  <!-- TABLAS -->
  
  <script type="text/javascript" src="../../css/dataTables.bootstrap.min.css"></script>
  <script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../../js/dataTables.bootstrap.min.js"></script>
  

</head>

<body onload="init();">
  <div id="wrapper">
    <!-- Menu -->
    <?php
    include '../../menu.php';
    ?>
    <!-- Header -->
    <div id="page-wrapper">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-8">
            <h1 class="page-header" style="color:#5e151d;">Reportes / Pabellones y areas participantes </h1>
          </div>
          <div class="col-lg-4">
            <br>
            <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
          </div>
        </div>  

        <!-- PAGUE CONTENT -->
        <font SIZE=5>Seleccione las areas para listar bapellones que particparan en el proceso  de Admision <?php echo " ".$_SESSION["nombreProceso"]. " ".$anioProceso."  FASE N° ".$_SESSION["faseProceso"]. " (".$_SESSION["fechaProceso"].") " ?> </font> 
        <br><br>
        <label id="idProceso" name="idProceso" style='display:none;'> <?php echo $_SESSION["idProceso"]?> </label> 
        
        <div class="col-lg-7">
          <div class="panel panel-default">
            <div class="panel-heading" >
              <font SIZE=3>
                <span >Selecciona un area para visualizar y generar reporte de pabellones</span>  </font><br>
              </div>
              <div class="panel-body">
               <div id="checkAreas" name="checkAreas" ></div>
             </div>
           </div>
         </div>
         <div class="col-lg-5" >
          <div class="panel panel-default">
            <div class="panel-heading" >
              <font SIZE=3 >Genere Reporte de Pabellones participantes</font><br>
            </div>
            <div class="panel-body">
             <br>
             <div class="col-lg-4">
              <label class="control-label col-sm-12"> <font SIZE=3>Reporte:</font></label>
            </div> 
            <div class="col-lg-8">
             <div id="reporte" name="reporte" ></div>
           </div>
           <br> <br> <br> 
         </div>
       </div>
     </div>


     <div id ="con" class="col-sm-12 pdf-page size-a8" >  
       <div class="row">
         <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading" >

              <font SIZE=3> Listado de pabellones que participaran en el proceso de admision en el area de <span id="nomArea"> </span>   </font><br>

            </div>


            <div class="panel-body">
              <div class="table-responsive" >
                <div id ="con" class="col-sm-12 pdf-page size-a12">
                  <main role="main" > <!-- class="col-sm-10 ml-sm-auto col-md-10 pt-3" -->
                   <div id="pab1" name="pab1" ></div>
                 </main>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>


 </div>  

</div>
</div>


</body>

