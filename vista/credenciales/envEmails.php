<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
    <title>OFICINA DE ADMISION</title>

    <!-- ESTILOS -->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
    <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

    <!-- SCRIPTS -->
    <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
    <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

     <script src="../../js/credenciales/correos.js"></script>

    <style type="text/css">
        /* Reset */
    #myProgress {
        width: 100%;
        background-color: grey;
    }
    #myBar {
        width: 1%;
        height: 30px;
        background-color: green;
    }   
    .checkSendEmail{
        margin-right: 10px;
    }
    .loader {
        border: 6px solid #f3f3f3;
        border-radius: 50%;
        border-top: 6px solid #3498db;
        width: 40px;
        height: 40px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

</style>

</head>


<body onload="init();">
    <div id="wrapper">
        <!-- Menu -->
        <?php
        include '../../menu.php';
        ?>

        <!-- Header -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="page-header" style="color:#5e151d;">Envio de Correos </h1>
                    </div>
                    <div class="col-lg-4">
                        <br>
                        <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
                    </div>
                </div>  

                <!-- PAGUE CONTENT -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                            Complete todos los campos (LOS CORREOS SERÁN ENVIADOS A LOS USUARIOS SORTEADOS Y ASIGNADOS)
                    </div>
                
                    <div class="panel-body">
						
						<form id= "formulario" name="formulario" action="../../controlador/credenciales/enviarEmail.php" method="post" enctype="multipart/form-data">

                            <div class="form-group">
<!--                                 
                            <div class="container-fluid">
                                <label>Enviar a</label> 
                                <button type="button" class="btn btn-outline-success btn-sm" onclick="selectAll()">Seleccionar Todos</button>
                                <button type="button" class="btn btn-outline-success btn-sm" onclick="unSelectAll()">Deselecionar Todos</button>
                            </div> -->
                            <div class="input-group">
                                <label>Enviar a</label> 
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="selectAll()">Seleccionar Todos</button>
                                    <button type="button" class="btn btn-outline-success btn-sm" onclick="unSelectAll()">Deselecionar Todos</button>
                                </div>
                            </div>
                            
                                <?php
                                    require_once("../../conexion/conexion.php");
                                    $mysqli = Conectar::conexion();
                                    $mysqli->set_charset("utf8");

                                    $query="SELECT * FROM cargo WHERE CarFKTipUsu=3 AND CarFKEstReg = 18";

                                    $resultado = $mysqli->query($query);

                                    while($row = $resultado->fetch_assoc()){

                                        $CarId = $row['CarId'];
                                        $CarDes = strtoupper($row['CarDes']);
                                ?>
                                        <div class="radio">
                                            <label class="checkSendEmaillll">
                                                <input type="checkbox" class="checkSendEmail" name="optionsRadios[]" id="optionsRadios" value=<?=$CarId?> > <?=$CarDes?>
                                            </label>
                                        </div>
                                <?php
                                    }
                                ?>


                                <div class="radio">
                                    <label>
                                        <input type="checkbox" class="checkSendEmail" name="optionsRadios[]" id="optionsRadios" value="0">ADMINISTRATIVOS
                                    </label>
                                </div>
                            </div>

							<div class="form-group">
                                <label>Asunto</label>
								<input class="form-control" id="asunto" name="asunto" type="text" value= "" >
							</div>

							<div class="form-group">
                                <label>Mensaje</label>
								<textarea class="form-control" id="mensaje" rows="10" name="mensaje" type="text"></textarea>
							</div>

                            <div class="form-group">
                                <label>Correo</label>
                                <input class="form-control" id="correo" rows="10" name="correo" type="email"></input>
                            </div>

                            <div class="form-group">
                                <label>Contraseña</label>
                                <input class="form-control" id="contrasena" rows="10" name="contrasena" type="password"></input>
                            </div>


							<button type="submit" id="send" class="btn btn-default">Enviar</button>
                            <div class="loader" id="loader"></div>
							<br>

						</form>

					</div>
				</div>
            </div>
            <!-- /.container-fluid -->


        </div>
    </div>
</body>

</html>	