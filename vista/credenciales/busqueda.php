<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!--Modificaciones propias-->
	<script src="../../js/asignados.js"></script>
</head>

<body onload="">
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Generar credencial por búsqueda</h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->

				<header>
						<!--Inicio-->
					<div class="table-wrapper">
		            	<div class="table-title">
		            		<div class="row">
		            			<div class="col-sm-12">
										-
								</div>
		            		</div>
		          		</div>
						<!-- <div class="col-sm-4">
							<div id="custom-search-input">
								<div class="input-group col-md-12">
									<a class="btn btn-default" href="../../controlador/credenciales/todos.php" role="button">Descargar todo</a>
								</div>
							</div>
						</div> -->
						<div class='col-sm-4 pull-right'>
							<div id="custom-search-input">
								
				          		<div class="input-group col-md-12">
				            		<input type="text" class="form-control" placeholder="Ingrese DNI o Nombre"  id="busqueda" onkeyup="cargar(1);" />
				            		<span class="input-group-btn">
						            	<button class="btn btn-info" type="button" onclick="cargar(1);">
						             		<span class="glyphicon glyphicon-search"></span>
						            	</button>
				           			</span>
				          		</div>
				        	</div>
						</div>

						<div class='clearfix'></div>
						<hr>
						<div id="loader"></div><!-- Carga de datos ajax aqui -->
						<div id="resultados"></div><!-- Carga de datos ajax aqui -->
						<div class='outer_div'></div><!-- Carga de datos ajax aqui -->
	        		</div>

		        	<table class="table table-striped table-hover" id="usuarios">
						<thead>
							<tr>
								<th class='text-center'>DNI</th>
								<th>Nombre </th>
								<th class='text-center'>Cargo </th>
								<th class='text-center'>Crendencial</th>

								<th></th>
							</tr>
						</thead>
					</table>
					<div id="paginacion">
					</div>
					<!--Fin-->
				</header>
				
			</div>

		</div>

	</div>
</body>

</html>
