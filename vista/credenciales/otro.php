<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
    <title>OFICINA DE ADMISION</title>

    <!-- ESTILOS -->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
    <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

    <!-- SCRIPTS -->
    <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
    <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

    <style type="text/css">
        /* Reset */
    #myProgress {
        width: 100%;
        background-color: grey;
    }
    #myBar {
        width: 1%;
        height: 30px;
        background-color: green;
    }
</style>

<script type="text/javascript">

    function enviarFormulario(){
        
    }

    function validateForm() {
        var tipo = document.forms["formulario"]["optionsRadios"].value;
        var asunto = document.forms["formulario"]["asunto"].value;
        var mensaje = document.forms["formulario"]["mensaje"].value;
        if (tipo == "" || asunto=="" || mensaje=="") {
            alert("Completar todos los campos");
            return false;
        }
    } 
    
    function init(){

    }
</script>

</head>


<body onload="init();">
    <div id="wrapper">
        <!-- Menu -->
        <?php
        include '../../menu.php';
        ?>

        <!-- Header -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="page-header" style="color:#5e151d;">Generacion de Credencial </h1>
                    </div>
                    <div class="col-lg-4">
                        <br>
                        <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
                    </div>
                </div>  

                <!-- PAGUE CONTENT -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                            Complete todos los campos
                    </div>
                
                    <div class="panel-body">
						
						<form id= "formulario" name="formulario" action="../../controlador/credenciales/credencialPersonalizada.php" method="post" enctype="multipart/form-data" target="_blank" onsubmit="return validateForm()">

							<div class="form-group">
                                <label>CARGO</label>
								<input class="form-control" id="cargo" name="cargo" type="text" value= "" >
							</div>

							<div class="form-group">
                                <label>APELLIDOS</label>
								<input class="form-control" id="apellidos" name="apellidos" type="text" value= "" >
							</div>

                            <div class="form-group">
                                <label>NOMBRES</label>
                                <input class="form-control" id="nombres" name="nombres" type="text" value= "" >
                            </div>

                            <div class="form-group">
                                <label>AREA</label>
                                <input class="form-control" id="area" name="area" type="text" value= "" >
                            </div>

                            <div class="form-group">
                                <label>LOCAL</label>
                                <input class="form-control" id="local" name="local" type="text" value= "" >
                            </div>

							<button type="submit" id="send" class="btn btn-default">Generar</button>
							<br>

						</form>

					</div>
				</div>
            </div>
            <!-- /.container-fluid -->


        </div>
    </div>
</body>

</html>	