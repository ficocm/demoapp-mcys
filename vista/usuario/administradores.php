<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!-- Mis modificaciones -->
	<link href="../../css/estilosMensajes.css" rel="stylesheet"> 
	<script src="../../js/util.js"></script>
	<script src="../../js/administradoresUsuarios.js"></script>

</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Usuario / Administradores </h1>
					</div>
					<div class="col-lg-4">
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->
				<div class="row grid-row ">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>

				<br>
				<div class='col-sm-12'>
					<div class="row pull-left col-lg-4">
						<input type="button" class="btn btn-primary" onclick="agregable();" id="submit" value="AGREGAR ADMINISTRADOR" data-toggle='modal' data-target='#myModal'>
					</div>
					<div id="custom-search-input ">
				    <div class="input-group col-md-4 pull-right">
				      <input type="text" class="form-control" placeholder="Ingrese DNI o Nombre"  id="busqueda" onkeyup="cargar(1);" />
				      <span class="input-group-btn">
				  	    <button class="btn btn-info" type="button" onclick="cargar(1);">
					    		<span class="glyphicon glyphicon-search"></span>
					     	</button>
				      </span>
				    </div>
				  </div>
				</div>
				<div id="table-scroll">
					<table class="table table-striped table-bordered estiloHover" id="usuarios">
					  <br><br>
					  <tbody>
					    <tr>
					    	<th>EDITAR</th>
					      <th>APELLIDOS</th>
					      <th>NOMBRES</th>
					      <th>CORREO ELECTRÓNICO</th>
					      <th>TELEFONO</th>
					      <th>DIRECCION</th>
					      <th>NOMBRE DE USUARIO</th>
					      <th>CONTRASEÑA</th>
					    </tr>
					  </tbody>
					</table>
					<div id="paginacion">
					</div>
				</div>

				<!-- FIN -->
				
						<!-- Modal content-->
						<div class="modal fade" id="myModal" role="dialog">
    					<div class="modal-dialog modal-lg">

					      <div class="modal-content">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <h4 class="modal-title text-center">Modificar Operador</h4>
					        </div>
					        <div class="modal-body">
					        	<form class="form-horizontal" id="formulario">

					        		<div class="row">
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Nombres:<span style="color: #FF0000">*</span></label>
												    <div class="col-sm-10">
												       <input type="text" class="form-control" name="nombres" id="nombres">
												    </div>
												  </div>
						        		</div>
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Apellidos:<span style="color: #FF0000">*</span></label>
												    <div class="col-sm-10">
												       <input type="text" class="form-control" name="apellidos" id="apellidos">
												    </div>
												  </div>
						        		</div>
						        	</div>

						        	<div class="row">
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Correo:</label>
												    <div class="col-sm-10">
												       <input type="text" class="form-control" name="correo" id="correo">
												    </div>
												  </div>
						        		</div>
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Telefono:</label>
												    <div class="col-sm-10">
												       <input type="text" class="form-control" name="telefono" id="telefono">
												    </div>
												  </div>
						        		</div>
						        	</div>


											<div class="row">
						        		<div class="col-lg-12">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Direccion:</label>
												    <div class="col-sm-10">
												       <input type="text" class="form-control" name="direccion" id="direccion">
												    </div>
												  </div>
						        		</div>
						        	</div>

						        	<div class="row">
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-3">Nombre Usuario:<span style="color: #FF0000">*</span></label>
												    <div class="col-sm-9">
												       <input type="text" class="form-control" name="usunom" id="usunom">
												    </div>
												  </div>
						        		</div>
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-3">Contraseña Usuario:<span style="color: #FF0000">*</span></label>
												    <div class="col-sm-9">
												       <input type="password" class="form-control" name="usucon" id="usucon">
												    </div>
												  </div>
						        		</div>
						        	</div>


					        	</form>

										
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					          <button type="button" class="btn btn-primary" id="save">Guardar</button>
					        </div>
					      </div>
					      
					    </div>
					  </div>
					  <!-- End Modal-->
				
			</div>
		</div>

		<!-- Management Error -->
		<div class="error"></div>
		<div class="correcto"></div>

	</div>
</body>

</html>
