<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">	<!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>	<!-- jQuery -->	
	<script src="../../vendor/jquery/popper.min.js"></script>	<!-- Poppper -->	
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	
	<link href="../../css/estilosMensajes.css" rel="stylesheet"> 
	<script src="../../js/util.js"></script>
	<script src="../../js/docentesAdministrativosUsuarios.js"></script>

</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Usuario / Administrativos y Docentes </h1>
					</div>
					<div class="col-lg-4">
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>	

				<!-- PAGUE CONTENT -->
				
				<div class="row grid-row ">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>
				<br>
				<div class='col-sm-12'>
					<div class="row pull-left col-lg-3">
						<input type="button" class="btn btn-primary" onclick="agregable();" id="submit" value="AGREGAR" data-toggle='modal' data-target='#myModal'>
					</div>
					<div class="row pull-center col-lg-3" style="margin-left: 10px">
						<select class="form-control" name="fitroTipo" id="fitroTipo" onchange="cargar(1)">
							<option value="TODOS" selected>MOSTRAR TODOS</option>
							<option value="DOCENTE">MOSTRAR DOCENTES</option>
							<option value="ADMINISTRATIVO">MOSTRAR ADMINISTRATIVOS</option>
						</select>
					</div>
					<div id="custom-search-input ">
						<div class="input-group col-md-4 pull-right">
							<input type="text" class="form-control" placeholder="Ingrese DNI o Nombre"	id="busqueda" onkeyup="cargar(1);" />
							<span class="input-group-btn">
								<button class="btn btn-info" type="button" onclick="cargar(1);">
									<span class="glyphicon glyphicon-search"></span>
							 	</button>
							</span>
						</div>
					</div>
				</div>
				<br>
				<br>
				<div id="table-scroll">
					<table class="table table-striped table-bordered estiloHover" id="usuarios">
						<tbody style="">
								<tr>
									<th>EDITAR</th>
									<th>DNI</th>
									<th>APELLIDOS</th>
									<th>NOMBRES</th>
									<th>TIPO</th>
									<th>CORREO ELECTRÓNICO</th>
									<th>TELEFONO</th>
									<th>DIRECCION</th>
									<th>CATEGORIA</th>
									<th>GÉNERO</th>
									<th>NUMERO DE PROCESOS</th>
									<th>DEPARTAMENTO ACADEMICO</th>
									<th>AREA</th>
									<th>FUNCION</th>
									<th>DEPENDENCIA</th>
									<?php if(in_array(Codigos::codAdministrador, $tipo)){
										echo "<th>ELIMINAR</th>";
										echo "<script>var administrador = true;</script>";
									}else echo "<script>var administrador = false;</script>";?>
								</tr>
						</tbody>
					</table>
					<div id="paginacion">
					</div>
				</div>

				<!-- FIN -->
			</div>
		</div>

						<!-- Modal content-->
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog modal-lg">

								<form class="form-horizontal" id="formulario">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title text-center">Administrativo</h4>
									</div>
									<div class="modal-body">

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Nombres:<span style="color: #FF0000">*</span></label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="nombres" id="nombres" required>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Apellidos:<span style="color: #FF0000">*</span></label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="apellidos" id="apellidos" required>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">DNI:<span style="color: #FF0000">*</span></label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="dni" id="dni" placeholder="45631243">
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Telefono:</label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="telefono" id="telefono" placeholder="95263726">
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Correo:<span style="color: #FF0000">*</span></label>
														<div class="col-sm-10">
															 <input type="email" class="form-control" name="correo" id="correo" placeholder="nombre@unsa.edu.pe">
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Numero<br>Procesos:</label>
														<div class="col-sm-10">
															 <input type="number" class="form-control" name="numeroProcesos" id="numeroProcesos" placeholder="0" value="0">
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-3">Docente:</label>
														<div class="col-sm-3">
															 <input type="checkbox" class="form-control" name="tipo1" id="tipo1" value="3">
														</div>
														<label class="control-label col-sm-3">Administrativo:</label>
														<div class="col-sm-3">
															 <input type="checkbox" class="form-control" name="tipo2" id="tipo2" value="4">
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Dirección:</label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="direccion" id="direccion">
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Funcion:</label>
														<div class="col-sm-10">
															 <input type="text" class="form-control" name="funcion" id="funcion">
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-3">Dependecia:</label>
														<div class="col-sm-9">
															 <input type="text" class="form-control" name="dependencia" id="dependencia">
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Area:</label>
														<div class="col-sm-10">
															 <select class="form-control" name="area" id="area">
															 </select>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-3">Departamento:</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" name="departamento" id="departamento">
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Género:<span style="color: #FF0000">*</span></label>
														<div class="col-sm-10">
															 <select class="form-control" name="genero" id="genero">
															 </select>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Categoria:</label>
														<div class="col-sm-10">
															 <select class="form-control" name="categoria" id="categoria">
															 </select>
														</div>
													</div>
												</div>
											</div>
											
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
										<button type="button" class="btn btn-primary" id="save">Guardar</button>
									</div>
								</div>

								</form>
								
							</div>
						</div>
						<!-- End Modal-->

						<!-- Modal content-->
						<div class="modal fade confirmacion" role="dialog">
							<div class="modal-dialog modal-sm">

								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title text-center">Mensaje de Confirmación</h4>
									</div>
									<div class="modal-body" id="mensajeConfirmacion">
											Confirma que desea eliminar
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" id="cancelar">Cancelar</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmar">Confirmar</button>
									</div>
								</div>
								
							</div>
						</div>
						<!-- End Modal-->



		<!-- Management Error -->
		<div class="error"></div>
		<div class="correcto"></div>
	</div>
</body>

</html>
