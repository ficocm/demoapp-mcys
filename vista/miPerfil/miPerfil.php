<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2-new.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	


</head>

<body>

	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Mi Perfil </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->

				  <div class="login-panel panel panel-default">
                    <div class="panel-body">
                        <form role="form" class="login-form">
                            <fieldset>
                            	<input type="hidden" id="id" name="id" value="<?php echo $_SESSION["idUsuario"]?>">
                       
                                <div class="form-group">
                                   	Nombres<input class="form-control" placeholder="Nombres" id="nombres" name="nombres" type="text" value=""  disabled autofocus>
                                </div>
                                <div class="form-group">
                                    Apellidos<input class="form-control" placeholder="Apellidos" id="apellidos" name="apellidos" type="text" value="" disabled>
                                </div>
                                <div class="form-group">
                                    DNI<input class="form-control" placeholder="DNI" id="dni" name="dni" type="text" value="" disabled>
                                </div>
                                <div class="form-group">
                                    Correo Electrónico<input class="form-control" placeholder="Correo Electrónico" id="correo" name="correo" type="email" value="" disabled>
                                </div>
                                <div class="form-group">
                                    Celular<input class="form-control" placeholder="Celular" id="celular" name="celular" type="text" value="" disabled>
                                </div>
                                <!--div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Recordar mi correo UNSA
                                    </label>
                                </div-->
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="../miPerfil/miPerfilMod.php" class="btn btn-lg btn-success btn-block">Editar</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
			</div>


		</div>

	</div>
	<script src="../../js/cargarDatos.js"></script>
</body>

</html>
