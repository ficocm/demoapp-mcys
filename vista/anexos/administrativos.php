<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
    <title>OFICINA DE ADMISION</title>

    <!-- ESTILOS -->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
    <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

    <!-- SCRIPTS -->
    <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
    <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->


	<style>
	table {
	    font-family: arial, sans-serif;
	    border-collapse: collapse;
	    width: 80%;
	}

	td, th {
	    border: 1px solid #dddddd;
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even) {
	    background-color: #dddddd;
	}
	</style>
</head>
<script>
	function eliminar(file) 
  	{
  		
        var r = confirm("Esta seguro que desea eliminar este PDF?")
        if(r == true)
        {
	        	    
	        $.ajax({
	            url: '../../controlador/anexos/borrar.php',
	            data: {'file' : file},
	            success: function (response) {
	             	// do something
	             	location.reload(true);
	          	},
	          	error: function () {
	             	// do something
	          }
	        });

        }
    }

    function validateForm() {
        var fileToUpload = document.forms["formulario"]["fileToUpload"].value;
        if (fileToUpload == "") {
            alert("Seleccione un archivo");
            return false;
        }
    } 

	function init(){
		
	}
</script>
<body onload="init();">
    <div id="wrapper">
        <!-- Menu -->
        <?php
        include '../../menu.php';
        ?>

        <!-- Header -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="page-header" style="color:#5e151d;">Anexos / Administrativos </h1>
                    </div>
                    <div class="col-lg-4">
                        <br>
                        <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
                    </div>
                </div>  

                <!-- PAGUE CONTENT -->

				<table>
					<tr>
						<th>Documentos</th>
						<th>Eliminar</th>
					</tr>
							
				    <?php
				    	 
					    //Asignamos la ruta a la variable path
					    $path="../../Documentos/Administrativos/";
					    //asignamos a $directorio el objeto dir creado con la ruta
					    $directorio=dir($path);

					    //y ahora lo vamos leyendo hasta el final
					    while ($archivo = $directorio->read())
					    {
					    //
						    if($archivo!="." OR $archivo!=".."){
						    //ponemos el nombre de archivo a minuscula y recojemos solo los tres caracteres por la izquierda
						    //para saber la extensión
							    if (strtolower(substr($archivo, -3) == "pdf"))
							    {
							    //si es pdf,lo mostramos por pantalla
							    	echo "<tr>
							    	<td><a target=\"_blank\" href=".$path.$archivo.">".$archivo."</a></td>
							    	<td><button onclick=\"eliminar('".$path.$archivo."')\" class=\"btn btn-danger btn-circle\" ><i class=\"fa fa-times\"></i>
                            </button>
							    	</tr>";
							    }
						    }
						}
					    //descargo el objeto
					    $directorio->close();
					?>
				</table>
				<br>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Subir Archivos
                    </div>
                
                    <div class="panel-body">

						<form action="../../controlador/anexos/uploadAdministrativos.php" id="formulario" enctype="multipart/form-data" method="post" onsubmit="return validateForm()">

		                    <div class="form-group">
		                        <label>Seleccione un archivo pdf</label>
		                        <input type="file" name="fileToUpload" id="fileToUpload">
		                    </div>
						    
						    <button type="submit" id="send" class="btn btn-default" >Cargar	</button>
						</form>


					</div>
				</div>
			
            </div>
            <!-- /.container-fluid -->

        </div>
    </div>
</body>

</html>