<?php
require_once("../../modelo/usuario/util.php");
require_once ("../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos

  // escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();
$output  = '';
$confirmacion  = '';
$canInsert = 0;
$canUpdate = 0;
if (isset($_POST["import"])) {
    $file       = $_FILES["excel"]["name"];
    $name_parts = explode('.', $file);
    $extension  = strtolower(end($name_parts));
    
    $allowed_extension = array(
        "xls",
        "xlsx",
        "csv"
    ); //allowed extension
    if (in_array($extension, $allowed_extension)) { //check selected file extension is present in allowed extension array
        $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
        
        include("PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code
        //$file2 = "C:\Users\cesar\Downloads\L_Docentes.xlsx";
        $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file
        
        $output .= "<label class='text-success'>Data Inserted</label><br /><table class='table table-bordered'>";
        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            $highestRow = $worksheet->getHighestRow();
            for ($row = 2; $row <= $highestRow; $row++) {
                $output .= "<tr>";
                $FKtipoDeTrabajador = 20; //Administrativo
                $tipocompleto       = $db->real_escape_string($worksheet->getCellByColumnAndRow(0, $row)->getValue());
                $sexo               = $db->real_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                $dni                = $db->real_escape_string($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                $nombreCompleto     = $db->real_escape_string($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                $direccion          = $db->real_escape_string($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                $telefono           = $db->real_escape_string($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                $dependencia        = $db->real_escape_string($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                $subdependencia     = $db->real_escape_string($worksheet->getCellByColumnAndRow(7, $row)->getValue());
                $correo             = $db->real_escape_string($worksheet->getCellByColumnAndRow(8, $row)->getValue());

                
                //echo "tipo : " .$tipocompleto. "<br>";
                //echo "sexo : " .$sexo. "<br>";
                //echo "dni : " .$dni. "<br>";
                //echo "nombress : " .$nombreCompleto. "<br>";
                //echo "direccion : " .$direccion. "<br>";
                //echo "telefono : " .$telefono. "<br>";
                //echo "dependencia : " .$dependencia. "<br>";
                //echo "sub dependencia : " .$subdependencia. "<br>";



                

                //*******SACAR ID DE SEXO DE TRABAJADOR*******
                if(strcmp($sexo,"F")==0)
                    $FKSexoDeTrabajador=2;
                else if (strcmp($sexo,"M")==0) 
                    $FKSexoDeTrabajador=1;
                else
                     $FKSexoDeTrabajador=1;


                //*******SACAR ID DE TIPO DE TRABAJADOR*******

                $tipotrabajador=explode(" ",$tipocompleto);
                $tipo=$tipotrabajador[0]; 
                $categoria=$tipotrabajador[2];

                if(strcmp($tipo,"DOC")==0)
                    $FKtipoDeTrabajador=3;
                else if (strcmp($tipo,"ADM")==0) 
                    $FKtipoDeTrabajador=4;
                else
                     $FKtipoDeTrabajador=3;

                 //*******SACAR ID DE CATEGORIA DE TRABAJADOR*******
                 if(strcmp($categoria,"NOM")==0)
                    $FKCategoriaDeTrabajador=11;
                else if (strcmp($categoria,"CON")==0) 
                    $FKCategoriaDeTrabajador=12;
                else if (strcmp($categoria,"CAS")==0) 
                    $FKCategoriaDeTrabajador=31;
                else
                     $FKCategoriaDeTrabajador=11;

               

                //*******SACAR APELLIDOS  Y NOMBRES DE TRABAJADOR*******

                $nombres1         = explode(" ",$nombreCompleto);
                $apellidos        = $nombres1[0]." ".$nombres1[1];

                $nombres2         = explode($nombres1[1],$nombreCompleto);
                $nombres          = $nombres2[1];
                $nombres          = substr($nombres, 1);

                //$dni      = filtroCaracteresEspeciales($dni);
                $apellidos      = filtroCaracteresEspeciales($apellidos);
                $nombres        = filtroCaracteresEspeciales($nombres);              
                $direccion       = filtroCaracteresEspeciales($direccion);
               // $correo        = filtroCaracteresEspeciales($correo);
                $dependencia        = filtroCaracteresEspeciales($dependencia);
                $subdependencia        = filtroCaracteresEspeciales($subdependencia);
                 
                $deparAcademico   = $subdependencia;
                $funcion = $subdependencia;
               
                //echo "apellidos >>".$apellidos."<br>";
                //echo "nombres >>".$nombres."<br>";
                //echo "tipo >>".$FKtipoDeTrabajador."<br>";
                //echo "categoriaa >>".$FKCategoriaDeTrabajador."<br>"; 
                //echo "sexo >>".$FKSexoDeTrabajador."<br>"; 
                //echo "CORREO >>".$correo."<br>";
                //echo "derpatemnto aca >>".$deparAcademico."<br>";
                //echo "funcion >>".$funcion."<br>";
                //echo "dependencia >>".$dependencia."<br>";
                //echo "direccion : " .$direccion. "<br>";
                //echo "/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*<br>"; 

            //Filtros de tildes y mayusculas

               // echo "apellidos>>".$apellidos."||nombres>>".$nombres;
              
                
                
                 $queryVerRepeticiones = "SELECT UsuId FROM usuario WHERE UsuDni LIKE ('%".$dni."%') ";
                 //echo "REPETICIONES: ".$queryVerRepeticiones."<br>";

                 $UsuarioId            = seleccionBaseDeDatos($queryVerRepeticiones, $db, "UsuId");
                if($dni != NULL || $dni !="")
                if ($UsuarioId == NULL || $UsuarioId == "") {
                    
                    
                    if($FKtipoDeTrabajador==3){
                        //Insertar usuario docente
                        $query = "INSERT INTO usuario(UsuDni, UsuNom, UsuApe, UsuCorEle, UsuFKTip, UsuFKCat, UsuTel, UsuFKGen, UsuDepAca, UsuDir, UsuNomUsu, UsuConUsu) VALUES ('".$dni."', '".$nombres."', '".$apellidos."', '" . $correo . "', '" . $FKtipoDeTrabajador . "', '" . $FKCategoriaDeTrabajador . "', '" . $telefono . "', '" . $FKSexoDeTrabajador . "', '" . $deparAcademico . "', '" . $direccion . "'  , '".$correo."',  '" . $dni . "' )";
                        //echo "query: " . $query . "<br>";
                        if ($db->query($query)) {
                            //echo "Se actualizo el usuario " . $apellidos . " con dni >> ".$dni."<br>";
                        } else {
                            echo "Error: " . $query . "<br>" . $db->error;
                        }
                        $canInsert = $canInsert + 1;
                    }

                    if($FKtipoDeTrabajador==4){
                        //Insertar usuario docente
                        $query = "INSERT INTO usuario(UsuDni, UsuNom, UsuApe, UsuCorEle, UsuFKTip2, UsuFKCat, UsuTel, UsuFKGen, UsuDep, UsuDir, usuFun, UsuNomUsu, UsuConUsu) VALUES ('".$dni."', '".$nombres."', '".$apellidos."', '".$correo."', '" . $FKtipoDeTrabajador . "', '" . $FKCategoriaDeTrabajador . "', '" . $telefono . "', '" . $FKSexoDeTrabajador . "', '" . $dependencia . "', '" . $direccion . "' , '" .$funcion. "' , '".$correo."',  '" . $dni . "')";
                        //echo "query: " . $query . "<br>";
                        if ($db->query($query)) {
                           //echo "Se actualizo el usuario " . $apellidos . " con dni >> ".$dni."<br>";
                        } else {
                            echo "Error: " . $query . "<br>" . $db->error;
                        }
                        $canInsert = $canInsert + 1;
                    }
                    
                    



                    //Impresion de la tabla registrada
                   // $output .= '<td>' . $nombres . '</td>';
                    //$output .= '<td>' . $apellidos . '</td>';
                   // $output .= '<td>' . $correo . '</td>';
                    //$output .= '<td>' . $cui . '</td>';
                   // $output .= '<td>' . $escuela . '</td>';
                    //$output .= '</tr>';

                } else { //Actualización de Usuario
                    

                    if($FKtipoDeTrabajador==3){

                        $queryActualizacion = "UPDATE usuario SET UsuFKTip = '".$FKtipoDeTrabajador."', UsuNomUsu= '" . $correo . "', UsuConUsu = '" . $dni . "' WHERE UsuId = ".$UsuarioId;
                        $canUpdate = $canUpdate + 1;
                        if ($db->query($queryActualizacion)) {
                           //echo "Se actualizo el usuario " . $apellidos . " con dni >> ".$dni."<br>";
                        } else {
                            echo "Error: " . $queryActualizacion . "<br>" . $db->error;
                        }
                    }
                    if($FKtipoDeTrabajador==4){

                        $queryActualizacion = "UPDATE usuario SET UsuFKTip2 = '".$FKtipoDeTrabajador."' , UsuNomUsu= '" . $correo . "', UsuConUsu = '" . $dni . "' WHERE UsuId = ".$UsuarioId;
                        $canUpdate = $canUpdate + 1;
                        if ($db->query($queryActualizacion)) {
                            //echo "Se actualizo el usuario " . $apellidos . " con dni >> ".$dni."<br>";
                        } else {
                            echo "Error: " . $queryActualizacion . "<br>" . $db->error;
                        }
                    }



                }
            }
        }
       // $output .= '</table>';
        $confirmacion  = '<h3>Se Insertaron '.$canInsert.' nuevos registros y se actualizaron '.$canUpdate.' registros</h3>';
        //$db->close;
    } else {
        $output = '<label class="text-danger">Seleccione Archivo Excel</label>'; //if non excel file then
    }
    
}

function filtroCaracteresEspeciales($texto)
{
    $utf8 = array(
        '/[áàâãªä]/u' => 'a',
        '/[ÁÀÂÃÄ]/u' => 'A',
        '/[ÍÌÎÏ]/u' => 'I',
        '/[íìîï]/u' => 'i',
        '/[éèêë]/u' => 'e',
        '/[ÉÈÊË]/u' => 'E',
        '/[óòôõºö]/u' => 'o',
        '/[ÓÒÔÕÖ]/u' => 'O',
        '/[úùûü]/u' => 'u',
        '/[ÚÙÛÜ]/u' => 'U',
        '/ç/' => 'c',
        '/Ç/' => 'C',
        '/ñ/' => 'n',
        '/Ñ/' => 'N',
        '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u' => ' ', // Literally a single quote
        '/[“”«»„]/u' => ' ', // Double quote
        '/ /' => ' ' // nonbreaking space (equiv. to 0x160)
    );
    
    $resultado = preg_replace(array_keys($utf8), array_values($utf8), $texto);
    $resultado = preg_replace('/[^A-Za-z0-9\-\ ]/', '', $resultado); // Removes special chars.
    //$resultado = strtolower($resultado);
    return $resultado;
}

function seleccionBaseDeDatos($sql, $db, $id){
    $result = $db->query($sql);
    $resultId = "";
    
    if ($result->num_rows == 0 || $result->num_rows== NULL) {
        //echo "<br> 0 results <br>";
        $resultId = "";
    } else {
    // output data of each row
        if($row = $result->fetch_assoc()) {
            $resultId = $row[$id];
        }
    }
    return $resultId;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
    <title>OFICINA DE ADMISION</title>

    <!-- ESTILOS -->
    <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
    <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
    <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

    <!-- SCRIPTS -->
    <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
    <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
    <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
    <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
    <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

</head>

<body>
    <div id="wrapper">
        <!-- Menu -->
        <?php
        include '../../menu.php';
        ?>

        <!-- Header -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8">
                        <h1 class="page-header" style="color:#5e151d;">Carga de datos de Docentes y Administrativos</h1>
                    </div>
                    <div class="col-lg-4">
                        <br>
                        <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
                    </div>
                </div>  


  <title>Carga de Alumnos</title>
 


</head>
 


    <!-- Header -->
          
            <!-- Main -->
            <div id="main" class="wrapper style1">
                
                <div class="container">
                        <!-- Content -->
                        <header class="major">  
                                 
                            <div class="container box">
                    <h3 >Importar la lista de Docente y Administrativos</h3><br />
                    <form method="post" enctype="multipart/form-data">
                        <h4>Seleccionar el Archivo en excel</h4>
                        <input type="file" width="500px" class="btn btn-outline btn-warning" name="excel" />
                        <br />
                        <input type="submit" name="import" class="btn btn-success" value="Cargar Archivo" />
                    </form>
                    <br/>
                    <br/>
                    <?php
                    //echo $output;
                    echo $confirmacion;
                    ?>
                </div>

                        </header>
                </div>
            </div>
        <!-- Footer -->

            </div>
        </div>

    </div>

      
    </body>
</html>