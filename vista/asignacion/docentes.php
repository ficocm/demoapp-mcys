<!DOCTYPE html>
<html lang="es">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">	<!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>	<!-- jQuery -->	
	<script src="../../vendor/jquery/popper.min.js"></script>	<!-- Poppper -->	
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!-- Mis modificaciones -->
	<link href="../../css/estilosMensajes.css" rel="stylesheet"> 
	<script src="../../js/util.js"></script>
	<script src="../../js/docentesSorteados.js"></script>

</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Asignación / Docentes </h1>
					</div>
					<div class="col-lg-4">
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>

				<div class="row grid-row">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>
				<br>	

				<!-- PAGUE CONTENT -->
				<div class="col-lg-12">
					<div id="table-wrapper">
						<div class="row">
							<div class='col-sm-5 pull-right'>
								<div id="custom-search-input">
									<div class="input-group col-md-12">
										<input type="text" class="form-control" placeholder="Busqueda (Nombre)"	id="busqueda" onkeyup="cargar(1)" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" onclick="cargar(1);">
												<span class="glyphicon glyphicon-search"></span>
											</button>
										</span>
									</div>
								</div>
							</div>

							<div class="col-lg-6 pull-left">
								<div class="form-group">
									<label class="control-label col-sm-4">Filtro por Cargo:</label>
									<div class="col-sm-8">
										<SELECT class="form-control" name="cargo1" id="cargo1" onchange="cargar(1);">
											<option value="-3" selected>TODOS</option>
										</SELECT>
									</div>
								</div>
							</div>
						</div>

						<br>
						<div id="table-scroll">
							<table class="table table-striped estiloHover" id="usuarios">
								<tbody style="">
									<tr>
										<td><strong>Editar <br>Asignación</strong></td>
										<td><strong>Dni</strong></td>
										<td><strong>Apellidos</strong></td>
										<td><strong>Nombres</strong></td>
										<td><strong>Cargo</strong></td>
										<td><strong>Area</strong></td>
										<td><strong>Pabellon1</strong></td>
										<td><strong>Aula1</strong></td>
									</tr>
								</tbody>
							</table>
							<div id="paginacion">
							</div>
						</div>
					</div>
				</div>


						<!-- Modal content-->
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog modal-lg">

								<form class="form-horizontal" id="formulario">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title text-center">Editar Asignación</h4>
										</div>

										<div class="modal-body">
											<form class="form-horizontal" id="formulario">

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Area:</label>
														<div class="col-sm-10">
															<SELECT class="form-control" name="area" id="area" onchange="seleccionarArea()">
																<option value="" selected >Seleccione Area</option>
															</SELECT>
														</div>
													</div>
												</div>
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Pabellon1:</label>
														<div class="col-sm-10">
															<SELECT class="form-control" name="pabellon1" id="pabellon1" onchange="seleccionarPabellon()">
				 												<option value="" selected >Seleccione Pabellon</option>
															</SELECT>
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-lg-6">
													<div class="form-group">
														<label class="control-label col-sm-2">Aula:</label>
														<div class="col-sm-10">
															<SELECT class="form-control" name="aula1" id="aula1">
																<option value="" selected >Seleccione Aula</option>
															</SELECT>
														</div>
													</div>
												</div>
												<div class="col-lg-6" id="seguridad">
													<div class="form-group">
														<label class="control-label col-sm-6" id="mensajeSeguridad">¿Es de Seguridad?:</label>
														<div class="col-sm-6">
															<input type="radio" name="seguridad" id="seguridad1" VALUE="SI">Si <br>
															<input type="radio" name="seguridad" id="seguridad2" VALUE="NO" checked>No
														</div>
													</div>
												</div>
											</div>

										</form>
										<!--input type="button" onclick="resetForm();" class="btn btn-primary" id="submit" value="BORRAR ASIGNACIONES"-->
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
										<button type="button" class="btn btn-primary" id="save">Guardar</button>
									</div>
								</div>

								</form>
								
							</div>

						</div>
						<!-- End Modal-->
			</div>
		</div>

		<!-- Management Error -->
		<div class="error"></div>
		<div class="correcto"></div>

	</div>
</body>

</html>
