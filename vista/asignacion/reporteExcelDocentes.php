<?php
if (PHP_SAPI == 'cli')
	die('Este ejemplo sólo se puede ejecutar desde un navegador Web');
 
require_once("../../conexion/conexion.php");
$mysqli = Conectar::conexion();

/** Incluye PHPExcel */
require_once("../../vendor/excelReport/PHPExcel.php");
// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();
 
// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Oficina Admision")
							 ->setLastModifiedBy("Oficina Admision")
							 ->setTitle("Reporte de Docentes Asignados")
							 ->setSubject("Reporte de Docentes Asignados")
							 ->setDescription("Reporte de Docentes Asignados")
							 ->setKeywords("Docentes Asignados")
							 ->setCategory("Archivo Excel");
 
 
 
// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
 
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'REPORTE DOCENTES ASIGNADOS')
            ->setCellValue('A2', 'NOMBRE')
            ->setCellValue('B2', 'CARGO')
            ->setCellValue('C2', 'CORREO')
            ->setCellValue('D2', 'TELEFONO')
            ->setCellValue('E2', 'AREA')
            ->setCellValue('F2', 'PABELLON1')
            ->setCellValue('G2', 'PABELLON2');
			
// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
 
$objPHPExcel->getActiveSheet()->getStyle('A1:G2')->applyFromArray($boldArray);		
 
	
			
//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);	
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);			
 
/*Extraer datos de MYSQL*/
	# conectare la base de datos
    
	$sql="select admisiondetalle.AdmDetId as id, concat(usuario.UsuApe, ', ',usuario.UsuNom) as nombre, usuario.UsuCorEle, usuario.UsuTel, 
(SELECT area.AreDes from area INNER join admisionarea on admisionarea.AdmAreFKAreId=area.AreId INNER join admisiondetalle on admisionarea.AdmAreFKAreId=admisiondetalle.AdmDetFKAre where admisiondetalle.AdmDetId=id) as area, 
(SELECT pabellon.PabDes from pabellon INNER join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab where admisiondetalle.AdmDetId=id) as pabellon1 ,
(SELECT pabellon.PabDes from pabellon INNER join admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId inner join admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab2 where admisiondetalle.AdmDetId=id) as pabellon2,
(SELECT cargo.CarDes from cargo INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKCar=cargo.CarId  where admisiondetalle.AdmDetId=id) as cargo1, 
(SELECT cargo.CarDes from cargo INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKCar2=cargo.CarId where admisiondetalle.AdmDetId=id) as cargo2
from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=17) and usuario.UsuFKTip=3 and admisiondetalle.AdmDetFKAdmCabId=1 order by cargo1,cargo2,usuario.UsuApe";
	$resultado = $mysqli->query($sql);

	$cel=3;//Numero de fila donde empezara a crear  el reporte
	while ($row = $resultado->fetch_assoc()){
		$nombre=$row['nombre'];
		$correo=$row['UsuCorEle'];
		$telefono=$row['UsuTel'];
		$area=$row['area'];
		$pabellon1=$row['pabellon1'];
		$pabellon2=$row['pabellon2'];
		$cargo1=$row['cargo1'];
		
			$a="A".$cel;
			$b="B".$cel;
			$c="C".$cel;
$d="D".$cel;
$e="E".$cel;
$f="F".$cel;
$g="G".$cel;
			// Agregar datos
			$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a, $nombre)
            ->setCellValue($b, $cargo1)
            ->setCellValue($c, $correo)
            ->setCellValue($d, $telefono)
            ->setCellValue($e, $area)
            ->setCellValue($f, $pabellon1)
            ->setCellValue($g, $pabellon2);
			
	$cel+=1;
	}
 
 if($cel>3){
	/*Fin extracion de datos MYSQL*/
	$rango="A2:$g";
	$styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
	'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
	);
	$objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
}
// Cambiar el nombre de hoja de cálculo
$objPHPExcel->getActiveSheet()->setTitle('Docentes Asignados');
 
 
// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);
 
 
// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="DocentesAsignados.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');
 
// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>
