<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">	<!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>	<!-- jQuery -->	
	<script src="../../vendor/jquery/popper.min.js"></script>	<!-- Poppper -->	
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!--Modificaciones propias-->
	<link href="../../css/estilosMensajes.css" rel="stylesheet">
	<script src="../../js/util.js"></script>
	<script src="../../js/docentesTecnicosSorteados.js"></script>
	
</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Participantes / Ayudantes </h1>
					</div>
					<div class="col-lg-4">
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>

				<div class="row grid-row">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>

				
				<!-- COMIENZO -->
				<div class="table-wrapper">
					<div class="table-title">
						<div class="row">
							<div class="col-sm-12">
								<b>LISTA DE DOCENTES TÉCNICOS</b>
							</div>
									
						</div>
					</div>
					<div class='col-sm-4 pull-right'>
						<div id="custom-search-input">
							<div class="input-group col-md-12">
								<input type="text" class="form-control" placeholder="Ingrese DNI o Nombre"	id="busqueda" onkeyup="cargar(1);" />
								<span class="input-group-btn">
												<button class="btn btn-info" type="button" onclick="cargar(1);">
										<span class="glyphicon glyphicon-search"></span>
												</button>
								</span>
							</div>
						</div>
					</div>
				</div>
				<br><br>
				<div id="table-scroll">
					<table class="table table-striped estiloHover" id="usuarios">
						<tbody>
							<tr>
								<th class='text-center'>DNI</th>
								<th>Apellidos</th>
								<th>Nombres</th>
								<th class='text-center'>Cantidad de Ayudantes</th>
								<th class='text-center'>Agregar o Ver Ayudantes Tecnicos</th>
							</tr>
						</tbody>
					</table>
					<div id="paginacion">
					</div>
				</div>
				
					<!-- Modal -->
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog modal-lg">
							
								<!-- Modal content-->
								<div class="modal-content">
								 
									<div class="modal-body">
										<h4 class="text-center">Ayudantes Asignados</h4>
										<table class="table table-striped table-hover" id="ayudantesAsignados">
											<thead>
												<tr>
													<th class='text-center'>CUI</th>
													<th>Apellidos</th>
													<th>Nombre</th>
													<th class='text-center'>Categoria</th>
													<th class='text-center'>Accion</th>
												</tr>
											</thead>
										</table>

										<br>
										<div class='col-sm-4 pull-right'>
											<div id="custom-search-input">
												<div class="input-group col-md-12">
													<input type="text" class="form-control" placeholder="Ingrese (CUI o Nombre)"	id="busquedaEstudiantes" onkeyup="cargarEstudiantes(1);" />
													<span class="input-group-btn">
														<button class="btn btn-info" type="button" onclick="cargar(1);">
														 <span class="glyphicon glyphicon-search"></span>
														</button>
													</span>
												</div>
											</div>
										</div>
										<h4 class="text-center">Lista de estudiantes</h4>
										<table class="table table-striped table-hover" id="listaEstudiantes">
											<thead>
												<tr>
													<th class='text-center'>CUI</th>
													<th>Apellidos</th>
													<th>Nombres</th>
													<th class='text-center'>Categoria</th>
													<th class='text-center'>Asignado a</th>
													<th class='text-center'>Accion</th>
												</tr>
											</thead>
										</table>
										<div class='col-sm-12'>
											<div id="paginacionEstudiantes">
											</div>
										</div>
										
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									</div>
								</div>
								
							</div>
						</div>
						<!-- End Modal-->

						<!-- Modal content-->
						<div class="modal fade confirmacion" role="dialog">
							<div class="modal-dialog modal-sm">

								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title text-center">Mensaje de Confirmación</h4>
									</div>
									<div class="modal-body" id="mensajeConfirmacion">
										DESEA ELIMINAR
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal" id="cancelar">Cancelar</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal" id="confirmar">Confirmar</button>
									</div>
								</div>
								
							</div>
						</div>
						<!-- End Modal-->

				<!--FIN -->

			</div>
		</div>

		<!-- Management Error -->
		<div class="error"></di	v>
		<div class="correcto"></div>

	</div>
</body>

</html>
