<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" style="border-color: #5e151d; background-color:#141e42; color: #fff;" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header" >
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a  class="navbar-brand"  style=" background-color:#141e42; color: #fff; font-size: 23px;"><strong>OFICINA DE ADMISION - UNIVERSIDAD NACIONAL DE SAN AGUSTIN DE AREQUIPA</strong> </a>
    </div>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" style="background-color:#5e151d;" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    <!-- /input-group -->
                </li>
                   <li>
            <a style="color: #ffffff" href="#"><i class="fa fa-refresh fa-spin" style="font-size:20px"></i> Guia de desarrollo (Eliminar)<span class="fa arrow"></span></a>
            <ul style="color: #ffffff" class="nav nav-second-level">
                <li><a style="color: #ffffff" href="../../GuiaDesarrolllo-Eliminar/buttons.php">Botones</a></li>
                <li><a style="color: #ffffff" href="../../GuiaDesarrolllo-Eliminar/forms.php">Formularios</a></li>
                <li><a style="color: #ffffff" href="../../GuiaDesarrolllo-Eliminar/tables.php">Tablitas</a></li>
            </ul>
        </li>
                <li>
                    <a style="color: #ffffff"  href="../../inicio/inicio.php"><i class="fa" style="font-size:18px">&#xf004;</i>  Inicio</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="../../miPerfil/miPerfil.php"><i class="fa" style="font-size:18px">&#xf007;</i>  Mi Perfil</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="#"><i class="fa fa-cog fa-spin" style="font-size:22px"></i> Proceso<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../../proceso/postulantes.php">Postulantes</a></li>
                        <li><a style="color: #ffffff" href="../../proceso/distribuicion.php">Distribuicion</a></li>
                        <li><a style="color: #ffffff" href="../../proceso/capacidades.php">Capacidades</a></li>
                    </ul>
                </li>
                <li>
                    <a style="color: #ffffff" href="../../usuario/usuario.php"><i style="font-size:22px" class="fa">&#xf21d;</i> Usuario</a>
                </li>
                <li>
                    <a style="color: #ffffff" href="#"><i class="fa" style="font-size:20px">&#xf0c0;</i> Particpantes<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../../participantes/docentes.php">Docentes</a></li>
                        <li><a style="color: #ffffff" href="../../participantes/administrativos.php">Administrativos</a></li>
                        <li><a style="color: #ffffff" href="../../participantes/capacidades.php">Capacidades</a></li>
                        <li><a style="color: #ffffff" href="../../participantes/reemplazo.php">Reemplazo</a></li>
                    </ul>
                </li>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0e8;</i> Sorteo<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../../sorteo/sortAdmVar.php">Sorteo Administrativo Varones</a></li>
                        <li><a style="color: #ffffff" href="../../sorteo/sortAdmMuj.php">Sorteo Administrativo Mujeres</a></li>
                        <li><a style="color: #ffffff" href="../../sorteo/controlador.php">Sorteo Controladores</a></li>
                    </ul>
                </li>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0e3;</i>       Asignacion<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a style="color: #ffffff" href="../../asignacion/docentes.php">Docentes</a></li>
                        <li><a style="color: #ffffff" href="../../asignacion/administrativos.php">Aministrativos</a></li>
                        <li><a style="color: #ffffff" href="../../asignacion/comAdm.php">Comision Administrativos</a></li>
                        <li><a style="color: #ffffff" href="../../asignacion/comDoc.php">Comision Docentes</a></li>
                    </ul>
                </li>
                <li>
                    <a style="color: #ffffff" href="#"><i style="font-size:22px" class="fa">&#xf1ea;</i> Credenciales<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                       <li><a style="color: #ffffff" href="../../credenciales/busqueda.php">Busqueda</a></li>
                       <li><a style="color: #ffffff" href="../../credenciales/tipo.php">Tipo</a></li>
                       <li><a style="color: #ffffff" href="../../credenciales/envEmails.php">Envio De Emails</a></li>
                   </ul>
               </li>

               <li>
                <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf0b1;</i>  Abastecimiento<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a style="color: #ffffff" href="../../abastecimiento/material.php">Material</a></li>
                    <li><a style="color: #ffffff" href="../../abastecimiento/examenes.php">Examenes</a></li>
                </ul>
            </li>

            <li>
                <a style="color: #ffffff" href="#"><i style="font-size:22px" class="fa">&#xf1ea;</i> Credenciales<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                   <li><a style="color: #ffffff" href="../../credenciales/busqueda.php">Busqueda</a></li>
                   <li><a style="color: #ffffff" href="../../credenciales/tipo.php">Tipo</a></li>
                   <li><a style="color: #ffffff" href="../../credenciales/envEmails.php">Envio De Emails</a></li>
               </ul>
           </li>
           <li>
            <a style="color: #ffffff" href="#"><i class="fa fa-refresh fa-spin" style="font-size:20px"></i>  Mantenimiento<span class="fa arrow"></span></a>
            <ul style="color: #ffffff" class="nav nav-second-level">
                <li><a style="color: #ffffff" href="../../mantenimiento/areas">Areas</a></li>
                <li><a style="color: #ffffff" href="../../mantenimiento/facultades.php">Facultades</a></li>
                <li><a style="color: #ffffff" href="../../mantenimiento/departamentos.php">Departamentos</a></li>
                <li><a style="color: #ffffff" href="../../mantenimiento/pabellones.php">Pabellones</a></li>
                <li><a style="color: #ffffff" href="../../mantenimiento/aulas.php">Aulas</a></li>
                <li><a style="color: #ffffff" href="../../mantenimiento/cargos.php">Cargos</a></li>
            </ul>
        </li>
        <li>
            <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf15c;</i> Anexos<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a style="color: #ffffff" href="../../anexos/administrativos.php">Administrativos</a></li>
                <li><a style="color: #ffffff" href="../../anexos/tecnicos.php">Tecnicos</a></li>
                <li><a style="color: #ffffff" href="../../anexos/controladores.php">Controladores</a></li>
            </ul>
        </li>
        <li>
            <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf02d;</i> Reportes<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a style="color: #ffffff" href="../../reportes/general.php">General</a></li>
                <li><a style="color: #ffffff" href="../../reportes/pabArePart.php">Pabellones y Areas Participantes</a></li>
                <li><a style="color: #ffffff" href="../../reportes/admEstPart.php">Administrativos por Estado de Participación</a></li>
                <li><a style="color: #ffffff" href="../../reportes/docEstPart.php">Docente Por Estado de Participación</a></li>
                <li><a style="color: #ffffff" href="../../reportes/estEstPart.php">Estudiantes Por Estado de Participación </a></li>
                <li><a style="color: #ffffff" href="../../reportes/asiPab.php">Asistencias por Pabellon</a></li>
                <li><a style="color: #ffffff" href="../../reportes/recRef.php">Recepción de Refrigerios</a></li>
                <li><a style="color: #ffffff" href="../../reportes/cantidad.php">Cantidades</a></li>
                <li><a style="color: #ffffff" href="../../reportes/capAre.php">Capacidades de las Areas</a></li>
                <li><a style="color: #ffffff" href="../../reportes/distInf.php">Distribución para Informática</a></li>
                <li><a style="color: #ffffff" href="../../reportes/comAdmAdm.php">Comisión de Admisión Administrativos</a></li>
                <li><a style="color: #ffffff" href="../../reportes/comAdmDoc.php">comisión de Admisión Docentes</a></li>
            </ul>
        </li>
        <li>
            <a style="color: #ffffff" href="../../incripcion/incripcion.php"><i style="font-size:24px" class="fa">&#xf1fc;</i>Incripcion</a>
        </li>
        <li>
            <a style="color: #ffffff" href="../../archivos/archivos.php"><i style="font-size:24px" class="fa">&#xf07c;</i>Archivos</a>
        </li>
        <li>
            <a style="color: #ffffff" href="#"><i style="font-size:20px" class="fa">&#xf044;</i>Editar<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a style="color: #ffffff" href="../../editar/tipPar.php">Tipos de Participantes</a></li>
            </ul>
        </li>

        <li>
            <a style="color: #ffffff" href="../../../index.php"><i style="font-size:20px" class="fa">&#xf122;</i> SALIR</a>
        </li>

    </ul>
</div>
<!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>