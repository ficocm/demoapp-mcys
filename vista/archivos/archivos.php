<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->


</head>
<style>
	table {
	    font-family: arial, sans-serif;
	    border-collapse: collapse;
	    width: 66%;
	}
	.d1{
		border: 1px solid #dddddd;
	    text-align: left;
	    width: 600px;
	    padding: 8px;
	}
	.d2{
		border: 1px solid #dddddd;
	    text-align: left;
	    width: 60px;
	    padding: 8px;
	}

	tr:nth-child(even) {
	    background-color: #dddddd;
	}
</style>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Archivos</h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div> 

				<input type="hidden" id="id" name="id" value="<?php echo $_SESSION["idUsuario"]?>">
				<input type="hidden" id="estReg" name="estReg" value="">
				<input type="hidden" id="tipo" name="tipo" value=""> 

				<!-- PAGUE CONTENT -->

					<table>
					<tr>
						<th>Documentos</th>
					</tr>
					<tr>
						<th>
							<div id="pdf">
								
							</div>
						</th>
					</tr>
		
				</table>
				<br>
						
			</div>
			<!-- /.container-fluid -->


		</div>
	</div>
	<script src="../../js/verifAdmi.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$tipo=document.getElementById("tipo").value;
			$estReg=document.getElementById("estReg").value;

        	$("#pdf").load('../../vista/anexos/pdf.php',{t:$tipo,estReg:$estReg},function(response, status, xhr) {
						  if (status == "error") {
						    var msg = "Error!, algo ha sucedido: ";
						    $("pdf").html(msg + xhr.status + " " + xhr.statusText);
						  }
						});
        	return false;
		});
	</script>
	<script src="../../js/estRegDet.js"></script>
</body>

</html>
