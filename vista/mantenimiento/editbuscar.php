<?php
//Archivo de conexión a la base de datos
require_once("../../modelo/usuario/util.php");
require_once("../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
  $db = Conectar::conexion();

//Variable de búsqueda
$consultaBusqueda = $_POST['valorBusqueda'];
$PadDes="";

$mensaje = "";


//Comprueba si $consultaBusqueda está seteado
if (isset($consultaBusqueda)) {

 $consulta = $db->query('SELECT * FROM aula WHERE AulFKPab='.$consultaBusqueda);


  //Obtiene la cantidad de filas que hay en la consulta
  $filas = $db->num_rows;


  //Si no existe ninguna fila que sea igual a $consultaBusqueda, entonces mostramos el siguiente mensaje
  if ($filas === 0) {
    $mensaje = "<p>No hay ningún pabellon con ese Id</p>";
  } else {
    
$consultaPabellon = $db->query('SELECT PabDes FROM pabellon WHERE PabId='.$consultaBusqueda);
 while($pabdescripcion = $consultaPabellon->fetch_assoc()) {
         $PabDes = $pabdescripcion['PabDes'];
        
      }

echo '
<div name="tablaaulas">
  <div class="col-sm-6">
    <a href="#addProductModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar nueva aula</span></a>
  </div>
  <h2>Aulas de '.$PabDes.'</h2>  <br>   
  <table id="data_table" class="table table-striped">
    <thead>
      <tr>
        <th>Id</th>
        <th>Aula</th>
        <th>Aforo</th>
        <th>Piso</th>
        <th>Fila</th> 
        <th>Columna</th>
        <th>Capacidad</th>
      </tr>
    </thead>
    <tbody>';
       
      $sql_query = "SELECT * FROM aula WHERE AulFKPab=".$consultaBusqueda;
      $resultset = $db->query($sql_query) or die("database error:". $db->error);
      while( $developer = $resultset->fetch_assoc() ) {
      
         $mensaje .= '
        <tr id='.$developer['AulId'].'>
         <td>' .$developer ['AulId'] .'</td>
         <td>' . $developer ['AulNum'] . '</td>
         <td>' . $developer ['AulAfo'] . '</td>
         <td>' . $developer ['AulPis']. '</td>
         <td>' . $developer ['AulFil']. '</td>
         <td>' . $developer ['AulCol']. '</td>
         <td>' . $developer ['AulCap']. '</td>
         </tr>';
         
      } echo '</tbody>';
      echo $mensaje;
    echo '</table>  
  </div><script type="text/javascript" src="custom_table_edit.js">
  </script><script src="js/script_aulas.js"></script>';

   include("html/modal_add_aulas.php");
  }; //Fin else $filas

};//Fin isset $consultaBusqueda






