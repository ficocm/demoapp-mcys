$(document).ready(function(){
	$('#data_table').Tabledit({
		deleteButton: false,
		editButton: true,   		
		columns: {
		  identifier: [0, 'AulId'],                    
		  editable: [[1, 'AulNum'], [2, 'AulAfo'], [3, 'AulPis'], [4, 'AulFil'], [5, 'AulCol'], [6, 'AulCap']]
		},
		hideIdentifier: true,
		url: 'live_edit.php'		
	});
});