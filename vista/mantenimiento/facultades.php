<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/custom.css">

	<!-- programa Part -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<script>
		//Funcion General para areas en select - options
		function cargarAreas(facultad_area){
			var parametros = {"action":"ajax"};
			$.ajax({
				url:'../../controlador/inicio/procesosControlador.php?accion=mostrarAreas',
				data: parametros,
				type: "POST",
				beforeSend: function(objeto){
					$("#here-Areas").html("Cargando...");
				},
				success:function(result){
					var areas = JSON.parse(result);
					var myDiv = document.getElementById("here-Areas");
					var arrayAreas = areas;

					//console.log(document.getElementById("edit_areas").value);

					//Create and append select list
					var selectList = document.createElement("select");
					selectList.id = "edit_areas";
					selectList.name = "edit_areas";
					selectList.className = "form-control";

					myDiv.appendChild(selectList);

					//Create and append the options
					for (var i = 0; i < arrayAreas.length; i++) {
						var option = document.createElement("option");
						if(facultad_area == arrayAreas[i]['AreDes']){
							option.value = arrayAreas[i]['AreId'];
							option.text = arrayAreas[i]['AreDes'];
							option.selected = "selected";
							selectList.appendChild(option);
						}else{
							option.value = arrayAreas[i]['AreId'];
							option.text = arrayAreas[i]['AreDes'];
							selectList.appendChild(option);
						}
					}

				}
			})
		}

	</script><!-- Custom Theme JavaScript -->

</head>
<body>

	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Mantenimiento de Pabellones </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->

				<div class="table-wrapper">
					<div class="table-title">
						<div class="row">
							<div class="col-sm-6">
								<h2>Administrar Pabellones</h2>
							</div>
							<div class="col-sm-6">
								<a href="#addFacultadModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar nuevo pabellon</span></a>
							</div>
						</div>
					</div>
					<div class='col-sm-4 pull-right'>
						<div id="custom-search-input">
							<div class="input-group col-md-12">
								<input type="text" class="form-control" placeholder="Buscar"  id="q" onkeyup="load(1);" />
								<span class="input-group-btn">
									<button class="btn btn-info" type="button" onclick="load(1);">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</span>
							</div>
						</div>
					</div>
					<div class='clearfix'></div>
					<hr>
					<div id="loader"></div><!-- Carga de datos ajax aqui -->
					<div id="resultados"></div><!-- Carga de datos ajax aqui -->
					<div class='outer_div'></div><!-- Carga de datos ajax aqui -->


				</div>
				<!-- Edit Modal HTML -->
				<?php include("html/modal_add_pabellon.php");?>
				<!-- Edit Modal HTML -->
				<?php include("html/modal_edit_pabellon.php");?>
				<!-- Delete Modal HTML -->
				<?php include("html/modal_delete_pabellon.php");?>
				<script src="js/script_pabellon.js"></script>
				
			</div>

		</div>

	</div>

</body>
</html>                                		                            