<?php
	if (empty($_POST['edit_id'])){
		$errors[] = "Nombre de area vacio";
	} elseif (!empty($_POST['edit_id'])){
		require_once("../../../modelo/usuario/util.php");
	require_once ("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();

	$area_des = $db->real_escape_string(strip_tags(strtoupper($_POST["edit_name"]),ENT_QUOTES));
	
	$area_id=intval($_POST['edit_id']);
	// UPDATE data into database
    $sql = "UPDATE area SET AreDes='".$area_des."'   WHERE AreId='".$area_id."' ";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
        $messages[] = "El area ha sido actualizada con éxito.";
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
							
				</div>
				<?php
			}
?>			