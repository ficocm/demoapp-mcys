<?php
	
	/* Connect To Database*/
	require_once ("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	require_once("../../../modelo/usuario/util.php");
	// escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();

	
$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){
	$query = $db->real_escape_string(strip_tags($_REQUEST['query'], ENT_QUOTES));

	$tables="cargo";
	$campos="*";
	$sWhere=" cargo.CarDes LIKE '%".$query."%' and CarFKEstReg=18";
	$sWhere.=" order by CarFKTipUsu,cargo.CarDes ASC, CarId ASC ";
	
	
	include 'pagination.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   = $db->query("SELECT count(*) AS numrows FROM $tables where $sWhere ");
	if ($row= $count_query->fetch_assoc()){$numrows = $row['numrows'];}
	else {echo $db->error;}
	$total_pages = ceil($numrows/$per_page);
	//main query to fetch the data
	$query = $db->query("SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");
	//loop through fetched data
	


		
	
	if ($numrows>0){
		
	?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class='text-center'>Tipo de participante</th>
						<th class='text-center'>Cargo</th>
						<th class='text-center'>Cargo de Comision</th>
						<!--<th>Producto </th>
						<th>Categoría </th>
						<th class='text-center'>Stock</th>
						<th class='text-right'>Precio</th>-->
						<th></th>
					</tr>
				</thead>
				<tbody>	
						<?php 
						$finales=0;
						while($row = $query->fetch_assoc()){	
							$cargo_id=$row['CarId'];
							$cargo_des=$row['CarDes'];
							$cargo_tip=$row['CarFKTipUsu'];
							$cargo_com=$row['CarCom'];
							if($cargo_tip==3)
								$cargo_tip="Docente";
							if($cargo_tip==4)
								$cargo_tip="Administrativo";
							if($cargo_tip==20)
								$cargo_tip="Estudiante";


							$finales++;
						?>	
						<tr class="<?php echo $text_class;?>">
							<td ><?php echo $cargo_tip;?></td>
							<td ><?php echo $cargo_des;?></td>	
							<td ><?php echo $cargo_com;?></td>						
							<td>
								<a href="#"  data-target="#editProductModal" class="edit" data-toggle="modal" data-tip="<?php echo $cargo_tip?>" data-name="<?php echo $cargo_des?>" data-com="<?php echo $cargo_com?>" data-id="<?php echo $cargo_id; ?>"><i class="material-icons" data-toggle="tooltip" title="Editar" >&#xE254;</i></a>
								<a href="#deleteProductModal" class="delete" data-toggle="modal" data-id="<?php echo $cargo_id;?>"><i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i></a>
                    		</td>
						</tr>
						<?php }?>
						<tr>
							<td colspan='6'> 
								<?php 
									$inicios=$offset+1;
									$finales+=$inicios -1;
									echo "Mostrando $inicios al $finales de $numrows registros";
									echo paginate( $page, $total_pages, $adjacents);
								?>
							</td>
						</tr>
				</tbody>			
			</table>
		</div>	

	
	
	<?php	
	}	
}
?>          
		  
