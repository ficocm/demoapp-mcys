<?php
	
	/* Connect To Database*/
	require_once("../../../modelo/usuario/util.php");
	require_once("../../../conexion/conexion.php");


	$db = Conectar::conexion();
	
	
$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){

	$query = $db->real_escape_string(strip_tags($_REQUEST['query'], ENT_QUOTES));

	$tables="admisioncabecera INNER JOIN general on general.GenId=admisioncabecera.AdmCabFKTipPro ";

	$campos="admisioncabecera.AdmCabId, general.GenSubCat, CONCAT(CASE admisioncabecera.AdmCabNumFas WHEN 1 THEN 'PRIMERA' WHEN 2 THEN 'SEGUNDA' WHEN 3 THEN 'TERCERA' END, ' FASE', ' - ', CONCAT(IF(general.GenSubCat = 'ORDINARIO', 'DIA N°', 'EVALUACION N°'), ' ', admisioncabecera.AdmCabNumEva)) AS descripcion, admisioncabecera.AdmCabNumFas, admisioncabecera.AdmCabNumEva, admisioncabecera.AdmCabFec, admisioncabecera.AdmCabAno, admisioncabecera.AdmCabFKEstReg";
	$sWhere="general.GenSubCat LIKE '%".$query."%' AND (admisioncabecera.AdmCabFKEstReg = 31 OR admisioncabecera.AdmCabFKEstReg = 32 OR admisioncabecera.AdmCabFKEstReg = 33)";
	$sWhere.="  order by admisioncabecera.AdmCabFec DESC, general.GenSubCat ASC, admisioncabecera.AdmCabNumFas ASC";
	
	
	include 'pagination.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   = $db->query("SELECT count(*) AS numrows FROM $tables where $sWhere ");
	if ($row= $count_query->fetch_assoc()){$numrows = $row['numrows'];}
	else {echo $db->error;}
	$total_pages = ceil($numrows/$per_page);
	//main query to fetch the data
	$query = $db->query("SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");
	//loop through fetched data
	//echo("SELECT $campos FROM  $tables where $sWhere ");


		
	
	if ($numrows>0){
		
	?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Tipo de Proceso</th>
						<th>Año</th>
						<th>Descripción</th>
						<th>Fecha de evaluacion</th>
					</tr>
				</thead>
				<tbody>	
						<?php 
						$finales=0;
						while($row = $query->fetch_assoc()){	
							$proceso_id=$row['AdmCabId'];
							$proceso_tipo=$row['GenSubCat'];
							$proceso_anio=$row['AdmCabAno'];
							$proceso_des=$row['descripcion'];						
							$proceso_fec=$row['AdmCabFec'];

							$proceso_fas=$row['AdmCabNumFas'];
							$proceso_numeva=$row['AdmCabNumEva'];
							$finales++;
						?>	
						<tr class="<?php echo $text_class;?>">
							<td><?php echo $proceso_tipo;?></td>
							<td><?php echo $proceso_anio;?></td>
							<td ><?php echo $proceso_des;?></td>					
							<td ><?php echo $proceso_fec;?></td>
							<td>
								<a href="#"  data-target="#editProductModal" class="edit" data-toggle="modal" data-name="<?php echo $proceso_tipo;?>" data-anio="<?php echo $proceso_anio;?>" data-fase="<?php echo $proceso_fas ?>" data-eva="<?php echo $proceso_numeva ?>" data-fecha="<?php echo $proceso_fec; ?>" data-id="<?php echo $proceso_id; ?>"><i class="material-icons" data-toggle="tooltip" title="Editar" >&#xE254;</i></a>
								<a href="#deleteProductModal" class="delete" data-toggle="modal" data-id="<?php echo $proceso_id;?>"><i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i></a>
                    		</td>

						</tr>
						<?php }?>
						<tr>
							<td colspan='6'> 
								<?php 
									$inicios=$offset+1;
									$finales+=$inicios -1;
									echo "Mostrando $inicios al $finales de $numrows registros";
									echo paginate( $page, $total_pages, $adjacents);
								?>
							</td>
						</tr>
				</tbody>			
			</table>
		</div>	

	
	
	<?php	
	}	
}
?>          
		  
