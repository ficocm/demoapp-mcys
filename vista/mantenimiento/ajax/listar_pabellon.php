<?php
	
	/* Connect To Database*/
	require_once("../../../conexion/conexion.php");
	require_once("../../../modelo/usuario/util.php");

	$db = Conectar::conexion();
$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){
	$query = $db->real_escape_string(strip_tags($_REQUEST['query'], ENT_QUOTES));

//SELECT facultad.FacId, facultad.FacDes, area.AreDes FROM facultad INNER JOIN area ON facultad.FacFKAre=area.AreId
	$tables="pabellon INNER JOIN area ON pabellon.PabFKAre=area.AreId";
	$campos="pabellon.PabId, pabellon.PabDes, area.AreDes";
	$sWhere=" (pabellon.PabDes LIKE '%".$query."%' AND pabellon.PabFKEstReg = 18) ORDER BY pabellon.PabDes";
	
	
	include 'pagination.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	//$count_query   = mysqli_query($con,"SELECT count(*) AS numrows FROM facultad where $sWhere ");
	$count_query   = $db->query("SELECT count(*) AS numrows FROM pabellon where $sWhere ");
	$numrows = 0;
    if($fila = $count_query->fetch_assoc()) {
      $numrows = $fila['numrows'];
    }else{
    	echo $db->error;
    }
	
	$total_pages = ceil($numrows/$per_page);
	//main query to fetch the data
	
	//$query = mysqli_query($con,"SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");
	$query = $db->query("SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");


	//loop through fetched data
	
	
	if ($numrows>0){
		
	?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<!--<th class='text-center'>Código</th>-->
						<th >Pabellon</th>
						<th >Area</th>
						<th></th>
					</tr>
				</thead>
				<tbody>	
						<?php 
						$finales=0;
						while($row = $query->fetch_assoc()){	
							$facultad_id=$row['PabId'];
							$facultad_nombre=$row['PabDes'];
							$facultad_area=$row['AreDes'];
							$finales++;
						?>	
						
							<td ><?php echo $facultad_nombre;?></td>
							<td ><?php echo $facultad_area;?></td>
							<td>
								<a href="#"  data-target="#editFacultadModal" class="edit" data-toggle="modal" data-name="<?php echo $facultad_nombre?>" data-area="<?php echo $facultad_area?>" data-id="<?php echo $facultad_id; ?>"><i class="material-icons" data-toggle="tooltip" title="Editar" onclick="cargarAreas(<?php echo "'".$facultad_area."'"; ?>);">&#xE254;</i></a>
								<a href="#deleteFacultadModal" class="delete" data-toggle="modal" data-id="<?php echo $facultad_id;?>"><i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i></a>
                    		</td>
						</tr>
						<?php }?>
						<tr>
							<td colspan='6'> 
								<?php 
									$inicios=$offset+1;
									$finales+=$inicios -1;
									echo "Mostrando $inicios al $finales de $numrows registros";
									echo paginate( $page, $total_pages, $adjacents);
								?>
							</td>
						</tr>
				</tbody>			
			</table>
		</div>	

	
	
	<?php	
	}	
}
?>          
		  
