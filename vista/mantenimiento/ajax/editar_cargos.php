<?php
	if (empty($_POST['edit_id'])){
		$errors[] = "Nombre de cargo vacio";
	} elseif (!empty($_POST['edit_id'])){
		require_once("../../../modelo/usuario/util.php");
	require_once ("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();
    
	$cargo_tip = $db->real_escape_string(strip_tags($_POST["edit_tipo"],ENT_QUOTES));
	$cargo_des = $db->real_escape_string(strip_tags(strtoupper($_POST["edit_name"]),ENT_QUOTES));
	$cargo_com = $db->real_escape_string(strip_tags($_POST["edit_comision"],ENT_QUOTES));
	
	$cargo_id=intval($_POST['edit_id']);
	// UPDATE data into database

    $sql = "UPDATE cargo SET CarFKTipUsu='".$cargo_tip."', CarDes='".$cargo_des."', CarCom='".$cargo_com."'   WHERE CarId='".$cargo_id."' ";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
        $messages[] = "El cargo ha sido actualizada con éxito.";
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
    }
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=cargos.php">-->
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
						<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=cargos.php">-->
				</div>
				<?php
			}
?>			