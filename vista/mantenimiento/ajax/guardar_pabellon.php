<?php
	if (empty($_POST['name'])){
		$errors[] = "Ingresa el nombre del producto.";
	} elseif (!empty($_POST['name'])){
		require_once("../../../modelo/usuario/util.php");
		require_once("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
		
		$db = Conectar::conexion();
		$idProceso=$_SESSION["idProceso"];
    $facultad_areaId = $db->real_escape_string(strip_tags($_POST["area"],ENT_QUOTES));
    $facultad_nombre = strtoupper($_POST["name"]);

	// REGISTER data into database
    $sql = "INSERT INTO pabellon(PabDes, PabFKAre) VALUES ('$facultad_nombre','$facultad_areaId')";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
    	$last_id = $db->insert_id;
    	//echo "last id >>>".$last_id;
    	//echo "<<<id de procesos >>>".$idProceso;
    	$sql_select ="SELECT admisionarea.AdmAreId from admisionarea inner join area on admisionarea.AdmAreFKAreId =area.AreId inner join pabellon on area.AreId=pabellon.PabFKAre where admisionarea.AdmAreFKAdmCabId=".$idProceso." and pabellon.PabId =".$last_id;
    	$result = $db->query($sql_select);
    	$resultId = "";
    		
        if($row = $result->fetch_assoc()) {
            $resultId = $row['AdmAreId'];
        }
        //echo "<<<id admareid >>>".$resultId;
    	$sql2 = "INSERT INTO admisionpabellon(AdmPabFKPabId,AdmPabFKAdmAreId,AdmPabFKEstReg) VALUES (".$last_id.",".$resultId.",19)";
     	$query2 = $db->query($sql2);
        $messages[] = "El pabellon ha sido registrada con éxito.";
        //echo '<meta http-equiv="refresh" content="0; url=facultades.php">';
    } else {
        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";
        //echo '<meta http-equiv="refresh" content="0; url=facultades.php">';
    }

     
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>			