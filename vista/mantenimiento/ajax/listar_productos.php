<?php
	
	/* Connect To Database*/
	require_once("../../../modelo/usuario/util.php");
	require_once ("../../../conexion/conexion.php");
	//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();


	
$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){
	$query = $db->real_escape_string(strip_tags($_REQUEST['query'], ENT_QUOTES));

	$tables="area";
	$campos="*";
	$sWhere=" area.AreDes LIKE '%".$query."%' and AreFKEstReg=18";
	$sWhere.=" order by area.AreId";
	
	
	include 'pagination.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = intval($_REQUEST['per_page']); //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   = $db->query("SELECT count(*) AS numrows FROM $tables where $sWhere ");
	if ($row= $count_query->fetch_assoc()){$numrows = $row['numrows'];}
	else {echo $db->error;}
	$total_pages = ceil($numrows/$per_page);
	//main query to fetch the data
	$query = $db->query("SELECT $campos FROM  $tables where $sWhere LIMIT $offset,$per_page");
	//loop through fetched data
	


		
	
	if ($numrows>0){
		
	?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class='text-center'>Areas</th>
						<!--<th>Producto </th>
						<th>Categoría </th>
						<th class='text-center'>Stock</th>
						<th class='text-right'>Precio</th>-->
						<th></th>
					</tr>
				</thead>
				<tbody>	
						<?php 
						$finales=0;
						while($row = mysqli_fetch_array($query)){	
							$area_id=$row['AreId'];
							$area_des=$row['AreDes'];						
							$finales++;
						?>	
						<tr class="<?php echo $text_class;?>">
							<td ><?php echo $area_des;?></td>					
							<td>
								<a href="#"  data-target="#editProductModal" class="edit" data-toggle="modal" data-name="<?php echo $area_des?>" data-id="<?php echo $area_id; ?>"><i class="material-icons" data-toggle="tooltip" title="Editar" >&#xE254;</i></a>
								<a href="#deleteProductModal" class="delete" data-toggle="modal" data-id="<?php echo $area_id;?>"><i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i></a> 
                    		</td>
						</tr>
						<?php }?>
						<tr>
							<td colspan='6'> 
								<?php 
									$inicios=$offset+1;
									$finales+=$inicios -1;
									echo "Mostrando $inicios al $finales de $numrows registros";
									echo paginate( $page, $total_pages, $adjacents);
								?>
							</td>
						</tr>
				</tbody>			
			</table>
		</div>	

	
	
	<?php	
	}	
}
?>          
		  
