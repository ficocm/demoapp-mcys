<?php
	if (empty($_POST['name'])){
		$errors[] = "Ingresa el nombre del cargo.";
	} elseif (!empty($_POST['name'])){
		require_once("../../../modelo/usuario/util.php");
	require_once ("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();
    
	$cargo_tip = $db->real_escape_string(strip_tags($_POST["tipo"],ENT_QUOTES));
	$cargo_des = $db->real_escape_string(strip_tags(strtoupper($_POST["name"]),ENT_QUOTES));
	$cargo_com = $db->real_escape_string(strip_tags($_POST["comision"],ENT_QUOTES));
	

	// REGISTER data into database
    $sql = "INSERT INTO cargo(CarFKTipUsu, CarDes, CarCom) VALUES ('$cargo_tip', '$cargo_des', '$cargo_com' )";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
        $messages[] = "El cargo ha sido guardado con éxito.";
    } else {
        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>			