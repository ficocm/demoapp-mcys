<?php
	if (empty($_POST['name'])){
		$errors[] = "Ingresa el nombre del Area.";
	} elseif (!empty($_POST['name'])){
		require_once("../../../modelo/usuario/util.php");
		require_once ("../../../conexion/conexion.php");
    	$db=Conectar::conexion();
    	$idProceso=$_SESSION["idProceso"];
		$area_des = $db->real_escape_string(strip_tags(strtoupper($_POST["name"]),ENT_QUOTES));
	
	// REGISTER data into database
    $sql = "INSERT INTO area(AreDes) VALUES ('$area_des')";
   
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
    	$last_id = $db->insert_id;
    	$sql2 = "INSERT INTO admisionarea(AdmAreFKAreId,AdmAreFKAdmCabId) VALUES ('$last_id','$idProceso')";
     	$query = $db->query($sql2);
        $messages[] = "El area ha sido guardado con éxito.";

    } else {
        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";
    }


     


		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
							
				</div>
				<?php
			}
?>			