<?php
	if (empty($_POST['tipo'])){
		$errors[] = "Seleciona el tipo de Proceso.";
	} elseif (!empty($_POST['tipo'])){
		require_once("../../../modelo/usuario/util.php");
	require_once("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	// escaping, additionally removing everything that could be (html/javascript-) code
	$db = Conectar::conexion();

	$idProceso = $_SESSION['idProceso'];
    
	$proceso_tip = $db->real_escape_string(strip_tags($_POST["tipo"],ENT_QUOTES));
	$proceso_fech = $db->real_escape_string(strip_tags($_POST["fecha"],ENT_QUOTES));
	$proceso_fase = $db->real_escape_string(strip_tags($_POST["fase"],ENT_QUOTES));
	$proceso_eval = $db->real_escape_string(strip_tags($_POST["evaluacion"],ENT_QUOTES));
	$proceso_anio = $db->real_escape_string(strip_tags($_POST["anio"],ENT_QUOTES));

	$proceso_estReg = 33;
	$flag = false;
	

	// REGISTER data into database
    $sql = "INSERT INTO admisioncabecera(AdmCabFKTipPro, AdmCabFec, AdmCabNumFas, AdmCabNumEva, AdmCabAno, AdmCabFKEstReg ) VALUES ('$proceso_tip', '$proceso_fech', '$proceso_fase', '$proceso_eval', '$proceso_anio', '$proceso_estReg')";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
    	//echo "<h1>CABECERA AGREGADA</h1>";
    	$last_idAdmCab = $db->insert_id;
    	$sqlInsertAreas = "INSERT INTO admisionarea (admisionarea.AdmAreFKAreId, admisionarea.AdmAreFKAdmCabId, admisionarea.AdmAreFKEstReg) SELECT area.AreId, $last_idAdmCab ,19 FROM area WHERE area.AreFKEstReg = 18";
    	$areasAgregadas = $db->query($sqlInsertAreas);
    	if ($areasAgregadas) {
    		$sqlNumPabs = "SELECT pabellon.PabId, admisionarea.AdmAreId, 19 FROM admisionarea INNER JOIN pabellon ON pabellon.PabFKAre = admisionarea.AdmAreFKAreId INNER JOIN area ON area.AreId = admisionarea.AdmAreFKAreId  WHERE pabellon.PabFKEstReg =18 AND admisionarea.AdmAreFKAdmCabId = $last_idAdmCab";
    		$pabsAdds = $db->query($sqlNumPabs);
    		//echo "<h1>AREAS AGREGADAS</h1>";
    		$sqlInsertPabellones = "INSERT INTO admisionpabellon (admisionpabellon.AdmPabFKPabId, admisionpabellon.AdmPabFKAdmAreId, admisionpabellon.AdmPabFKEstReg) SELECT pabellon.PabId, admisionarea.AdmAreId, 19 FROM admisionarea INNER JOIN pabellon ON pabellon.PabFKAre = admisionarea.AdmAreFKAreId INNER JOIN area ON area.AreId = admisionarea.AdmAreFKAreId  WHERE pabellon.PabFKEstReg =18 AND admisionarea.AdmAreFKAdmCabId = $last_idAdmCab";
    		$pabellonesAgregados = $db->query($sqlInsertPabellones);

    		if ($pabellonesAgregados) {
    			//echo "<h1>PABELLONES AGREGADOS</h1>";
    			$lasIdPabs = $db->insert_id;
    			$row_cntPabs = $pabsAdds->num_rows;
    			//echo "<h1>Numero de Pabellones: ".$row_cntPabs.", ".$lasIdPabs." </h1>";
    			for ($i=$lasIdPabs; $i < $lasIdPabs + $row_cntPabs; $i++) { 
    				$sqlInsertAulas = "INSERT INTO admisionaula (admisionaula.AdmAulFKAulId, admisionaula.AdmAulFKAdmPabId, admisionaula.AdmAulCap, admisionaula.AdmAulAfo, admisionaula.AdmAulFil, admisionaula.AdmAulCol, admisionaula.AdmAulFKEstReg) SELECT aula.AulId, admisionpabellon.AdmPabId, aula.AulCap, aula.AulAfo, aula.AulFil, aula.AulCol, 19 FROM admisionpabellon INNER JOIN aula ON aula.AulFKPab = admisionpabellon.AdmPabFKPabId INNER JOIN pabellon ON pabellon.PabId = admisionpabellon.AdmPabFKPabId WHERE aula.AulFKEstReg = 18 AND admisionpabellon.AdmPabId = ".$i;
    				$aulasAgregadas = $db ->query($sqlInsertAulas);
    				if ($aulasAgregadas) {
    					//Proceso creado satisfactoriamente
    					$flag = true;
    				}
    			}
    			if ($flag) {
    				$messages[] = "El proceso de admision ha sido guardado con éxito.";
    			}
    			
    		}

    	}
        //$messages[] = "El proceso de admision ha sido guardado con éxito.";
    } else {
        $errors[] = "Lo sentimos, el registro falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
							<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
				</div>
				<?php
			}
?>			