<?php
	if (empty($_POST['edit_id'])){
		$errors[] = "ID está vacío.";
	} elseif (!empty($_POST['edit_id'])){
		require_once("../../../modelo/usuario/util.php");
	require_once("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$db = Conectar::conexion();
	$idProceso=$_SESSION["idProceso"];
	$facultad_nombre = strtoupper($_POST["name"]);
	$facultad_nombre = $db->real_escape_string(strip_tags(strtoupper($_POST["edit_name"]),ENT_QUOTES));
	$facultadFK_area = $db->real_escape_string(strip_tags($_POST["edit_areas"],ENT_QUOTES));
	

	$facultad_id = intval($_POST['edit_id']);
	// UPDATE data into database
    $sql = "UPDATE pabellon SET PabDes='".$facultad_nombre."', PabFKAre=".$facultadFK_area." WHERE PabId='".$facultad_id."' ";
    
    
    $query = $db->query($sql);

    // if product has been added successfully
    if ($query) {
    	$sql_select ="SELECT admisionarea.AdmAreId from admisionarea inner join area on admisionarea.AdmAreFKAreId =area.AreId where admisionarea.AdmAreFKAdmCabId=".$idProceso." and admisionarea.AdmAreFKAreId =".$facultadFK_area;
    	$result = $db->query($sql_select);
    	$resultId = "";
    	if($row = $result->fetch_assoc()) {
            $resultId = $row['AdmAreId'];
        }

       
    	$sql2="UPDATE admisionpabellon SET AdmPabFKAdmAreId=".$resultId."WHERE  admisionpabellon.AdmPabFKPabId=".$facultad_id;
        $messages[] = "Los datos del pabellon han sido actualizados con éxito.";
        
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
        
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
				</div>
				<?php
			}
?>			