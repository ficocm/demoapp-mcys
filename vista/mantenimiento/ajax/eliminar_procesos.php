<?php
	if (empty($_POST['delete_id'])){
		$errors[] = "Id vacío.";
	} elseif (!empty($_POST['delete_id'])){
		require_once("../../../modelo/usuario/util.php");
	require_once("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	$db = Conectar::conexion();
	// escaping, additionally removing everything that could be (html/javascript-) code
    $proceso_id=intval($_POST['delete_id']);
	

	// DELETE FROM  database
    $sql = "UPDATE admisioncabecera SET AdmCabFKEstReg = 19 WHERE AdmCabId='$proceso_id'";
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
        $messages[] = "El proceso de admision ha sido eliminado con éxito.";
    } else {
        $errors[] = "Lo sentimos, la eliminación falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
							<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
				</div>
				<?php
			}
?>			