<?php
	if (empty($_POST['edit_id'])){
		$errors[] = "Nombre de proceso vacio";
	} elseif (!empty($_POST['edit_id'])){
		require_once("../../../modelo/usuario/util.php");
	require_once("../../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
	$db = Conectar::conexion();
	// escaping, additionally removing everything that could be (html/javascript-) code
    
	$proceso_tipo = $db->real_escape_string(strip_tags($_POST["edit_name"],ENT_QUOTES));
	$proceso_fase = $db->real_escape_string(strip_tags($_POST["edit_fase"],ENT_QUOTES));
	$proceso_eva = $db->real_escape_string(strip_tags($_POST["edit_eva"],ENT_QUOTES));
	$proceso_fecha = $db->real_escape_string(strip_tags($_POST["edit_fecha"],ENT_QUOTES));
	$proceso_anio = $db->real_escape_string(strip_tags($_POST["edit_anio"],ENT_QUOTES));

	$proceso_id=intval($_POST['edit_id']);

	// UPDATE data into database
    $sql = "UPDATE admisioncabecera SET AdmCabNumFas= $proceso_fase, AdmCabAno= $proceso_anio, AdmCabNumEva= $proceso_eva, AdmCabFec = '$proceso_fecha' WHERE AdmCabId=".$proceso_id;

    //echo $sql;
    $query = $db->query($sql);
    // if product has been added successfully
    if ($query) {
        $messages[] = "El proceso de admision ha sido actualizado con éxito.";
    } else {
        $errors[] = "Lo sentimos, la actualización falló. Por favor, regrese y vuelva a intentarlo.";
    }
		
	} else 
	{
		$errors[] = "desconocido.";
	}
if (isset($errors)){
			
			?>
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Error!</strong> 
					<?php
						foreach ($errors as $error) {
								echo $error;
							}
						?>
						<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
			</div>
			<?php
			}
			if (isset($messages)){
				
				?>
				<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>¡Bien hecho!</strong>
						<?php
							foreach ($messages as $message) {
									echo $message;
								}
							?>
							<!--<META HTTP-EQUIV="REFRESH" CONTENT="1;URL=procesos.php">-->
				</div>
				<?php
			}
?>			