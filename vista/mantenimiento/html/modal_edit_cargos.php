<div id="editProductModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_product" id="edit_product">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Cargos</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<input type="hidden" name="edit_id" id="edit_id" >
						</div>

						<div class="form-group">
							<label>Cargo</label>
							<input type="text" name="edit_name" id="edit_name" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Tipo de Participante</label>
							<select name="edit_tipo" id="edit_tipo">
  								<option value="4">Administrativo</option>
  								<option value="3">Docente</option>
  							</select>
						</div>

						<div class="form-group">
							<label>Cargo de Comision</label>
  							<select name="edit_comision" id="edit_comision">
  								<option value="NO">NO</option>
  								<option value="SI">SI</option>	
  							</select>
							
						</div>
											
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Guardar datos">
					</div>
				</form>
			</div>
		</div>
	</div>