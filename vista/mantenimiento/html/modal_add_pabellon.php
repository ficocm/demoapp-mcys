<div id="addFacultadModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="add_facultad" id="add_facultad">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar Facultad</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
					<!--
						<div class="form-group">
							<label>Código</label>
							<input type="text" name="code"  id="code" class="form-control" required>	
						</div>
					-->
						<div class="form-group">
							<label>Nombre de Pabellon</label>
							<input type="text" name="name" id="name" class="form-control" required>
						</div>


						<div class="form-group">
							<label>Seleccionar Area</label>
							<select name="area" id="area" class="form-control" form="add_facultad" required>
								<option value="1">BIOMEDICAS</option>
								<option value="2">INGENIERIAS</option>
								<option value="3">SOCIALES</option>
								<option value="4">CEPRUNSA</option>
								<option value="5">AGRONOMIA</option>
								<option value="6">OTRO</option>

							</select>
						</div>

					<!--
						<div class="form-group">
							<label>Stock</label>
							<input type="number" name="stock" id="stock" class="form-control" required>
						</div>
					-->
						<!-- 
						<div class="form-group">
							<label>Precio</label>
							<input type="text" name="price" id="price" class="form-control" required>
						</div>				-->

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Guardar datos">
						
					</div>
				</form>
			</div>
		</div>
	</div>