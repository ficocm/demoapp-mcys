<div id="editProductModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="edit_product" id="edit_product">
					<div class="modal-header">						
						<h4 class="modal-title">Editar Proceso de Admisión</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<input type="hidden" name="edit_id" id="edit_id" >
						</div>
						<div class="form-group">
							<label>Tipo de Proceso</label>
							<input type="text" name="edit_name" id="edit_name" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Año de Proceso</label>
							<input type="number" name="edit_anio" id="edit_anio" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Número de Fase</label>
							<input type="number" name="edit_fase" id="edit_fase" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Numero de Evaluación</label>
							<input type="number" name="edit_eva" id="edit_eva" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Fecha</label>
							<input type="date" name="edit_fecha" id="edit_fecha" class="form-control" required>
						</div>

											
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-info" value="Guardar datos">
					</div>
				</form>
			</div>
		</div>
	</div>