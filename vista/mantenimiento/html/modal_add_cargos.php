<div id="addProductModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="add_product" id="add_product">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar Cargo</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">

						<div class="form-group">
							<label>Cargo</label>
							<input type="text" name="name" id="name" class="form-control" required>
							
						</div>

						<div class="form-group">
							<label>Tipo de Participante</label>
							<select name="tipo" id="tipo">
  								<option value="4">Administrativo</option>
  								<option value="3">Docente</option>
  							</select>
  						</div>

  						<div class="form-group">
  							<label>Cargo de Comision</label>
  							<select name="comision" id="comision">
  								<option value="NO">NO</option>
  								<option value="SI">SI</option>	
  							</select>
						</div>	

					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Guardar datos">
						
					</div>
				</form>
			</div>
		</div>
	</div>