<?php 
require_once("../../modelo/usuario/util.php");
require_once("../../conexion/conexion.php");
$db = Conectar::conexion();
?>
<script type="text/javascript">
	function mostraraulas(dato) {
		if (dato == "2") {
			//Aquí el resto de acciones
		}
	}
</script>
<div id="addProductModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="add_product" id="add_product">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar Proceso de Admisión</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">	
						<div class="form-group">
							<label>Tipo de Proceso </label>

							<div class="form-group">
								<select name="tipo" class="form-control" onchange="mostraraulas(this.value)>
								<option value="0">Seleccione:</option>
								<?php
										$query="SELECT general.GenSubCat, general.GenId  FROM general WHERE general.GenCat='TIPO DE PROCESO'";
										$resultado = $db->query($query)or die("database error:". $db->error);
										if ($resultado)
											while($procesos = $resultado->fetch_assoc()){
												echo '<option value="'.$procesos['GenId'].'">'.$procesos['GenSubCat'].'</option>';}
								?>
							</select>
							<br>
							<label>Año de Inicio de Ingresantes</label>
							<input type="number" name="anio" id="anio" class="form-control" placeholder="Ingrese el año (2019, 2020, 2021)">
						</div>

							<div class="form-group">
							<label>Fecha del Proceso </label>
							<input type="date" name="fecha" id="fecha" class="form-control">
						</div>
						<div class="form-group">
							<label>Numero de Fase </label>
							<input type="number" name="fase" id="fase" placeholder= "Ingrese el numero de fase (1, 2, 3)" class="form-control">
						</div>
						<div class="form-group">
							<label>Numero de Evaluacion </label>
							<input type="number" name="evaluacion" id="evaluacion" placeholder= "Ingrese el numero de evaluación (1, 2, 3)" class="form-control">
						</div>
						</div>					
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						<input type="submit" class="btn btn-success" value="Guardar datos">
						
					</div>
				</form>
			</div>
		</div>
	</div>