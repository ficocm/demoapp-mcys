<?php 
require_once("../../modelo/usuario/util.php");
require_once("../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
$db = Conectar::conexion();

//$connect = new mysqli($host,$user,$pass,$db) or die($mysqli->error);


?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>OFICINA DE ADMISION</title>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/custom.css">



	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>


	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->


	<script type="text/javascript" src="dist/jquery.tabledit.js"></script>
<script>
	function mostrarAulas() {
    var textoBusqueda = $("select#comboseleccion").val();
 
     if (textoBusqueda != "") {
        $.post("editbuscar.php", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
         }); 
     } else { 
        $("#resultadoBusqueda").html('<p>Consulta vacia</p>');
        };
};
</script>

	

</head>
<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Mantenimiento / Pabellones </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->




	<h3>Seleccione Pabellon</h3>	
	<select name="comboseleccion" id="comboseleccion" onchange="mostrarAulas();">
		<option value="0">Seleccione:</option>

		<?php
		$query="SELECT pabellon.* ,area.AreDes FROM pabellon LEFT join area on area.AreId = pabellon.PabFKAre where PabFKEstReg = 18 ORDER BY pabellon.PabDes" ;
		$resultado = $db->query($query)or die("database error:". $db->error);
		if ($resultado)
		while($pabellones = $resultado->fetch_assoc()){
			
			echo '<option value="'.$pabellones['PabId'].'">'.$pabellones['PabDes'].'  -  '.$pabellones['AreDes'].'</option>';
		}
		?>

	</select>
	<div id="resultadoBusqueda"></div>
	
<!-- 
	<div name="tablaaulas">
	<h2>Aulas</h2>		 
	<table id="data_table" class="table table-striped">
		<thead>
			<tr>
				<th>Id</th>
				<th>Aula</th>
				<th>Aforo</th>
				<th>Piso</th>
				<th>Fila</th>	
				<th>Columna</th>
				<th>Capacidad</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$sql_query = "SELECT * FROM aula WHERE AulFKPab=1";
			$resultset = mysqli_query($con, $sql_query) or die("database error:". mysqli_error($con));
			while( $developer = mysqli_fetch_assoc($resultset) ) {
			?>
			   
			   <tr id="<?php echo $developer ['AulId']; ?>">
			   <td><?php echo $developer ['AulId']; ?></td>
			   <td><?php echo $developer ['AulNum']; ?></td>
			   <td><?php echo $developer ['AulAfo']; ?></td>
			   <td><?php echo $developer ['AulPis']; ?></td>
			   <td><?php echo $developer ['AulFil']; ?></td>   
			   <td><?php echo $developer ['AulCol']; ?></td>
			   <td><?php echo $developer ['AulCap']; ?></td>   
			   </tr>
			<?php } ?>
		</tbody>
    </table>	
	</div>

-->

<script type="text/javascript" src="custom_table_edit.js"></script>
<div class="insert-post-ads1" style="margin-top:20px;">

</div>
</div>

				</div>
	
		</div>
	
	</div>
	
</body></html>
 



                                                                                                       