<?php
/* Database connection settings */
require_once("../../modelo/usuario/util.php");
require_once ("../../conexion/conexion.php");//Contiene funcion que conecta a la base de datos
  // escaping, additionally removing everything that could be (html/javascript-) code
    $db=Conectar::conexion();
$idProceso=$_SESSION["idProceso"];
$AdmCabId=$_SESSION["idProceso"];
if (!empty($_POST["numSorteado"]) && !empty($_POST["cantidad"])) {
$numSorteado=$_POST["numSorteado"];
if(strcmp($numSorteado, "O")==0){
  $numSorteado=0;
}
$cantidad=$_POST["cantidad"];
$sumador = 0;
$flag = false;


//$querySorteado="SELECT UsuId,UsuDni,UsuNom,UsuApe FROM usuario  WHERE UsuId LIKE '%".$numSorteado."' AND UsuFKTip=4 AND UsuFKEstReg=18 AND UsuFKGen=2";
$querySorteado="SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuTel FROM usuario INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=" . $AdmCabId . " and admisiondetalle.AdmDetFKEstReg=7 and usuario.UsuId like '%".$numSorteado."' and usuario.UsuFKTip2=4 and usuario.UsuFKGen=1";
//echo ("consulta>>>>".$querySorteado);
                    $resultSorteado = $db->query($querySorteado);
                    while ($row1 = $resultSorteado->fetch_assoc() and $flag==false){
                      $sumador=$sumador+1;
                      if ($sumador == $cantidad) {
                        $flag=true;
                      }
                    } 
                    
                    $restante=$cantidad-$sumador;
                  }
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
  <title>OFICINA DE ADMISION</title>

  <!-- ESTILOS -->
  <link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
  <link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
  <link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
  <link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

  <!-- SCRIPTS -->
  <script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
  <script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
  <script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
  <script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
  <script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
  <script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
     

     <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <script src="js/TweenMax.min.js"></script>
    <script src="js/Winwheel.min.js"></script>


    <style type="text/css">
    button {
      color: #FFF;
      background-color: #900;
      font-weight: bold;
      height: 35px;
    }  
    </style>
    
    <link rel="stylesheet" type="text/css" href="../../css/skel.css">
    <link rel="stylesheet" type="text/css" href="../../css/style.css">

</head>
  <body>
  <div id="wrapper">
    <!-- Menu -->
    <?php
    include '../../menu.php';
    ?>

    <!-- Header -->
    <div id="page-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <h1 class="page-header" style="color:#5e151d;">Sorteo de Administrativos Varones</h1>
          </div>
          <div class="col-lg-4">
            <br>
            <img src="../../img/logoUnsa.jpg" width="100%" height="100%">
          </div>
        </div>
            <!-- Content -->
            <header>  
              
                  
                    
            </header>
            <center>
            <canvas id='canvas' height="200" width="200"></canvas>
            <br>
            <input type="button" class = "btn btn-outline btn-danger" value="Girar" onclick="miRuleta.startAnimation();">
            </center>
            <br>
            <script>
              var miRuleta = new Winwheel({
              'numSegments': 10, // Número de segmentos
              'outerRadius': 85,
              'segments':[ // Datos de los segmentos
                { 'fillStyle': '#35820E', 'text': 'O' },
                { 'fillStyle': '#EF0404', 'text': '1' },
                { 'fillStyle': '#000000', 'text': '2' },
                { 'fillStyle': '#EF0404', 'text': '3' },
                { 'fillStyle': '#000000', 'text': '4' },
                { 'fillStyle': '#EF0404', 'text': '5' },
                { 'fillStyle': '#000000', 'text': '6' },
                { 'fillStyle': '#EF0404', 'text': '7' },
                { 'fillStyle': '#000000', 'text': '8' },
                { 'fillStyle': '#EF0404', 'text': '9' },
              ] ,
              'animation': { 
                  'type': 'spinToStop', // Giro y alto
                  'duration': 7, // Duración de giro
                  'callbackFinished': 'Mensaje()', // Función para mostrar mensaje
                
                  'callbackAfter': 'dibujarIndicador()', // Funciona de pintar indicador
                  'callbackSound' : playSound
                }
              });

                var audio = new Audio('js/roullette.mp3');  // Create audio object and load desired file.
                  console.log (audio);
                function playSound()
                  {
                // Stop and rewind the sound (stops it if already playing).
                    audio.pause();
                    audio.currentTime = 0;
 
                    // Play the sound.
                  audio.play();
                }


                dibujarIndicador();
                function Mensaje() {
                   var SegmentoSeleccionado = miRuleta.getIndicatedSegment();
                   //alert("El número seleccionado es el:" + SegmentoSeleccionado.text + "!");
                   document.f1.numSorteado.value = SegmentoSeleccionado.text
                   miRuleta.stopAnimation(false);
                   miRuleta.rotationAngle = 0;
                   //miRuleta.draw();
                   dibujarIndicador();
               }
               function dibujarIndicador() {
                    var ctx = miRuleta.ctx;
                    ctx.strokeStyle = 'white';     
                    ctx.fillStyle = 'black';     
                    ctx.lineWidth = 1;
                    ctx.beginPath();             
                    ctx.moveTo(85, 0);          
                    ctx.lineTo(115, 0);          
                    ctx.lineTo(100, 25);
                    ctx.lineTo(85, 0);
                    ctx.stroke();                
                    ctx.fill();                  
               }
            </script>

            <form name="f1" id="f1" action="sortAdmVar.php" method="post" >
              <table>
                <tr>
                  
                  <td width="220px">
                    <h4>Cantidad Necesaria</h4><br>
                     
                  <input name="cantidad" class ="form-control" placeholder = "Ingrese Cantidad" type="text" value= "<?php echo !empty($restante)?$restante:'';?>" <?php echo !empty($restante)?"readonly":'';?> style="width:180px;height=10px">
                  </td>
                  <td width="220px">
                    <h4>Numero Sorteado</h4><br>
                     <!-- nombre de pabellon -->
                    <input name="numSorteado" readonly="readonly" class ="form-control" placeholder = "Numero Ganador" id="numSorteado" type="text" style="width:180px;height=10px">
                  </td>
                  <td>
                     <input type="hidden" name="control" value="2">
                     <br>
                     <br>
                    <button type="submit"  class= "btn btn-success" value="ListarDocentes">Listar Sorteados</button> 
                  </td>
                </tr>
              </table>
            </form>
<br><br><br>

    <form action="sortAdmVar.php" method="post">

              <!-- //class="table table-striped table-bordered" cellpadding="1" width="50%" cellspacing="1" border="1" id="example"  -->
              <div class="col-lg-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                      Lista de Sorteados</div>
                  <div class="panel-body">
                    

    <div class="table-responsive">          
  <table class="table table-hover" id="example">
    <!-- <div class="alert alert-success">
      <h2 style="text-align:left;">Pabellones registrados</h2>
    </div> -->
    <thead>
      <tr>
        
        <th>Id</th>
        <th>DNI</th>
        <th>Nombre</th>
        <th>Apellido</th>
      </tr>
    </thead>
    <tbody>
    <?php 
       if (!empty($_POST["numSorteado"]) && !empty($_POST["cantidad"])) {

    

   $querySorteado="SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuTel FROM usuario INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=" . $AdmCabId . " and admisiondetalle.AdmDetFKEstReg=7 and usuario.UsuId like '%".$numSorteado."' and usuario.UsuFKTip2=4 and usuario.UsuFKGen=1";
                    $resultSorteado = $db->query($querySorteado);
                    while ($row1 = $resultSorteado->fetch_assoc() and $cantidad>=1) {
                    $UsuId = $row1['UsuId'];
                    $cantidad=$cantidad-1;
 
                    $queryDetalle = "UPDATE admisiondetalle SET AdmDetFKEstReg=5 WHERE AdmDetFKEstReg=7 and AdmDetFKUsu=".$UsuId." and AdmDetFKAdmCabId=".$AdmCabId;

                    
                    if ($db->query($queryDetalle)) {
                        //echo "Se registro el usuario (" . $nombres . ") con éxito";
                    } else {
                        echo "Error: " . $queryDetalle . "<br>" . $db->error;
                    } 
                      



     
    ?>
      <tr>
        <td><?php echo $row1['UsuId']?></td>
        <td><?php echo $row1['UsuDni']?></td>
        <td><?php echo $row1['UsuNom']?></td>
        <td><?php echo $row1['UsuApe']?></td>
        <!--<td style="text-align:left; font-size:15px;">
          <input name="pabellones[]" type="checkbox" value="<?php echo $UsuId; ?>">
        </td>-->
      </tr>
    <?php  } }?>            
    </tbody>
  </table>
</div>

</div>
</div>
  <input type="hidden" name="control" value="1">
  <!-- 
  <button  style="font-family:cursive;" type="submit">Cargar Datos</button>
  -->
</form>


    </div>
    </div>
  </div>
</body>