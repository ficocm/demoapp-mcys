            <center>
                        <canvas id='canvas' height="200" width="200"></canvas>
                        <br>
                        <input type="button" value="Girar" onclick="miRuleta.startAnimation();">
                        </center>
            <br>

            <script>
              var miRuleta = new Winwheel({
              'numSegments': 9, // Número de segmentos
              'outerRadius': 85,
              'segments':[ // Datos de los segmentos
                { 'fillStyle': '#2ecc71', 'text': '1' },
                { 'fillStyle': '#e67e22', 'text': '2' },
                { 'fillStyle': '#e74c3c', 'text': '3' },
                { 'fillStyle': '#8e44ad', 'text': '4' },
                { 'fillStyle': '#f1c40f', 'text': '5' },
                { 'fillStyle': '#2ecc71', 'text': '6' },
                { 'fillStyle': '#e67e22', 'text': '7' },
                { 'fillStyle': '#e74c3c', 'text': '8' },
                { 'fillStyle': '#8e44ad', 'text': '9' },
              ] ,
              'animation': { 
                  'type': 'spinToStop', // Giro y alto
                  'duration': 3, // Duración de giro
                  'callbackFinished': 'Mensaje()', // Función para mostrar mensaje
                  'callbackAfter': 'dibujarIndicador()' // Funciona de pintar indicador
                }
              });
                dibujarIndicador();
                //function deleteSegment();
                function Mensaje() {
                   var SegmentoSeleccionado = miRuleta.getIndicatedSegment();
                   //alert("El número seleccionado es el:" + SegmentoSeleccionado.text + "!");
                   document.f1.numSorteado.value = SegmentoSeleccionado.text
                   deleteSegment();
                   miRuleta.stopAnimation(false);
                   miRuleta.rotationAngle = 0;

                   //miRuleta.draw();
                   dibujarIndicador();
               }
               function dibujarIndicador() {
                    var ctx = miRuleta.ctx;
                    ctx.strokeStyle = 'navy';     
                    ctx.fillStyle = 'black';     
                    ctx.lineWidth = 1;
                    ctx.beginPath();             
                    ctx.moveTo(85, 0);          
                    ctx.lineTo(115, 0);          
                    ctx.lineTo(100, 40);
                    ctx.lineTo(85, 0);
                    ctx.stroke();                
                    ctx.fill();                  
               }

                function deleteSegment()
                {
                    // Call function to remove a segment from the wheel, by default the last one will be
                    // removed; you can pass in the number of the segment to delete if desired.
                    var SegmentoSeleccionado = miRuleta.getIndicatedSegmentNumber();
                    miRuleta.deleteSegment(SegmentoSeleccionado);
                    //document.f1.numSorteado.value = miRuleta.getLengthSegment().text;
             
                    // The draw method of the wheel object must be called to render the changes.
                    miRuleta.draw();
                }
            </script>