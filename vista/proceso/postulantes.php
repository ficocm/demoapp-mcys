
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!--Modificaciones propias-->
	<script src="../../js/util.js"></script>
	<script src="../../js/procesos/postulantes.js"></script>
	<link href="../../css/estilosMensajes.css" rel="stylesheet">


</head>

<body >
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Proceso / Postulantes </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<div class="row grid-row ">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>

				<!-- PAGUE CONTENT -->

				<div class="panel panel-default">
                    <div class="panel-heading">
                            Cantidad de postulantes por área
                    </div>
                
                    <div class="panel-body">

                    	<div class="col-lg-4">
						
							<form id= "formulario" name="formulario" action="../../controlador/proceso/actualizarCantidad.php?accion=actualizar" method="post" enctype="multipart/form-data">

								<div class="form-group">
	                                <label>Ingenierias</label>
									<input class="form-control" id="cantidadIngenierias" name="cantidadIngenierias" type="number" value= "0" >
								</div>

								<div class="form-group">
	                                <label>Sociales</label>
									<input class="form-control" id="cantidadSociales" name="cantidadSociales" type="number" value= "0" >
								</div>

								<div class="form-group">
	                                <label>Biomedicas</label>
									<input class="form-control" id="cantidadBiomedicas" name="cantidadBiomedicas" type="number" value= "0" >
								</div>

								<button type="submit" id="send" class="btn btn-default">Guardar</button>
	                            <div class="loader" id="loader"></div>
								<br>

							</form>

						</div>

					</div>
				</div>
				
			</div>

		</div>

		<!-- Management Error -->
		<div class="error"></div>
		<div class="correcto"></div>

	</div>
</body>

</html>
