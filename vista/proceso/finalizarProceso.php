
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->
	<link rel="stylesheet" href="../../vista/mantenimiento/css/custom.css">

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<script type="text/javascript">

		function finalizarProceso(idProceso){
			var parametros = {"action":"ajax","idProceso":idProceso};
			$.ajax({
				url:'../../controlador/proceso/procesoControlador.php?accion=finalizarEstadoProceso',
				data: parametros,
				type: "POST",
				beforeSend: function(objeto){
					$("#alert alert-success").html("Procesando...");
				},
				success:function(result){
					var resultado = JSON.parse(result);
					if(resultado.error == false){
						procesosAdmision = resultado.mensaje;
						//var button = document.createElement('button');
						//button.data = 'dismiss-alert';
						//button.className = 'close';
						//button.innerHTML = 'X';
						//document.getElementById('aquii').appendChild(button);
						document.getElementById("alert alert-success").innerHTML = resultado.mensaje;
						alert("El proceso de Admisión forma parte del pasado");
						location.href = '../../vista/inicio/inicio.php'
						
					}else{
						alert(resultado);
					}
				}
			})
		}

		function wait(ms){
			var start = new Date().getTime();
			var end = start;
			while(end < start + ms) {
				end = new Date().getTime();
			}
		}

	</script>

</head>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Proceso / Iniciar Proceso de Admision <?php echo $_SESSION['nombreProceso']. "  FASE N° ".$_SESSION['faseProceso']; ?> </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  

				<!-- PAGUE CONTENT -->

				<!--<h1>Bienvenidos</h1>-->

				<div class="row">
					<?php if ($_SESSION["estadoProceso"] ==  33) {?>
						<button class="btn btn-primary" value = "<?php echo $_SESSION['idProceso']; ?>" onclick="iniciarProceso(this.value)"> CONFIRMAR INICIO DE PROCESO DE ADMISIÓN Y EMPEZAR A GESTIONAR </button>
					<?php }else if ($_SESSION["estadoProceso"] ==  32) {?>
						<button class="btn btn-primary" value = "<?php echo $_SESSION['idProceso']; ?>" onclick="finalizarProceso(this.value)"> FINALIZAR PROCESO DE ADMISIÓN </button>
					<?php }else{}?>
				</div>
				<br>
				<div class="row">
					<div class="alert alert-success" id="alert alert-success">
						<!--<button type = "button" class = "close" data-dismiss="alert">x</button>-->
					</div>
					
				</div>
				
				
				
			</div>


		</div>

	</div>
</body>

</html>
