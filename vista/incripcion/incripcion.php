<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
	<script src="../../vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->
	<script src="../../js/admision.js"></script>

</head>
<style>
	table {
	    font-family: arial, sans-serif;
	    border-collapse: collapse;
	    width: 66%;
	}
	.d1{
		border: 1px solid #dddddd;
	    text-align: center;
	    width: 60px;
	    padding: 8px;
	}
	.d2{
		border: 1px solid #dddddd;
	    text-align: center;
	    width: 100px;
	    padding: 8px;
	}

	tr:nth-child(even) {
	    background-color: #dddddd;
	}
</style>

<body>
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;"> Incripción </h1>
					</div>
					<div class="col-lg-4">
						<br>
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>  
				<input type="hidden" id="id" name="id" value="<?php echo $_SESSION["idUsuario"]?>">
				<!-- PAGUE CONTENT -->
				<h2>Procesos de Admisión Disponibles</h2>
				<br>
				<table class="table table-striped table-bordered" id="usuarios">
					<thead>
						<tr>
							<th>FECHA</th>
							<th>TIPO</th>
							<th>FASE</th>
							<th>CARGO</th>
							<th>ESTADO</th>
						</tr>
					</thead>
				</table>
				<div id="paginacion">
				</div>
				<br>
			</div>
		</div>
	</div>

	<!-- Modal content-->
						<div class="modal fade" id="myModal" role="dialog">
    					<div class="modal-dialog modal-lg">

					      <div class="modal-content">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <input type="hidden" id="idAdm" name="idAdm" value="">
					          <h4 class="modal-title">Opciones</h4>
					        </div>
					        <div class="modal-body">
					        	<form class="form-horizontal" id="formulario">
						        	<div class="row">
						        		<div class="col-lg-6">
						        			<div class="form-group">
												    <label class="control-label col-sm-2">Cargo:</label>
												    <div class="col-sm-10">
												    <select class="form-control" name="categoria" id="categoria">
												    	<?php
												    		if(in_array(Codigos::codAdministrativo, $tipo) && !in_array(Codigos::codDocente, $tipo)){?>
												    			<option value="4">ADMINISTRATIVO</option>
			
												    		<?php
												    			}else{?>
												    					
												    						<?php
												    						if(!in_array(Codigos::codAdministrativo, $tipo) && in_array(Codigos::codDocente, $tipo)){
												    						?>
												    						<option value="2">TECNICO</option>
												    						<option value="3">CONTROLADOR</option>
												    						<?php
												    						}else if(in_array(Codigos::codAdministrativo, $tipo) && in_array(Codigos::codDocente, $tipo)){
												    						?>
												    						<option value="2">TECNICO</option>
												    						<option value="3">CONTROLADOR</option>
												    						<option value="4">ADMINISTRATIVO</option>
												    						<?php
												    						}
												    						?>
												       					
												    				<?php 
												    					}
												    				?>
												    			</select>
												       
												    </div>
												  </div>
						        		</div>
						        	</div>

					        	</form>

										
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					          <button type="button" onclick="generarAdm();" class="btn btn-primary" id="save">Guardar</button>
					        </div>
					      </div>
					      
					    </div>
					  </div>
					  <!-- End Modal-->

	<script src="../../js/borrarInscripcion.js"></script>
</body>


</html>
