<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="../../img/icoUnsa.png"/>
	<title>OFICINA DE ADMISION</title>

	<!-- ESTILOS -->
	<link href="../../vendor/bootstrap/css/bootstrap.css" rel="stylesheet">	<!-- Bootstrap Core CSS -->
	<link href="../../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
	<link href="../../dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
	<link href="../../vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

	<!-- SCRIPTS -->
	<script src="../../vendor/jquery/jquery-3.3.1.min.js"></script>	<!-- jQuery -->	
	<script src="../../vendor/jquery/popper.min.js"></script>	<!-- Poppper -->	
	<script src="../../vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
	<script src="../../vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
	<script src="../../vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
	<script src="../../dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

	<!-- Mis modificaciones -->
	<link href="../../css/estilosMensajes.css" rel="stylesheet"> 
	<script src="../../js/util.js"></script>
	<script src="../../js/bitacora.js"></script>

</head>

<body onload="cargarDetalles(1);">
	<div id="wrapper">
		<!-- Menu -->
		<?php
		include '../../menu.php';
		?>

		<!-- Header -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="page-header" style="color:#5e151d;">Bitacora / Proceso de admisión </h1>
					</div>
					<div class="col-lg-4">
						<img src="../../img/logoUnsa.jpg" width="100%" height="100%">
					</div>
				</div>	

				<!-- PAGUE CONTENT -->
				<div class="row grid-row ">
					<div class="col-sm-3 loader" id="loader"></div>
				</div>

				<br>
				<div id="table-scroll">
					<table class="table table-striped table-bordered estiloHover" id="usuarios">
						<br><br>
						<tbody>
							<tr>
								<th>ID</th>
								<th>Operación</th>
								<th>Responsable Codigo</th>
								<th>Responsable Nombre</th>
								<th>Fecha</th>
								<th>Host</th>
								<th>Detalle Codigo</th>
								<th>Cabecera Cod Antiguo</th>
								<th>Cabecera Cod Nuevo</th>
								<th>Usuario DNI Antiguo</th>
								<th>Usuario DNI Nuevo</th>
								<th>Cargo1 Antiguo</th>
								<th>Cargo1 Nuevo</th>
								<th>Cargo2 Antiguo</th>
								<th>Cargo2 Nuevo</th>
								<th>Area1 Antiguo</th>
								<th>Area1 Nuevo</th>
								<th>Area2 Antiguo</th>
								<th>Area2 Nuevo</th>
								<th>Pabellon1 Antiguo</th>
								<th>Pabellon1 Nuevo</th>
								<th>Pabellon2 Antiguo</th>
								<th>Pabellon2 Nuevo</th>
								<th>Aula Cod Antiguo</th>
								<th>Aula Cod Nuevo</th>
								<th>Puerta1 Antiguo</th>
								<th>Puerta1 Nuevo</th>
								<th>Puerta2 Antiguo</th>
								<th>Puerta2 Nuevo</th>
								<th>Lugar1 Antiguo</th>
								<th>Lugar1 Nuevo</th>
								<th>Lugar2 Antiguo</th>
								<th>Lugar2 Nuevo</th>
								<th>Asistio Antiguo</th>
								<th>Asistio Nuevo</th>
								<th>Correo Enviado Antiguo</th>
								<th>Correo Enviado Nuevo</th>
								<th>DNI Técnico Antiguo</th>
								<th>DNI Técnico Nuevo</th>
								<th>Estado Reg Antiguo</th>
								<th>Estado Reg Nuevo</th>
							</tr>
						</tbody>
					</table>
					<div id="paginacion">
					</div>
				</div>

				<!-- FIN -->

				
			</div>
		</div>

		<!-- Management Error -->
		<div class="error"></div>
		<div class="correcto"></div>

	</div>
</body>

</html>
