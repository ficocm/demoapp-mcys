<?php
abstract class DatosBD{
	const usuarioBDAdministrador = "admision";
	const contrasenaBDAdministrador = "Admis10n2018!";
	const nombreBD = "DuaPrueba";
}

class Conectar{
	public static function conexion(){
		session_start();
		if(isset($_SESSION["tipo"])){
			$tipos = $_SESSION["tipo"];
			if(in_array(Codigos::codDocente, $tipos) || in_array(Codigos::codAdministrativo, $tipos)){
				return Conectar::conexionDocenteAdministrativo();
			}else if(in_array(Codigos::codOperador, $tipos) || in_array(Codigos::codAdministrador, $tipos)){
				$usuario = $_SESSION["usuarioNombre"];
				$contrasena = $_SESSION["usuarioContraseña"];
				$conexion = new mysqli("localhost",$usuario,$contrasena,DatosBD::nombreBD);
				$conexion->query("SET NAMES 'utf8'");
				return $conexion;
			}
		}
		return;
	}

	public static function conexionSinRestriccion(){ //Para login y validación de caracteres
		$conexion = new mysqli("localhost",DatosBD::usuarioBDAdministrador,DatosBD::contrasenaBDAdministrador,DatosBD::nombreBD);
		$conexion->query("SET NAMES 'utf8'");
		return $conexion;
	}

	public static function conexionDocenteAdministrativo(){
		$conexion = new mysqli("localhost","docenteadministrativo","DosceAdmin1!",DatosBD::nombreBD);
		$conexion->query("SET NAMES 'utf8'");
		return $conexion;
	}
}
?>
