<?php

class bitacoraUsuarioModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getCantidadBitacoraUsuario(){
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros FROM bitacorausuario");
    return Util::convertFormatJson($consulta);
  }

	public function getBitacoraUsuario($offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT BitUsuId,usuario as ResCod, (select concat(usuario.UsuApe, ', ', usuario.UsuNom) from usuario where usuario.UsuId = ResCod) as ResNom, UsuId, UsuDniAnt, UsuDniNue, UsuCuiAnt, UsuCuiNue, UsuNomAnt, UsuNomNue, UsuApeAnt, UsuApeNue, UsuCorEleAnt, UsuCorEleNue, UsuTelAnt, UsuTelNue, UsuDirAnt, UsuDirNue, UsuNumProAnt, UsuNumProNue, (SELECT general.GenSubCat from general where general.GenId=UsuGenAnt) as GenAnt, (SELECT general.GenSubCat from general where general.GenId=UsuGenNue) as GenNue , (SELECT general.GenSubCat from general where general.GenId=UsuFKTip1Ant) as Tip1Ant , (SELECT general.GenSubCat from general where general.GenId=UsuFKTip1Nue) as Tip1Nue , (SELECT general.GenSubCat from general where general.GenId=UsuFKTip2Ant) as Tip2Ant , (SELECT general.GenSubCat from general where general.GenId=UsuFKTip2Nue) as Tip2Nue ,(SELECT general.GenSubCat from general where general.GenId=UsuFKCatAnt) as CatAnt,(SELECT general.GenSubCat from general where general.GenId=UsuFKCatNue) as CatNue, UsuNomUsuAnt, UsuNomUsuNue, UsuConUsuAnt, UsuConUsuNue, (SELECT general.GenSubCat from general where general.GenId=UsuFKEstRegAnt) as EstRegAnt,(SELECT general.GenSubCat from general where general.GenId=UsuFKEstRegNue) as EstRegNue,  fecha, operacion, host FROM bitacorausuario order by BitUsuId desc LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }
}
?>