<?php

class bitacoraAdmisionDetalleModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getCantidadBitacoraAdmisionDetalle(){
    $consulta = $this->db->query("select count(*) as numeroRegistros FROM bitacoraadmisiondetalle");
    return Util::convertFormatJson($consulta);
  }

	public function getBitacoraAdmisionDetalle($offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT BitAdmDetId,usuario as ResCod, (select concat(usuario.UsuApe, ', ', usuario.UsuNom) from usuario where usuario.UsuId = ResCod) as ResNom,  AdmDetId, AdmDetFKAdmCabIdAnt, AdmDetFKAdmCabIdNue,(SELECT usuario.UsuDni from usuario where usuario.UsuId=AdmDetFKUsuAnt) as UsuDniAnt,(SELECT usuario.UsuDni from usuario where usuario.UsuId=AdmDetFKUsuNue) as UsuDniNue, (SELECT cargo.CarDes FROM cargo WHERE cargo.CarId=AdmDetFKCarAnt) as CarAnt,(SELECT cargo.CarDes FROM cargo WHERE cargo.CarId=AdmDetFKCarNue) as CarNue,(SELECT cargo.CarDes FROM cargo WHERE cargo.CarId=AdmDetFKCar2Ant) as Car2Ant,(SELECT cargo.CarDes FROM cargo WHERE cargo.CarId=AdmDetFKCar2Nue) as Car2Nue, AdmDetLug1Ant, AdmDetLug1Nue, AdmDetLug2Ant, AdmDetLug2Nue, AdmDetFKAulAnt, AdmDetFKAulNue, 
(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId where admisionpabellon.AdmPabId=AdmDetFKPabAnt) as Pab1Ant,
(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId where admisionpabellon.AdmPabId=AdmDetFKPabNue) as Pab1Nue,
(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId where admisionpabellon.AdmPabId=AdmDetFKPab2Ant) as Pab2Ant,
(SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId where admisionpabellon.AdmPabId=AdmDetFKPab2Nue) as Pab2Nue,
(SELECT area.AreDes FROM area INNER JOIN admisionarea on admisionarea.AdmAreFKAreId=area.AreId where admisionarea.AdmAreId=AdmDetFKAreAnt) as AreAnt,
(SELECT area.AreDes FROM area INNER JOIN admisionarea on admisionarea.AdmAreFKAreId=area.AreId where admisionarea.AdmAreId=AdmDetFKAreNue) as AreNue,
(SELECT area.AreDes FROM area INNER JOIN admisionarea on admisionarea.AdmAreFKAreId=area.AreId where admisionarea.AdmAreId=AdmDetFKAre2Ant) as Are2Ant,
(SELECT area.AreDes FROM area INNER JOIN admisionarea on admisionarea.AdmAreFKAreId=area.AreId where admisionarea.AdmAreId=AdmDetFKAre2Nue) as Are2Nue,
(SELECT areapuerta.ArePueDes from areapuerta WHERE areapuerta.ArePueId=AdmDetFKArePueAnt) as ArePueAnt,
(SELECT areapuerta.ArePueDes from areapuerta WHERE areapuerta.ArePueId=AdmDetFKArePueNue) as ArePueNue,
(SELECT areapuerta.ArePueDes from areapuerta WHERE areapuerta.ArePueId=AdmDetFKArePue2Ant) as ArePue2Ant,
(SELECT areapuerta.ArePueDes from areapuerta WHERE areapuerta.ArePueId=AdmDetFKArePue2Nue) as ArePue2Nue,
AdmDetDetAsiAnt, AdmDetDetAsiNue, AdmDetCorEnvAnt, AdmDetCorEnvNue,
(SELECT usuario.UsuDni FROM usuario INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetId=AdmDetFKDepTecAnt) as DepTecAnt,
(SELECT usuario.UsuDni FROM usuario INNER JOIN admisiondetalle on admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetId=AdmDetFKDepTecNue) as DepTecNue,
(SELECT general.GenSubCat from general where general.GenId=AdmDetFKEstRegAnt) as EstRegAnt,
(SELECT general.GenSubCat from general where general.GenId=AdmDetFKEstRegNue) as EstRegNue, fecha, operacion, host FROM bitacoraadmisiondetalle  order by BitAdmDetId desc LIMIT $offset,$registrosPorPagina ");
    return Util::convertFormatJson($consulta);
  }
}
?>