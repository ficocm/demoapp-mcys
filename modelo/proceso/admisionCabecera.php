<?php

class admisionCabecera{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function modificarCantidad($idProceso, $cantidadIngenierias, $cantidadSociales, $cantidadBiomedicas){
    $consulta = $this->db->query("UPDATE admisioncabecera set AdmCabPosIng=$cantidadIngenierias, AdmCabPosSoc=$cantidadSociales, AdmCabPosBio=$cantidadBiomedicas where AdmCabId=$idProceso");
    return $this->db->errno;
  }

  public function getCantidades($idProceso){
    $consulta = $this->db->query("SELECT AdmCabPosBio, AdmCabPosIng, AdmCabPosSoc FROM admisioncabecera  WHERE AdmCabId=$idProceso");
    return Util::convertFormatJson($consulta);
  }

  public function iniciarProceso($idProceso){
    $consulta = $this->db->query("UPDATE admisioncabecera SET AdmCabFKEstReg = '32' WHERE AdmCabId = $idProceso");
    return $this->db->errno;
  }

  public function finalizarProceso($idProceso){
    $consulta = $this->db->query("UPDATE admisioncabecera SET AdmCabFKEstReg = '31' WHERE AdmCabId = $idProceso");
    return $this->db->errno;
  }

}
?>