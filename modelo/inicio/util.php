<?php
abstract class Codigos{
	//Ids
  //General
	const codActivo = 18;
	const codInactivo = 19;
	const codInscrito = 7;
  const codNoInscrito = 6;
  const codSorteado = 5;
  const codDocente = 3;
  const codAsignado = 17;
  const codAdministrativo = 4;
  const codEstudiante = 20;
  const codOperador = 23;
  const codAdministrador = 22;

  const codEstadoProcesoPasado = 28;
  const codEstadoProcesoActual = 29;
  const codEstadoProcesoFuturo = 30;
  
  //Cargo
  const codDocenteTecnico = 2;
  const codAyudanteTecnico = 4;

	//Categorias General
  const texGenero = "GENERO";
  const texTipoUsuario = "TIPO DE USUARIO";
  const texcategoria = "CATEGORIA";
}

class Util{
  public static function convertFormatJson($consulta){
    $datos = array();
    if($consulta == NULL || $consulta == "NULL"){
      return $datos;
    }
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
      $datos[$posicion] = $fila;
      $posicion++;
    }
    return $datos;
  } 

  public static function jsonMensaje($id, $men, $data = NULL){
    $mensaje->error=$id;
    $mensaje->mensaje=$men;
    $mensaje->datos = $data;
    return json_encode($mensaje);
  }

  public static function getParameter($nombre){
    if(isset($_POST[$nombre]) && !empty($_POST[$nombre]) && $_POST[$nombre]!=-1 && $_POST[$nombre]!=NULL){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      $dato = (is_numeric($dato))?$dato : "'".$dato."'";
      return $dato;
    }else{
      return "NULL";
    }
  }

  public static function getParameterBusqueda($nombre){
    if(isset($_POST[$nombre]) && $_POST[$nombre]!=NULL ){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      return $dato;
    }else{
      return "";
    }
  }
}
?>