<?php

class admisionCabeceraModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getProcesosDeAdmision(){
    $consulta = $this->db->query("SELECT admisioncabecera.AdmCabId, admisioncabecera.AdmCabNumPos, admisioncabecera.AdmCabFec, admisioncabecera.AdmCabAno, general.GenSubCat, admisioncabecera.AdmCabNumFas, admisioncabecera.AdmCabNumEva, admisioncabecera.AdmCabPosBio, admisioncabecera.AdmCabPosIng, admisioncabecera.AdmCabPosSoc FROM admisioncabecera INNER JOIN general ON admisioncabecera.AdmCabFKTipPro = general.GenId WHERE admisioncabecera.AdmCabFKEstReg=".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getProcesos($busqueda, $offset, $registrosPorPagina){
    $consulta = $this->db->query("SELECT admisioncabecera.AdmCabId, admisioncabecera.AdmCabNumPos, admisioncabecera.AdmCabFec, admisioncabecera.AdmCabAno, general.GenSubCat, admisioncabecera.AdmCabNumFas, admisioncabecera.AdmCabNumEva, admisioncabecera.AdmCabPosBio, admisioncabecera.AdmCabPosIng, admisioncabecera.AdmCabPosSoc, admisioncabecera.AdmCabFKEstReg FROM admisioncabecera INNER JOIN general ON admisioncabecera.AdmCabFKTipPro = general.GenId WHERE admisioncabecera.AdmCabFKEstReg <> ".Codigos::codInactivo. " AND (admisioncabecera.AdmCabAno LIKE '%".$busqueda."%' OR general.GenSubCat LIKE '%".$busqueda."%' OR admisioncabecera.AdmCabNumFas LIKE '%".$busqueda."%') ORDER BY admisioncabecera.AdmCabFec DESC LIMIT $offset, $registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadProcesos($busqueda){
    $consulta = $this->db->query("SELECT  count(*) as numeroRegistros from admisioncabecera INNER JOIN general ON admisioncabecera.AdmCabFKTipPro = general.GenId WHERE AdmCabFKEstReg=".Codigos::codActivo." AND (admisioncabecera.AdmCabAno LIKE '%".$busqueda."%' OR general.GenSubCat LIKE '%".$busqueda."%' OR admisioncabecera.AdmCabNumFas LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getArea($AreId){
    $consulta = $this->db->query("SELECT AreId, AreIdLog, AreDes, AreFKEstReg from area where AreFKEstReg = $codActivo AND AreId = $AreId");
    return Util::convertFormatJson($consulta);
  }

  public function getAreas(){
    $consulta = $this->db->query("SELECT AreId, AreDes FROM area WHERE AreFKEstReg = 18");
    return Util::convertFormatJson($consulta);
  }
}
?>
