<?php
class cargoModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getCargosAdministrativos(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='NO' and CarFKTipUsu=".Codigos::codAdministrativo." order by CarDes");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosDocentes(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='NO' and CarFKTipUsu=".Codigos::codDocente." order by CarDes");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosDocentesAdministrativos(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='NO' and (CarFKTipUsu=".Codigos::codDocente." or CarFKTipUsu=".Codigos::codAdministrativo.") order by CarDes");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosAdministrativosComision(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='SI' and CarFKTipUsu=".Codigos::codAdministrativo." order by CarDes");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosDocentesComision(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='SI' and CarFKTipUsu=".Codigos::codDocente." order by CarDes");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosComision(){
    $consulta = $this->db->query("select CarId, CarDes from cargo where cargo.CarCom='SI' order by CarDes");
    return Util::convertFormatJson($consulta);
  }

}
?>