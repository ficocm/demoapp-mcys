<?php

class admisiondetalleModelo{
	private $db;

	public function __construct(){
		$this->db = Conectar::conexion();
	}

	public function getCantidadDocentesSorteados($AdmDetFKAdmCabId, $busqueda, $tipo){
		$consulta = $this->db->query("select count(*) as numeroRegistros from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and (admisiondetalle.AdmDetFKCar=$tipo) and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getDocentesSorteados($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina, $tipo){
		$consulta = $this->db->query("select usuario.UsuId,usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuNumPro, admisiondetalle.AdmDetId, admisiondetalle.AdmDetFKAul, admisiondetalle.AdmDetFKPab, admisiondetalle.AdmDetFKPab2, admisiondetalle.AdmDetFKAre, admisiondetalle.AdmDetFKCar, admisiondetalle.AdmDetFKCar2 from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and (admisiondetalle.AdmDetFKCar=$tipo) and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe LIMIT $offset,$registrosPorPagina ");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadDocentesSorteadosConCargo($AdmDetFKAdmCabId, $busqueda){
		$consulta = $this->db->query("select count(*) as numeroRegistros from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId inner join cargo on cargo.CarId=admisiondetalle.AdmDetFKCar where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and (cargo.CarFKTipUsu=".Codigos::codDocente.") and admisiondetalle.AdmDetFKCar IS NOT NULL and cargo.CarCom='NO' and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getDocentesSorteadosConCargo($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("select usuario.UsuId,usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuNumPro, admisiondetalle.AdmDetId, admisiondetalle.AdmDetFKAul, admisiondetalle.AdmDetFKPab, admisiondetalle.AdmDetFKPab2, admisiondetalle.AdmDetFKAre, admisiondetalle.AdmDetFKCar, admisiondetalle.AdmDetFKCar2 from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId inner join cargo on cargo.CarId=admisiondetalle.AdmDetFKCar where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and (cargo.CarFKTipUsu=".Codigos::codDocente.") and admisiondetalle.AdmDetFKCar IS NOT NULL and cargo.CarCom='NO' and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe LIMIT $offset,$registrosPorPagina ");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadDocentesSorteadosSinTipo($AdmDetFKAdmCabId, $busqueda){
		$consulta = $this->db->query("select count(*) as numeroRegistros from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getDocentesSorteadosSinTipo($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("select usuario.UsuId,usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuNumPro, admisiondetalle.AdmDetId, admisiondetalle.AdmDetFKPab, admisiondetalle.AdmDetFKPab2, admisiondetalle.AdmDetFKAre,admisiondetalle.AdmDetFKAre2, admisiondetalle.AdmDetFKCar, admisiondetalle.AdmDetFKCar2 from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe LIMIT $offset,$registrosPorPagina ");
		return Util::convertFormatJson($consulta);
	}


	public function getCantidadAdministrativosSorteados($AdmDetFKAdmCabId, $busqueda){
		$consulta = $this->db->query("select count(*) as numeroRegistros from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip2=".Codigos::codAdministrativo." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getAdministrativosSorteados($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("select usuario.UsuId,usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, usuario.UsuNumPro, admisiondetalle.AdmDetId, admisiondetalle.AdmDetFKAul, admisiondetalle.AdmDetFKPab,admisiondetalle.AdmDetFKPab2,admisiondetalle.AdmDetFKAre,admisiondetalle.AdmDetFKAre2, admisiondetalle.AdmDetFKCar, admisiondetalle.AdmDetFKCar2, admisiondetalle.AdmDetFKArePue, admisiondetalle.AdmDetFKArePue2, admisiondetalle.AdmDetLug1, admisiondetalle.AdmDetLug2 from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip2=".Codigos::codAdministrativo." and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe LIMIT $offset,$registrosPorPagina ");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadDocentesTecnicos($AdmDetFKAdmCabId, $busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from admisiondetalle INNER JOIN usuario on usuario.UsuId=admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and (admisiondetalle.AdmDetFKCar=".Codigos::codDocenteTecnico." or admisiondetalle.AdmDetFKCar2=".Codigos::codDocenteTecnico.") and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getDocentesTecnicos($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT admisiondetalle.AdmDetId,admisiondetalle.AdmDetId as TecId, usuario.UsuDni,usuario.UsuNom, usuario.UsuApe, (SELECT COUNT(*) from admisiondetalle where admisiondetalle.AdmDetFKDepTec=TecId) as ayudantes from admisiondetalle INNER JOIN usuario on usuario.UsuId=admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and (admisiondetalle.AdmDetFKCar=".Codigos::codDocenteTecnico." or admisiondetalle.AdmDetFKCar2=".Codigos::codDocenteTecnico.") and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.")	and ".Util::busquedaSql($busqueda)." order by usuario.UsuApe");
		return Util::convertFormatJson($consulta);
	}

	public function getAreasParticipantes($AdmDetFKAdmCabId){
		$consulta = $this->db->query("select area.AreDes, area.AreId, admisionarea.AdmAreId from admisionarea inner join area on area.AreId=admisionarea.AdmAreFKAreId where admisionarea.AdmAreFKEstReg=".Codigos::codActivo." and admisionarea.AdmAreFKAdmCabId=$AdmDetFKAdmCabId order by area.AreDes");
		return Util::convertFormatJson($consulta);
	}

	public function getPabellonesParticipantes($AdmDetFKAdmCabId){
		$consulta = $this->db->query("select pabellon.PabDes, admisionpabellon.AdmPabId, admisionpabellon.AdmPabFKAdmAreId from admisionpabellon inner join pabellon on pabellon.PabId=admisionpabellon.AdmPabFKPabId inner join admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId where admisionarea.AdmAreFKAdmCabId=$AdmDetFKAdmCabId and admisionpabellon.AdmPabFKEstReg=".Codigos::codActivo." and admisionarea.AdmAreFKEstReg=".Codigos::codActivo." order by pabellon.PabDes");
		return Util::convertFormatJson($consulta);
	}

	public function getAulasParticipantes($AdmDetFKAdmCabId){
		$consulta = $this->db->query("select aula.AulNum,admisionaula.AdmAulId, admisionaula.AdmAulFKAdmPabId from admisionaula inner join aula on aula.AulId=admisionaula.AdmAulFKAulId inner join admisionpabellon on admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId inner join admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId where admisionarea.AdmAreFKEstReg=".Codigos::codActivo." and admisionpabellon.AdmPabFKEstReg=".Codigos::codActivo." and admisionaula.AdmAulFKEstReg=".Codigos::codActivo." and admisionarea.AdmAreFKAdmCabId=$AdmDetFKAdmCabId");
		return Util::convertFormatJson($consulta);
	}

	public function getPuertasAreas($AdmDetFKAdmCabId){
		$consulta = $this->db->query("SELECT admisionarea.AdmAreId, areapuerta.ArePueId, areapuerta.ArePueDes, areapuerta.ArePueFKAre from admisionarea INNER join areapuerta on admisionarea.AdmAreFKAreId=areapuerta.ArePueFKAre where areapuerta.ArePueFKEstReg=".Codigos::codActivo." and admisionarea.AdmAreFKAdmCabId=$AdmDetFKAdmCabId");
		return Util::convertFormatJson($consulta);
	}

	public function updatePuestosParticipante($id, $area1, $pabellon1, $cargo1, $lugar1, $puerta1, $area2, $pabellon2, $cargo2, $lugar2, $puerta2){
		$consulta = $this->db->query("update admisiondetalle set AdmDetFKCar=$cargo1, AdmDetFKArePue=$puerta1, AdmDetFKAre=$area1, AdmDetFKPab=$pabellon1, AdmDetFKCar2=$cargo2, AdmDetLug1=$lugar1, AdmDetLug2=$lugar2, AdmDetFKArePue2=$puerta2, AdmDetFKAre2=$area2, AdmDetFKPab2=$pabellon2, AdmDetFKEstReg=".Codigos::codAsignado." where AdmDetId=$id");
		return $this->db->errno;
	}

	public function updatePuestosParticipanteDocentes($id, $area1, $pabellon1, $aula1){
		$consulta = $this->db->query("update admisiondetalle set AdmDetFKAre=$area1, AdmDetFKPab=$pabellon1,AdmDetFKAul=$aula1, AdmDetFKEstReg=".Codigos::codAsignado." where AdmDetId=$id");
		return $this->db->errno;
	}

  public function updatePuestosParticipanteDocentesSeguridad($id, $area1, $pabellon1, $aula1,$area2, $pabellon2,  $cargo2){
    $consulta = $this->db->query("update admisiondetalle set AdmDetFKAre=$area1, AdmDetFKPab=$pabellon1,AdmDetFKAre2=$area2, AdmDetFKPab2=$pabellon2,AdmDetFKAul=$aula1,AdmDetFKCar2=$cargo2, AdmDetFKEstReg=".Codigos::codAsignado." where AdmDetId=$id");
    return $this->db->errno;
  }

	public function updatePuestosParticipanteComision($id, $area1, $area2, $pabellon1, $pabellon2,$cargo1, $cargo2){
		$consulta = $this->db->query("update admisiondetalle set AdmDetFKAre=$area1, AdmDetFKPab=$pabellon1, AdmDetFKCar=$cargo1, AdmDetFKAre2=$area2,AdmDetFKPab2=$pabellon2, AdmDetFKCar2=$cargo2,	AdmDetFKEstReg=".Codigos::codAsignado." where AdmDetId=$id");
		return $this->db->errno;
	}

	public function eliminarLugaresDocentes($id){
		$consulta = $this->db->query("update admisiondetalle set AdmDetFKPab=NULL, AdmDetFKPab2=NULL, AdmDetFKAre=NULL, AdmDetFKAre2=NULL, AdmDetFKEstReg=".Codigos::codSorteado." where AdmDetId=$id");
		return $this->db->errno;
	}

	public function eliminarLugaresAdministrativos($id){
		$consulta = $this->db->query("update admisiondetalle set AdmDetFKCar=NULL, AdmDetFKArePue=NULL, AdmDetFKArePue2=NULL, AdmDetFKCar2=NULL, AdmDetFKAul=NULL, AdmDetFKPab=NULL, AdmDetFKPab2=NULL, AdmDetFKAre=NULL, AdmDetFKAre2=NULL, AdmDetLug1=NULL, AdmDetLug2=NULL , AdmDetFKEstReg=".Codigos::codAsignado." where AdmDetId=$id");
		return $this->db->errno;
	}	



}
?>