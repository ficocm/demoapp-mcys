<?php

class admisiondetalleModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getUsuariosConSinInscripcionOParticipacion($AdmDetFKAdmCabId){
    $consulta = $this->db->query("SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, (SELECT admisiondetalle.AdmDetId from admisiondetalle  WHERE admisiondetalle.AdmDetFKUsu=usuario.UsuId and AdmDetFKEstReg=".Codigos::codInscrito." and AdmDetFKAdmCabId=$AdmDetFKAdmCabId ORDER by admisiondetalle.AdmDetFKAdmCabId DESC limit 1) as inscrito (SELECT admisiondetalle.AdmDetId from admisiondetalle  WHERE admisiondetalle.AdmDetFKUsu=usuario.UsuId and AdmDetFKEstReg=".Codigos::codSorteado." and AdmDetFKAdmCabId=$AdmDetFKAdmCabId ORDER by admisiondetalle.AdmDetFKAdmCabId DESC limit 1) as sorteado, (SELECT admisiondetalle.AdmDetId from admisiondetalle  WHERE admisiondetalle.AdmDetFKUsu=usuario.UsuId and AdmDetFKEstReg=".Codigos::codInscritoCancelado." and AdmDetFKAdmCabId=$AdmDetFKAdmCabId ORDER by admisiondetalle.AdmDetFKAdmCabId DESC limit 1) as noInscrito FROM usuario WHERE usuario.UsuFKEstReg=".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getDocentesConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT * from ( SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat, usuario.UsuNumPro, -1 as estadoRegistro,usuario.UsuFKTip,-1 as idDetalle, -1 as cargo from usuario WHERE (select count(*) from admisiondetalle where admisiondetalle.AdmDetFKUsu=UsuId and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId)=0 and usuario.UsuFKEstReg=".Codigos::codActivo." and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda)." UNION ALL SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat,usuario.UsuNumPro, admisiondetalle.AdmDetFKEstReg,usuario.UsuFKTip, admisiondetalle.AdmDetId, admisiondetalle.AdmDetFKCar as cargo from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip=".Codigos::codDocente." and ".Util::busquedaSql($busqueda).") a ORDER BY UsuApe, UsuNom, UsuId LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadDocentesConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda){
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros FROM usuario WHERE usuario.UsuFKEstReg=".Codigos::codActivo." and usuario.UsuFKTip=".Codigos::codDocente." and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  /**
  Muestra los usuarios inscritos, sorteados, confirmados, desinscritos en admisionDetalle
  En caso no estuvieran en admisiondetalle devuelve -1, (En cualquier caso devulve el codigo
  del usuario como UsuId)
  */
  public function getAdministrativosConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT * from ( SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat,usuario.UsuNumPro,-1 as estadoRegistro,usuario.UsuFKTip,-1 as idDetalle from usuario WHERE (select count(*) from admisiondetalle where admisiondetalle.AdmDetFKUsu=UsuId and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId)=0 and usuario.UsuFKEstReg=".Codigos::codActivo." and usuario.UsuFKTip2=".Codigos::codAdministrativo." and ".Util::busquedaSql($busqueda)." UNION ALL SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat,usuario.UsuNumPro, admisiondetalle.AdmDetFKEstReg,usuario.UsuFKTip, admisiondetalle.AdmDetId from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip2=".Codigos::codAdministrativo." and ".Util::busquedaSql($busqueda).") a ORDER BY UsuApe, UsuNom, UsuId LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadAdministrativosConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda){
    $consulta = $this->db->query("SELECT count(*) numeroRegistros from ( SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat,-1 as estadoRegistro,usuario.UsuFKTip,-1 as idDetalle from usuario WHERE (select count(*) from admisiondetalle where admisiondetalle.AdmDetFKUsu=UsuId and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId)=0 and usuario.UsuFKEstReg=".Codigos::codActivo." and usuario.UsuFKTip2=".Codigos::codAdministrativo."  and ".Util::busquedaSql($busqueda)." UNION ALL SELECT usuario.UsuId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKGen, usuario.UsuFKCat, admisiondetalle.AdmDetFKEstReg,usuario.UsuFKTip, admisiondetalle.AdmDetId from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codInscrito." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and usuario.UsuFKTip2=".Codigos::codAdministrativo." and ".Util::busquedaSql($busqueda).") a ");
    return Util::convertFormatJson($consulta);
  }

  public function getDocentesTecnicosSorteados($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){//Son docentes tecnicos que fueron escogidos para ser parte del proceso de admisión
    $consulta = $this->db->query("SELECT admisiondetalle.AdmDetId,admisiondetalle.AdmDetId as idTec, usuario.UsuDni, usuario.UsuNom, usuario.UsuApe, (select count(*) from admisiondetalle where admisiondetalle.AdmDetFKCar=".Codigos::codAyudanteTecnico." and admisiondetalle.AdmDetFKDepTec=idTec and admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado." and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId) as ayudantes from admisiondetalle inner join usuario on usuario.UsuId = admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetFKCar=".Codigos::codDocenteTecnico." and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId  and ".Util::busquedaSql($busqueda)." ORDER BY UsuApe, UsuNom, UsuId LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadDocentesTecnicosSorteados($AdmDetFKAdmCabId, $busqueda){//Son docentes tecnicos que fueron escogidos para ser parte del proceso de admisión
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros from admisiondetalle inner join usuario on usuario.UsuId = admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetFKCar=".Codigos::codDocenteTecnico." and (admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado.") and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and ".Util::busquedaSql($busqueda)."");
    return Util::convertFormatJson($consulta);
  }

  public function getAyudantesTecnicos($AdmDetFKAdmCabId, $docenteTecnicoId){
    $consulta = $this->db->query("SELECT admisiondetalle.AdmDetId, usuario.UsuDni, usuario.UsuCui, usuario.UsuNom, usuario.UsuApe, usuario.UsuFKCat from admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId where admisiondetalle.AdmDetFKDepTec=$docenteTecnicoId and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado." and admisiondetalle.AdmDetFKCar=".Codigos::codAyudanteTecnico);
    return Util::convertFormatJson($consulta);
  }

  public function getEstudidantes($busqueda,$offset,$registrosPorPagina, $AdmDetFKAdmCabId){
    $consulta = $this->db->query("SELECT UsuId, UsuId as idEstudiante, UsuDni, UsuCui, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg,(select admisiondetalle.AdmDetFKDepTec from admisiondetalle where admisiondetalle.AdmDetFKUsu=idEstudiante and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado." limit 1) as idTecnico, (select concat(usuario.UsuApe,', ',usuario.UsuNom) from admisiondetalle INNER join usuario on usuario.UsuId=admisiondetalle.AdmDetFKUsu where admisiondetalle.AdmDetId=idTecnico and admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and (admisiondetalle.AdmDetFKEstReg=".Codigos::codAsignado." or admisiondetalle.AdmDetFKEstReg=".Codigos::codSorteado.") limit 1) as docente from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY UsuApe, UsuNom, UsuId LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadEstudiantes($busqueda){//Son docentes tecnicos que fueron escogidos para ser parte del proceso de admisión
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)."");
    return Util::convertFormatJson($consulta);
  }

  public function crearInscrito($UsuId, $AdmDetFKAdmCabId, $AdmDetFKCar){
    $consulta = $this->db->query("INSERT INTO admisiondetalle (AdmDetFKAdmCabId, AdmDetFKUsu, AdmDetFKCar, AdmDetFKEstReg) VALUES ($AdmDetFKAdmCabId, $UsuId, $AdmDetFKCar, ".Codigos::codInscrito." )");
    $res = array();
    $res[0] = $this->db->errno;
    $res[1] = $this->db->insert_id;
    return $res;
  }

  public function crearSorteado($UsuId, $AdmDetFKAdmCabId, $AdmDetFKCar){
    $consulta = $this->db->query("INSERT INTO admisiondetalle (AdmDetFKAdmCabId, AdmDetFKUsu,AdmDetFKCar, AdmDetFKEstReg) VALUES ($AdmDetFKAdmCabId,$UsuId, $AdmDetFKCar, ".Codigos::codSorteado.")");
    $res = array();
    $res[0] = $this->db->errno;
    $res[1] = $this->db->insert_id;
    return $res;
  }

  public function crearAyudanteTecnico($docenteTecnicoId, $estudianteId, $AdmDetFKAdmCabId){
    $consulta = $this->db->query("INSERT INTO admisiondetalle (AdmDetFKAdmCabId, AdmDetFKUsu, AdmDetFKCar, AdmDetFKDepTec, AdmDetFKEstReg, AdmDetFKAre,AdmDetFKAre2,AdmDetFKPab,AdmDetFKPab2,AdmDetFKAul) SELECT $AdmDetFKAdmCabId,$estudianteId,".Codigos::codAyudanteTecnico.",$docenteTecnicoId, ".Codigos::codAsignado.", AdmDetFKAre,AdmDetFKAre2,AdmDetFKPab,AdmDetFKPab2,AdmDetFKAul FROM admisiondetalle WHERE admisiondetalle.AdmDetId=$docenteTecnicoId limit 1");
    return $this->db->errno;
  }

  public function eliminarAyudanteTecnico($estudianteId){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codAsignadoCancelado." where AdmDetId=$estudianteId");
    return $this->db->errno;
  }

  public function eliminarDelProceso($id){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codInactivo." where AdmDetId=$id");
    return $this->db->errno;
  }

  public function modificarAInscrito($AdmDetId, $AdmDetFKCar){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codInscrito.", AdmDetFKCar=$AdmDetFKCar where AdmDetId=$AdmDetId");
    return $this->db->errno;
  }

  public function modificarASorteado($AdmDetId, $AdmDetFKCar){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codSorteado.", AdmDetFKCar=$AdmDetFKCar where AdmDetId=$AdmDetId");
    return $this->db->errno;
  }

  public function modificarANoInscrito($AdmDetId, $AdmDetFKCar){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codInscritoCancelado.", AdmDetFKCar=$AdmDetFKCar where AdmDetId=$AdmDetId");
    return $this->db->errno;
  }

  public function modificarANoSorteado($AdmDetId, $AdmDetFKCar){
    $consulta = $this->db->query("UPDATE admisiondetalle set AdmDetFKEstReg=".Codigos::codSorteadoCancelado.", AdmDetFKCar=$AdmDetFKCar where AdmDetId=$AdmDetId");
    return $this->db->errno;
  }
}
?>