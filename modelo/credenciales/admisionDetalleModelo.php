<?php

class admisiondetalleModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }
    
  public function getAdministrativosConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT AdmDetId,usuario.UsuId, usuario.UsuNom, usuario.UsuApe, usuario.UsuDni, cargo.CarDes as cargo FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId inner join cargo on admisiondetalle.AdmDetFKCar= cargo.CarId WHERE admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId and admisiondetalle.AdmDetFKEstReg=17 and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') order by usuario.UsuApe,UsuNom LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadAdministrativosConSinInscripcionOParticipacion($AdmDetFKAdmCabId, $busqueda){

    $consulta = $this->db->query("SELECT count(*) as numeroRegistros FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKEstReg=17 and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getTecnicos($AdmDetFKAdmCabId, $busqueda, $offset, $registrosPorPagina){
    // echo "SELECT admisiondetalle.AdmDetId, usuario.UsuDni, usuario.UsuNom, usuario.UsuApe FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKCar=2 AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') order by UsuApe,UsuNom LIMIT $offset,$registrosPorPagina\n";
    $consulta = $this->db->query("SELECT admisiondetalle.AdmDetId, usuario.UsuDni, usuario.UsuNom, usuario.UsuApe FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKCar=2 AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') order by UsuApe,UsuNom LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadTecnicos($AdmDetFKAdmCabId, $busqueda){

    $consulta = $this->db->query("SELECT count(*) as numeroRegistros FROM admisiondetalle inner join usuario on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKCar=2 AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getCargos($AdmDetFKAdmCabId, $busqueda, $offset, $registrosPorPagina){
    // echo "SELECT cargo.CarId, cargo.CarDes as nombre, general.GenSubCat as tipo from admisiondetalle inner join cargo on admisiondetalle.AdmDetFKCar=cargo.CarId inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId INNER JOIN general ON general.GenId=cargo.CarFKTipUsu where  AdmDetFKAdmCabId=$AdmDetFKAdmCabId  AND admisioncabecera.AdmCabId=$AdmDetFKAdmCabId AND (CarDes LIKE '%".$busqueda."%')  AND AdmDetFKEstReg=17 group by cargo.CarId LIMIT $offset,$registrosPorPagina\n";
    $consulta = $this->db->query("SELECT CarId, CarDes as nombre, general.GenSubCat as tipo FROM cargo INNER JOIN general ON general.GenId=CarFKTipUsu WHERE CarFKEstReg=18 AND (CarDes LIKE '%".$busqueda."%') order by tipo,nombre LIMIT $offset,$registrosPorPagina");
    // $consulta = $this->db->query("SELECT cargo.CarId, cargo.CarDes as nombre, general.GenSubCat as tipo from admisiondetalle inner join cargo on admisiondetalle.AdmDetFKCar=cargo.CarId inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId INNER JOIN general ON general.GenId=cargo.CarFKTipUsu where  AdmDetFKAdmCabId=$AdmDetFKAdmCabId  AND admisioncabecera.AdmCabId=$AdmDetFKAdmCabId AND (CarDes LIKE '%".$busqueda."%')  AND AdmDetFKEstReg=17 group by cargo.CarId LIMIT $offset,$registrosPorPagina");
    
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadCargos($AdmDetFKAdmCabId, $busqueda){

    $consulta = $this->db->query("SELECT count(*) as numeroRegistros FROM cargo WHERE CarFKEstReg=18 AND (CarDes LIKE '%".$busqueda."%')");
    // $consulta = $this->db->query("SELECT count(*) as numeroRegistros from ( SELECT cargo.CarId from admisiondetalle inner join cargo on admisiondetalle.AdmDetFKCar=cargo.CarId inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId INNER JOIN general ON general.GenId=cargo.CarFKTipUsu where  AdmDetFKAdmCabId=$AdmDetFKAdmCabId  AND admisioncabecera.AdmCabId=$AdmDetFKAdmCabId AND (CarDes LIKE '%$busqueda%')  AND AdmDetFKEstReg=17 group by cargo.CarId    ) as t"); 
    // $consulta = $this->db->query("SELECT count(*) as numeroRegistros from admisiondetalle inner join cargo on admisiondetalle.AdmDetFKCar=cargo.CarId inner join admisioncabecera on admisiondetalle.AdmDetFKAdmCabId=admisioncabecera.AdmCabId INNER JOIN general ON general.GenId=cargo.CarFKTipUsu where  AdmDetFKAdmCabId=$AdmDetFKAdmCabId  AND admisioncabecera.AdmCabId=$AdmDetFKAdmCabId AND (CarDes LIKE '%".$busqueda."%')  AND AdmDetFKEstReg=17 ");
    return Util::convertFormatJson($consulta);
  }

}
?>
