<?php
class reporteModelo{

  private $db;

  private $areas;

  public function __construct(){
    $this->db = Conectar::conexion();
    $this->areas = array();
  }

  public function getAreas(){
    $consulta = $this->db->query("select AdmAreId, area.AreIdLog, area.AreDes, AdmAreFKAdmCabId FROM admisionarea inner JOIN area on AdmAreFKAreId = area.AreId WHERE AdmAreFKEstReg = 18");
    return $this->convertFormatJson($consulta);
  }

  public function getCargos(){
    $consultaC = $this->db->query("SELECT * FROM cargo WHERE CarFKEstReg=18");
    return $this->convertFormatJson($consultaC);
  }
  public function getUsuarios(){

    $consultaU = $this->db->query("select admisiondetalle.AdmDetFKEstReg, admisiondetalle.AdmDetFKCar, admisiondetalle.AdmDetFKCar2, AdmDetFKAdmCabId, admisiondetalle.AdmDetId as iddetalle,admisiondetalle.AdmDetFKAdmCabId as idcab , concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, usuario.UsuCorEle as correo, usuario.UsuTel as telefono, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon, (SELECT pabellon.PabDes from pabellon INNER JOIN admisionpabellon on admisionpabellon.AdmPabFKPabId=pabellon.PabId INNER JOIN admisionarea on admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN admisiondetalle on admisionpabellon.AdmPabId=admisiondetalle.AdmDetFKPab2 WHERE admisiondetalle.AdmDetId=iddetalle) as pabellon2 FROM usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE 1");
    return $this->convertFormatJson($consultaU);
  }
  
  public function getAulas($idArea, $idPab){
    $consultaAulas = $this->db->query("select aula.AulNum as numero, admisionaula.AdmAulAfo as aforo, aula.AulPis as piso, admisionaula.AdmAulCol as columna, admisionaula.AdmAulFil as fila, admisionaula.AdmAulCap as capacidad, admisionaula.AdmAulFKAdmPabId as idpabellon, admisionpabellon.AdmPabFKAdmAreId as idArea FROM admisionaula INNER JOIN admisionpabellon ON admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId INNER JOIN aula on admisionaula.AdmAulFKAulId=aula.AulId WHERE admisionaula.AdmAulFKEstReg=18 AND admisionpabellon.AdmPabFKAdmAreId=$idArea and admisionaula.AdmAulFKAdmPabId=$idPab");
    return $this->convertFormatJson($consultaAulas);
  }
  public function getGuarderos($idPro, $idPab){
    $consultaGuarderos = $this->db->query("select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom,' (',usuario.UsuTel,')') as nombreG from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=$idPro and (admisiondetalle.AdmDetFKCar=5 or admisiondetalle.AdmDetFKCar2=5) and admisiondetalle.AdmDetFKEstReg=17 AND (AdmDetFKPab=$idPab OR AdmDetFKPab2=$idPab)");
    return $this->convertFormatJson($consultaGuarderos);
  }
  public function getReemplazos($idPro){
    $consultaReem = $this->db->query("select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, usuario.UsuCorEle as correo, usuario.UsuTel as telefono, usuario.UsuDir as direccion, admisiondetalle.AdmDetObs as obs,(Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId) as cargo1, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId) as cargo2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE admisiondetalle.AdmDetFKAdmCabId=$idPro and admisiondetalle.AdmDetFKEstReg=30");
    return $this->convertFormatJson($consultaReem);
  }
  public function getComAdm($idArea, $idPro){
    $consultaAulas = $this->db->query("select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as cargo1, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as comision1, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as cargo2, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as comision2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE  admisiondetalle.AdmDetFKAdmCabId=$idPro and (usuario.UsuFKTip=4 or usuario.UsuFKTip2=4) and admisiondetalle.AdmDetFKEstReg=17 and (admisiondetalle.AdmDetFKAre=$idArea or admisiondetalle.AdmDetFKAre2=$idArea)");
    return $this->convertFormatJson($consultaAulas);
  }
  public function getComDoc($idArea, $idPro){
    $consultaAulas = $this->db->query("select concat(REPLACE(usuario.UsuApe,' ', '/'),', ',usuario.UsuNom) as nombre, usuario.UsuDni as dni, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as cargo1, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar=cargo.CarId and cargo.CarCom='SI') as comision1, (Select cargo.CarDes from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as cargo2, (Select cargo.CarCom from cargo WHERE admisiondetalle.AdmDetFKCar2=cargo.CarId and cargo.CarCom='SI') as comision2 from usuario inner join admisiondetalle on admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE  admisiondetalle.AdmDetFKAdmCabId=$idPro and (usuario.UsuFKTip=3 or usuario.UsuFKTip2=3) and admisiondetalle.AdmDetFKEstReg=17 and (admisiondetalle.AdmDetFKAre=$idArea or admisiondetalle.AdmDetFKAre2=$idArea)");
    return $this->convertFormatJson($consultaAulas);
  }

  function convertFormatJson($consulta){
    $datos = array();
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
      $datos[$posicion] = $fila;
      $posicion++;
    }
    return $datos;
  }
}
?>
