<?php

class general{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getPabellones($AdmDetFKAdmCabId){
    $consulta = $this->db->query("SELECT admisionpabellon.AdmPabId as AdmPabId , pabellon.PabDes as nombre,
    COALESCE((select CONCAT(usuario.UsuNom, ' ', usuario.UsuApe) from admisiondetalle INNER JOIN usuario ON admisiondetalle.AdmDetFKUsu=usuario.UsuId where admisiondetalle.AdmDetFKCar=5 AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKPab=AdmPabId),'') as guardero,
    COALESCE((select CONCAT(usuario.UsuNom, ' ', usuario.UsuApe) from admisiondetalle INNER JOIN usuario ON admisiondetalle.AdmDetFKUsu=usuario.UsuId where (admisiondetalle.AdmDetFKCar=1 OR admisiondetalle.AdmDetFKCar2=1) AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKPab=AdmPabId),'') as supervisor,
     (SELECT area.AreDes FROM area WHERE area.AreId=admisionarea.AdmAreFKAreId) as area, admisionpabellon.AdmPabFKAreIdLog as areaLogica, (SELECT COUNT(*) from admisionaula WHERE admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId AND admisionaula.AdmAulFKEstReg=18  AND admisionarea.AdmAreFKAdmCabId = $AdmDetFKAdmCabId) as aulas,
     (SELECT COUNT(*) FROM admisiondetalle WHERE admisiondetalle.AdmDetFKEstReg=17 AND (admisiondetalle.AdmDetFKCar=4 OR admisiondetalle.AdmDetFKCar2=4) AND (admisiondetalle.AdmDetFKPab=AdmPabId OR admisiondetalle.AdmDetFKPab2=AdmPabId) AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId) as cantAyudantes,
     (SELECT COUNT(*) FROM admisiondetalle WHERE admisiondetalle.AdmDetFKEstReg=17 AND (admisiondetalle.AdmDetFKCar=3 OR admisiondetalle.AdmDetFKCar2=3) AND (admisiondetalle.AdmDetFKPab=AdmPabId OR admisiondetalle.AdmDetFKPab2=AdmPabId) AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId) as cantControladores,
      (SELECT SUM(admisionaula.AdmAulCap) from admisionaula WHERE admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId AND admisionaula.AdmAulFKEstReg=18  AND admisionarea.AdmAreFKAdmCabId = $AdmDetFKAdmCabId) as capacidadAsignada, (SELECT SUM(admisionaula.AdmAulAfo) from admisionaula WHERE admisionaula.AdmAulFKAdmPabId=admisionpabellon.AdmPabId  AND admisionaula.AdmAulFKEstReg=18 AND admisionarea.AdmAreFKAdmCabId = $AdmDetFKAdmCabId) as aforo, admisionpabellon.AdmPabFKEstReg as estadoRegistro from admisionpabellon INNER JOIN admisionarea ON admisionarea.AdmAreId=admisionpabellon.AdmPabFKAdmAreId INNER JOIN pabellon ON pabellon.PabId=admisionpabellon.AdmPabFKPabId WHERE admisionarea.AdmAreFKAdmCabId = $AdmDetFKAdmCabId AND admisionpabellon.AdmPabFKEstReg=18 ORDER BY area, nombre");
    return $consulta;
  }

  public function getTecnicos($AdmDetFKAdmCabId, $IdPab){
    $consulta = $this->db->query("SELECT usuario.UsuNom, usuario.UsuApe FROM `admisiondetalle` INNER JOIN usuario ON admisiondetalle.AdmDetFKUsu=usuario.UsuId WHERE (admisiondetalle.AdmDetFKPab=$IdPab OR admisiondetalle.AdmDetFKPab2=$IdPab) AND admisiondetalle.AdmDetFKEstReg=17 AND admisiondetalle.AdmDetFKAdmCabId=$AdmDetFKAdmCabId AND (admisiondetalle.AdmDetFKCar=2 OR admisiondetalle.AdmDetFKCar2=2)");
    return $consulta;
  }

}
?>