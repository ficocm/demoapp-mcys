<?php
class pabellonModelo{
  private $db;
  private $pabellones;

  public function __construct(){
    $this->db = Conectar::conexion();
    $this->pabellones = array();
  }
  public function getPabellones(){
    $consulta = $this->db->query("select AdmPabId, AdmPabFKAdmAreId, pabellon.PabDes, pabellon.PabFKAre FROM admisionpabellon inner JOIN pabellon on AdmPabFKPabId = pabellon.PabId WHERE AdmPabFKEstReg = 18");
    return $this->convertFormatJson($consulta);
  }

  function convertFormatJson($consulta){
    $datos = array();
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
        $datos[$posicion] = $fila;
        $posicion++;
    }
    return $datos;
  }
}
?>
