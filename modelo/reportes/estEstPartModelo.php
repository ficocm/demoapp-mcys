<?php
class estEstPartModelo{
  private $db;
  private $pabellones;

  public function __construct(){
    $this->db = Conectar::conexion();
    $this->pabellones = array();
  }
  public function cargarCargos(){
    $consulta = $this->db->query("select CarId, CarDes FROM cargo WHERE ( CarFKTipTra = 20) & ( CarFKEstReg = 7)");
    return $this->convertFormatJson($consulta);
  }
  public function cargarEst(){
    $consulta = $this->db->query("select * FROM usuario WHERE UsuFKTip = 20");
    return $this->convertFormatJson($consulta);
  }

  function convertFormatJson($consulta){
    $datos = array();
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
        $datos[$posicion] = $fila;
        $posicion++;
    }
    return $datos;
  }

}
?>
