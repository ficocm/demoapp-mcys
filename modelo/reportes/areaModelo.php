<?php
class areaModelo{

  private $db;

  private $areas;

  public function __construct(){
    $this->db = Conectar::conexion();
    $this->areas = array();
  }

  public function getAreas(){
    $consulta = $this->db->query("select AdmAreId, area.AreIdLog, area.AreDes FROM admisionarea inner JOIN area on AdmAreFKAreId = area.AreId WHERE AdmAreFKEstReg = 18");
    return $this->convertFormatJson($consulta);
  }

  function convertFormatJson($consulta){
    $datos = array();
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
      $datos[$posicion] = $fila;
      $posicion++;
    }
    return $datos;
  }
}
?>
