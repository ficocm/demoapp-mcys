<?php

class generalModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

	public function getGeneros(){
    $consulta = $this->db->query("SELECT GenId, GenCat, GenSubCat, GenCodMos from general where GenCat = '".Codigos::texGenero."'");
    return Util::convertFormatJson($consulta);
  }

  public function getTiposDeUsuario(){
    $consulta = $this->db->query("SELECT GenId, GenCat, GenSubCat, GenCodMos from general where GenCat = '".Codigos::texTipoUsuario."'");
    return Util::convertFormatJson($consulta);
  }

  public function getTiposDeDocentesAdministrativos(){
    $consulta = $this->db->query("SELECT GenId, GenCat, GenSubCat, GenCodMos from general where (GenSubCat = '".Codigos::texDocente."' or GenSubCat='".Codigos::texAdministrativo."')");
    return Util::convertFormatJson($consulta);
  }

  public function getCategorias(){
    $consulta = $this->db->query("SELECT GenId, GenCat, GenSubCat, GenCodMos from general where GenCat = '".Codigos::texcategoria."'");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosDocente(){
    $consulta = $this->db->query("SELECT CarId, CarFKTipUsu, CarDes from cargo where CarFKEstReg = ".Codigos::codActivo." and CarFKTipUsu = ".Codigos::codDocente." and CarCom='NO'");
    return Util::convertFormatJson($consulta);
  }

  public function getCargosDocenteComision(){
    $consulta = $this->db->query("SELECT CarId, CarFKTipUsu, CarDes from cargo where CarFKEstReg = ".Codigos::codActivo." and CarFKTipUsu = ".Codigos::codDocente." and CarCom='SI'");
    return Util::convertFormatJson($consulta);
  }
}
?>