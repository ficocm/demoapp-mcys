<?php

class usuarioModeloVis{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getDatos($busqueda){
    $consulta = $this->db->query("SELECT UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel from usuario where UsuId=$busqueda");
    return Util::convertFormatJson($consulta);
  }

  public function getDatosAdmi(){
    $consulta = $this->db->query("SELECT  AdmCabId,AdmCabAno, AdmCabNumFas, GenSubCat from admisioncabecera a, general b where (AdmCabFKEstReg='32' or AdmCabFKEstReg='33') and b.GenId=a.AdmCabFKTipPro");
    return Util::convertFormatJson($consulta);
  }

  public function getCargo($busqueda,$idAdm){
    $consulta = $this->db->query("SELECT  AdmDetFKCar, CarDes from admisiondetalle a, cargo b where AdmDetFKAdmCabId=$idAdm and AdmDetFKUsu=$busqueda and b.CarId=a.AdmDetFKCar");
    return Util::convertFormatJson($consulta);
  }

  public function borrarRegistro($busqueda,$idAdm){
    $consulta = $this->db->query("UPDATE admisiondetalle SET AdmDetFKEstReg='6' where AdmDetFKUsu=$busqueda and AdmDetFKAdmCabId=$idAdm");
    //$consulta = $this->db->query("SELECT UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel from usuario where UsuId=$busqueda");
    return $this->db->errno;
  }

  public function getDatosAdmiUsu($busqueda,$id){
    $consulta = $this->db->query("SELECT AdmDetId, AdmDetFKAdmCabId, AdmDetFKUsu,AdmDetFKEstReg, CarDes  from admisiondetalle INNER JOIN cargo ON  AdmDetFKCar=CarId where AdmDetFKAdmCabId=$id AND AdmDetFKUsu=$busqueda ORDER BY admisiondetalle.AdmDetId DESC LIMIT 1");
    return Util::convertFormatJson($consulta);
  }

   public function verificarDatos($busqueda){
    $consulta = $this->db->query("SELECT AdmDetFKCar from admisiondetalle where AdmDetFKUsu=$busqueda and (SELECT AdmCabFKEstReg from admisioncabecera where AdmCabId=AdmDetFKAdmCabId)='32'");
    return Util::convertFormatJson($consulta);
  }

   public function crearRegistro($busqueda,$tipo,$idAdm){
    $consulta = $this->db->query("INSERT INTO admisiondetalle (AdmDetFKAdmCabId,AdmDetFKUsu,AdmDetFKCar,AdmDetFKCar2, AdmDetLug1,AdmDetLug2, AdmDetFKAul,AdmDetFKPab,AdmDetFKPab2, AdmDetFKAre,AdmDetFKAre2, AdmDetFKArePue,AdmDetFKArePue2, AdmDetDetAsi, AdmDetCorEnv, AdmDetFKDepTec, AdmDetObs,AdmDetFKEstReg) VALUES ($idAdm,$busqueda,$tipo,null,null,null,null,null,null,null,null,null,null,null,null,null,null,7)");
    return $this->db->errno;
  }

  public function actRegistro($busqueda,$tipo,$idAdm){
    $consulta = $this->db->query("UPDATE admisiondetalle SET AdmDetFKEstReg='7', AdmDetFKCar=$tipo where AdmDetFKUsu=$busqueda and AdmDetFKAdmCabId=$idAdm");
    //$consulta = $this->db->query("SELECT UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel from usuario where UsuId=$busqueda");
    return $this->db->errno;
  }

  public function setDatos($celular,$id){
    $consulta = $this->db->query("UPDATE usuario SET UsuTel=$celular WHERE UsuId=$id");
    //return $this->db->errno;
    return Util::convertFormatJson($consulta);
  }

  public function getEstudiantes($busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuCui, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuCui LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadEstudiantes($busqueda){
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuCui LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getDocentes($busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codDocente." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadDocentes($busqueda){
    $consulta = $this->db->query("SELECT  count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codDocente." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getAdministrativos($busqueda,$offset,$registrosPorPagina){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codAdministrativo." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%') LIMIT $offset,$registrosPorPagina");
    return Util::convertFormatJson($consulta);
  }

  public function getCantidadAdministrativos($busqueda){
    $consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codAdministrativo." and UsuFKEstReg=".Codigos::codActivo." and (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuNom LIKE '%".$busqueda."%' or usuario.UsuApe LIKE '%".$busqueda."%')");
    return Util::convertFormatJson($consulta);
  }

  public function getOperadores(){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codOperador." and UsuFKEstReg=".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getAdministradores(){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codAdministrador." and UsuFKEstReg=".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getUsuarioLogin($UsuNomUsu, $UsuConUsu){
    $consulta = $this->db->query("SELECT UsuId, UsuDni, UsuCui, UsuNom, UsuApe, UsuCorEle, UsuFKTip, UsuFKCat, UsuNomUsu, UsuConUsu from usuario where UsuNomUsu = $UsuNomUsu and UsuConUsu = md5($UsuConUsu) and UsuFKEstReg = ".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function updateUsuarioCuenta($UsuId, $UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKDepAca, $UsuFKAre, $UsuNumPro, $UsuFun, $UsuFKGen, $UsuFKTip, $UsuDep, $UsuFKCat, $UsuNomUsu, $UsuConUsu, $UsuFKEstReg){
    $consulta = $this->db->query("UPDATE usuario SET UsuDni=$UsuDni,UsuCui=$UsuCui,UsuNom=$UsuNom,UsuApe=$UsuApe,UsuCorEle=$UsuCorEle,UsuTel=$UsuTel,UsuDir=$UsuDir,UsuFKDepAca=$UsuFKDepAca,UsuFKAre=$UsuFKAre,UsuNumPro=$UsuNumPro,UsuFun=$UsuFun,UsuFKGen=$UsuFKGen,UsuFKTip=$UsuFKTip,UsuDep=$UsuDep,UsuFKCat=$UsuFKCat,UsuNomUsu=$UsuNomUsu,UsuConUsu=$UsuConUsu,UsuFKEstReg=$UsuFKEstReg WHERE UsuId=$UsuId");
    return $consulta;
  }

  public function updateUsuario($UsuId, $UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKDepAca, $UsuFKAre, $UsuNumPro, $UsuFun, $UsuFKGen, $UsuFKTip, $UsuDep, $UsuFKCat, $UsuFKEstReg){
    $consulta = $this->db->query("UPDATE usuario SET UsuDni=$UsuDni,UsuCui=$UsuCui,UsuNom=$UsuNom,UsuApe=$UsuApe,UsuCorEle=$UsuCorEle,UsuTel=$UsuTel,UsuDir=$UsuDir,UsuFKDepAca=$UsuFKDepAca,UsuFKAre=$UsuFKAre,UsuNumPro=$UsuNumPro,UsuFun=$UsuFun,UsuFKGen=$UsuFKGen,UsuFKTip=$UsuFKTip,UsuDep=$UsuDep,UsuFKCat=$UsuFKCat,UsuFKEstReg=$UsuFKEstReg WHERE UsuId=$UsuId");
    return $this->db->errno;
  }

  public function crearUsuario($UsuDni, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKDepAca, $UsuFKAre, $UsuNumPro, $UsuFun, $UsuFKGen, $UsuFKTip, $UsuDep, $UsuFKCat){
    $consulta = $this->db->query("INSERT INTO usuario (UsuDni,UsuNom,UsuApe,UsuCorEle,UsuTel,UsuDir,UsuFKDepAca,UsuFKAre,UsuNumPro,UsuFun,UsuFKGen,UsuFKTip,UsuDep,UsuFKCat,UsuNomUsu,UsuConUsu) VALUES ($UsuDni,$UsuNom,$UsuApe,$UsuCorEle,$UsuTel,$UsuDir,$UsuFKDepAca,$UsuFKAre,$UsuNumPro,$UsuFun,$UsuFKGen,$UsuFKTip,$UsuDep,$UsuFKCat,$UsuCorEle,md5($UsuDni))");
    return $this->db->errno;
  }

   public function crearEstudiante($UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir,  $UsuFKTip, $UsuFKCat){
    $consulta = $this->db->query("INSERT INTO usuario (UsuDni,UsuCui,UsuNom,UsuApe,UsuCorEle,UsuTel,UsuDir,UsuFKTip,UsuFKCat) VALUES ($UsuDni,$UsuCui,$UsuNom,$UsuApe,$UsuCorEle,$UsuTel,$UsuDir,$UsuFKTip,$UsuFKCat)");
    return $this->db->errno;
  }
}
?>