<?php

class usuarioModelo{
	private $db;

	public function __construct(){
		$this->db = Conectar::conexion();
	}

	public function getEstudiantes($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuDni, UsuCui, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadEstudiantes($busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codEstudiante." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getDocentes($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuFKTip2, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codDocente." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadDocentes($busqueda){
		$consulta = $this->db->query("SELECT	count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codDocente." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getAdministrativos($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuFKTip2, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip2=".Codigos::codAdministrativo." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadAdministrativos($busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip2=".Codigos::codAdministrativo." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getAdministrativosDocentes($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuDni, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuDepAca, UsuFKAre, UsuNumPro, UsuFun, UsuFKGen, UsuFKTip, UsuFKTip2, UsuDep, UsuFKCat, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where (UsuFKTip=".Codigos::codDocente." or UsuFKTip2=".Codigos::codAdministrativo.") and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadAdministrativosDocentes($busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where (UsuFKTip=".Codigos::codDocente." or UsuFKTip2=".Codigos::codAdministrativo.") and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getOperadores($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKTip, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codOperador." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadOperadores($busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codOperador." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getAdministradores($busqueda,$offset,$registrosPorPagina){
		$consulta = $this->db->query("SELECT UsuId, UsuNom, UsuApe, UsuCorEle, UsuTel, UsuDir, UsuFKTip, UsuNomUsu, UsuConUsu, UsuFKEstReg from usuario where UsuFKTip=".Codigos::codAdministrador." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda)." ORDER BY usuario.UsuApe LIMIT $offset,$registrosPorPagina");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadAdministradores($busqueda){
		$consulta = $this->db->query("SELECT count(*) as numeroRegistros from usuario where UsuFKTip=".Codigos::codAdministrador." and UsuFKEstReg=".Codigos::codActivo." and ".Util::busquedaSql($busqueda));
		return Util::convertFormatJson($consulta);
	}

	public function getUsuarioLogin($UsuNomUsu, $UsuConUsu){
		$consulta = Conectar::conexionSinRestriccion()->query("SELECT UsuId, UsuDni, UsuCui, UsuNom, UsuApe, UsuCorEle, UsuFKTip, UsuFKTip2, UsuFKCat, UsuNomUsu, UsuConUsu from usuario where UsuNomUsu = $UsuNomUsu and UsuConUsu = $UsuConUsu and UsuFKEstReg = ".Codigos::codActivo);
		return Util::convertFormatJson($consulta);
	}

	public function updateUsuarioCuenta($UsuId, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuNomUsu, $UsuConUsu, $UsuFKEstReg){
		$consulta = $this->db->query("UPDATE usuario SET UsuNom=$UsuNom,UsuApe=$UsuApe,UsuCorEle=$UsuCorEle,UsuTel=$UsuTel,UsuDir=$UsuDir,UsuNomUsu=$UsuNomUsu,UsuConUsu=$UsuConUsu,UsuFKEstReg=$UsuFKEstReg WHERE UsuId=$UsuId");
		return $this->db->errno;
	}

	public function crearOperadorAdministrador($UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKTip, $UsuNomUsu, $UsuConUsu){
		$consulta = $this->db->query("INSERT INTO usuario (UsuNom,UsuApe,UsuCorEle,UsuTel,UsuDir, UsuFKTip,UsuNomUsu,UsuConUsu) VALUES ($UsuNom,$UsuApe,$UsuCorEle,$UsuTel,$UsuDir,$UsuFKTip,$UsuNomUsu,$UsuConUsu)");
		$error = $this->db->errno;
		if($error == 0){
			$id = $this->db->insert_id;
			$consulta = $this->db->query("CREATE USER '$id'@'%' IDENTIFIED BY '".md5($id)."';");
			if($this->db->errno==0){
				if($UsuFKTip==Codigos::codAdministrador){
					$consulta = $this->db->query("GRANT ALL PRIVILEGES ON *.* TO '$id'@'%' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;");
				}else if($UsuFKTip==Codigos::codOperador){
					$consulta = $this->db->query("GRANT ALL PRIVILEGES ON *.* TO '$id'@'%';");
				}
				
				return $this->db->errno;
			}else{
				return $this->db->errno;
			}
		}else{
			return $error;
		}
	}

	public function updateUsuario($UsuId, $UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuDepAca, $UsuFKAre, $UsuNumPro, $UsuFun, $UsuFKGen, $UsuFKTip, $UsuFKTip2, $UsuDep, $UsuFKCat){
		$consulta = $this->db->query("UPDATE usuario SET UsuDni=$UsuDni,UsuCui=$UsuCui,UsuNom=$UsuNom,UsuApe=$UsuApe,UsuCorEle=$UsuCorEle,UsuTel=$UsuTel,UsuDir=$UsuDir,UsuDepAca=$UsuDepAca,UsuFKAre=$UsuFKAre,UsuNumPro=$UsuNumPro,UsuFun=$UsuFun,UsuFKGen=$UsuFKGen,UsuFKTip=$UsuFKTip,UsuFKTip2=$UsuFKTip2,UsuDep=$UsuDep,UsuFKCat=$UsuFKCat,UsuNomUsu=$UsuCorEle,UsuConUsu=$UsuDni WHERE UsuId=$UsuId");
		return $this->db->errno;
	}

	public function crearUsuario($UsuDni, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuDepAca, $UsuFKAre, $UsuNumPro, $UsuFun, $UsuFKGen, $UsuFKTip,$UsuFKTip2, $UsuDep, $UsuFKCat){
		$consulta = $this->db->query("INSERT INTO usuario (UsuDni,UsuNom,UsuApe,UsuCorEle,UsuTel,UsuDir,UsuDepAca,UsuFKAre,UsuNumPro,UsuFun,UsuFKGen,UsuFKTip,UsuFKTip2,UsuDep,UsuFKCat,UsuNomUsu,UsuConUsu) VALUES ($UsuDni,$UsuNom,$UsuApe,$UsuCorEle,$UsuTel,$UsuDir,$UsuDepAca,$UsuFKAre,$UsuNumPro,$UsuFun,$UsuFKGen,$UsuFKTip,$UsuFKTip2,$UsuDep,$UsuFKCat,$UsuCorEle,$UsuDni)");
		return $this->db->errno;
	}

	public function updateEstudiante($UsuId, $UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKCat){
		$consulta = $this->db->query("UPDATE usuario SET UsuDni=$UsuDni,UsuCui=$UsuCui,UsuNom=$UsuNom,UsuApe=$UsuApe,UsuCorEle=$UsuCorEle,UsuTel=$UsuTel,UsuDir=$UsuDir, UsuFKCat=$UsuFKCat WHERE UsuId=$UsuId");
		return $this->db->errno;
	}

	 public function crearEstudiante($UsuDni, $UsuCui, $UsuNom, $UsuApe, $UsuCorEle, $UsuTel, $UsuDir, $UsuFKCat){
		$consulta = $this->db->query("INSERT INTO usuario (UsuDni,UsuCui,UsuNom,UsuApe,UsuCorEle,UsuTel,UsuDir,UsuFKTip,UsuFKCat) VALUES ($UsuDni,$UsuCui,$UsuNom,$UsuApe,$UsuCorEle,$UsuTel,$UsuDir,".Codigos::codEstudiante.",$UsuFKCat)");
		return $this->db->errno;
	}

	//VALIDACIONES DE DNI, CORREO
	public function getCantidadDni($id, $dni){
		$consulta = $this->db->query("SELECT usuario.UsuNom, usuario.UsuApe, general.GenSubCat from usuario inner join general on (general.GenId=usuario.UsuFKTip or general.GenId=usuario.UsuFKTip2) where UsuFKEstReg=".Codigos::codActivo." and usuario.UsuDni = $dni and usuario.UsuId != $id");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadCui($id, $cui,$tipo){
		$consulta = $this->db->query("SELECT usuario.UsuNom, usuario.UsuApe from usuario where UsuFKTip=$tipo and UsuFKEstReg=".Codigos::codActivo." and usuario.UsuCui = $cui and usuario.UsuId != $id");
		return Util::convertFormatJson($consulta);
	}

	public function getCantidadCorreo($id, $correo){
		$consulta = $this->db->query("SELECT usuario.UsuNom, usuario.UsuApe, general.GenSubCat from usuario inner join general on (general.GenId=usuario.UsuFKTip or general.GenId=usuario.UsuFKTip2) where UsuFKEstReg=".Codigos::codActivo." and usuario.UsuCorEle = $correo and usuario.UsuId != $id");
		return Util::convertFormatJson($consulta);
	}

	public function eliminarUsuario($id){
		$consulta = $this->db->query("UPDATE usuario SET UsuFKEstReg=".Codigos::codInactivo." WHERE UsuId=$id");
		if($this->db->errno == 0){
			$consulta = $this->db->query("drop user '$id'");
		}
		return $this->db->errno;
	}

	public function eliminarDocenteAdministrativo($id){
		$consulta = $this->db->query("DELETE FROM usuario WHERE UsuId=$id");
		return $this->db->errno;
	}

}
?>