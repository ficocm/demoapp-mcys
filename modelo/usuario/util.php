<?php
abstract class Codigos{
	//Ids
  //General
	const codActivo = 18;
	const codInactivo = 19;
	const codInscrito = 7;
  const codInscritoCancelado = 6;
	const codSorteado = 5;
  const codSorteadoCancelado = 28;
  const codDocente = 3;
  const codAsignado = 17;
  const codAsignadoCancelado = 29;
  const codConfirmado = 16;
  const codAdministrativo = 4;
  const codEstudiante = 20;
  const codOperador = 23;
  const codAdministrador = 22;

  const codEstadoProcesoPasado = 31;
  const codEstadoProcesoActual = 32;
  const codEstadoProcesoFuturo = 33;

  //Cargo
  const codDocenteTecnico = 2;
  const codAyudanteTecnico = 4;
  const codDocenteSeguridad = 67;

	//Categorias General
	const texGenero = "GENERO";
	const texTipoUsuario = "TIPO DE USUARIO";
	const texcategoria = "CATEGORIA";
  const texDocente = "DOCENTE";
  const texAdministrativo = "ADMINISTRATIVO";

}
class Util{
  public static function convertFormatJson($consulta){
    $datos = array();
  	if($consulta == NULL || $consulta == "NULL"){
  		return $datos;
  	}
    $posicion=0;
    while($fila = $consulta->fetch_assoc()) {
        $datos[$posicion] = $fila;
        $posicion++;
    }
    return $datos;
  }

  public static function jsonMensaje($id, $men, $data = NULL){
    $mensaje->error=$id;
    $mensaje->mensaje=$men;
    $mensaje->datos = $data;
    return json_encode($mensaje);
  }

  public static function getParameter($nombre){
    if(isset($_POST[$nombre]) && !empty($_POST[$nombre]) && $_POST[$nombre]!=-1 && $_POST[$nombre]!=NULL){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexionSinRestriccion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      $dato = (is_numeric($dato))?$dato : "'".Util::limpiarEntrada($dato)."'";
      return $dato;
    }else{
      return "NULL";
    }
  }

  public static function limpiarEntrada($nombre){//Limpia muchos espacios
    $limpio1 = "";
    $nombre = str_replace(array("     ", "    ", "   " ,"  "), " ", $nombre);
    for ($i = 0; $i < strlen($nombre); $i++) {
      if($nombre{$i}!= " "){
        $limpio1 = substr($nombre, -(strlen($nombre)-$i));
        break;
      }
    }
    $limpio2 = "";
    for ($i = strlen($limpio1)-1; $i >= 0 ; $i--) {
      if($limpio1{$i}!= " "){
        $limpio2 = substr($limpio1,0, $i+1);
        break;
      }
    }

    return $limpio2;
  }

  public static function getParameterNumeroCadena($nombre){
    if(isset($_POST[$nombre]) && !empty($_POST[$nombre]) && $_POST[$nombre]!=-1 && $_POST[$nombre]!=NULL){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexionSinRestriccion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      return "'".$dato."'";
    }else{
      return "NULL";
    }
  }

  public static function getParameterDefaultZero($nombre){
    if(isset($_POST[$nombre]) && !empty($_POST[$nombre]) && $_POST[$nombre]!=-1 && $_POST[$nombre]!=NULL){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexionSinRestriccion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      if($dato == NULL) $dato=0;
      return "'".$dato."'";
    }else{
      return "'0'";
    }
  }

   public static function getParameterBusqueda($nombre){
    if(isset($_POST[$nombre]) && $_POST[$nombre]!=NULL ){
      $dato = $_POST[$nombre];
      $dato = Conectar::conexionSinRestriccion()->real_escape_string($dato);
      $dato = htmlspecialchars($dato);
      $dato = strip_tags($dato);
      return $dato;
    }else{
      return "";
    }
  }

  public static function busquedaSql($busqueda){
    return " (usuario.UsuDni LIKE '%".$busqueda."%' or usuario.UsuCui LIKE '%".$busqueda."%' or concat(usuario.UsuNom,' ',usuario.UsuApe) LIKE '%".$busqueda."%' or concat(usuario.UsuApe,' ',usuario.UsuNom) LIKE '%".$busqueda."%') ";
  }
}
?>