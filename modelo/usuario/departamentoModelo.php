<?php

class departamentoModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getDepartamentos(){
    $consulta = $this->db->query("SELECT DepId, DepDes, DepFKFac, DepFKEstReg from departamento where DepFKEstReg = ".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getDepartamento($DepId){
  	$consulta = $this->db->query("SELECT DepId, DepDes, DepFKFac, DepFKEstReg from departamento where DepFKEstReg = ".Codigos::codActivo." and DepId=$DepId");
    return Util::convertFormatJson($consulta);
  }
}
?>