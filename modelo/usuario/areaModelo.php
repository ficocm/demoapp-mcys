<?php

class areaModelo{
  private $db;

  public function __construct(){
    $this->db = Conectar::conexion();
  }

  public function getAreas(){
    $consulta = $this->db->query("SELECT AreId, AreIdLog, AreDes, AreFKEstReg from area where AreFKEstReg = ".Codigos::codActivo);
    return Util::convertFormatJson($consulta);
  }

  public function getArea($AreId){
    $consulta = $this->db->query("SELECT AreId, AreIdLog, AreDes, AreFKEstReg from area where AreFKEstReg = ".Codigos::codActivo." and AreId = $AreId");
    return Util::convertFormatJson($consulta);
  }
}
?>