<?php
	session_start();
	unset($_SESSION["idUsuario"]);
	unset($_SESSION["nombres"]);
	unset($_SESSION["apellidos"]);
	unset($_SESSION["tipo"]);
	unset($_SESSION["idProceso"]);
	unset($_SESSION["nombreProceso"]);
	unset($_SESSION["faseProceso"]);
	unset($_SESSION["fechaProceso"]);
	unset($_SESSION["anioProceso"]);
	unset($_SESSION["usuarioNombre"]);
	unset($_SESSION["usuarioContraseña"]);
	echo "<script>location.href = 'index.php'</script>";
?>