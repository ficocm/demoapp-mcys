<!DOCTYPE html>

  <!-- CONEXION BASE DE DATOS -->
  <?php
  session_start();
  if($_SESSION["idUsuario"] != NULL || $_SESSION["tipo"] != NULL){
    echo "<script>location.href = 'vista/inicio/inicio.php'</script>";
    die();
  }
?>
<html>

<body background="img/unsaPortada.jpg" style="background-size: cover; padding: 0.5em 0; text-align: center; background-position: 0% 55%; position: relative;">

    <head>
    </body>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="img/icoUnsa.png"/>
    <title>OFICINA DE ADMISION</title>

    <!-- ESTILOS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">  <!-- Bootstrap Core CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet"><!-- MetisMenu CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet"><!-- Custom CSS -->
    <link href="vendor/morrisjs/morris.css" rel="stylesheet"><!-- Morris Charts CSS -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"><!-- Custom Fonts -->

    <!-- SCRIPTS -->
    <script src="vendor/jquery/jquery-3.3.1.min.js"></script>  <!-- jQuery -->  
    <script src="vendor/jquery/popper.min.js"></script>  <!-- Poppper -->  
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script><!-- Bootstrap Core JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script> <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/flot/excanvas.min.js"></script><!-- Flot Charts JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script><!-- Custom Theme JavaScript -->

    <!-- Archivos para Login-->
    <link href="css/estilosMensajes.css" rel="stylesheet" type="text/css">
    <script src="js/login.js"></script>
    <script src="js/util.js"></script>

</head>

<body>

    <div class="container">

        <div class="row">
            <div class="col-sm"> 
                <h1 align="center" class="page-header" style="color:#fff;">BIENVENIDOS A LA OFICINA DE ADMISIÓN </h1>
            </div><br><br>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Iniciar sesión</h1>
                    </div>
                    <div class="panel-body">
                        <form role="form" class="login-form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuario" name="user" id="user" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Contraseña" name="pass" id="pass" type="password" value="">
                                </div>
                                <a onclick="enviarFormulario();" class="btn btn-lg btn-success btn-block">Login</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Management Error -->
    <div class="error"></div>
    <div class="correcto"></div>

</body>
</html>
